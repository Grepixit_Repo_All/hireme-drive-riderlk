package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.Model.Agency;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.fonts.Fonts;

import java.util.ArrayList;

public class AgencyAdapter extends ArrayAdapter<String> {
    private final Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.KARLA);
    // private Controller controller;
    private final LayoutInflater inflater;
    private ArrayList<Agency> agencies = new ArrayList<>();
    // private final Context context;

    public AgencyAdapter(Context context, int spinner_text, ArrayList<Agency> agencies) {
        super(context, spinner_text);
        //this.context = context;
        inflater = LayoutInflater.from(context);
        this.agencies = agencies;
    }

    @Override
    public int getCount() {
        return agencies.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        //   controller = ( Controller ) getContext().getApplicationContext();
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_text, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItemsName = convertView.findViewById(R.id.spinner_textview);
            mViewHolder.dropItemsName.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.dropItemsName.setText(agencies.get(position).getU_name());
        mViewHolder.dropItemsName.setTypeface(font);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_text, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItemsName = convertView.findViewById(R.id.spinner_textview);
            mViewHolder.dropItemsName.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.dropItemsName.setText(agencies.get(position).getU_name());
        mViewHolder.dropItemsName.setTypeface(font);
        mViewHolder.dropItemsName.setTextColor(Color.parseColor("#000000"));

        return convertView;
    }

    public String getAgencyId(int selectedItemPosition) {
        return agencies.get(selectedItemPosition).getUser_id();
    }

    public void setAgencies(ArrayList<Agency> agencies) {
        this.agencies = agencies;
        notifyDataSetChanged();
    }

    private class MyViewHolder {
        TextView dropItemsName;
    }
}
