package com.ridertechrider.driver.webservice;

import java.util.Timer;
import java.util.TimerTask;

public class RepeatTimerManager {
    private final RepeatTimerManagerCallBack repeatTimerManagerCallBack;
    private final int timeInterval;
    private Timer timer;
    private boolean isRunning = false;


    public RepeatTimerManager(RepeatTimerManagerCallBack repeatTimerManagerCallBack, int timeInterval) {
        this.repeatTimerManagerCallBack = repeatTimerManagerCallBack;
        this.timeInterval = timeInterval;
    }

    public void startRepeatCall() {
        stopRepeatCall();
        isRunning = true;
        repeatTimerManagerCallBack.onRepeatPerfrom();
    }

    public void setTimerForRepeat() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                repeatTimerManagerCallBack.onRepeatPerfrom();
            }
        }, timeInterval);
    }

    public void stopRepeatCall() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        repeatTimerManagerCallBack.onStopRepeatPerfrom();
        isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public interface RepeatTimerManagerCallBack {
        void onRepeatPerfrom();

        void onStopRepeatPerfrom();
    }
}
