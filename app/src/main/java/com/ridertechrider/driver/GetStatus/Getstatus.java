package com.ridertechrider.driver.GetStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Getstatus {

@SerializedName("header")
@Expose
private Header header;
@SerializedName("inputs")
@Expose
private List<Input> inputs = null;

public Header getHeader() {
return header;
}

public void setHeader(Header header) {
this.header = header;
}

public List<Input> getInputs() {
return inputs;
}

public void setInputs(List<Input> inputs) {
this.inputs = inputs;
}

}