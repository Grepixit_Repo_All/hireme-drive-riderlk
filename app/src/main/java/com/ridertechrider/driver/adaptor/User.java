package com.ridertechrider.driver.adaptor;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by grepix on 2/1/2017.
 */
public class User implements Serializable {


    public String u_device_token;
    private String user_id;
    private String c_code;
    private String api_key;
    private String group_id;
    private String username;
    private String user_password;
    private String u_name;
    private String u_fname;
    private String city_id;
    private String u_lname;
    private String u_email;
    private String u_password;
    private String u_phone;
    private String u_address;
    private String rating;
    private String rating_count;
    private String u_profile_image_path;
    private String u_city;
    private String u_state;
    private String u_country;
    private String u_zip;
    private String u_lat;
    private String u_lng;
    private String u_lang;
    private String u_degree;
    private String image_id;
    private String u_device_type;
    private String u_is_available;

    public static User parseJson(String userJson) {

        return new Gson().fromJson(userJson, User.class);
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating_count() {
        return rating_count;
    }

    public void setRating_count(String rating_count) {
        this.rating_count = rating_count;
    }

    public String getUProfileImagePath() {
        return u_profile_image_path;
    }

    public String getU_city() {
        return u_city;
    }

    public String getU_state() {
        return u_state;
    }

    public String getU_country() {
        return u_country;
    }

    public String getU_zip() {
        return u_zip;
    }

    public String getU_lat() {
        return u_lat;
    }

    public String getU_lng() {
        return u_lng;
    }

    public String getU_degree() {
        return u_degree;
    }

    public String getImage_id() {
        return image_id;
    }

    public String getU_device_type() {
        return u_device_type;
    }

    public String getU_device_token() {
        return u_device_token;
    }

    public String getU_is_available() {
        return u_is_available;
    }

    public String getUserId() {
        return user_id;
    }


    public String getUser_api_key() {
        return api_key;
    }


    public String getGroup_id() {
        return group_id;
    }


    public String getUserName() {
        return username;
    }

    public boolean isProfileImagePathEmpty() {
        if (getUProfileImagePath() == null) {
            return false;
        }
        Objects.requireNonNull(getUProfileImagePath());
        return true;
    }

    public String getUser_password() {
        return user_password;
    }

    public String getU_lang() {
        return u_lang;
    }

    public void setU_lang(String u_lang) {
        this.u_lang = u_lang;
    }

    public String getU_name() {
        return u_name;
    }


    public String getU_fname() {
        return u_fname;
    }


    public String getU_lname() {
        return u_lname;
    }


    public String getU_email() {
        return u_email;
    }


    public String getU_password() {
        return u_password;
    }


    public String getU_phone() {
        return u_phone;
    }


    public String getU_address() {
        return u_address;
    }

    public String getC_code() {
        return c_code;
    }

    public void setC_code(String c_code) {
        this.c_code = c_code;
    }
}
