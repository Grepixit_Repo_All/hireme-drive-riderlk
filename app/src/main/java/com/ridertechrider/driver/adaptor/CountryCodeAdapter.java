package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.Model.City;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.fonts.Fonts;

import java.util.ArrayList;

public class CountryCodeAdapter extends ArrayAdapter<String> {
    private final Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.KARLA);
    // private Controller controller;
    private final LayoutInflater inflater;
    private ArrayList<City> citiesCountryCode = new ArrayList<>();
    // private final Context context;

    public CountryCodeAdapter(Context context, int spinner_text, ArrayList<City> citiesCountryCode) {
        super(context, spinner_text);
        //this.context = context;
        inflater = LayoutInflater.from(context);
        this.citiesCountryCode = citiesCountryCode;
    }

    @Override
    public int getCount() {
        return citiesCountryCode.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        //   controller = ( Controller ) getContext().getApplicationContext();
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_text_cc, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItemsCC = convertView.findViewById(R.id.spinner_country_code);
            mViewHolder.dropItemsSeprator = convertView.findViewById(R.id.spinner_seprator);
            mViewHolder.dropItemsName = convertView.findViewById(R.id.spinner_country_name);
            mViewHolder.dropItemsSeprator.setTypeface(font);
            mViewHolder.dropItemsCC.setTypeface(font);
            mViewHolder.dropItemsName.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        if (position == 0) {
            mViewHolder.dropItemsCC.setText(String.format("%s", citiesCountryCode.get(position).getCity_name()));
            mViewHolder.dropItemsSeprator.setText("");
            mViewHolder.dropItemsName.setText("");
        } else {
            mViewHolder.dropItemsCC.setText(String.format("+%s", citiesCountryCode.get(position).getCountry_code()));
            mViewHolder.dropItemsSeprator.setText("");
            mViewHolder.dropItemsName.setText("");
        }
        mViewHolder.dropItemsCC.setTypeface(font);
        mViewHolder.dropItemsSeprator.setTypeface(font);
        mViewHolder.dropItemsName.setTypeface(font);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_text_cc, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItemsCC = convertView.findViewById(R.id.spinner_country_code);
            mViewHolder.dropItemsName = convertView.findViewById(R.id.spinner_country_name);
            mViewHolder.dropItemsSeprator = convertView.findViewById(R.id.spinner_seprator);
            mViewHolder.dropItemsSeprator.setTypeface(font);
            mViewHolder.dropItemsCC.setTypeface(font);
            mViewHolder.dropItemsName.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        if (position == 0) {
            mViewHolder.dropItemsCC.setText(String.format("%s", citiesCountryCode.get(position).getCity_name()));
            mViewHolder.dropItemsName.setText("");
            mViewHolder.dropItemsSeprator.setText("");
        } else {
            mViewHolder.dropItemsCC.setText(String.format("%s", citiesCountryCode.get(position).getCountry_code()));
            mViewHolder.dropItemsName.setText(String.format("%s", citiesCountryCode.get(position).getCity_name()));
            mViewHolder.dropItemsSeprator.setText(" | ");
        }
        mViewHolder.dropItemsCC.setTypeface(font);
        mViewHolder.dropItemsName.setTypeface(font);
        mViewHolder.dropItemsSeprator.setTypeface(font);
        mViewHolder.dropItemsSeprator.setTextColor(Color.parseColor("#000000"));
        mViewHolder.dropItemsCC.setTextColor(Color.parseColor("#000000"));
        mViewHolder.dropItemsName.setTextColor(Color.parseColor("#000000"));

        return convertView;
    }

    public String getCityId(int selectedItemPosition) {
        if (selectedItemPosition != -1 && selectedItemPosition < citiesCountryCode.size())
            return citiesCountryCode.get(selectedItemPosition).getCity_id();
        return "";
    }

    public String getCountryCode(int selectedItemPosition) {
        if (selectedItemPosition != -1 && selectedItemPosition < citiesCountryCode.size())
            return citiesCountryCode.get(selectedItemPosition).getCountry_code();
        return "";
    }

    public void setCountryCodes(ArrayList<City> citiesCountryCode) {
        this.citiesCountryCode = citiesCountryCode;
        notifyDataSetChanged();
    }

    private class MyViewHolder {
        TextView dropItemsCC;
        TextView dropItemsSeprator;
        TextView dropItemsName;
    }
}
