package com.ridertechrider.driver.adaptor;

import com.ridertechrider.driver.webservice.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by grepix on 2/1/2017.
 */
public class Driver implements Serializable {

    private String d_is_verified;
    private String driver_id, api_key, d_name, d_fname, d_lname, d_email, d_license_id, d_rc, car_id, d_device_type, d_device_token, d_rating;
    private String d_rating_count, d_is_available, d_created, d_modified, d_profile_image_path, d_license_image_path, d_rc_image_path, d_insurance_image_path;
    private String category_id, car_name, car_desc, car_reg_no, car_model, car_currency, car_created, car_modified;
    private String d_lat;
    private String city_id;
    private String company_id;
    private String d_lng;
    private String d_bearing;
    private String text_password;
    private String d_degree;
    private String d_phone;
    private String c_code;
    private String car_make;
    private String isOnline;
    private String d_wallet;
    private String d_lang;
    private String d_bank_info;

    private String d_car_image_path;


    public static Driver parseJson(String driverJson) {
        return new Gson().fromJson(driverJson, Driver.class);
    }

    public static Driver parseJsonAfterLogin(String driverJson) {
        JSONObject jsonRootObject;
        try {
            jsonRootObject = new JSONObject(driverJson);
            JSONObject childObject = jsonRootObject.getJSONObject(Constants.Keys.RESPONSE);
            return Driver.parseJson(childObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isDCarImageUploaded() {
        return this.getD_car_image_path() == null || this.getD_car_image_path().equals("") || this.getD_car_image_path().equals("null");
    }
    public String getD_car_image_path() {
        return d_car_image_path;
    }

    public String getD_lang() {
        return d_lang;
    }

    public String getC_code() {
        return c_code;
    }

    public void setC_code(String c_code) {
        this.c_code = c_code;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getText_password() {
        return text_password;
    }

    public void setText_password(String text_password) {
        this.text_password = text_password;
    }

    public String getIsOnline() {

        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getD_wallet() {
        return d_wallet;
    }

    public void setD_wallet(String d_wallet) {
        this.d_wallet = d_wallet;
    }

    public String getCar_make() {
        return car_make;
    }

    public String getD_phone() {
        return d_phone;
    }

    public String getDriverId() {
        return driver_id;
    }

    public String getApiKey() {
        return api_key;
    }


    public String getD_name() {
        return d_name;
    }


    public String getD_fname() {
        return d_fname;
    }

    public String getD_lname() {
        return d_lname;
    }


    public String getD_email() {
        return d_email;
    }


    public String getD_license_id() {
        return d_license_id;
    }

    public String getD_rc() {
        return d_rc;
    }


    public String getCar_id() {
        return car_id;
    }


    public String getD_device_type() {
        return d_device_type;
    }


    public String getD_device_token() {
        return d_device_token;
    }

    public String getD_rating() {
        return d_rating;
    }

    public String getD_rating_count() {
        return d_rating_count;
    }


    public String getD_is_available() {
        if (d_is_available == null)
            return "0";
        return d_is_available;
    }


    public String getD_created() {
        return d_created;
    }

    public String getD_modified() {
        return d_modified;
    }


    public String getD_profile_image_path() {
        return d_profile_image_path;
    }


    public String getD_license_image_path() {
        return d_license_image_path;
    }


    public String getD_rc_image_path() {
        return d_rc_image_path;
    }


    public String getCategory_id() {
        return category_id;
    }


    public String getCar_name() {
        return car_name;
    }


    public String getCar_desc() {
        return car_desc;
    }


    public String getCar_reg_no() {
        return car_reg_no;
    }


    public String getCar_model() {
        return car_model;
    }


    public String getCar_currency() {
        return car_currency;
    }


    public String getCar_created() {
        return car_created;
    }


    public String getCar_modified() {
        return car_modified;
    }


    public String getD_lat() {
        return d_lat;
    }

    public void setD_lat(String d_lat) {
        this.d_lat = d_lat;
    }

    public String getD_lng() {
        return d_lng;
    }

    public void setD_lng(String d_lng) {
        this.d_lng = d_lng;
    }

    public String getD_bearing() {
        return d_bearing;
    }

    public void setD_bearing(String d_bearing) {
        this.d_bearing = d_bearing;
    }

    public String getD_degree() {
        return d_degree;
    }

    public void setD_degree(String d_degree) {
        this.d_degree = d_degree;
    }

    private String getD_insurance_image_path() {
        return d_insurance_image_path;
    }

    public boolean isDLicenceUploaded() {
        return this.getD_license_image_path() == null || this.getD_license_image_path().equals("") || this.getD_license_image_path().equals("null");
    }

    public boolean isDrcUploaded() {
        return this.getD_rc_image_path() == null || this.getD_rc_image_path().equals("") || this.getD_rc_image_path().equals("null");
    }

    public boolean isDInsuranceUploaded() {
        return this.getD_insurance_image_path() == null || this.getD_insurance_image_path().equals("") || this.getD_insurance_image_path().equals("null");
    }

    public boolean isDocVerified() {
        if (this.d_is_verified == null) {
            return false;
        }
        return this.d_is_verified.equalsIgnoreCase("1");
    }

    public boolean getD_is_verified() {
        if (this.d_is_verified == null) {
            return false;
        } else {
            return this.d_is_verified.equalsIgnoreCase("1");
        }
    }

    public boolean isDocUploaded() {
        return this.getD_license_image_path() == null || this.getD_license_image_path().equals("") ||
                this.getD_license_image_path().equals("null") || this.getD_rc_image_path() == null || this.getD_rc_image_path().equals("")
                || this.getD_rc_image_path().equals("null") || this.getD_insurance_image_path() == null
                || this.getD_insurance_image_path().equals("") || this.getD_insurance_image_path().equals("null") || this.getCity_id()== null
                || this.getCity_id().equals("null")|| this.getCity_id().equals("0") || this.getCategory_id()== null|| this.getCategory_id().equals("null")|| this.getCategory_id().equals("0")  ;
    }

    public boolean isCarDetailsAdded() {
        return this.getCar_reg_no() == null || this.getCar_reg_no().equals("") || this.getCar_reg_no().equals("null") ||
                this.getCar_make() == null || this.getCar_make().equals("") || this.getCar_make().equals("null") ||
                this.getCar_model() == null || this.getCar_model().equals("") || this.getCar_model().equals("null") ||
                this.getCar_name() == null || this.getCar_name().equals("") || this.getCar_name().equals("null")  ||
                this.getCity_id()== null || this.getCity_id().equals("null")|| this.getCity_id().equals("0") ||
                this.getCategory_id()== null|| this.getCategory_id().equals("null")|| this.getCategory_id().equals("0");
    }


    public boolean isProfileImagePathEmpty() {
        if (getD_profile_image_path() == null) {
            return false;
        }
        Objects.requireNonNull(getD_profile_image_path());

        return true;
    }
    public String getD_bank_info() {
        if (d_bank_info == null || d_bank_info.equalsIgnoreCase("null") || d_bank_info.isEmpty())
            return null;
        return d_bank_info;
    }


    public String getDriver_id() {
        return driver_id;
    }

    public String getApi_key() {
        return api_key;
    }

    public String getCompany_id() {
        return company_id;
    }
}
