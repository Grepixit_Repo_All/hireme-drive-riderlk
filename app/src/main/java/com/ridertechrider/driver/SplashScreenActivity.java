package com.ridertechrider.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.grepix.grepixutils.WebServiceUtil;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashScreenActivity extends BaseCompatActivity {

    private static final int SPLASH_SHOW_TIME = 2000;
    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    private Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_driver);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        controller = (Controller) getApplicationContext();
        controller.setShowYesNowDialog(Constants.Keys.IS_YSE_NO_DIALOG);
        localizeLang();
        ((TextView) findViewById(R.id.version)).setText(String.format("Version %s", BuildConfig.VERSION_NAME));

        String login_response = controller.pref.getString("login_response");
//        Log.d(TAG, "onCreate: login_response: " + login_response);
        if (login_response != null) {
            Driver driver = new Gson().fromJson(login_response, Driver.class);
            controller.setLoggedDriver(driver);
        }
        //new BackgroundSplashTask().execute();
        localizerStringAPI();
        addDeviceTokenToServer();
//        ImageView mimageView = findViewById(R.id.img1);
//        Bitmap mbitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.logo1)).getBitmap();
//        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
//        Canvas canvas = new Canvas(imageRounded);
//        Paint mpaint = new Paint();
//        mpaint.setAntiAlias(true);
//        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
//        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 30, 30, mpaint);// Round Image Corner 100 100 100 100
//        mimageView.setImageBitmap(imageRounded);


    }

    private void localizerStringAPI() {
        Map<String, String> params = new HashMap<>();
        //params.put(Constants.Keys.TYPE, Constants.Keys.driver);
        WebServiceUtil.excuteRequest(this, null, Constants.Urls.URL_LOCALIZER_KEY, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    controller.setLocalizeData(String.valueOf(data));
//                    Log.i("LOCALIZE_KEY->", "" + data);
                } else {
                    Log.i("LOCALIZE_KEY_ERROR->", "" + error);
                }
                try {

                    new BackgroundSplashTask().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void localizeLang() {
        Map<String, String> params = new HashMap<>();
        WebServiceUtil.excuteRequest(this, null, Constants.Urls.URL_LOCALIZE_LANG, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    try {
                        controller.setLocalizeLang(String.valueOf(data));
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(controller);
                        String lang = prefs.getString(Constants.Keys.APP_LANGUAGE, "NONE");
                        if (lang.equals("NONE")) {
                            JSONObject jsonObject, jsonParseData;
                            JSONArray localizeLangJson;
                            try {
                                jsonObject = new JSONObject(data.toString());
                                if (jsonObject != null && jsonObject.has("response")) {
                                    localizeLangJson = jsonObject.getJSONArray("response");
                                    for (int i = 0; i < localizeLangJson.length(); i++) {
                                        jsonParseData = localizeLangJson.getJSONObject(i);
                                        if (jsonParseData.getString("is_default").equals("1")) {
                                            String defaultLanguage = jsonParseData.getString("code");
//                                            Log.i("defaultLang", defaultLanguage);
                                            prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                            prefs.edit().putString(Constants.Keys.APP_LANGUAGE, defaultLanguage).commit();
                                            controller.pref.setSelectedLanguage(defaultLanguage);
                                            Utils.applyAppLanguage(getApplicationContext());
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("LOCALIZE_LANG->", "" + error);
                }
            }
        });
    }


    private void addDeviceTokenToServer() {
        if (controller.isLoggedIn()) {
            String refreshedToken = Controller.getSaveDiceToken();
            if (refreshedToken != null) {
                MyFirebaseMessagingService.driverTokenUpdateProfile(getApplicationContext(), refreshedToken);
            }
        }
    }

    private class BackgroundSplashTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                Thread.sleep(SPLASH_SHOW_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (controller.isLoggedIn()) {
                    if ( controller.getLoggedDriver().getD_is_verified()) {
                        if (controller.getLoggedDriver().isCarDetailsAdded()) {
                            Intent intent = new Intent(SplashScreenActivity.this, DocUploadActivity.class);
                            intent.putExtra("isFromRegister", true);
                            Toast.makeText(controller, R.string.error_doc_not_uploaded, Toast.LENGTH_SHORT).show();
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Intent intent;
                        if ( controller.getLoggedDriver().isCarDetailsAdded()) {
                            intent = new Intent(SplashScreenActivity.this, DocUploadActivity.class);
                            intent.putExtra("isFromRegister", true);
                            Toast.makeText(controller, R.string.error_doc_not_uploaded, Toast.LENGTH_SHORT).show();
                        } else {
                            intent = new Intent(SplashScreenActivity.this, SlideMainActivity.class);
                            Toast.makeText(controller, R.string.not_verified_yet, Toast.LENGTH_SHORT).show();
                        }
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(SplashScreenActivity.this, SignInSignUpMainActivity.class);
                    startActivity(intent);
                    finish();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();

                controller.logout();

                Intent intent = new Intent(SplashScreenActivity.this, SignInSignUpMainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
