package com.ridertechrider.driver.webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by grepix on 11/30/2016.
 */
public class CategoryActors {

    private String status;
    private String code;
    private String message;
    private String cat_name;
    private String cat_desc;
    private String cat_created;
    private String cat_modified;
    private String cat_icon_path;
    private String cat_base_includes;
    private String cat_special_fare;
    private String cat_night_charges;
    private String sort_order;
    private String cat_icon;
    private String category_id;
    private String cat_base_price;
    private String cat_fare_per_km;
    private String cat_fare_per_min;
    private String cat_max_size;
    private String cat_is_fixed_price;
    private String cat_prime_time_percentage;
    private String cat_status;
    private float wait_per_min;
    private int wait_free_min;
    private String cat_map_icon_path;
    private String show_paymode = "0";
    private String show_fare = "0";
    private String commission = "0";
    private String airport_instructions;

    public static ArrayList<CategoryActors> parseCarCategoriesResponse(String s) {
        ArrayList<CategoryActors> categoryList = new ArrayList<>();
        try {
            JSONObject rootObject = new JSONObject(s);
            JSONArray jsonArray = rootObject.getJSONArray(Constants.Keys.RESPONSE);
            for (int i = 0; i < jsonArray.length(); i++) {
                CategoryActors actors = new CategoryActors();
                JSONObject childObject = jsonArray.getJSONObject(i);
                String category_id = childObject.getString("category_id");
                actors.setCategory_id(category_id);
                String cat_name = childObject.getString("cat_name");
                actors.setCat_name(cat_name);
                String cat_desc = childObject.getString("cat_desc");
                actors.setCat_desc(cat_desc);
                String sort = childObject.getString("sort_order");
                actors.setSort_order(sort);
                String cat_base_price = childObject.getString("cat_base_price");
                actors.setCat_base_price(cat_base_price);
                String cat_base_includes = childObject.getString("cat_base_includes");
                actors.setCat_base_includes(cat_base_includes);
                String cat_fare_per_km = childObject.getString("cat_fare_per_km");
                actors.setCat_fare_per_km(cat_fare_per_km);
                String cat_fare_per_min = childObject.getString("cat_fare_per_min");
                actors.setCat_fare_per_min(cat_fare_per_min);
                String cat_max_size = childObject.getString("cat_max_size");
                actors.setCat_max_size(cat_max_size);
                String cat_is_fixed_price = childObject.getString("cat_is_fixed_price");
                actors.setCat_is_fixed_price(cat_is_fixed_price);
                String cat_prime_time_percentage = childObject.getString("cat_prime_time_percentage");
                actors.setCat_prime_time_percentage(cat_prime_time_percentage);
                String cat_special_fare = childObject.getString("cat_special_fare");
                actors.setCat_special_fare(cat_special_fare);
                String cat_status = childObject.getString("cat_status");
                actors.setCat_status(cat_status);
                String cat_created = childObject.getString("cat_created");
                actors.setCat_created(cat_created);
                String cat_modified = childObject.getString("cat_modified");
                actors.setCat_modified(cat_modified);
                String cat_icon_path = childObject.getString("cat_icon_path");
                actors.setCat_icon_path(cat_icon_path);
                if (childObject.has("cat_map_icon_path")) {
                    String cat_map_icon_path = childObject.getString("cat_map_icon_path");
                    actors.setCat_map_icon_path(cat_map_icon_path);
                }
                float wait_per_min = (float) childObject.getDouble("wait_per_min");
                actors.setWait_per_min(wait_per_min);
                int wait_free_min = childObject.getInt("wait_free_min");
                actors.setWait_free_min(wait_free_min);

                if (childObject.has("show_paymode"))
                    actors.setShow_paymode(childObject.getString("show_paymode"));
                if (childObject.has("show_fare"))
                    actors.setShow_fare(childObject.getString("show_fare"));
                if (childObject.has("commission"))
                    actors.setCommission(childObject.getString("commission"));
                if (childObject.has("airport_instructions"))
                    actors.setAirport_instructions(childObject.getString("airport_instructions"));

                categoryList.add(actors);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categoryList;
    }

    public String getCat_night_charges() {
        return cat_night_charges;
    }

    public void setCat_night_charges(String cat_night_charges) {
        this.cat_night_charges = cat_night_charges;
    }

    public String getSort_order() {
        return sort_order;
    }

    private void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getCat_base_includes() {
        return cat_base_includes;
    }

    private void setCat_base_includes(String cat_base_includes) {
        this.cat_base_includes = cat_base_includes;
    }

    public String getCat_special_fare() {
        return cat_special_fare;
    }

    private void setCat_special_fare(String cat_special_fare) {
        this.cat_special_fare = cat_special_fare;
    }

    public String getCat_icon_path() {
        return cat_icon_path;
    }

    private void setCat_icon_path(String cat_icon_path) {
        this.cat_icon_path = cat_icon_path;
    }

    public String getCat_icon() {
        return cat_icon;
    }

    public void setCat_icon(String cat_icon) {
        this.cat_icon = cat_icon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCat_name() {
        return cat_name;
    }

    private void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_desc() {
        return cat_desc;
    }

    private void setCat_desc(String cat_desc) {
        this.cat_desc = cat_desc;
    }

    public String getCat_created() {
        return cat_created;
    }

    private void setCat_created(String cat_created) {
        this.cat_created = cat_created;
    }

    public String getCat_modified() {
        return cat_modified;
    }

    private void setCat_modified(String cat_modified) {
        this.cat_modified = cat_modified;
    }

    public String getCategory_id() {
        return category_id;
    }

    private void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCat_base_price() {
        return cat_base_price;
    }

    private void setCat_base_price(String cat_base_price) {
        this.cat_base_price = cat_base_price;
    }

    public String getCat_fare_per_km() {
        return cat_fare_per_km;
    }

    private void setCat_fare_per_km(String cat_fare_per_km) {
        this.cat_fare_per_km = cat_fare_per_km;
    }

    public String getCat_fare_per_min() {
        return cat_fare_per_min;
    }

    private void setCat_fare_per_min(String cat_fare_per_min) {
        this.cat_fare_per_min = cat_fare_per_min;
    }

    public String getCat_max_size() {
        return cat_max_size;
    }

    private void setCat_max_size(String cat_max_size) {
        this.cat_max_size = cat_max_size;
    }

    public String getCat_is_fixed_price() {
        return cat_is_fixed_price;
    }

    private void setCat_is_fixed_price(String cat_is_fixed_price) {
        this.cat_is_fixed_price = cat_is_fixed_price;
    }

    public String getCat_prime_time_percentage() {
        return cat_prime_time_percentage;
    }

    private void setCat_prime_time_percentage(String cat_prime_time_percentage) {
        this.cat_prime_time_percentage = cat_prime_time_percentage;
    }

    public String getCat_status() {
        return cat_status;
    }

    private void setCat_status(String cat_status) {
        this.cat_status = cat_status;
    }

    public float getWait_per_min() {
        return wait_per_min;
    }

    public void setWait_per_min(float wait_per_min) {
        this.wait_per_min = wait_per_min;
    }

    public int getWait_free_min() {
        return wait_free_min;
    }

    public void setWait_free_min(int wait_free_min) {
        this.wait_free_min = wait_free_min;
    }

    public String getCat_map_icon_path() {
        return cat_map_icon_path;
    }

    public void setCat_map_icon_path(String cat_map_icon_path) {
        this.cat_map_icon_path = cat_map_icon_path;
    }

    public String getShow_paymode() {
        return show_paymode;
    }

    public void setShow_paymode(String show_paymode) {
        this.show_paymode = show_paymode;
    }

    public String getShow_fare() {
        return show_fare;
    }

    public void setShow_fare(String show_fare) {
        this.show_fare = show_fare;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getAirport_instructions() {
        return airport_instructions;
    }

    public void setAirport_instructions(String airport_instructions) {
        this.airport_instructions = airport_instructions;
    }
}