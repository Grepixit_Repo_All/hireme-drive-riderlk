package com.ridertechrider.driver;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by test on 2/4/16.
 */
public class TripDetailActivity extends BaseActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tripdetail);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        Controller controller = (Controller) getApplicationContext();

        Typeface typeface = Typeface.createFromAsset(getAssets(), Fonts.KARLA);
        TextView namy = findViewById(R.id.name);
        namy.setTypeface(typeface);
        TextView disty = findViewById(R.id.trip_distance);
        disty.setTypeface(typeface);
        TextView tax_amount = findViewById(R.id.tax_amt);
        tax_amount.setTypeface(typeface);
        TextView timy = findViewById(R.id.detaildate);
        ImageView cancel_icon = findViewById(R.id.canceled_icon);
        timy.setTypeface(typeface);
        TextView picky = findViewById(R.id.detailpickup1);
        picky.setTypeface(typeface);
        TextView dropy = findViewById(R.id.detaildrop1);
        dropy.setTypeface(typeface);
        TextView tripempty = findViewById(R.id.tripempty);
        tripempty.setTypeface(typeface);
        TextView totalAmount = findViewById(R.id.total_amount);
        tripempty.setTypeface(typeface);
        TextView tvCarName = findViewById(R.id.car_name);
        CircleImageView driver_profile = findViewById(R.id.driver_profile);
        ImageView recancel = findViewById(R.id.recancel);
        LinearLayout tax_lay = findViewById(R.id.tax_lay);
        final Intent i = getIntent();
        final TripModel tripModel = (TripModel) i.getSerializableExtra("trip_history");

        if (tripModel.trip.isTripCancel()) {
            cancel_icon.setVisibility(View.VISIBLE);
        } else {
            cancel_icon.setVisibility(View.GONE);
        }

        recancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String categoryId = tripModel.driver.getCategory_id();
        ArrayList<CategoryActors> categoryResponseList = CategoryActors.parseCarCategoriesResponse(controller.pref.getCategoryResponse());
        String catgoryName = "";
        for (CategoryActors catagories : categoryResponseList) {
            if (catagories.getCategory_id().equals(categoryId)) {
                catgoryName = catagories.getCat_name();
            }
        }

        if (tripModel.user.isProfileImagePathEmpty()) {
            AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripModel.user.getUProfileImagePath(), driver_profile);
        }

        showPickupAndDropLocation(tripModel);
        String fullName = tripModel.user.getU_fname() + " " + tripModel.user.getU_lname();
        Double amountDouble1 = Double.valueOf(tripModel.trip.getTrip_pay_amount());
        namy.setText(fullName.trim());
        if (tripModel.trip != null && tripModel.trip.getActual_from_loc() != null && tripModel.trip.getActual_from_loc().trim().length() != 0) {
            if (tripModel.trip.getPickup_notes() != null)
                picky.setText(String.format("%s, %s", tripModel.trip.getPickup_notes(), tripModel.trip.getActual_from_loc()));
            else
                picky.setText(tripModel.trip.getActual_from_loc());
//            picky.setText(tripModel.trip.getActual_from_loc());
        } else {

            if (Objects.requireNonNull(tripModel.trip).getPickup_notes() != null)
                picky.setText(String.format("%s, %s", Objects.requireNonNull(tripModel.trip).getPickup_notes(),
                        Objects.requireNonNull(tripModel.trip).getTrip_from_loc()));
            else
                picky.setText(Objects.requireNonNull(tripModel.trip).getTrip_from_loc());

//            picky.setText(Objects.requireNonNull(tripModel.trip).getTrip_from_loc());
        }
        if (tripModel.trip != null && tripModel.trip.getActual_to_loc() != null && tripModel.trip.getActual_to_loc().trim().length() != 0) {
            dropy.setText(tripModel.trip.getActual_to_loc());

        } else {
            dropy.setText(Objects.requireNonNull(tripModel.trip).getTrip_to_loc());

        }
        if (tripModel.trip.getTrip_tax_amt() != null) {
            if (tripModel.trip.getTrip_tax_amt().equalsIgnoreCase("0")) {
                tax_lay.setVisibility(View.GONE);
            } else {
                tax_amount.setText(controller.formatAmountWithCurrencyUnit(tripModel.trip.getTrip_tax_amt()));
            }
        } else {
            tax_lay.setVisibility(View.GONE);
        }

        totalAmount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));
        disty.setText(controller.formatDistanceWithUnit(tripModel.trip.getTrip_distance()));
        timy.setText(Utils.convertServerDateToAppLocalDate(tripModel.trip.getTrip_date()));
        tvCarName.setText(catgoryName);
    }

    private void showPickupAndDropLocation(final TripModel tripModel) {
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                String TAG = "MainScreenActivity";
                try {
                    // Customise map styling via JSON file
                    boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(TripDetailActivity.this, R.raw.uberstyle));

                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }
                MarkerOptions marker = new MarkerOptions();
                LatLng pick;
                LatLng drop;
                if (tripModel.trip.getTrip_actual_pick_lat() == null || tripModel.trip.getTrip_actual_pick_lat().equalsIgnoreCase("null")) {
                    pick = new LatLng(Double.parseDouble(tripModel.trip.getTrip_scheduled_pick_lat()), Double.parseDouble(tripModel.trip.getTrip_scheduled_pick_lng()));
                    drop = new LatLng(Double.parseDouble(tripModel.trip.getTrip_scheduled_drop_lat()), Double.parseDouble(tripModel.trip.getTrip_scheduled_drop_lng()));

                } else {
                    pick = new LatLng(Double.parseDouble(tripModel.trip.getTrip_actual_pick_lat()), Double.parseDouble(tripModel.trip.getTrip_actual_pick_lng()));
                    drop = new LatLng(Double.parseDouble(tripModel.trip.getTrip_actual_drop_lat()), Double.parseDouble(tripModel.trip.getTrip_actual_drop_lng()));

                }
                marker.position(pick).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_20));
                googleMap.addMarker(marker);
                marker.position(drop).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_20));
                googleMap.addMarker(marker);
                googleMap.getUiSettings().setScrollGesturesEnabled(false);
                googleMap.getUiSettings().setZoomGesturesEnabled(false);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(pick);
                builder.include(drop);
                LatLngBounds bounds = builder.build();
                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels / 2;
                int padding = (int) (width * 0.30); // offset from edges of the map 10% of screen
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                googleMap.animateCamera(cu);
            }
        });
    }


}
