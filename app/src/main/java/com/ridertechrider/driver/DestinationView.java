package com.ridertechrider.driver;

import android.content.Context;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.fonts.Fonts;

/**
 * Created by grepixinfotech on 21/07/16.
 */
class DestinationView {
    public final View view;
    private final Context context;
    private DestinationViewCallBack callback;
    private boolean isDelivery = false;

    public DestinationView(Context context, View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        this.context = context;
        this.view = view;
        init();
    }

    public DestinationView(Context context, View view, boolean isDelivery) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        this.context = context;
        this.view = view;
        this.isDelivery = isDelivery;
        init();
    }

    public void setDestinationActivityCallBack(DestinationViewCallBack callback) {
        this.callback = callback;
    }

    private void init() {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), Fonts.HELVETICA_NEUE);

        Button YES = view.findViewById(R.id.ad_bt_yes);
        BTextView titleText = view.findViewById(R.id.titleText);
        if (isDelivery) {
            titleText.setText("Delivered? ");
        }
        YES.setTypeface(typeface);
        YES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  hide();
                callback.onYESButtonCliked();
            }
        });
        Button NO = view.findViewById(R.id.ad_bt_no);
        NO.setTypeface(typeface);
        NO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
                callback.onNOButtonClicked();
            }
        });
        setLocalizeData();
    }

    private void setLocalizeData() {
        Button YES = view.findViewById(R.id.ad_bt_yes);
        Button NO = view.findViewById(R.id.ad_bt_no);
        BTextView titleText = view.findViewById(R.id.titleText);

        YES.setText(Localizer.getLocalizerString("k_21_s4_yes"));
        NO.setText(Localizer.getLocalizerString("k_22_s4_no"));
        titleText.setText(Localizer.getLocalizerString("k_24_s4_client_reached_dest"));
    }

    public void hide() {
        this.view.setVisibility(View.GONE);
    }

    public void show() {
        this.view.setVisibility(View.VISIBLE);
    }

    public interface DestinationViewCallBack {
        void onYESButtonCliked();

        void onNOButtonClicked();
    }


}
