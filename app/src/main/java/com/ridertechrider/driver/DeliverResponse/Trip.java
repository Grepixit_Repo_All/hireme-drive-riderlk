package com.ridertechrider.driver.DeliverResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Trip {

    @SerializedName("trip_id")
    @Expose
    private String tripId;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("group_id")
    @Expose
    private Object groupId;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("trip_date")
    @Expose
    private String tripDate;
    @SerializedName("trip_customer_details")
    @Expose
    private String tripCustomerDetails;
    @SerializedName("trip_from_loc")
    @Expose
    private String tripFromLoc;
    @SerializedName("trip_to_loc")
    @Expose
    private String tripToLoc;
    @SerializedName("trip_distance")
    @Expose
    private String tripDistance;
    @SerializedName("trip_hrs")
    @Expose
    private String tripHrs;
    @SerializedName("trip_delivery_fee")
    @Expose
    private String tripDeliveryFee;
    @SerializedName("trip_wait_time")
    @Expose
    private Object tripWaitTime;
    @SerializedName("trip_pickup_time")
    @Expose
    private Object tripPickupTime;
    @SerializedName("trip_drop_time")
    @Expose
    private Object tripDropTime;
    @SerializedName("trip_reason")
    @Expose
    private String tripReason;
    @SerializedName("trip_validity")
    @Expose
    private String tripValidity;
    @SerializedName("trip_feedback")
    @Expose
    private String tripFeedback;
    @SerializedName("trip_status")
    @Expose
    private String tripStatus;
    @SerializedName("trip_rating")
    @Expose
    private String tripRating;
    @SerializedName("trip_scheduled_pick_lat")
    @Expose
    private String tripScheduledPickLat;
    @SerializedName("trip_scheduled_pick_lng")
    @Expose
    private String tripScheduledPickLng;
    @SerializedName("trip_actual_pick_lat")
    @Expose
    private String tripActualPickLat;
    @SerializedName("trip_actual_pick_lng")
    @Expose
    private String tripActualPickLng;
    @SerializedName("trip_scheduled_drop_lat")
    @Expose
    private String tripScheduledDropLat;
    @SerializedName("trip_scheduled_drop_lng")
    @Expose
    private String tripScheduledDropLng;
    @SerializedName("trip_actual_drop_lat")
    @Expose
    private String tripActualDropLat;
    @SerializedName("trip_actual_drop_lng")
    @Expose
    private String tripActualDropLng;
    @SerializedName("trip_searched_addr")
    @Expose
    private String tripSearchedAddr;
    @SerializedName("trip_search_result_addr")
    @Expose
    private String tripSearchResultAddr;
    @SerializedName("trip_quote")
    @Expose
    private String tripQuote;
    @SerializedName("trip_pay_mode")
    @Expose
    private String tripPayMode;
    @SerializedName("trip_pay_amount")
    @Expose
    private String tripPayAmount;
    @SerializedName("trip_pay_date")
    @Expose
    private Object tripPayDate;
    @SerializedName("trip_pay_status")
    @Expose
    private String tripPayStatus;
    @SerializedName("pay_url")
    @Expose
    private String payUrl;
    @SerializedName("trip_driver_commision")
    @Expose
    private String tripDriverCommision;
    @SerializedName("trip_tip")
    @Expose
    private String tripTip;
    @SerializedName("promo_id")
    @Expose
    private String promoId;
    @SerializedName("trip_promo_code")
    @Expose
    private String tripPromoCode;
    @SerializedName("trip_promo_amt")
    @Expose
    private String tripPromoAmt;
    @SerializedName("tax_amt")
    @Expose
    private String taxAmt;
    @SerializedName("is_dispatch")
    @Expose
    private String isDispatch;
    @SerializedName("u_device_type")
    @Expose
    private String uDeviceType;
    @SerializedName("u_device_token")
    @Expose
    private String uDeviceToken;
    @SerializedName("trip_created")
    @Expose
    private String tripCreated;
    @SerializedName("trip_modified")
    @Expose
    private String tripModified;
    @SerializedName("is_manual")
    @Expose
    private String isManual;
    @SerializedName("is_send_notification")
    @Expose
    private String isSendNotification;
    @SerializedName("is_oneway")
    @Expose
    private String isOneway;
    @SerializedName("is_delivery")
    @Expose
    private String isDelivery;
    @SerializedName("Driver")
    @Expose
    private Driver driver;
    @SerializedName("User")
    @Expose
    private User user;

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Object getGroupId() {
        return groupId;
    }

    public void setGroupId(Object groupId) {
        this.groupId = groupId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripCustomerDetails() {
        return tripCustomerDetails;
    }

    public void setTripCustomerDetails(String tripCustomerDetails) {
        this.tripCustomerDetails = tripCustomerDetails;
    }

    public String getTripFromLoc() {
        return tripFromLoc;
    }

    public void setTripFromLoc(String tripFromLoc) {
        this.tripFromLoc = tripFromLoc;
    }

    public String getTripToLoc() {
        return tripToLoc;
    }

    public void setTripToLoc(String tripToLoc) {
        this.tripToLoc = tripToLoc;
    }

    public String getTripDistance() {
        return tripDistance;
    }

    public void setTripDistance(String tripDistance) {
        this.tripDistance = tripDistance;
    }

    public String getTripHrs() {
        return tripHrs;
    }

    public void setTripHrs(String tripHrs) {
        this.tripHrs = tripHrs;
    }

    public String getTripDeliveryFee() {
        return tripDeliveryFee;
    }

    public void setTripDeliveryFee(String tripDeliveryFee) {
        this.tripDeliveryFee = tripDeliveryFee;
    }

    public Object getTripWaitTime() {
        return tripWaitTime;
    }

    public void setTripWaitTime(Object tripWaitTime) {
        this.tripWaitTime = tripWaitTime;
    }

    public Object getTripPickupTime() {
        return tripPickupTime;
    }

    public void setTripPickupTime(Object tripPickupTime) {
        this.tripPickupTime = tripPickupTime;
    }

    public Object getTripDropTime() {
        return tripDropTime;
    }

    public void setTripDropTime(Object tripDropTime) {
        this.tripDropTime = tripDropTime;
    }

    public String getTripReason() {
        return tripReason;
    }

    public void setTripReason(String tripReason) {
        this.tripReason = tripReason;
    }

    public String getTripValidity() {
        return tripValidity;
    }

    public void setTripValidity(String tripValidity) {
        this.tripValidity = tripValidity;
    }

    public String getTripFeedback() {
        return tripFeedback;
    }

    public void setTripFeedback(String tripFeedback) {
        this.tripFeedback = tripFeedback;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripRating() {
        return tripRating;
    }

    public void setTripRating(String tripRating) {
        this.tripRating = tripRating;
    }

    public String getTripScheduledPickLat() {
        return tripScheduledPickLat;
    }

    public void setTripScheduledPickLat(String tripScheduledPickLat) {
        this.tripScheduledPickLat = tripScheduledPickLat;
    }

    public String getTripScheduledPickLng() {
        return tripScheduledPickLng;
    }

    public void setTripScheduledPickLng(String tripScheduledPickLng) {
        this.tripScheduledPickLng = tripScheduledPickLng;
    }

    public String getTripActualPickLat() {
        return tripActualPickLat;
    }

    public void setTripActualPickLat(String tripActualPickLat) {
        this.tripActualPickLat = tripActualPickLat;
    }

    public String getTripActualPickLng() {
        return tripActualPickLng;
    }

    public void setTripActualPickLng(String tripActualPickLng) {
        this.tripActualPickLng = tripActualPickLng;
    }

    public String getTripScheduledDropLat() {
        return tripScheduledDropLat;
    }

    public void setTripScheduledDropLat(String tripScheduledDropLat) {
        this.tripScheduledDropLat = tripScheduledDropLat;
    }

    public String getTripScheduledDropLng() {
        return tripScheduledDropLng;
    }

    public void setTripScheduledDropLng(String tripScheduledDropLng) {
        this.tripScheduledDropLng = tripScheduledDropLng;
    }

    public String getTripActualDropLat() {
        return tripActualDropLat;
    }

    public void setTripActualDropLat(String tripActualDropLat) {
        this.tripActualDropLat = tripActualDropLat;
    }

    public String getTripActualDropLng() {
        return tripActualDropLng;
    }

    public void setTripActualDropLng(String tripActualDropLng) {
        this.tripActualDropLng = tripActualDropLng;
    }

    public String getTripSearchedAddr() {
        return tripSearchedAddr;
    }

    public void setTripSearchedAddr(String tripSearchedAddr) {
        this.tripSearchedAddr = tripSearchedAddr;
    }

    public String getTripSearchResultAddr() {
        return tripSearchResultAddr;
    }

    public void setTripSearchResultAddr(String tripSearchResultAddr) {
        this.tripSearchResultAddr = tripSearchResultAddr;
    }

    public String getTripQuote() {
        return tripQuote;
    }

    public void setTripQuote(String tripQuote) {
        this.tripQuote = tripQuote;
    }

    public String getTripPayMode() {
        return tripPayMode;
    }

    public void setTripPayMode(String tripPayMode) {
        this.tripPayMode = tripPayMode;
    }

    public String getTripPayAmount() {
        return tripPayAmount;
    }

    public void setTripPayAmount(String tripPayAmount) {
        this.tripPayAmount = tripPayAmount;
    }

    public Object getTripPayDate() {
        return tripPayDate;
    }

    public void setTripPayDate(Object tripPayDate) {
        this.tripPayDate = tripPayDate;
    }

    public String getTripPayStatus() {
        return tripPayStatus;
    }

    public void setTripPayStatus(String tripPayStatus) {
        this.tripPayStatus = tripPayStatus;
    }

    public String getPayUrl() {
        return payUrl;
    }

    public void setPayUrl(String payUrl) {
        this.payUrl = payUrl;
    }

    public String getTripDriverCommision() {
        return tripDriverCommision;
    }

    public void setTripDriverCommision(String tripDriverCommision) {
        this.tripDriverCommision = tripDriverCommision;
    }

    public String getTripTip() {
        return tripTip;
    }

    public void setTripTip(String tripTip) {
        this.tripTip = tripTip;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

    public String getTripPromoCode() {
        return tripPromoCode;
    }

    public void setTripPromoCode(String tripPromoCode) {
        this.tripPromoCode = tripPromoCode;
    }

    public String getTripPromoAmt() {
        return tripPromoAmt;
    }

    public void setTripPromoAmt(String tripPromoAmt) {
        this.tripPromoAmt = tripPromoAmt;
    }

    public String getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(String taxAmt) {
        this.taxAmt = taxAmt;
    }

    public String getIsDispatch() {
        return isDispatch;
    }

    public void setIsDispatch(String isDispatch) {
        this.isDispatch = isDispatch;
    }

    public String getUDeviceType() {
        return uDeviceType;
    }

    public void setUDeviceType(String uDeviceType) {
        this.uDeviceType = uDeviceType;
    }

    public String getUDeviceToken() {
        return uDeviceToken;
    }

    public void setUDeviceToken(String uDeviceToken) {
        this.uDeviceToken = uDeviceToken;
    }

    public String getTripCreated() {
        return tripCreated;
    }

    public void setTripCreated(String tripCreated) {
        this.tripCreated = tripCreated;
    }

    public String getTripModified() {
        return tripModified;
    }

    public void setTripModified(String tripModified) {
        this.tripModified = tripModified;
    }

    public String getIsManual() {
        return isManual;
    }

    public void setIsManual(String isManual) {
        this.isManual = isManual;
    }

    public String getIsSendNotification() {
        return isSendNotification;
    }

    public void setIsSendNotification(String isSendNotification) {
        this.isSendNotification = isSendNotification;
    }

    public String getIsOneway() {
        return isOneway;
    }

    public void setIsOneway(String isOneway) {
        this.isOneway = isOneway;
    }

    public String getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
