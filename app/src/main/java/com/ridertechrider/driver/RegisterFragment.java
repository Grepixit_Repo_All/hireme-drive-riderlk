package com.ridertechrider.driver;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.BuildConfig;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.hbb20.CountryCodePicker;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static com.ridertechrider.driver.utils.Utils.removedFirstZeroMobileNumber;
import static com.ridertechrider.driver.webservice.Constants.Urls.URL_VALIDATE_REFERAL_CODE;
import static com.ridertechrider.driver.webservice.Constants.Values.USE_EMAIL_AUTH;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    protected static final String TAG = "RegisterFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private CustomProgressDialog dialog;
    private EditText email;
    private EditText refId;
    private CountryCodePicker ccp;
    private EditText etmobile;
    private EditText password;
    private EditText repassword;
    private EditText etfirst_name;
    private EditText etlast_name;

    private EditText bank_name;
    private EditText bank_user_name;
    private EditText bank_account_number;
    private EditText bank_IFSC_code;
    //    private Spinner spCountryCode;
//    private Spinner spAgencies;
    private Controller controller;
    //    private ArrayList<City> citiesCountryCode = new ArrayList<>();
//    private ArrayList<Agency> agencies = new ArrayList<>();
    //    private CountryCodeAdapter adapterCountryCode;
//    private AgencyAdapter adapterAgency;

    public RegisterFragment() {
        // Required empty public constructor
    }

    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        dialog = new CustomProgressDialog(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hire_me, container, false);
        email = view.findViewById(R.id.email);
        ccp = view.findViewById(R.id.ccp);
        etmobile = view.findViewById(R.id.mobile_no);
        password = view.findViewById(R.id.password);
        repassword = view.findViewById(R.id.confirm_password);
        etfirst_name = view.findViewById(R.id.first_name);
        etlast_name = view.findViewById(R.id.last_name);
        refId = view.findViewById(R.id.refId);

        bank_name = view.findViewById(R.id.bank_name);
        bank_user_name = view.findViewById(R.id.bank_user_name);
        bank_account_number = view.findViewById(R.id.bank_account_number);
        bank_IFSC_code = view.findViewById(R.id.bank_IFSC_code);

        TextView next = view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String ref_id = refId.getText().toString().trim();

                if (!ref_id.isEmpty()) {
                    Map<String, String> params = new HashMap<>();
                    params.put("ref_id", ref_id);
                    WebServiceUtil.excuteRequestWithNoAlert(getActivity(), params, URL_VALIDATE_REFERAL_CODE, new WebServiceUtil.DeviceTokenServiceListener() {
                        @Override
                        public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                            if (isUpdate) {
                                refId.setError(null);
                                try {
                                    JSONObject obj = new JSONObject(String.valueOf(data));
                                    if (obj != null && obj.has("response") && !obj.isNull("response") && obj.get("response") instanceof JSONObject) {
                                        signUpDriver(ref_id);
                                    } else
                                        refId.setError(Localizer.getLocalizerString("k_30_s2_invalid_refrel"));
                                } catch (Exception e) {
                                    refId.setError(Localizer.getLocalizerString("k_30_s2_invalid_refrel"));
                                }
                            } else {
                                refId.setError(Localizer.getLocalizerString("k_30_s2_invalid_refrel"));
                                if (error != null && error.networkResponse != null) {
                                    Log.d(TAG, "onUpdateDeviceTokenOnServer: ValideRefIdError: " + new String(error.networkResponse.data));
                                }
                            }
                        }
                    });
                } else {
                    signUpDriver(ref_id);
                }
            }
        });
//        spAgencies = view.findViewById(R.id.spAgencies);
//        agencies = Controller.getInstance().getAgencies();
//        agencies.add(0, new Agency(getString(R.string.select)));
//        adapterAgency = new AgencyAdapter(getContext(), R.layout.spinner_text, agencies);
//        spAgencies.setAdapter(adapterAgency);

//        spCountryCode = view.findViewById(R.id.spCountryCode);
//        citiesCountryCode = Controller.getInstance().getCities();
//        if (!USE_EMAIL_AUTH)
//            citiesCountryCode.add(0, new City(getString(R.string.select)));
//        adapterCountryCode = new CountryCodeAdapter(getContext(), R.layout.spinner_text_cc, citiesCountryCode);
//        spCountryCode.setAdapter(adapterCountryCode);
        if (USE_EMAIL_AUTH) {
//            spCountryCode.setVisibility(View.GONE);
//            ccp.setVisibility(View.GONE);
            email.setHint(Localizer.getLocalizerString("s_4_s1_tap_to_enter_email"));
        }

        ccp.setDefaultCountryUsingPhoneCode(94);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    password.setError(Localizer.getLocalizerString("k_7_s1_psd_space_nt_allowed"));
                    str = str.replaceAll(" ", "");
                    password.setText(str);
                }
            }
        });

        repassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    repassword.setError(Localizer.getLocalizerString("k_7_s1_psd_space_nt_allowed"));
                    str = str.replaceAll(" ", "");
                    repassword.setText(str);
                }
            }
        });

//        getCities();
//        getAgencies();
        setLocalizedData(view);
    }

    private void setLocalizedData(View view) {

        EditText etrepassword, etemail, etrefId, etmobile, etpassword, etfirst_name, etlast_name;
        TextView firstName, lastName, mobileNumber, email, /*agency,*/ password, cPassword, referral;

        EditText bank_name, bank_user_name, bank_account_number, bank_IFSC_code;
        TextView tv_bank_name, tv_bank_user_name, tv_bank_account_number, tv_bank_IFSC_code, tv_bank_info;


        firstName = view.findViewById(R.id.tv_first_name);
        lastName = view.findViewById(R.id.tv_last_name);
        mobileNumber = view.findViewById(R.id.tv_mobile_number);
        email = view.findViewById(R.id.tv_email);
//        agency = view.findViewById(R.id.tv_agency);
        password = view.findViewById(R.id.tv_password);
        cPassword = view.findViewById(R.id.tv_confirm_password);
        referral = view.findViewById(R.id.tv_referral);

        etemail = view.findViewById(R.id.email);
        etmobile = view.findViewById(R.id.mobile_no);
        etpassword = view.findViewById(R.id.password);
        etrepassword = view.findViewById(R.id.confirm_password);
        etfirst_name = view.findViewById(R.id.first_name);
        etlast_name = view.findViewById(R.id.last_name);
        etrefId = view.findViewById(R.id.refId);

        bank_name = view.findViewById(R.id.bank_name);
        bank_user_name = view.findViewById(R.id.bank_user_name);
        bank_account_number = view.findViewById(R.id.bank_account_number);
        bank_IFSC_code = view.findViewById(R.id.bank_IFSC_code);
        tv_bank_name = view.findViewById(R.id.tv_bank_name);
        tv_bank_user_name = view.findViewById(R.id.tv_bank_user_name);
        tv_bank_account_number = view.findViewById(R.id.tv_bank_account_number);
        tv_bank_IFSC_code = view.findViewById(R.id.tv_bank_IFSC_code);
        tv_bank_info = view.findViewById(R.id.tv_bank_info);

        TextView next = view.findViewById(R.id.next);

        next.setText(Localizer.getLocalizerString("k_16_s2_register"));

        etfirst_name.setHint(Localizer.getLocalizerString("k_2_s2_fname_hint"));
        etlast_name.setHint(Localizer.getLocalizerString("k_4_s2_lname_hint"));
        etmobile.setHint(Localizer.getLocalizerString("k_r6_s2_enter_mob_no"));
        etemail.setHint(Localizer.getLocalizerString("k_6_s3_email_address"));
        etpassword.setHint(Localizer.getLocalizerString("k_12_s2_password_hint"));
        etrepassword.setHint(Localizer.getLocalizerString("k_14_s2_confirm_password_hint"));
        etrefId.setHint(Localizer.getLocalizerString("k_15_s2_referral_id"));

        bank_name.setHint(Localizer.getLocalizerString("k_2_s2_bank_name_hint"));
        bank_user_name.setHint(Localizer.getLocalizerString("k_2_s2_bank_user_name_hint"));
        bank_account_number.setHint(Localizer.getLocalizerString("k_2_s2_bank_account_number_hint"));
        bank_IFSC_code.setHint(Localizer.getLocalizerString("k_2_s2_bank_ifsc_code_hint"));

        tv_bank_name.setText(Localizer.getLocalizerString("k_2_s2_bank_name"));
        tv_bank_user_name.setText(Localizer.getLocalizerString("k_2_s2_bank_user_name"));
        tv_bank_account_number.setText(Localizer.getLocalizerString("k_2_s2_bank_account_number"));
        tv_bank_IFSC_code.setText(Localizer.getLocalizerString("k_2_s2_bank_ifsc_code"));

        firstName.setText(Localizer.getLocalizerString("k_1_s2_fname"));
        lastName.setText(Localizer.getLocalizerString("k_3_s2_lname"));
        mobileNumber.setText(Localizer.getLocalizerString("k_2_s1_mobile_number_hint"));
        email.setText(Localizer.getLocalizerString("k_13_s1_email"));
//        agency.setText(Localizer.getLocalizerString("k_9_s2_agency"));
        password.setText(Localizer.getLocalizerString("k_11_s2_password"));
        cPassword.setText(Localizer.getLocalizerString("k_13_s2_confirm_password"));
        referral.setText(Localizer.getLocalizerString("k_15_s2_referral_id"));

    }

    private void signUpDriver(final String ref_id) {

        final String f_name_ = etfirst_name.getText().toString().trim();
        final String l_name_ = etlast_name.getText().toString().trim();
        final String email_ = email.getText().toString().trim();
        final String mobileNo_ = etmobile.getText().toString().trim();
//        String city_id = adapterCountryCode.getCityId(spCountryCode.getSelectedItemPosition());
        String c_code = ccp.getSelectedCountryCodeAsInt() + "";
        final String password_ = password.getText().toString();
//        final String agencyId = adapterAgency.getAgencyId(spAgencies.getSelectedItemPosition());
        final String con_password = repassword.getText().toString().trim();

//        if (USE_EMAIL_AUTH && citiesCountryCode.size() > 0) {
//            city_id = citiesCountryCode.get(0).getCity_id ();
//            c_code = citiesCountryCode.get(0).getCountry_code();
//        }

        final String bankName = bank_name.getText().toString().trim();
        final String bankUserName = bank_user_name.getText().toString().trim();
        final String bankAccountNumber = bank_account_number.getText().toString().trim();
        final String bankIFSCCode = bank_IFSC_code.getText().toString().trim();

        Pattern pattern = Pattern.compile("\\d+");
        if (f_name_.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_18_s2_plz_enter_first_name"), Toast.LENGTH_LONG).show();
            etfirst_name.requestFocus();
        } else if (f_name_.contains("  ")) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_19_s2_plz_enter_last_name"), Toast.LENGTH_LONG).show();
            etfirst_name.requestFocus();
        } else if (l_name_.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_19_s2_plz_enter_last_name"), Toast.LENGTH_LONG).show();
            etlast_name.requestFocus();
//        } else if (!USE_EMAIL_AUTH && spCountryCode.getSelectedItemPosition() == 0) {
        } else if (l_name_.contains("  ")) {
            /* Rest */
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_32_s2_spaces"), Toast.LENGTH_LONG).show();
            etlast_name.requestFocus();
        } else if (!USE_EMAIL_AUTH && ccp.getSelectedCountryCodeAsInt() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_24_s2_select_contry_code"), Toast.LENGTH_LONG).show();
            etmobile.requestFocus();
        } else if (mobileNo_.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_21_s2_plz_enter_valid_mobile_number"), Toast.LENGTH_LONG).show();
            etmobile.requestFocus();
        } else if (mobileNo_.length() <= 5 || mobileNo_.length() >= 15 || !pattern.matcher(mobileNo_).matches()) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_21_s2_plz_enter_valid_mobile_number"), Toast.LENGTH_LONG).show();
            etmobile.requestFocus();
        } else if (!USE_EMAIL_AUTH ? email_.trim().length() > 0 && !android.util.Patterns.EMAIL_ADDRESS.matcher(email_).matches() : !android.util.Patterns.EMAIL_ADDRESS.matcher(email_).matches()) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_14_s3_plz_enter_valid_email"), Toast.LENGTH_LONG).show();
            email.requestFocus();
//        } else if (spAgencies.getSelectedItemPosition() == 0) {
//            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_31_s2_please_select_agency"), Toast.LENGTH_LONG).show();
        } else if (password_.trim().length() < 6) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_22_s2_plz_enter_valid_password"), Toast.LENGTH_LONG).show();
            password.requestFocus();
        } else if (con_password.trim().length() < 6) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_23_s2_plz_enter_confirm_password"), Toast.LENGTH_LONG).show();
            repassword.requestFocus();
        } else if (!password_.equals(con_password)) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_25_s2_password_not_match"), Toast.LENGTH_LONG).show();
            password.requestFocus();
        } else if (bankName.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_2_s2_bank_name_hint"), Toast.LENGTH_LONG).show();
            bank_name.requestFocus();
        } else if (bankUserName.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_2_s2_bank_user_name_hint"), Toast.LENGTH_LONG).show();
            bank_user_name.requestFocus();
        } else if (bankAccountNumber.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_2_s2_bank_account_number_hint"), Toast.LENGTH_LONG).show();
            bank_account_number.requestFocus();
        } else if (bankIFSCCode.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_2_s2_bank_ifsc_code_hint"), Toast.LENGTH_LONG).show();
            bank_IFSC_code.requestFocus();
        } else {
            JSONObject objBankInfo = new JSONObject();
            try {
                objBankInfo.put("bank_name", bankName);
                objBankInfo.put("user_name", bankUserName);
                objBankInfo.put("account_number", bankAccountNumber);
                objBankInfo.put("ifsc_code", bankIFSCCode);
            } catch (Exception e) {
                Log.e(TAG, "updateProfileApi: " + e.getMessage(), e);
            }
            Log.e(TAG, "signUpDriver bank_details: " + objBankInfo.toString());
            if (!USE_EMAIL_AUTH) {

                Map<String, String> params = new HashMap<>();
//                params.put(Constants.Keys.CITY_ID, adapterCountryCode.getCityId(spCountryCode.getSelectedItemPosition()));
//                params.put(Constants.Keys.COUNTRY_CODE, adapterCountryCode.getCountryCode(spCountryCode.getSelectedItemPosition()));
                params.put(Constants.Keys.COUNTRY_CODE, ccp.getSelectedCountryCodeAsInt() + "");
                params.put(Constants.Keys.USERNAME, removedFirstZeroMobileNumber(mobileNo_));

//                final String city_id_final = city_id;
                final String c_code_final = c_code;
                dialog.showDialog();
                Log.e("register params", "" + params);
                WebServiceUtil.excuteRequest(getActivity(), params, Constants.Urls.URL_DRIVER_PHONE_VALIDATE, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        dialog.dismiss();
                        if (isUpdate) {
                            String response = data.toString();
                            ErrorJsonParsing parser = new ErrorJsonParsing();
                            CloudResponse res = parser.getCloudResponse("" + response);
                            dialog.dismiss();
                            if (res.isStatus()) {

                                final String otp = genrateOtp();

                                dialog.showDialog();
                                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                                String onlyDigitsMobileNumber = mobileNo_.replaceAll("[^0-9]+", "");
                                char value1 = onlyDigitsMobileNumber.charAt(0);
                                if (String.valueOf(value1).equals("0")) {
                                    StringBuilder builder = new StringBuilder(onlyDigitsMobileNumber);
                                    builder.deleteCharAt(0);
                                    onlyDigitsMobileNumber = builder.toString();
                                }
                                Log.e("onlyDigistsN", "" + onlyDigitsMobileNumber);
                                String phoneNumber = c_code_final + onlyDigitsMobileNumber;

//                                String otpMessage = otpMessage(otp);
//                                Call<ResponseBody> request = apiInterface.sendOtp(otpMessage, phoneNumber);
//                                request.enqueue(new Callback<ResponseBody>() {
//                                    @Override
//                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                                        if (response.isSuccessful()) {
//                                            dialog.dismiss();
//                                            Toast.makeText(getActivity(), "Message sent successfully to your mobile number", Toast.LENGTH_SHORT).show();


                                Intent intent = new Intent(getActivity(), OTPActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("otp", otp);
                                bundle.putString("d_bank_info", objBankInfo.toString());
                                bundle.putString("u_fname", f_name_);
                                bundle.putString("u_lname", l_name_);
                                bundle.putString("u_email", email_);
//                                            bundle.putString("u_city_id", city_id_final);
                                bundle.putString("u_country_code", c_code_final);
//                                bundle.putString("agency_id", agencyId);
                                bundle.putString("u_phone", removedFirstZeroMobileNumber(mobileNo_));
                                bundle.putString("u_password", password_);
                                bundle.putString("ref_id", ref_id);
                                intent.putExtras(bundle);
                                startActivity(intent);
//                                        } else {
//                                            Log.e("responseError", "" + response.errorBody().toString());
//                                            Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
//                                            dialog.dismiss();
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//                                        t.printStackTrace();
//                                        Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
//                                        dialog.dismiss();
//                                    }
//                                });


                            } else {
                                Toast.makeText(getActivity(), res.getError(), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            if (error instanceof ServerError) {
                                String s = new String(error.networkResponse.data);
                                try {
                                    JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (controller != null)
                                    Toast.makeText(controller, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
            } else {

                Map<String, String> params = new HashMap<>();
                params.put(Constants.Keys.EMAIL, email_);
                params.put(Constants.Keys.PHONE, mobileNo_);
                params.put(Constants.Keys.USERNAME, mobileNo_);
//                if (city_id != null)
//                    params.put(Constants.Keys.CITY_ID, city_id);
                if (c_code != null)
                    params.put(Constants.Keys.COUNTRY_CODE, c_code);
                params.put("d_bank_info", objBankInfo.toString());
                params.put(Constants.Keys.PASSWORD, password_);
//                params.put(COMPANY_ID, agencyId);

                final String refreshedToken = Controller.getSaveDiceToken();
                if (refreshedToken != null) {
                    params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);
                    params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
                }
                params.put("ref_id", ref_id);
                params.put(Constants.Keys.FNAME, f_name_);
                params.put(Constants.Keys.LNAME, l_name_);
                params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
                params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
                params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

                dialog.showDialog();
                Log.e("register params", "" + params);
                WebServiceUtil.excuteRequest(getActivity(), params, Constants.Urls.URL_DRIVER_REGISTER, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        dialog.dismiss();
                        if (isUpdate) {
                            String response = data.toString();
                            ErrorJsonParsing parser = new ErrorJsonParsing();
                            CloudResponse res = parser.getCloudResponse("" + response);
                            if (res.isStatus()) {
                                controller.pref.setPASSWORD(password_);
                                Driver driver = Driver.parseJsonAfterLogin(response);
                                controller.setLoggedDriver(driver);
                                controller.pref.setIsLogin(true);
                                Intent main = new Intent(getActivity(), DocUploadActivity.class);
                                main.putExtra("isFromRegister", true);
                                main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(main);
                                getActivity().finishAffinity();
                            } else {
                                Toast.makeText(Controller.getInstance(), res.getError(), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            if (error instanceof ServerError) {
                                String s = new String(error.networkResponse.data);
                                try {
                                    JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(Controller.getInstance(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
            }

//            progressDialog.showDialog();
//            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
//            String onlyDigitsMobileNumber = mobileNo_.replaceAll("[^0-9]+", "");
//            char value1 = onlyDigitsMobileNumber.charAt(0);
//            if (String.valueOf(value1).equals("0")) {
//                StringBuilder builder = new StringBuilder(onlyDigitsMobileNumber);
//                builder.deleteCharAt(0);
//                onlyDigitsMobileNumber = builder.toString();
//            }
//            Log.e("onlyDigistsN", "" + onlyDigitsMobileNumber);
//            String phoneNumber = ccp.getSelectedCountryCodeAsInt() + onlyDigitsMobileNumber;
//
//            String otpMessage = otpMessage(otp);
//            Call<ResponseBody> request = apiInterface.sendOtp(otpMessage, phoneNumber);
//            request.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    if (response.isSuccessful()) {
//                        progressDialog.dismiss();
//                        Toast.makeText(getActivity(), "Message sent successfully to your mobile number", Toast.LENGTH_SHORT).show();
//
//                        Intent intent = new Intent(getActivity(), OTPActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("otp", otp);
//                        bundle.putString("u_fname", f_name_);
//                        bundle.putString("u_lname", l_name_);
//                        bundle.putString("u_email", email_);
//                        bundle.putString("u_phone", ccp.getSelectedCountryCodeAsInt() + mobileNo_);
//                        bundle.putString("u_password", password_);
//                        intent.putExtras(bundle);
//                        startActivity(intent);
//                    } else {
//                        Log.e("responseError", "" + response.errorBody().toString());
//                        Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    t.printStackTrace();
//                    Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
//                }
//            });


//            Map<String, String> params = new HashMap<>();
//            params.put(Constants.Keys.EMAIL, email_);
//            params.put(Constants.Keys.PHONE, mobileNo_);
//            params.put(Constants.Keys.PASSWORD, password_);
//
//            final String refreshedToken = Controller.getSaveDiceToken();
//            if (refreshedToken != null) {
//                params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);
//                params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
//            }
//            params.put(Constants.Keys.FNAME, f_name_);
//            params.put(Constants.Keys.LNAME, l_name_);
//            dialog.showDialog();
//            Log.e("register params", ""+params);
//            WebServiceUtil.excuteRequest(getActivity(), params, Constants.Urls.URL_DRIVER_REGISTER, new WebServiceUtil.DeviceTokenServiceListener() {
//                @Override
//                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                    dialog.dismiss();
//                    if (isUpdate) {
//                        String response = data.toString();
//                        ErrorJsonParsing parser = new ErrorJsonParsing();
//                        CloudResponse res = parser.getCloudResponse("" + response);
//                        dialog.dismiss();
//                        if (res.isStatus()) {
//                            controller.pref.setPASSWORD(password.getText().toString());
//                            Driver driver = Driver.parseJsonAfterLogin(response);
//                            controller.setLoggedDriver(driver);
//                            controller.pref.setIsLogin(true);
//                            Intent main = new Intent(getActivity(), DocUploadActivity.class);
//                            main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(main);
//                            Objects.requireNonNull(getActivity()).finish();
//                        } else {
//                            Toast.makeText(getActivity(), res.getError(), Toast.LENGTH_LONG).show();
//                        }
//                    } else {
//                        if (error instanceof ServerError) {
//                            String s = new String(error.networkResponse.data);
//                            try {
//                                JSONObject jso = new JSONObject(s);
////Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
////
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        } else {
//                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }
//            });

        }
    }


//    public void getCities() {
//        dialog.showDialog();
//        WebServiceUtil.excuteRequest(getActivity(), null, Constants.Urls.URL_GET_CITIES, new WebServiceUtil.DeviceTokenServiceListener() {
//            @Override
//            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                dialog.dismiss();
//                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
//                if (isUpdate) {
//                    String response = data.toString();
//                    citiesCountryCode = City.parseCities(response);
//                    if (!USE_EMAIL_AUTH)
//                        citiesCountryCode.add(0, new City(getString(R.string.select)));
//                    controller.pref.setCitiesResponse(response);
//                    adapterCountryCode.setCountryCodes(citiesCountryCode);
//                } else {
//                    if (error instanceof ServerError) {
//                        String s = new String(error.networkResponse.data);
//                        try {
//                            JSONObject jso = new JSONObject(s);
////Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
////
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        if (controller != null)
//                            Toast.makeText(controller, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//    }

//    public void getAgencies() {
//        dialog.showDialog();
//        Map<String, String> params = new HashMap<>();
////        params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey());
//        WebServiceUtil.excuteRequest(getActivity(), params, Constants.Urls.URL_GET_AGENCIES, new WebServiceUtil.DeviceTokenServiceListener() {
//            @Override
//            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                dialog.dismiss();
//                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
//                if (isUpdate) {
//                    String response = data.toString();
//                    agencies = Agency.parseAgencies(response);
//                    agencies.add(0, new Agency(Localizer.getLocalizerString("k_26_s2_select")));
//                    controller.pref.setAgenciesResponse(response);
//                    adapterAgency.setAgencies(agencies);
//                } else {
//                    if (error instanceof ServerError) {
//                        String s = new String(error.networkResponse.data);
//                        try {
//                            JSONObject jso = new JSONObject(s);
////Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
////
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        if (controller != null)
//                            Toast.makeText(controller, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//    }

    private String otpMessage(String otp) {
        return String.format(Localizer.getLocalizerString("k_27_s2_mav_t_otp_message"), otp);
    }

    private String genrateOtp() {

        Random rnd = new Random();
        int otp = 1000 + rnd.nextInt(9000);
        Log.e("tp genrates", "" + otp);
        return String.valueOf(otp);
    }

}
