package com.ridertechrider.driver;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.model.AppData;
import com.grepix.grepixutils.Utils;
import com.grepix.grepixutils.WebServiceUtil;


public class TripTestActivity extends BaseActivity {

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TripModel trip = new TripModel();
            trip.tripId = "2950";
            trip.tripStatus = Constants.TripStatus.REQUEST;
            AppData.getInstance(getApplicationContext()).setTripModel(trip).saveTripTrip();
            updateTripStatusApi();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_test);
        findViewById(R.id.test_bt_accept).setOnClickListener(onClickListener);
        TextView tvCTS = findViewById(R.id.test_tv_cuurent_trip_status);
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            tvCTS.setText("CTS : " + tripModel.tripStatus);
        } else {
            tvCTS.setText("NO TRIP");
        }
    }

    private void updateTripStatusApi() {
        if (Utils.net_connection_check(TripTestActivity.this)) {

            final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel == null) {
                Log.d("TripTestActivity", "TripModel is Null");
                return;
            }
            tripModel.tripStatus = Constants.TripStatus.REQUEST;
            tripModel.trip_reason = "";
            showProgressBar();
            ServerApiCall.callWithApiKey(this, tripModel.getParams(1), Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    hideProgressBar();
                    if (isUpdate) {
                        tripModel.tripStatus = Constants.TripStatus.REQUEST;
                        AppData.getInstance(getApplicationContext()).saveTripTrip();
                        showToast();
                    }
                }
            });
        }
    }


}
