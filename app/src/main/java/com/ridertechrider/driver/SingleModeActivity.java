package com.ridertechrider.driver;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ridertechrider.driver.MapHelper.google_place_details.GooglePlaceDetails;
import com.ridertechrider.driver.MapHelper.google_place_details.GooglePlaceDetailsAddressComponent;
import com.ridertechrider.driver.Model.PickDropSelectionModel;
import com.ridertechrider.driver.Model.PlacePredictions;
import com.ridertechrider.driver.adaptor.AppController;
import com.ridertechrider.driver.adaptor.AutoCompleteAdapter;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.GoogleSDRoute;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.distance_martix.DistanceMatrixResponse;
import com.ridertechrider.driver.distance_martix.Element;
import com.ridertechrider.driver.service.LocationService;
import com.ridertechrider.driver.service.PickUpLocationService;
import com.ridertechrider.driver.service.PreferencesUtils;
import com.ridertechrider.driver.service.TrackRecordingServiceConnectionUtils;
import com.ridertechrider.driver.track_route.TrackRouteSaveData;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.TripFareCalculator;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.DriverConstants;
import com.ridertechrider.driver.webservice.RepeatTimerManager;
import com.ridertechrider.driver.webservice.SingleObject;
import com.ridertechrider.driver.webservice.WebService;
import com.ridertechrider.driver.webservice.controller.TripController;
import com.ridertechrider.driver.webservice.model.AppData;
import com.ridertechrider.driver.webservice.model.LoginResponse;
import com.github.kayvannj.permission_utils.Func2;
import com.github.kayvannj.permission_utils.PermissionUtil;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.Utils;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.ridertechrider.driver.service.ILocationConstants.LOACTION_ACTION;
import static com.ridertechrider.driver.service.ILocationConstants.LOCATION_MESSAGE;
import static com.ridertechrider.driver.service.ILocationConstants.PERMISSION_ACCESS_LOCATION_CODE;

public class SingleModeActivity extends BaseCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        Response.ErrorListener, Response.Listener<String>, TripController.TripControllerCallBack {
    private static final int REQUEST_PERMISSIONS = 100;
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 3; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 5000; // in Milliseconds
    private static final String TAG = SingleModeActivity.class.getSimpleName();
    public static ArrayList<LatLng> coverLatLongs = new ArrayList<>();
    public static ArrayList<LatLng> coverLatLongsD = new ArrayList<>();
    public static String pickupAddress = "";
    public static String dropAddress = "";
    public static Controller controller;
    final Handler handler1 = new Handler();
    private final Handler waitingHandler = new Handler();
    public VolleyJSONRequest request;
    public Handler handler;
    public String GETPLACE = "places_hit";
    public boolean isZoomEnabled = true;
    ImageView imgLocation;
    @BindView(R.id.searchResultLV)
    ListView mAutoCompleteList;
    boolean destinationPopUpVisible = false, pickupPopupVisible = false, commentBoxVisible = false;
    boolean isTripBegin = false;
    //    double lastKnownLat = 0.0, lastKnownLng = 0.0;
    boolean isTripBeginWD = false;
    boolean notReached = false;
    boolean isTripCreated = false;
    @BindView(R.id.loc_search_cancel)
    ImageView ivCleardrop;
    Runnable r = new Runnable() {
        @Override
        public void run() {
            isZoomEnabled = true;
            handler1.removeCallbacks(r);
        }
    };
    BTextView waitinTimeView;
    private final Runnable waitingTimerThread = new Runnable() {

        public void run() {
            if (!controller.pref.getIsWaiting()) {
                return;
            }
            int waitingTime = controller.pref.getWaitingTime();
            int waitingTimeTemp = 0;
            if (controller.pref.getWaitingStartTime() > 0) {
                waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
            }
            waitingTime = waitingTime + waitingTimeTemp;
//            waitingTime = waitingTime + 1;
//            controller.pref.setWaitingTime(waitingTime);
            Log.e("waitnig", "" + waitingTime);
            if (waitinTimeView != null)
                waitinTimeView.setText(getString(R.string.waiting_time) + waitingTime + getString(R.string.minute));
            waitingHandler.postDelayed(this, 30 * 1000);
        }

    };
    BTextView waitingView;
    boolean isWaiting = false;
    private int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 1;
    private LocationRequest mLocationRequest;
    private GoogleApiClient googleApiClient = null;
    private LocationManager locationManager;
    private Location myCurrentLocation;
    private Location myLastCurrentLocation;
    private GoogleMap mGoogleMap;
    private EditText search_dropoff;
    private PlacePredictions predictions;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private HandleItemClick handleItemClick;
    private CustomProgressDialog progressDialog;
    private GoogleSDRoute googleSDRoute;
    private GoogleSDRoute.RouteResponse mRouteResponse;
    private boolean isCallingForGetRoute = false;
    private ArrayList<Polyline> polylines;
    private float distancedirection;
    private double val = 0.1;
    private BTextView begin_trip;
    private LatLng latLng;
    private ArrayList<Polyline> coverRoutePolyLine = new ArrayList<>();
    private ArrayList<Polyline> coverRoutePolyLineD = new ArrayList<>();
    private LocationReceiver locationReceiver;
    private double distanceCoverInKm;
    private RelativeLayout search_layout;
    private RelativeLayout end_layout;
    private boolean isBeginRouteDrow;
    private BTextView end_trip;
    private DestinationView destinationActivity;
    private String currentTime;
    private String approxTime = "0";
    private String distancebtw = "0";
    private DistanceAndTime distanceAndTimeView;
    private String url;
    private double pikLat;
    private double pikLng;
    private String address;
    private String place;
    private GoogleApiClient client;
    private String pickLocLat;
    private String pickLocLng;
    private LocationListener locationListenerCurrent;
    private String finalTime = "1 mn";
    private String pickupTime;
    private String droptime;
    private LinearLayout ll_center_layout;
    private ArrayList<Catagories> catList = new ArrayList<>();
    //    private String distance = "";
    private String tripTime = "0 mn";
    private Marker markerDriverCarD;
    private Marker trackStartMarker;
    private boolean isAlready = false;
    private CategoryActors driverCat;
    //    ImageView googleNavigation;
    private TripModel tripModel;
    private boolean isCalling = false;
    private String upudateProfileApiTag = "updateProfileApiTag";
    private RequestQueue requestQueue;
    private BackToOrderView backToOrderView;
    private Handler customHandler = new Handler();
    private boolean isTripStatusUpdating = false;
    private boolean isRequest = true;
    private Call<ResponseBody> callTripApi;
    private boolean isStopMethodCalled = false;
    private RepeatTimerManager repeatTimerManagerTripStatus = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
        @Override
        public void onRepeatPerfrom() {
            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel != null) {
                getTrip(tripModel);
            }
        }

        @Override
        public void onStopRepeatPerfrom() {
            cancelPreviousCallingTripApi();
        }
    }, 10000);

    private Call<LoginResponse> updateDriverLocation;
    private boolean isOnPauseMethodCalled = false;
    private boolean driver_rejected;
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            getTime();
            getcategory();
            customHandler.postDelayed(this, 1000 * 1);

        }

    };
    private float v;
    private boolean isZoomEnabledFirst = true;
    private ArrayList<Location> latLngPublishRelay = new ArrayList<>();

    @OnClick(R.id.ll_tap_layout)
    public void onTapLayoutClicked() {
        PickDropSelectionModel pickdropModel = controller.getPickDropSelectionModelObserver();
        if (pickdropModel != null) {
            LatLng scrollPosition = mGoogleMap.getCameraPosition().target;
            fatchAddress(scrollPosition.latitude, scrollPosition.longitude, false, pickdropModel);
        }
    }

    private void getConstantApi() {
        ServerApiCall.callWithApiKey(SingleModeActivity.this, null, Constants.Urls.GET_CONSTANTS_API, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (data != null) {
                        controller.setConstantsList(response);
                    } else {
                        getConstantApi();
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    getConstantApi();
                    //  Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private Location setLocationData(double c_lat, double c_lng, float c_bearing) {
        Location location = new Location("");
        location.setLatitude(c_lat);
        location.setLongitude(c_lng);
        location.setBearing(c_bearing);
        return location;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = (Controller) getApplicationContext();
        String selectedLanguageLocale = controller.pref.getSelectedLanguage();

        Locale locale = new Locale(selectedLanguageLocale);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_single_mode);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            v = savedInstanceState.getFloat("v", 0);
            latLngPublishRelay = savedInstanceState.getParcelableArrayList("latLngPublishRelay");
            if (latLngPublishRelay == null)
                latLngPublishRelay = new ArrayList<>();

            if (latLngPublishRelay.size() > 0) {
//                myLastCurrentLocation = setLocationData(latLngPublishRelay.get(0).latitude, latLngPublishRelay.get(0).longitude, 0);
                myLastCurrentLocation = latLngPublishRelay.get(0);
                if (latLngPublishRelay.size() > 1) {
//                    myCurrentLocation = setLocationData(latLngPublishRelay.get(1).latitude, latLngPublishRelay.get(1).longitude, 0);
                    myCurrentLocation = latLngPublishRelay.get(1);
                } else {
//                    myCurrentLocation = setLocationData(latLngPublishRelay.get(0).latitude, latLngPublishRelay.get(0).longitude, 0);
                    myCurrentLocation = latLngPublishRelay.get(0);
                }
            } else {
                myLastCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
                myCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
            }
        } else {
            myLastCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
            myCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
        }


        ll_center_layout = findViewById(R.id.ll_center_layout);
        imgLocation = findViewById(R.id.imgMyLocation);
        progressDialog = new CustomProgressDialog(SingleModeActivity.this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        search_dropoff = findViewById(R.id.search_dropoff);
        mAutoCompleteList.setVisibility(View.GONE);
        search_layout = findViewById(R.id.search_layout);
        end_layout = findViewById(R.id.end_layout);
        begin_trip = findViewById(R.id.begin_trip);
        end_trip = findViewById(R.id.end_trip);
        waitinTimeView = findViewById(R.id.waitingTimeView);
        waitingView = findViewById(R.id.waiting);
//        googleNavigation = findViewById(R.id.googleNavigation);

        checkAndroidVersionAndAskPermission();
        setupGoogleClient();
        fn_permission();
        manageButton(false);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            if (checkLocationPermission()) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MINIMUM_TIME_BETWEEN_UPDATES,
                        MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new MyLocationListener()
                );
            } else {
                if (Build.VERSION.SDK_INT >= 28) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.FOREGROUND_SERVICE}, 1);
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            }
        }

        getCarCategoryApi();
        try {
            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel != null) {
                ll_center_layout.setVisibility(View.GONE);
            }
            if (!tripModel.tripStatus.equals(Constants.TripStatus.BEGIN)) {
                clearBeginTripDistanceAndTime();

            } else {
//                googleNavigation.setVisibility(View.VISIBLE);
                isAlready = true;
                if (coverLatLongs == null) {
                    coverLatLongs = new ArrayList<>();
                }
                if (tripModel.trip.getTrip_to_loc().trim().length() == 0) {
                    coverLatLongsD.clear();
                    coverLatLongsD.addAll(TrackRouteSaveData.getInstance(getApplicationContext()).getAllLatLngsD());
                    //Log.e("coveredLatLng", "" + this.coverLatLongsD.size());
                    drawCoverRoutePolyLineOnMapD();
                } else {
                    coverLatLongs.clear();
                    coverLatLongs.addAll(TrackRouteSaveData.getInstance(getApplicationContext()).getAllLatLngsD());
                    drawCoverRoutePolyLineOnMap();
                }
                end_layout.setVisibility(View.VISIBLE);
                search_layout.setVisibility(View.GONE);

            }
        } catch (Exception e) {
            AppData.getInstance(getApplicationContext()).clearTrip();
            clearBeginTripDistanceAndTime();
        }


        begin_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<DriverConstants> constantList = controller.getConstantsList();
                String driver_wallet_cons = "0";
                String wallet_amt = controller.getLoggedDriver().getD_wallet();

                for (DriverConstants constants : constantList) {
                    if (constants.getConstant_key().equals("driver_wallet_cons")) {
                        Log.e("constants.getConstant", "" + constants.getConstant_value());
                        driver_wallet_cons = constants.getConstant_value();
                        break;
                    }
                }
                if (driver_wallet_cons.equals("1") && (wallet_amt != null && wallet_amt.trim().length() != 0) && Integer.parseInt(wallet_amt) <= 0) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(SingleModeActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setCancelable(false);

                    builder.setMessage(R.string.please_add_money_in_wallet);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                } else if (!isTripBegin) {
                    //if (latLng != null && latLng.longitude != 0.0) {
                    callTripApi();
                }
            }
        });

        end_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goButtonClickListener();

            }
        });

        handleItemClick = new HandleItemClick(search_dropoff);

        search_dropoff.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (search_dropoff.getText().length() >= 2) {
                    mAutoCompleteList.setVisibility(View.VISIBLE);
                    mAutoCompleteList.setOnItemClickListener(handleItemClick);
                    Runnable run = new Runnable() {
                        @Override
                        public void run() {
                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            AppController.volleyQueueInstance.cancelRequestInQueue(GETPLACE);
                            //build Get url of Place Autocomplete and hit the url to fetch result.
                            request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(search_dropoff.getText().toString()), null, null, SingleModeActivity.this, SingleModeActivity.this);
                            //Give a tag to your request so that you can use this tag to cancle request later.
                            request.setTag(GETPLACE);
                            AppController.volleyQueueInstance.addToRequestQueue(request);

                        }

                    };

                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 1000);
                } else if (search_dropoff.getText().length() == 0) {
                    mAutoCompleteList.setVisibility(View.GONE);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        distanceAndTimeView = new DistanceAndTime(this, findViewById(R.id.distance_time_set), distancebtw, controller.formatAmountWithCurrencyUnitInt((int) Math.round(0)), currentTime);
        distanceAndTimeView.hide();
        destinationActivity = new DestinationView(this, findViewById(R.id.endtrip_request));
        destinationActivity.hide();
        destinationPopUpVisible = false;
        destinationActivity.setDestinationActivityCallBack(getDestinationCall());
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        locationReceiver = new LocationReceiver();
        startDistanceServiceWithDistance();
        //setupMap();
        imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (myCurrentLocation.getLatitude() == 0.0 && myCurrentLocation.getLongitude() == 0.0) {

                    } else {
                        LatLng latLng = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                        mGoogleMap.animateCamera(cameraUpdate);
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        LatLngBounds bounds;
                        bounds = builder.build();

                        // define value for padding
                        int padding = 20;
                        //This cameraupdate will zoom the map to a level where both location visible on map and also set the padding on four side.
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        mGoogleMap.moveCamera(cu);
                    }

                } catch (Exception e) {

                }

            }
        });
        isWaiting = controller.pref.getIsWaiting();
        waitingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int waitingTime = controller.pref.getWaitingTime();
                waitinTimeView.setText(getString(R.string.waiting_time) + waitingTime + getString(R.string.minute));
                waitinTimeView.setVisibility(View.VISIBLE);
                if (controller.pref.getIsWaiting()) {
                    waitingView.setText(R.string.start_waiting);
                    controller.pref.setIsWaiting(false);

                    int waitingTimeTemp = 0;
                    if (controller.pref.getWaitingStartTime() > 0) {
                        waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
                    }
                    waitingTime = waitingTime + waitingTimeTemp;
                    controller.pref.setWaitingTime(waitingTime);
                    Log.e("waitnig", "" + waitingTime);
                    controller.pref.setWaitingStartTime(0);
                    waitinTimeView.setText(getString(R.string.waiting_time) + waitingTime + getString(R.string.minute));
                    waitinTimeView.setVisibility(View.GONE);

                    if (waitingHandler != null) {
                        try {
                            waitingHandler.removeCallbacks(waitingTimerThread);

                        } catch (Exception ignore) {

                        }
                    }
                } else {
                    controller.pref.setWaitingStartTime(Calendar.getInstance().getTimeInMillis());
                    controller.pref.setIsWaiting(true);
                    waitingView.setText(R.string.stop_waiting);

                    if (waitingHandler != null) {
                        try {
                            waitingHandler.postDelayed(waitingTimerThread, 30 * 1000);
                        } catch (Exception ignore) {
                        }
                    }
                }
            }
        });
        getConstantApi();

//        googleNavigation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
//                if (tripModel != null && tripModel.trip.getTrip_scheduled_drop_lat() != null) {
//                    String uri = "http://maps.google.com/maps?saddr=" + myCurrentLocation.getLatitude() + "," + myCurrentLocation.getLongitude() + "&daddr=" + tripModel.trip.getTrip_scheduled_drop_lat() + "," + tripModel.trip.getTrip_scheduled_drop_lng();
//                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//                    startActivity(intent);
//                }
//
//            }
//        });
    }

    public boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @OnClick(R.id.recancel)
    void finishAct() {
        onBackPressed();
    }

    private void fatchAddress(final double lat, final double lng, final boolean isPickUp, final PickDropSelectionModel pickdropModel) {
        progressDialog.showDialog();

        String url = "https://maps.google.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + controller.getConstantsValueForKey("gkey");
        url = url.replaceAll(" ", "%20");
        Map<String, String> params = new HashMap<String, String>();
        WebService.excuteRequest(SingleModeActivity.this, Request.Method.GET, params, url, new WebService.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    manageButton(true);
                    if (data != null) {
                        try {
                            JSONObject component = null;
                            JSONObject result = new JSONObject(data.toString());
                            String status = result.getString("status");
                            if (status.equalsIgnoreCase("OK")) {
                                if (result.has("results")) {

                                    JSONArray array = result.getJSONArray("results");
                                    if (array.length() > 0) {
                                        JSONObject place1 = array.getJSONObject(0);
                                        String completeAddress = place1.optString("formatted_address");

                                        if (isPickUp) {
                                            pickdropModel.setPickData(true, completeAddress, new LatLng(lat, lng));
                                            search_dropoff.setText(completeAddress);
                                            latLng = new LatLng(lat, lng);
                                            address = completeAddress;
                                            drawDriverRoute(lat, lng, myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                                            mAutoCompleteList.setVisibility(View.GONE);
                                            getLocationNameByLatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());

                                        } else {
                                            pickdropModel.setDropData(true, completeAddress, new LatLng(lat, lng));
                                            search_dropoff.setText(completeAddress);
                                            latLng = new LatLng(lat, lng);
                                            address = completeAddress;
                                            drawDriverRoute(lat, lng, myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                                            mAutoCompleteList.setVisibility(View.GONE);
                                            getLocationNameByLatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                                        }

                                    }
                                }

                            } else {
                                if (status.equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                                    Toast.makeText(getApplicationContext(), result.getString("error_message"), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), (getString(R.string.unable_to_fetch) + (isPickUp ? " " + getString(R.string.pickup_location) : " " + getString(R.string.drop_location)) + getString(R.string.please_try_again_j)), Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), (getString(R.string.unable_to_fetch) + (isPickUp ? " " + getString(R.string.pickup_location) : " " + getString(R.string.drop_location)) + getString(R.string.please_try_again_j)), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), (getString(R.string.unable_to_fetch) + (isPickUp ? " " + getString(R.string.pickup_location) : " " + getString(R.string.drop_location)) + getString(R.string.please_try_again_j)), Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (error != null) {
                        Toast.makeText(getApplicationContext(), (getString(R.string.unable_to_fetch) + (isPickUp ? " " + getString(R.string.pickup_location) : " " + getString(R.string.drop_location)) + getString(R.string.please_try_again_j)), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void driverUpdateProfile(String is_available, final boolean isdeactivate, final String isVarified) {
        if (isCalling) {
            return;
        } else {
            try {
                progressDialog.showDialog();
                isCalling = true;
                final Controller controller = (Controller) getApplicationContext();
                String refreshedToken = Controller.getSaveDiceToken();
                Map<String, String> params = new HashMap<>();
                params.put(Constants.Keys.D_IS_AVAILABLE, is_available);
                if (refreshedToken != null) {
                    params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
                }
                if (isdeactivate) {
                    params.put("active", isVarified);
                }
                params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);

                params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
                params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
                params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

                if (requestQueue != null) {
                    requestQueue.cancelAll(upudateProfileApiTag);
                }
                requestQueue = ServerApiCall.callWithApiKeyAndDriverId(SingleModeActivity.this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        isCalling = false;
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (isUpdate) {
                            String response = data.toString();
                            ErrorJsonParsing parser = new ErrorJsonParsing();
                            CloudResponse res = parser.getCloudResponse("" + response);
                            if (res.isStatus()) {
                                if (response != null) {
                                    Driver driver = Driver.parseJsonAfterLogin(response);
                                    controller.setDriver(driver, getApplicationContext());
                                    String d_is_available = driver.getD_is_available();
                                    controller.pref.setD_IS_AVAILABLE(d_is_available);
                                    if (isdeactivate) {
                                        logoutApi(controller);
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            //  Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                        }
                    }
                }, upudateProfileApiTag);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void logoutApi(final Controller controller) {
        SingleModeActivity.controller = controller;
        progressDialog.showDialog();
        Map<String, String> params = new HashMap<>();
        params.put("d_is_available", "0");
        if (requestQueue != null) {
            requestQueue.cancelAll(upudateProfileApiTag);
        }
        requestQueue = ServerApiCall.callWithApiKeyAndDriverId(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    controller.logout();
                    Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                    startActivity(intent);
                    finishAffinity();
                } else {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_LONG).show();
                }

            }
        }, upudateProfileApiTag);
    }

    private void goButtonClickListener() {

        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            String status = tripModel.tripStatus;
            if (status.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                if (controller.isShowYesNowDialog()) {
                    controller.pref.setIS_FARE_SUMMARY_OPEN("open_destination_view");
                    showDistanceAndTimeView();
                    // mGoogleMap.clear();
                    //destinationActivity.show();
                    stopPickUpLocationServiceWithDistance();
                    calculateDistances();
                    destinationPopUpVisible = true;
                    distanceAndTimeView.hide();
                } else {
                    updateTripStatusApi(Constants.TripStatus.END, "");
                    destinationActivity.hide();
                    destinationPopUpVisible = false;
                }
            }
        }

    }

    private void stopPickUpLocationServiceWithDistance() {
        boolean recordingServiceRunning = TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, PickUpLocationService.class);
        if (recordingServiceRunning) {
            Intent serviceIntent = new Intent(this, PickUpLocationService.class);
            stopService(serviceIntent);
        }
    }

    private DestinationView.DestinationViewCallBack getDestinationCall() {
        return new DestinationView.DestinationViewCallBack() {
            @Override
            public void onYESButtonCliked() {
                stopPickUpLocationServiceWithDistance();
                calculateDistances();
            }

            @Override
            public void onNOButtonClicked() {
                //repeatTimerManagerTripStatus.stopRepeatCall();
                notReached = true;
                end_layout.setVisibility(View.GONE);
                backToOrderView = new BackToOrderView(findViewById(R.id.no_client_reached));
                backToOrderView.show();
                end_trip.setVisibility(View.GONE);

                commentBoxVisible = true;
                BackToOrderView.editText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black_dark));
                showDistanceAndTimeView();
                backToOrderView.setBackToOrderCallback(new BackToOrderView.BackToOrderViewCallback() {
                    @Override
                    public void onOkButtonClicked() {
                        String reasonMsg = BackToOrderView.editText.getText().toString().trim();
                        if (reasonMsg.length() == 0) {
                            Toast.makeText(getApplicationContext(), getString(R.string.please) + reasonMsg, Toast.LENGTH_LONG).show();
                        } else {
                            // driverUpdateProfile("1", false, "0");
                            calculateDistancesCancelDrop();

                            //updateTripStatusApi(Constants.TripStatus.DRIVER_CANCEL_AT_DROP, reasonMsg);
                        }
                    }

//                    @Override
//                    public void onCancel() {
//
//                        end_trip.setVisibility(View.VISIBLE);
//                        end_layout.setVisibility(View.VISIBLE);
//
//                        backToOrderView.hide();
//
//
//                    }

                });
            }
        };
    }

    private void calculateDistances() {
        progressDialog.showDialog();
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationListenerCurrent != null) {
            if (locationManager != null) {
                locationManager.removeUpdates(locationListenerCurrent);
                locationListenerCurrent = null;
            }
        }

        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

        double finalCurrentLat = myCurrentLocation.getLatitude();
        double finalCurrentLng = myCurrentLocation.getLongitude();

        Double originLat = Double.valueOf(tripModel.trip.getTrip_scheduled_pick_lat());
        Double originLng = Double.valueOf(tripModel.trip.getTrip_scheduled_pick_lng());


        String origin = originLat + "," + originLng;
        String destination = finalCurrentLat + "," + finalCurrentLng;
        url = calculateDistance(origin, destination);
        Log.e("urlk", "" + url);
        progressDialog.dismiss();
        parseDistanceMatrixResponse(url);

    }

    private void calculateDistancesCancelDrop() {

        progressDialog.showDialog();
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationListenerCurrent != null) {
            if (locationManager != null) {
                locationManager.removeUpdates(locationListenerCurrent);
                locationListenerCurrent = null;
            }

        }

        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

        double finalCurrentLat = myCurrentLocation.getLatitude();
        double finalCurrentLng = myCurrentLocation.getLongitude();

        Double originLat = Double.valueOf(tripModel.trip.getTrip_scheduled_pick_lat());
        Double originLng = Double.valueOf(tripModel.trip.getTrip_scheduled_pick_lng());

        String origin = originLat + "," + originLng;
        String destination = finalCurrentLat + "," + finalCurrentLng;

        url = calculateDistance(origin, destination);
        progressDialog.dismiss();
        parseDistanceMatrixResponseCancelDrop(url);

    }

    private String calculateDistance(String origins, String destinations) {
        String urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" +
                origins +
                "&destinations=" +
                destinations +
                "&key=" + controller.getConstantsValueForKey("gkey");
        return urlString;
    }

    private void getCarCategoryApi() {
        ServerApiCall.callWithApiKey(SingleModeActivity.this, null, Constants.Urls.GET_CAR_CATEGORY, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        Log.e("categoryResonseSinglr", "" + response);
                        controller.pref.setCategoryResponse(response);
                        controller.setCategoryResponseList(CategoryActors.parseCarCategoriesResponse(response));
                    } else {
                        //  getCarCategoryApi();
                    }
                } else {
                    //   getCarCategoryApi();
                }
            }
        });

    }

    public void getTime() {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            Date datePickup = null;
            if (tripModel.trip != null && tripModel.trip.getTrip_pickup_time() != null) {
                datePickup = Utils.stringToDate(tripModel.trip.getTrip_pickup_time());
            }

            Date dateDrop = Utils.stringToDate(AppUtil.getCurrentDateInGMTZeroInServerFormat());
            if (datePickup != null && dateDrop != null) {
                long diffrent_between_time = dateDrop.getTime() - datePickup.getTime();
                long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                    tripTime = diffrent_between_time_inMinute + " " + "mn";
                } else if (diffrent_between_time_inMinute < 1 || diffrent_between_time_inMinute == 0) {
                    if (diffrent_between_time_inSeconds < 0) {
                        tripTime = "0" + " " + "mn";
                    } else {
                        tripTime = diffrent_between_time_inSeconds + " " + "sec";
                    }
                }
            }
            if (datePickup != null && dateDrop != null) {
                String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                Date dateDrop1 = Utils.stringToDate(dropDate);
                long diffrent_between_time = dateDrop1.getTime() - datePickup.getTime();
                long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                    tripTime = diffrent_between_time_inMinute + " " + "mn";
                } else if (diffrent_between_time_inMinute < 1 || diffrent_between_time_inMinute == 0) {
                    if (diffrent_between_time_inSeconds < 0) {
                        tripTime = "0" + " " + "mn";
                    } else {
                        tripTime = diffrent_between_time_inSeconds + " " + "sec";
                    }
                }
            }
            if (datePickup == null && dateDrop != null) {

                try {
                    Date tripStartDate = AppUtil.convertStringDateToAppTripDate(controller.pref.getTripStartTime());
                    String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                    // Date dateDrop1 = com.driver.damrei.utils.Utils.stringToDate(dropDate);
                    Date tripEndDate = new Date();

                    long diffrent_between_time = AppUtil.getTimeBetweenTwoDateDifferenceInMi(tripStartDate, tripEndDate);
                    // long diffrent_between_time = dateDrop1.getTime() - tripStartDate.getTime();
                    long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                    long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                    if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                        tripTime = diffrent_between_time_inMinute + " " + "mn";
                    } else if (diffrent_between_time_inMinute < 1 || diffrent_between_time_inMinute == 0) {
                        if (diffrent_between_time_inSeconds < 0) {
                            tripTime = "0" + " " + "mn";
                        } else {
                            tripTime = diffrent_between_time_inSeconds + " " + "sec";
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(distanceCoverInKm));
            controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(tripTime));
            controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(tripTime));

        }
    }

    private void getcategory() {
        TripModel tripModel = AppData.getInstance(SingleModeActivity.this).getTripModel();
        // CategoryResponse body = response.body();
        if (controller.getCategoryResponseList() != null && controller.getCategoryResponseList().size() != 0 && tripModel != null && tripModel.driver != null && tripModel.driver.getCategory_id() != null) {
            for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                //Log.e("categoryActorsId", "" + categoryActors.getCategory_id());
                Log.e("tripModelCategoryId", "" + tripModel.driver.getCategory_id());

                if (categoryActors.getCategory_id().equalsIgnoreCase(tripModel.driver.getCategory_id())) {
                    driverCat = categoryActors;
                    Log.d("insideLoop", "" + driverCat.getCat_special_fare());
                    break;
                }
            }
        }
        CategoryActors driverCat = null;

        distanceAndTimeView.show();
        currentTime = getcurrenttime();
        DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        int type = 0;
        if (controller != null && controller.getLoggedDriver() != null && controller.getLoggedDriver().getCategory_id() != null) {
            type = Integer.parseInt(controller.getLoggedDriver().getCategory_id());
        }
        approxTime = controller.pref.getTRIP_DURATION_CHANGEABLE();
        double distanceCoverInKm2 = controller.pref.getCalculatedDistance();
        distancebtw = controller.formatMeterToDistanceWithUnit(controller.convertDistanceKmToUnit(distanceCoverInKm2));
        for (int k = 0; k < catList.size(); k++) {
            if (type == Integer.parseInt(catList.get(k).getCategory_id())) {
                type = k;
                break;
            }
        }
        if (tripModel != null && tripModel.driver != null) {
            for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                if (categoryActors.getCategory_id().equalsIgnoreCase(tripModel.driver.getCategory_id())) {
                    driverCat = categoryActors;
                }
            }
        }
        if (driverCat != null) {
            double totalPrice = 0.0;

//            try {
//                distanceCoverInKm = controller.convertDistanceKmToUnit(distanceCoverInKm);
//            } catch (NumberFormatException e) {
//                e.printStackTrace();
//            }
            String specialFare = "";
            if (driverCat != null) {
                specialFare = driverCat.getCat_special_fare();
            }
            TripFareCalculator tripFareCalculator = new TripFareCalculator(controller, distanceCoverInKm, specialFare);
            TripFareCalculator.TripFare tripFare = tripFareCalculator.calculateFare();
            totalPrice = tripFare.getTotalPrice();
            try {
                boolean distancetime = controller.pref.getTIME_DISTANCE_VIEW();
                boolean driver_rej1 = getDriver_rejected();
                if (driver_rej1) {
                    distanceAndTimeView = new DistanceAndTime(SingleModeActivity.this, findViewById(R.id.distance_time_set), distancebtw, controller.formatAmountWithCurrencyUnitInt((int) Math.round(totalPrice)), tripTime);
                    distanceAndTimeView.show();
                } else if (distancetime) {
                    distanceAndTimeView = new DistanceAndTime(SingleModeActivity.this, findViewById(R.id.distance_time_set), distancebtw, controller.formatAmountWithCurrencyUnitInt((int) Math.round(totalPrice)), tripTime);
                    distanceAndTimeView.show();
                } else {
                    distanceAndTimeView = new DistanceAndTime(SingleModeActivity.this, findViewById(R.id.distance_time_set), distancebtw, controller.formatAmountWithCurrencyUnitInt((int) Math.round(totalPrice)), tripTime);
                    distanceAndTimeView.show();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void parseDistanceMatrixResponseCancelDrop(String url1) {
        progressDialog.showDialog();
        //Log.("urldistance", "" + url1);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url1, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject result = null;
                        try {
                            result = new JSONObject(response.toString());
                            //Log.("resultdISTANCE", "" + result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Gson gson = new Gson();
                        DistanceMatrixResponse distanceMatrixResponse = gson.fromJson(response.toString(), DistanceMatrixResponse.class);
                        if (distanceMatrixResponse.getStatus().equals("INVALID_REQUEST")) {
                            progressDialog.dismiss();
                            Toast.makeText(SingleModeActivity.this, R.string.please_select_source_or_destination, Toast.LENGTH_SHORT).show();
                        } else {
                            if (distanceMatrixResponse.getStatus().equalsIgnoreCase("OK")) {
                                try {
                                    dropAddress = getCompleteAddressString(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                                    //dropAddress = distanceMatrixResponse.getDestinationAddresses().get(0);
                                    List<Element> elements;
                                    elements = distanceMatrixResponse.getRows().get(0).getElements();
                                    for (Element e : elements) {

                                        if (e.getStatus().equals("NOT_FOUND")) {
                                            if (progressDialog != null && progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(SingleModeActivity.this, R.string.please_select_source_or_destination, Toast.LENGTH_SHORT).show();

                                        } else {
                                            Integer getLastDestToOriginDistance = null;
                                            if (e != null && e.getDistance() != null && e.getDistance().getValue() != null) {
                                                getLastDestToOriginDistance = e.getDistance().getValue();
                                                //getLastDestToOriginDistance = (int) (getLastDestToOriginDistance * 0.001f);
                                            }

                                            if (getLastDestToOriginDistance == null) {
                                                //getLastDestToOriginDistance = (int) distanceCoverInKm;
                                                //getLastDestToOriginDistance = Integer.parseInt(String.valueOf(distanceCoverInKm));
                                            }

                                            updateTripStatusApi(Constants.TripStatus.DRIVER_CANCEL_AT_DROP, "", 0);
                                        }

                                    }

                                } catch (ArrayIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                    Log.d("Error", "Size is zero");
                                }
                            } else {

                                if (distanceMatrixResponse.getStatus().equalsIgnoreCase("REQUEST_DENIED") || distanceMatrixResponse.getStatus().equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                                    try {
                                        Toast.makeText(SingleModeActivity.this, result.getString("error_message"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(SingleModeActivity.this);
        requestQueue.add(jsonObjReq);
        // progressDialog.dismiss();
    }

    public void parseDistanceMatrixResponse(String url1) {
        progressDialog.showDialog();
        //Log.("urldistance", "" + url1);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url1, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject result = null;
                        try {
                            result = new JSONObject(response.toString());
                            //Log.("resultdISTANCE", "" + result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Gson gson = new Gson();
                        DistanceMatrixResponse distanceMatrixResponse = gson.fromJson(response.toString(), DistanceMatrixResponse.class);
                        if (distanceMatrixResponse.getStatus().equalsIgnoreCase("INVALID_REQUEST")) {
                            progressDialog.dismiss();
                            Toast.makeText(SingleModeActivity.this, R.string.please_select_source_or_destination, Toast.LENGTH_SHORT).show();
                        } else {
                            if (distanceMatrixResponse.getStatus().equalsIgnoreCase("OK")) {
                                try {
                                    dropAddress = getCompleteAddressString(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                                    //dropAddress = distanceMatrixResponse.getDestinationAddresses().get(0);
                                    List<Element> elements;
                                    elements = distanceMatrixResponse.getRows().get(0).getElements();
                                    for (Element e : elements) {

                                        if (e.getStatus().equals("NOT_FOUND")) {
                                            if (progressDialog != null && progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(SingleModeActivity.this, R.string.please_select_source_or_destination, Toast.LENGTH_SHORT).show();

                                        } else {
                                            Integer getLastDestToOriginDistance = null;
                                            if (e != null && e.getDistance() != null && e.getDistance().getValue() != null) {
                                                getLastDestToOriginDistance = e.getDistance().getValue();
                                                //getLastDestToOriginDistance = (int) (getLastDestToOriginDistance * 0.001f);
                                            }

                                            if (getLastDestToOriginDistance == null) {
                                                //getLastDestToOriginDistance = (int) distanceCoverInKm;
                                                //getLastDestToOriginDistance = Integer.parseInt(String.valueOf(distanceCoverInKm));
                                            }

                                            updateTripStatusApi(Constants.TripStatus.END, "", 0);
                                        }

                                    }

                                } catch (ArrayIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                    Log.d("Error", "Size is zero");
                                }
                            } else {

                                if (distanceMatrixResponse.getStatus().equalsIgnoreCase("REQUEST_DENIED") || distanceMatrixResponse.getStatus().equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                                    try {
                                        Toast.makeText(SingleModeActivity.this, result.getString("error_message"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(SingleModeActivity.this);
        requestQueue.add(jsonObjReq);
        // progressDialog.dismiss();
    }

    private void updateTripStatusApi(final String trip_status, final String trip_cancel_reason) {
        updateTripStatusApi(trip_status, trip_cancel_reason, 0);
    }

    private void updateTripStatusApi(final String trip_status, final String trip_cancel_reason, float distanceCv) {
        if (Utils.net_connection_check(SingleModeActivity.this)) {
            if (isTripStatusUpdating) {
                return;
            } else {
                isTripStatusUpdating = true;
                final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                if (tripModel == null) {
                    return;
                }
                tripModel.tripStatus = trip_status;
                tripModel.trip_reason = trip_cancel_reason;
                if (trip_status.equals(Constants.TripStatus.BEGIN)) {
                    TrackRouteSaveData.getInstance(this).clearData();

                    if (myCurrentLocation != null && myCurrentLocation.getLongitude() != 0.0 && myCurrentLocation.getLatitude() != 0.0) {
                        tripModel.actual_pickup_lat = String.valueOf(myCurrentLocation.getLatitude());
                        tripModel.actual_pickup_lng = String.valueOf(myCurrentLocation.getLongitude());
                    } else {
                        tripModel.actual_pickup_lat = String.valueOf(controller.getLoggedDriver().getD_lat());
                        tripModel.actual_pickup_lng = String.valueOf(controller.getLoggedDriver().getD_lng());
                    }
                }

                if (trip_status.equals(Constants.TripStatus.END) || trip_status.equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
                    int waitingTime = controller.pref.getWaitingTime();
                    if (controller.pref.getIsWaiting()) {
                        int waitingTimeTemp = 0;
                        if (controller.pref.getWaitingStartTime() > 0) {
                            waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
                        }
                        waitingTime = waitingTime + waitingTimeTemp;
                        controller.pref.setWaitingTime(waitingTime);
                        Log.e("waitnig", "" + waitingTime);
                        controller.pref.setWaitingStartTime(0);
                        waitinTimeView.setText(getString(R.string.waiting_time) + waitingTime + getString(R.string.minute));
                        waitinTimeView.setVisibility(View.GONE);

                        if (waitingHandler != null) {
                            try {
                                waitingHandler.removeCallbacks(waitingTimerThread);

                            } catch (Exception ignore) {

                            }
                        }
                    }
                    if (waitingTime <= 5) {
                        waitingTime = 0;

                    } else {
                        waitingTime = waitingTime - 5;
                    }
                    tripModel.wait_duration = String.valueOf(waitingTime);

                    CategoryActors driverCat = null;
                    for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                        if (categoryActors.getCategory_id().equalsIgnoreCase(tripModel.driver.getCategory_id())) {
                            driverCat = categoryActors;
                        }
                    }
//                    distanceCoverInKm = controller.convertDistanceKmToUnit(distanceCoverInKm);
                    String specialFare = "";
                    if (driverCat != null) {
                        specialFare = driverCat.getCat_special_fare();
                    }
                    TripFareCalculator tripFareCalculator = new TripFareCalculator(controller, distanceCoverInKm, specialFare);
                    TripFareCalculator.TripFare tripFare = tripFareCalculator.calculateFare();
                    double waitingFare = Double.parseDouble(driverCat.getCat_fare_per_min()) * waitingTime;

                    tripModel.tripAmount = String.format(Locale.ENGLISH, "%.02f", (tripFare.getTotalPrice() + waitingFare));
                    tripModel.minutes = tripFareCalculator.getTotalMinutes();

                    tripModel.tripDistance = String.format(Locale.ENGLISH, "%.02f", (float) controller.convertDistanceKmToUnit(distanceCoverInKm));
                    tripModel.tripDistanceUnit = controller.checkCityDistanceUnit();
                    tripModel.taxAmount = String.valueOf(controller.formatAmountWithCurrencyUnit((float) tripFare.getTripTax()));

                    tripModel.drop_time = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                    if (trip_status.equals(Constants.TripStatus.END) || trip_status.equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
                        tripModel.actual_drop_lat = String.valueOf(myCurrentLocation.getLatitude());
                        tripModel.actual_drop_lng = String.valueOf(myCurrentLocation.getLongitude());
                    }

//                    distance = tripModel.tripDistance;
                }
                if (trip_status.equals(Constants.TripStatus.BEGIN)) {
                    tripModel.pick_time = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                }
                cancelPreviousCallingTripApi();
                Log.e("tripModelParams", "" + tripModel.getParams(2));
                ServerApiCall.callWithApiKey(this, tripModel.getParams(2), Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        isTripStatusUpdating = false;
                        if (isUpdate) {
                            progressDialog.dismiss();
                            handleTripUpdateResponse(data, tripModel, trip_status);
                        } else {
                            repeatTimerManagerTripStatus.setTimerForRepeat();
                        }
                    }
                });
            }
        }
    }

    private void handleTripUpdateResponse(Object data, TripModel tripModel, String
            trip_status) {
        tripModel.tripStatus = trip_status;
        AppData.getInstance(getApplicationContext()).saveTripTrip();
        JSONObject jsonRootObject = null;
        try {
            jsonRootObject = new JSONObject(data.toString());
            int response = jsonRootObject.getInt(Constants.Keys.RESPONSE);
            if (response == 1) {
                TripController tripController = new TripController(tripModel);
                tripController.setTripControllerCallBack(SingleModeActivity.this);
                if (trip_status.equals(Constants.TripStatus.END) || trip_status.equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
                    CategoryActors driverCat = null;
                    for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                        if (categoryActors.getCategory_id().equalsIgnoreCase(tripModel.driver.getCategory_id())) {
                            driverCat = categoryActors;
                        }
                    }
//                    distanceCoverInKm = controller.convertDistanceKmToUnit(distanceCoverInKm);
                    String specialFare = "";
                    if (driverCat != null) {
                        specialFare = driverCat.getCat_special_fare();
                    }
                    TripFareCalculator tripFareCalculator = new TripFareCalculator(controller, distanceCoverInKm, specialFare);

                    TripFareCalculator.TripFare tripFare = tripFareCalculator.calculateFare();

                    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
                    DecimalFormat formatter = (DecimalFormat) nf;
                    formatter.applyPattern("##.##");
                    float totFare = Float.parseFloat(formatter.format(tripFare.getTotalPrice()));
                    controller.pref.setTRIP_PAY_AMOUNT("" + totFare);
                    isRequest = true;
                    progressDialog.dismiss();
                    isStopMethodCalled = true;
                    tripController.setChnageTripStatusLocal(trip_status);
                } else {
                    repeatTimerManagerTripStatus.setTimerForRepeat();
                    tripController.setChnageTripStatus(trip_status);
                    tripController.setChnageTripStatusLocal(trip_status);
                }
                if (trip_status.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                    setBeginTripDistanceAndTime();

                }
            } else {
                repeatTimerManagerTripStatus.setTimerForRepeat();
                // not update
            }
        } catch (JSONException e) {
            e.printStackTrace();
            repeatTimerManagerTripStatus.setTimerForRepeat();
        }
    }

    private void setBeginTripDistanceAndTime() {
        DateFormat df = AppUtil.getDateFormat();
        controller.pref.setTripStartTime(df.format(new Date()));
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();
    }

    private void cancelPreviousCallingTripApi() {
        if (callTripApi != null) {
            callTripApi.cancel();
            callTripApi = null;
        }
    }

    private void clearBeginTripDistanceAndTime() {
        controller.pref.setTripStartTime("");
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();
    }

    private void drawCoverRoutePolyLineOnMap() {
        for (int i = 0; i < coverLatLongs.size(); i++) {
            if (coverLatLongs.get(i).longitude == 0.0) {
                coverLatLongs.remove(i);
            }
        }
        PolylineOptions polylineOptionses = new PolylineOptions();
        polylineOptionses.addAll(coverLatLongs);
        polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
        polylineOptionses.color(getResources().getColor(R.color.grey_color));
        polylineOptionses.geodesic(true);
        polylineOptionses.zIndex(4);
        if (mGoogleMap != null) {
            coverRoutePolyLine.add(mGoogleMap.addPolyline(polylineOptionses));
            if (coverLatLongs.size() > 0) {
                LatLng latLngFirst = coverLatLongs.get(0);

            }
        }
    }

    private void getTrip(final TripModel tripModel) {
        getTrip(tripModel, true);
    }

    private void getTrip(final TripModel tripModel, final boolean isResetTimer) {
        cancelPreviousCallingTripApi();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        callTripApi = apiInterface.getTripById(controller.getLoggedDriver().getApiKey(), tripModel.tripId);
        callTripApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        TripController tripController = new TripController(tripModel);
                        tripController.setTripControllerCallBack(SingleModeActivity.this);
                        tripController.handleTripResponse(string);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (isRequest) {
                    isRequest = false;
                    progressDialog.dismiss();
                }
                if (isResetTimer) {
                    resetTripApiTimer();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (isRequest) {
                    isRequest = false;
                    progressDialog.dismiss();
                }
                if (call.isCanceled()) {

                } else {
                    if (isResetTimer) {
                        resetTripApiTimer();
                    }
                }
            }
        });
    }

    private void resetTripApiTimer() {
        if (!isStopMethodCalled) {
            repeatTimerManagerTripStatus.setTimerForRepeat();
        }
    }

    private void getLocationNameByLatLng(double origins, double destinations) {

        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + origins + "," + destinations + "&key=" + controller.getConstantsValueForKey("gkey");
        WebServiceUtil.excuteRequest(SingleModeActivity.this, new HashMap<String, String>(), url, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                try {
                    if (data != null) {
                        JSONObject result = new JSONObject(data.toString());
                        String status = result.getString("status");
                        if (status.equalsIgnoreCase("OK")) {
                            if (isUpdate) {
                                JSONObject jsonObject = new JSONObject(data.toString());
                                JSONArray array = jsonObject.getJSONArray("results");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject1 = array.getJSONObject(0);
                                    place = jsonObject1.getString("formatted_address");
                                    System.out.print(place);
                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                // Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                //Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction", "Canont get Address!");
        }
        return strAdd;
    }

//    @Override
//    public void onCuurentTripStatus(String tripStatus) {
//
//    }

    private void callTripApi() {
        if (!net_connection_check()) {
            Toast.makeText(SingleModeActivity.this, Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_LONG).show();
        } else {
            pickLocLat = String.valueOf(myCurrentLocation.getLatitude());
            pickLocLng = String.valueOf(myCurrentLocation.getLongitude());
            progressDialog.showDialog();
            String tripdate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey()); //Verified
            params.put(Constants.Keys.USER_ID, "1"); //Verified
            params.put(Constants.Keys.DRIVER_ID, controller.getLoggedDriver().getDriverId()); //Verified
            if (latLng != null && latLng.longitude != 0.0) {

            } else {
                latLng = new LatLng(0.0, 0.0);
                address = "";
                if (myCurrentLocation != null && myCurrentLocation.getLongitude() != 0.0 && myCurrentLocation.getLatitude() != 0.0) {
                    place = getCompleteAddressString(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                } else {
                    place = getCompleteAddressString(Double.valueOf(controller.getLoggedDriver().getD_lat()), Double.valueOf(controller.getLoggedDriver().getD_lng()));
                }
                pickupAddress = place;
            }
            pickupAddress = place;
            //Verified}
            if (address.contains("'")) {
                address = address.replace("'", "");
            }
            params.put(Constants.Keys.SEARCH_RESULT_ADDR, address);
            params.put(Constants.Keys.SEARCH_ADDR, address);
            params.put(Constants.Keys.TRIP_DEST_LOC, address);
            params.put(Constants.Keys.SCHEDULED_DROP_LAT, String.valueOf(latLng.latitude)); //Verified
            params.put(Constants.Keys.SCHEDULED_DROP_LNG, String.valueOf(latLng.longitude));
            params.put(Constants.Keys.CITY_ID, controller.getLoggedDriver().getCity_id());
            params.put(Constants.Keys.TRIP_STATUS, "begin");
            params.put(Constants.Keys.SCHEDULED_PICK_LAT, pickLocLat);
            params.put(Constants.Keys.SCHEDULED_PICK_LNG, pickLocLng);
            params.put(Constants.Keys.TRIP_DATE, tripdate);
            params.put(Constants.Keys.TRIP_PICK_LOC, place);

            params.put(Constants.Keys.TRIP_DISTANCE, String.format(Locale.ENGLISH, "%.02f", ((float) controller.convertDistanceKmToUnit(distanceCoverInKm))));
            params.put(Constants.Keys.TRIP_DISTANCE_UNIT, controller.checkCityDistanceUnit());

            params.put(Constants.Keys.TRIP_PAY_AMOUNT, "" + 0.0);
            Log.e("params", "" + params);
            WebServiceUtil.excuteRequest(this, params, Constants.Urls.URL_USER_CREATE_TRIP, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    progressDialog.dismiss();
                    if (isUpdate) {
                        isTripBegin = true;

                        ll_center_layout.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        try {
                            isPickCaption();
                            JSONObject jsonRootObject = new JSONObject(data.toString());
                            JSONObject response = jsonRootObject.getJSONObject(Constants.Keys.RESPONSE);
                            tripModel = TripModel.parseJson(String.valueOf(response));
                            controller.pref.setSingleMode(true);
                            //Loeg.e("tripmoDELsINGLEMode", "" + tripModel);
                            tripModel.tripId = tripModel.trip.getTrip_id();
                            tripModel.tripStatus = tripModel.trip.getTrip_status();
                            if (coverLatLongs != null) {
                                coverLatLongs.clear();

                            }
                            if (coverLatLongsD != null) {
                                coverLatLongsD.clear();
                            }
                            PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, 0);


                            TrackRouteSaveData.getInstance(getApplicationContext()).clearData();
                            AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
                            updateTripStatusApi(Constants.TripStatus.BEGIN, "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    }

    private void isPickCaption() {
        driverUpdateProfile("0", false, "0");
        distanceAndTimeView.show();
        showDistanceAndTimeView();
        end_layout.setVisibility(View.VISIBLE);
        search_layout.setVisibility(View.GONE);

    }

//    @Override
//    public void onTripPick(TripModel tripModel) {
//
//    }

    @Override
    protected void onStop() {
        super.onStop();
        client.disconnect();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        repeatTimerManagerTripStatus.stopRepeatCall();
        AppData.getInstance(getApplicationContext()).saveData();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
        // LocalBroadcastManager.getInstance(this).unregisterReceiver(pickUpLocationReceiver);
        googleApiClient.disconnect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        client.connect();
        repeatTimerManagerTripStatus.startRepeatCall();
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            String tripStatus = tripModel.tripStatus;
            if (tripStatus != null && tripStatus.equals(Constants.TripStatus.BEGIN)) {

            /*
              Runtime permissions are required on Android M and above to access User's location
             */
                if (Build.VERSION.SDK_INT >= 28) {

                    if (AppUtil.hasM() && !(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) && ContextCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_GRANTED) {
                        askPermissions();
                    }
                } else {
                    if (AppUtil.hasM() && !(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                        askPermissions();
                    }
                }
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver, new IntentFilter(LOACTION_ACTION));
        //LocalBroadcastManager.getInstance(this).registerReceiver(pickUpLocationReceiver, new IntentFilter(PICKUP_LOCATION_ACTION));
        startDistanceServiceWithDistance();
    }

    private void askPermissions() {
        Context context = SingleModeActivity.this;
        AppCompatActivity activity = (AppCompatActivity) context;
        if (Build.VERSION.SDK_INT >= 28) {
            PermissionUtil.with(activity).request(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.FOREGROUND_SERVICE).onResult(
                    new Func2() {
                        @Override
                        protected void call(int requestCode, String[] permissions, int[] grantResults) {

                            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                                startDistanceServiceWithDistance();

                            } else {
                                Toast.makeText(SingleModeActivity.this, R.string.permission_denied, Toast.LENGTH_LONG).show();
                            }
                        }

                    }).ask(PERMISSION_ACCESS_LOCATION_CODE);
        } else {
            PermissionUtil.with(activity).request(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION).onResult(
                    new Func2() {
                        @Override
                        protected void call(int requestCode, String[] permissions, int[] grantResults) {

                            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                                startDistanceServiceWithDistance();

                            } else {
                                Toast.makeText(SingleModeActivity.this, R.string.permission_denied, Toast.LENGTH_LONG).show();
                            }
                        }

                    }).ask(PERMISSION_ACCESS_LOCATION_CODE);
        }
    }

    @Override
    public void onTripResponseValid(TripModel tripModel) {

    }

    @Override
    public void onTripAccept(TripModel tripModel) {

    }

    @Override
    public void onTripBegin(TripModel tripModel) {
        //Log.("BeginTrip", "BeginTripCalled");
        repeatTimerManagerTripStatus.stopRepeatCall();
        showDistanceAndTimeView();
        end_layout.setVisibility(View.VISIBLE);
        search_layout.setVisibility(View.GONE);
        if (!isBeginRouteDrow && !isTripBeginWD) {
            isTripBegin = true;
            removeDriverMarker();
            mGoogleMap.clear();
            animateCarOnMap(latLngPublishRelay);
            //setupMap();
        }

        isWaiting = controller.pref.getIsWaiting();
        waitingView.setVisibility(View.VISIBLE);
        if (isWaiting) {

            int waitingTime = controller.pref.getWaitingTime();
            int waitingTimeTemp = 0;
            if (controller.pref.getWaitingStartTime() > 0) {
                waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
            }
            waitingTime = waitingTime + waitingTimeTemp;

            waitinTimeView.setText("Waiting Time : " + waitingTime + " min");
            waitinTimeView.setVisibility(View.VISIBLE);
            waitingView.setText("Stop Waiting");
        } else {
            waitingView.setText("Start Waiting");
            waitinTimeView.setVisibility(View.GONE);
        }

//        googleNavigation.setVisibility(View.VISIBLE);

        startDistanceServiceWithDistance();
    }

    @Override
    public void onTripCancel(TripModel tripModel) {
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
        if (repeatTimerManagerTripStatus != null) repeatTimerManagerTripStatus.stopRepeatCall();

        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }
    }

    @Override
    public void onTripEnd(TripModel tripModel) {
        repeatTimerManagerTripStatus.stopRepeatCall();
        clearBeginTripDistanceAndTime();
        stopLocationRecordService();
        TrackRouteSaveData.getInstance(this).clearData();
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
        isAlready = false;

        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }

        // driverUpdateProfile("1", false, "0");
        SingleObject obj = SingleObject.getInstance();
        obj.setFareSummaryLinearLayout(false);
        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
        destinationActivity.view.setVisibility(View.GONE);
        TrackRouteSaveData.getInstance(this).clearData();
        removeDriverMarker();
        Date datePickup = Utils.stringToDate(tripModel.trip.getTrip_pickup_time());
        Date dateDrop = Utils.stringToDate(tripModel.trip.getTrip_drop_time());
        //Log.("pickupdate", "" + tripModel.trip.getTrip_pickup_time());
        //Log.("dateDrop", "" + tripModel.trip.getTrip_drop_time());
        if (datePickup != null && dateDrop != null) {
            long diffrent_between_time = dateDrop.getTime() - datePickup.getTime();
            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
            long diffrent_between_time_inSeconds = diffrent_between_time / (1000);


            if (diffrent_between_time != 0 && diffrent_between_time_inMinute > 0) {
                finalTime = diffrent_between_time_inMinute + " " + "mn";
            } else {
                finalTime = diffrent_between_time_inSeconds + " " + "sec";
            }
        }
        if (datePickup != null && dateDrop == null) {
            String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
            Date dateDrop1 = Utils.stringToDate(dropDate);
            long diffrent_between_time = dateDrop1.getTime() - datePickup.getTime();
            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
            //Log.("diffrent_between_tim", "" + diffrent_between_time_inMinute);
            // if(diffrent_between_time_inMinute==0){diffrent_between_time_inMinute =1;}
            long diffrent_between_time_inSeconds = diffrent_between_time / (1000);


            if (diffrent_between_time != 0 && diffrent_between_time_inMinute > 0) {
                finalTime = diffrent_between_time_inMinute + " " + "mn";
            } else {
                finalTime = diffrent_between_time_inSeconds + " " + "sec";
            }
        }
        try {
            customHandler.removeCallbacks(updateTimerThread, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getTime();

        Intent intent = new Intent(getApplicationContext(), FareSummaryActivity.class);
        intent.putExtra("singleMode", "true");
        intent.putExtra("time", tripTime);
//        if (distance.trim().length() != 0) {
//            intent.putExtra("distance", distance);
//        }

        startActivity(intent);
        finish();
    }

    private void removeDriverMarker() {
        if (markerDriverCarD != null) {
            markerDriverCarD.remove();
            markerDriverCarD = null;
        }
    }

    private void stopLocationRecordService() {

        /*if(jobScheduler!=null){ jobScheduler.cancelAll();}else if(AppUtil.jobScheduler!=null){
            AppUtil.jobScheduler.cancelAll();
        }*/
        if (TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class)) {
            Log.d("isLOcationSERVIC", "" + "YYES");
            Intent serviceIntent = new Intent(this, LocationService.class);
            stopService(serviceIntent);
        } else {
            Log.d("isLOcationSERVI", "" + "no");
        }
    }

    @Override
    public void onTripRequest(TripModel tripModel) {
        isBeginRouteDrow = false;
    }

    @Override
    public void onTripError(String message) {
        repeatTimerManagerTripStatus.stopRepeatCall();

        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }

    }

    @Override
    public void onTripDriverCancelAtDrop(TripModel tripModel) {
      /*  repeatTimerManagerTripStatus.stopRepeatCall();
        if (markerDriverCar != null) {
            markerDriverCar.remove();
        }
        TrackRouteSaveData.getInstance(this).clearData();
        isAlready = false;
        destinationActivity.view.setVisibility(View.GONE);
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);

        AppData.getInstance(getApplicationContext()).clearTrip();
        Intent intent = new Intent(getApplicationContext(), MyJobActivity.class);

        startActivity(intent);
        finish();*/

        repeatTimerManagerTripStatus.stopRepeatCall();
        clearBeginTripDistanceAndTime();
        stopLocationRecordService();
        TrackRouteSaveData.getInstance(this).clearData();
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
        isAlready = false;
        // driverUpdateProfile("1", false, "0");
        SingleObject obj = SingleObject.getInstance();
        obj.setFareSummaryLinearLayout(false);
        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
        destinationActivity.view.setVisibility(View.GONE);
        TrackRouteSaveData.getInstance(this).clearData();
        removeDriverMarker();
        Date datePickup = Utils.stringToDate(tripModel.trip.getTrip_pickup_time());
        Date dateDrop = Utils.stringToDate(tripModel.trip.getTrip_drop_time());
        //Log.("pickupdate", "" + tripModel.trip.getTrip_pickup_time());
        //Log.("dateDrop", "" + tripModel.trip.getTrip_drop_time());
        if (datePickup != null && dateDrop != null) {
            long diffrent_between_time = dateDrop.getTime() - datePickup.getTime();
            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
            long diffrent_between_time_inSeconds = diffrent_between_time / (1000);


            if (diffrent_between_time != 0 && diffrent_between_time_inMinute > 0) {
                finalTime = diffrent_between_time_inMinute + " " + "mn";
            } else {
                finalTime = diffrent_between_time_inSeconds + " " + "sec";
            }
        }
        if (datePickup != null && dateDrop == null) {
            String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
            Date dateDrop1 = Utils.stringToDate(dropDate);
            long diffrent_between_time = dateDrop1.getTime() - datePickup.getTime();
            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
            //Log.("diffrent_between_tim", "" + diffrent_between_time_inMinute);
            // if(diffrent_between_time_inMinute==0){diffrent_between_time_inMinute =1;}
            long diffrent_between_time_inSeconds = diffrent_between_time / (1000);


            if (diffrent_between_time != 0 && diffrent_between_time_inMinute > 0) {
                finalTime = diffrent_between_time_inMinute + " " + "mn";
            } else {
                finalTime = diffrent_between_time_inSeconds + " " + "sec";
            }
        }
        try {
            customHandler.removeCallbacks(updateTimerThread, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getTime();
        try {
            customHandler.removeCallbacks(updateTimerThread, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(getApplicationContext(), FareSummaryActivity.class);
        intent.putExtra("singleMode", "true");
        intent.putExtra("time", tripTime);
//        if (distance.trim().length() != 0) {
//            intent.putExtra("distance", distance);
//        }

        startActivity(intent);
        finish();

    }

    @Override
    public void onTripDriverCancelAtPickup(TripModel tripModel) {
        isAlready = false;
        repeatTimerManagerTripStatus.stopRepeatCall();
        TrackRouteSaveData.getInstance(this).clearData();
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);

    }

    @Override
    public void onTripArrive(TripModel tripModel) {

    }

    private void startDistanceServiceWithDistance() {
        // jobScheduler = AppUtil.scheduleJob(getApplicationContext());
        Intent serviceIntent = new Intent(this, LocationService.class);
        boolean recordingServiceRunning = TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class);

        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.tripStatus.equals(Constants.TripStatus.BEGIN)) {
            if (!PreferencesUtils.getBoolean(this, R.string.is_recording_start, false)) {
                // PreferencesUtils.getBoolean(this, R.string.is_recording_start, true);
                PreferencesUtils.setBoolean(this, R.string.is_recording_start, true);
                if (!isAlready) {
                    PreferencesUtils.setFloat(this, R.string.recorded_distance, 0);
                }
            }
            controller.pref.setCalculatedDistance(PreferencesUtils.getFloat(this, R.string.recorded_distance) / 1000);
        }

        if (!recordingServiceRunning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(serviceIntent);
            } else {
                startService(serviceIntent);
            }
        }
    }

    private String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //urlString.append("&components=country:in");
        urlString.append("&location=");
        if (myCurrentLocation != null) {
            urlString.append(myCurrentLocation.getLatitude() + "," + myCurrentLocation.getLongitude());
        }
        urlString.append("&radius=500&language=en");
        urlString.append("&key=" + controller.getConstantsValueForKey("gkey"));
        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {

    }

    @Override
    public void onResponse(String s) {
        JSONObject result;
        try {
            result = new JSONObject(s);
            String status = result.getString("status");
            if (status.equalsIgnoreCase("OK")) {
                Gson gson = new Gson();
                predictions = gson.fromJson(s, PlacePredictions.class);
                if (mAutoCompleteAdapter == null) {
                    mAutoCompleteAdapter = new AutoCompleteAdapter(getApplicationContext(), predictions.getPlaces());
                    mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
                } else {
                    mAutoCompleteAdapter.clear();
                    mAutoCompleteAdapter.addAll(predictions.getPlaces());
                    mAutoCompleteAdapter.notifyDataSetChanged();
                    mAutoCompleteList.invalidate();
                }
            } else {
                if (status.equalsIgnoreCase("REQUEST_DENIED") || status.equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                    Toast.makeText(this, result.getString("error_message"), Toast.LENGTH_SHORT).show();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callPlaceDetailsApi(String placeID, final EditText editText) {
        progressDialog.showDialog();
        String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeID + "&key=" + controller.getConstantsValueForKey("gkey");
        WebServiceUtil.excuteRequest(SingleModeActivity.this, new HashMap<String, String>(), url, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                try {
                    if (data != null) {
                        JSONObject result = new JSONObject(data.toString());
                        String status = result.getString("status");
                        if (status.equalsIgnoreCase("OK")) {
                            if (isUpdate) {
                                GooglePlaceDetails placeDetails = new GooglePlaceDetails().parseGooglePlaceDetails(data.toString());
                                List<GooglePlaceDetailsAddressComponent> component = placeDetails.getResult().getAddressComponents();
                                String name = null, subLoc2 = null, subLoc1 = null, streetNo = null, route = null, cityName = null, stateName = null, disName = null, country = null, zipcode = null;
                                name = placeDetails.getResult().getName();
                                for (GooglePlaceDetailsAddressComponent addressComponent : component) {
                                    if (addressComponent.getTypes().size() > 0) {
                                        String type = addressComponent.getTypes().get(0);
                                        if (type.equalsIgnoreCase("sublocality_level_2")) {
                                            subLoc2 = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("sublocality_level_1")) {
                                            subLoc1 = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("street_number")) {
                                            streetNo = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("route")) {
                                            route = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("locality")) {
                                            cityName = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("administrative_area_level_1")) {
                                            stateName = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("administrative_area_level_2")) {
                                            disName = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("administrative_area_level_2")) {
                                            disName = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("postal_code")) {
                                            zipcode = addressComponent.getLongName();
                                        }
                                        if (type.equalsIgnoreCase("country")) {
                                            country = addressComponent.getLongName();
                                        }
                                    }
                                }
                                address = (subLoc1 != null ? subLoc1 + ", " : "") + (subLoc2 != null ? subLoc2 + ", " : "") + (streetNo != null ? streetNo + ", " : "") + (route != null ? route + ", " : "") +
                                        (cityName != null ? cityName + ", " : "") + (disName != null ? disName + ", " : "") + (stateName != null ? stateName + ", " : "") + (country != null ? country + " " : "") + (zipcode != null ? zipcode : "");

                                latLng = new LatLng(placeDetails.getResult().getGeometry().getLocation().getLat(), placeDetails.getResult().getGeometry().getLocation().getLng());

                                editText.setText(address);
                                manageButton(true);
                                drawDriverRoute(latLng.latitude, latLng.longitude, myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                                mAutoCompleteList.setVisibility(View.GONE);
                                getLocationNameByLatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                            }
                        } else {
                            if (status.equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                                Toast.makeText(SingleModeActivity.this, result.getString("error_message"), Toast.LENGTH_LONG).show();
                                mAutoCompleteList.setVisibility(View.GONE);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    @OnClick(R.id.loc_search_cancel)
    public void onCancleDropLocation() {
        manageButton(false);
        PickDropSelectionModel pickDropModel = controller.getPickDropSelectionModelObserver();
        pickDropModel.setPickData(false, "", null);
        if (mGoogleMap != null) {
            removeDriverMarker();
            mGoogleMap.clear();
            animateCarOnMap(latLngPublishRelay);
        }
        search_dropoff.setText("");
        search_dropoff.setHint(R.string.select_drop_off);
    }

    public void manageButton(boolean isEnable) {
        ivCleardrop.setImageResource(isEnable ? R.drawable.remove_loc : R.drawable.search);
        ivCleardrop.setEnabled(isEnable);
    }

    private void checkAndroidVersionAndAskPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasLocationPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);

            int hasSMSPermission = checkSelfPermission(android.Manifest.permission.SEND_SMS);
            List<String> permissions = new ArrayList<>();
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (Build.VERSION.SDK_INT >= 28) {
                int foregroundService = checkSelfPermission(Manifest.permission.FOREGROUND_SERVICE);
                if (foregroundService != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.FOREGROUND_SERVICE);
                }
            }
            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.SEND_SMS);
            }
            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
            }
        }
    }

    private void setupGoogleClient() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this,
                    requestCode);
            dialog.show();
        } else { // Google Play Services are available
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(GData.UPDATE_INTERVAL_IN_MILLISECONDS);
            // Use high accuracy
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // Set the interval ceiling to one minute
            mLocationRequest.setFastestInterval(GData.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
            // Note that location updates are off until the user turns them on
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        setupMap();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(SingleModeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION))) {


            } else {
                ActivityCompat.requestPermissions(SingleModeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION

                        },
                        REQUEST_PERMISSIONS);
            }
        }
        if (Build.VERSION.SDK_INT >= 28) {
            if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED)) {

                if ((ActivityCompat.shouldShowRequestPermissionRationale(SingleModeActivity.this, Manifest.permission.FOREGROUND_SERVICE))) {


                } else {
                    ActivityCompat.requestPermissions(SingleModeActivity.this, new String[]{Manifest.permission.FOREGROUND_SERVICE

                            },
                            REQUEST_PERMISSIONS);
                }
            }
        }
    }

    private void buildAlertMessageNoGps() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.Dialog);
        builder.setMessage(R.string.gps_disabled)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean net_connection_check() {
        ConnectionManager cm = new ConnectionManager(this);
        boolean connection = cm.isConnectingToInternet();
        if (!connection) {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.there_is_no_network_please_connect, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 70);
            toast.show();
        }
        return connection;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isTripBeginWD = true;
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.trip != null && tripModel.trip.getTrip_status() != null && tripModel.trip.getTrip_status().equals(Constants.TripStatus.BEGIN)) {
            isAlready = true;
        }
        if (notReached) {
            end_trip.setVisibility(View.GONE);

        } else if (!notReached) {

            end_trip.setVisibility(View.VISIBLE);
        }
        setupMap();
    }

    private void setupMap() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map1)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                String selectedLanguageLocale = controller.pref.getSelectedLanguage();

                TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

                if (tripModel != null) {
                    if (tripModel.trip.getTrip_to_loc().trim().length() == 0) {
                        drawCoverRoutePolyLineOnMapD();
                    } else {
                        drawCoverRoutePolyLineOnMap();
                    }
                }
                if (tripModel != null) {
                    String pickLat = tripModel.trip.getTrip_scheduled_pick_lat();
                    pikLat = Double.parseDouble(pickLat);
                    String pickLng = tripModel.trip.getTrip_scheduled_pick_lng();
                    pikLng = Double.parseDouble(pickLng.trim());
                    String dropLat = tripModel.trip.getTrip_scheduled_drop_lat();
                    double drop_Lat = Double.parseDouble(dropLat.trim());
                    String dropLng = tripModel.trip.getTrip_scheduled_drop_lng();
                    double drop_Lng = Double.parseDouble(dropLng);

                    if (tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0 && tripModel.actual_pickup_lat != null && tripModel.actual_pickup_lng != null) {
                        try {
                            drawDriverRoute(drop_Lat, drop_Lng, Double.parseDouble(tripModel.actual_pickup_lat), Double.parseDouble(tripModel.actual_pickup_lng));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*if (myCurrentLocation == null) {
                            drawDriverRoute(drop_Lat, drop_Lng, Double.parseDouble(tripModel.driver.getD_lat()), Double.parseDouble(tripModel.driver.getD_lng()));
                        } else {
                            drawDriverRoute(drop_Lat, drop_Lng, Double.parseDouble(tripModel.actual_pickup_lat), Double.parseDouble(tripModel.actual_pickup_lng));
                        }*/
                    } else {
                        coverLatLongs.clear();
                        clearGoogleRoutePolyLine();
                        clearCoveredRoute();
                        removeDriverMarker();
                        mGoogleMap.clear();
                        animateCarOnMap(latLngPublishRelay);
                        drawRouteWithoutDestination(myCurrentLocation);
                    }
                }
                googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {
                        if (i == REASON_GESTURE) {
                            isZoomEnabled = false;
                            try {
                                handler1.removeCallbacks(r);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            handler1.postDelayed(r, 30000);
                        }
                    }
                });
                animateCameraAndLocation();
            }
        });

        layoutZoomControlOnMap();
        layoutUserLocationButtonOnMap();
        animateCameraAndLocation();

    }

    public void onBackPressed() {
        if (pickupPopupVisible) {
            pickupPopupVisible = false;
        } else if (destinationPopUpVisible) {
            destinationActivity.hide();
            destinationPopUpVisible = false;
        } else if (commentBoxVisible) {
            backToOrderView.hide();
            end_layout.setVisibility(View.VISIBLE);
            end_trip.setVisibility(View.VISIBLE);
            notReached = false;
            commentBoxVisible = false;
        } else if (isTripBegin) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            //  finish();
        } else {
            if (tripModel != null) {
                Toast.makeText(controller, R.string.please_complete_your_trip_first, Toast.LENGTH_SHORT).show();
            } else {
                startActivity(new Intent(SingleModeActivity.this, SlideMainActivity.class));
                finish();
            }
        }
    }

    private void animateCameraAndLocation() {
        if (Build.VERSION.SDK_INT >= 28) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        if (mGoogleMap != null) {
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            myCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
//            if (myCurrentLocation == null) {
//                double driverLat = Double.parseDouble(controller.pref.getDriver_Lat());
//                double driverLong = Double.parseDouble(controller.pref.getDriver_Lng());
//                myCurrentLocation = new Location("");
//                if (lastKnownLng != 0.0 && lastKnownLat != 0.0) {
//                    myCurrentLocation = new Location("");
//                    myCurrentLocation.setLatitude(lastKnownLat);
//                    myCurrentLocation.setLongitude(lastKnownLng);
//                    latLong = new LatLng(lastKnownLat, lastKnownLng);
//
//                } else {
//                    latLong = new LatLng(driverLat, driverLong);
//                }
//
//
//            } else {
//                lastKnownLat = myCurrentLocation.getLatitude();
//                lastKnownLng = myCurrentLocation.getLongitude();
//                latLong = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
//                controller.pref.setDriver_Lat(String.valueOf(myCurrentLocation.getLatitude()));
//                controller.pref.setDriver_Lng(String.valueOf(myCurrentLocation.getLongitude()));
//            }
            mGoogleMap.setMyLocationEnabled(false);
//            if (isZoomEnabled) {
//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target().zoom(16f).build();
//                mGoogleMap.animateCamera(CameraUpdateFactory
//                        .newCameraPosition(cameraPosition));
//            }
        }
    }

    private void layoutZoomControlOnMap() {
        // Zoom Control Position
        @SuppressLint("ResourceType") View zoomControls = getFragmentManager().findFragmentById(
                R.id.map1).getView().findViewById(0x1);
        if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            // ZoomControl is inside of RelativeLayout
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();

            // Align it to - parent top|left
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            // Update margins, set to 10dp
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
                    getResources().getDisplayMetrics());
            params.setMargins(margin, margin, margin, margin + 200);
        }
    }

    private void layoutUserLocationButtonOnMap() {
        @SuppressLint("ResourceType") View locationButton = getFragmentManager().findFragmentById(R.id.map1).getView().findViewById(2);
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rlp.setMargins(0, 230, 30, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putFloat("v", v);
        outState.putParcelableArrayList("latLngPublishRelay", latLngPublishRelay);

    }

    private void drawDriverRoute(double destLat, double destLng, double originLat,
                                 double originLng) {
        if (isCallingForGetRoute) {
            return;
        }
        isCallingForGetRoute = true;
        LatLng dest = new LatLng(destLat, destLng);
        LatLng origin = new LatLng(originLat, originLng);
        ArrayList<LatLng> allLatLngs = TrackRouteSaveData.getInstance(getApplicationContext()).getAllLatLngsD();
        String waypoints = "";
        /*if (allLatLngs != null && allLatLngs.size() != 0 && allLatLngs.size() < 23) {
            for (int i = 0; i < allLatLngs.size(); i++) {
                waypoints = waypoints + "|" + allLatLngs.get(i).latitude + "," + allLatLngs.get(i).longitude;
            }
        } else if (allLatLngs != null && allLatLngs.size() != 0 && allLatLngs.size() > 23) {
            for (int i = 0; i < 10; i++) {
                waypoints = waypoints + "|" + allLatLngs.get(i).latitude + "," + allLatLngs.get(i).longitude;
            }
            for (int i = allLatLngs.size() - 13; i < allLatLngs.size(); i++) {
                waypoints = waypoints + "|" + allLatLngs.get(i).latitude + "," + allLatLngs.get(i).longitude;
            }
        }*/
        googleSDRoute = new GoogleSDRoute(this, origin, dest, waypoints);
        googleSDRoute.getRoute(getGoogeSdRoute());
    }

    private GoogleSDRoute.GoogleSDRouteCallBack getGoogeSdRoute() {
        return new GoogleSDRoute.GoogleSDRouteCallBack() {
            @Override
            public void onCompleteSDRoute(GoogleSDRoute.RouteResponse routeResponse) {
                isCallingForGetRoute = false;
                mRouteResponse = routeResponse;
                if (routeResponse != null) {
                    isBeginRouteDrow = true;
                    clearGoogleRoutePolyLine();
                    removeDriverMarker();
                    mGoogleMap.clear();
                    animateCarOnMap(latLngPublishRelay);
                    polylines = routeResponse.addRouteOnMap(mGoogleMap, true, Constants.Values.BeginTripRouteWidth);
//                markerDriverCar = routeResponse.addStartLocation(mGoogleMap, controller.getLoggedDriver().getCategory_id(), new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude()));
//                    markerDriverCarD = routeResponse.addStartLocation(mGoogleMap, new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude()));
                    handleRouteDataAfterPolyLine(routeResponse);
                    TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                    /**/
                    if (tripModel != null) {
                        String trip_status = tripModel.tripStatus;
                        if (trip_status != null) {
                            if (tripModel != null) {
                                if (tripModel.trip.getTrip_to_loc().trim().length() == 0) {
                                    drawCoverRoutePolyLineOnMapD();
                                } else {
                                    drawCoverRoutePolyLineOnMap();
                                }
                            }
                            if (trip_status.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                                //showDistanceAndTimeView();
                             /* if (tripModel != null) {
                                    if (tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
                                        checkOffRouteAndRedrwaRoute(myCurrentLocation);
                                    }
                                }*/
                            }
                        }
                    }
                }
            }
        };
    }

    private void clearGoogleRoutePolyLine() {
        if (polylines != null) {
            for (Polyline polyline : polylines) {
                polyline.remove();
            }
            polylines.clear();
        }
    }

    private void handleRouteDataAfterPolyLine(GoogleSDRoute.RouteResponse routeResponse) {
        if (routeResponse.maneuver.size() > 1) {
            if (myCurrentLocation == null) {
                return;
            }
            Location locationA = new Location("point A");
            locationA.setLatitude(myCurrentLocation.getLatitude());
            locationA.setLongitude(myCurrentLocation.getLongitude());
            Location locationB = new Location("point B");
            locationB.setLatitude(routeResponse.ending_lat.get(1));
            locationB.setLongitude(routeResponse.ending_long.get(1));
            distancedirection = locationA.distanceTo(locationB);
            distancedirection = locationA.distanceTo(locationB) / 1000;

        }
    }

    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());
            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(500); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        if (isZoomEnabled) {
                            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                    .target(newPosition)
                                    .zoom(16)
                                    .build()));
                        }
                        // marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            });
            valueAnimator.start();
        }
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private boolean isTripBegin() {
        TripModel tripModel = AppData.getInstance(getApplication()).getTripModel();
        return tripModel != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN);
    }

    private void drawRouteWithoutDestination(Location location) {
        clearCoveredRoute();
        coverLatLongs.clear();
        clearGoogleRoutePolyLine();
        clearCoveredRouteD();
        //Log.e("updatredLocationTO Draw", "" + location.getLatitude() + "," + location.getLongitude());
        if (location != null) {
            setAndUpdateDriverLocation(location);

            updateDriverProfileApi(location.getLatitude(), location.getLongitude());
            makeRouteAndRedrwaRouteWithoutDestination(location);
            //addDriverCurrentLocationMarker(location);
        }
    }

    private void clearCoveredRouteD() {
        if (coverRoutePolyLineD == null) {
            coverRoutePolyLineD = new ArrayList<>();
        }
        for (Polyline polyline : coverRoutePolyLineD) {
            polyline.remove();
        }
        coverRoutePolyLineD.clear();
//        if (markerDriverCarD != null) {
//            markerDriverCarD.remove();
//            markerDriverCarD = null;
//        }
        if (trackStartMarker != null) {
            trackStartMarker.remove();
            trackStartMarker = null;
        }
    }

    private void drawCoverRoutePolyLineOnMapD() {
        for (int i = 0; i < coverLatLongsD.size(); i++) {
            if (coverLatLongsD.get(i).longitude == 0.0) {
                coverLatLongsD.remove(i);
            }
        }
        PolylineOptions polylineOptionses = new PolylineOptions();
        polylineOptionses.addAll(coverLatLongsD);
        polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
        polylineOptionses.color(getResources().getColor(R.color.grey_color));
        polylineOptionses.geodesic(true);
        polylineOptionses.zIndex(4);
        if (mGoogleMap != null) {
            coverRoutePolyLineD.add(mGoogleMap.addPolyline(polylineOptionses));
            if (coverLatLongsD.size() > 0) {
                LatLng latLngFirst = coverLatLongsD.get(0);
                Location location = new Location("");
                location.setLatitude(latLngFirst.latitude);
                location.setLongitude(latLngFirst.longitude);
                LatLng latlng = coverLatLongsD.get(coverLatLongsD.size() - 1);
//                if (markerDriverCarD != null) {
//                    if (latlng != null) {
//                        Location location1 = new Location("");
//                        location1.setLatitude(latlng.latitude);
//                        location1.setLongitude(latlng.longitude);
//                        animateMarkerNew(location1, markerDriverCarD);
//                    }
//                } else {
//                    ////Log.e("tuk tuk kahmer", "ytes");
//                    MarkerOptions markerOptions = null;
//                    markerOptions = new MarkerOptions().position(latlng).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
//
//
//                    //markerOptions.anchor(-0.5f, -15.5f);
//                    //Log.e("redrawImage", "yes");
//                    markerDriverCarD = mGoogleMap.addMarker(markerOptions);
//
//                }
                if (trackStartMarker != null) {
                    trackStartMarker.setPosition(latLngFirst);
                } else {
                    trackStartMarker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(latLngFirst)
                            .title("")
                            .snippet("")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_25)));

                }
            }
        }
    }

    private void addDriverCurrentLocationMarker(Location location) {
        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());

//        if (markerDriverCarD != null) {
//            if (latLong != null) {
//                Location location1 = new Location("");
//                location1.setLatitude(latlng.latitude);
//                location1.setLongitude(latlng.longitude);
//                animateMarkerNew(location1, markerDriverCarD);
//                // MarkerAnimation.animateMarkerToICS(markerDriverCarD, latLong, new LatLngInterpolator.Spherical());
//            }
//        } else {
//            if (mGoogleMap != null && latLong != null) {
//                MarkerOptions markerOptions = null;
//                markerOptions = new MarkerOptions().position(latlng).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
//                markerDriverCarD = mGoogleMap.addMarker(markerOptions);
//            }
//        }

    }

    public void drawOffSideRoute() {
        for (int i = 0; i < coverLatLongsD.size(); i++) {
            if (coverLatLongsD.get(i).longitude == 0.0) {
                coverLatLongsD.remove(i);
            }
        }
        PolylineOptions polylineOptionses = new PolylineOptions();
        polylineOptionses.addAll(coverLatLongsD);
        polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
        polylineOptionses.color(getResources().getColor(R.color.grey_color));
        polylineOptionses.geodesic(true);
        polylineOptionses.zIndex(4);
        if (mGoogleMap != null) {
            coverRoutePolyLineD.add(mGoogleMap.addPolyline(polylineOptionses));
        }
    }

    private void makeRouteAndRedrwaRouteWithoutDestination(Location location) {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
//        addDriverCurrentLocationMarker(location);
        if (tripModel != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
            clearCoveredRouteD();
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if (latLng.longitude != 0.0 && latLng.latitude != 0.0) {
                coverLatLongsD.add(latLng);
            }
            //Log.e("sizecovers", "" + coverLatLongsD.size());
            //Log.e("latLng", "" + latLng);
            if (TrackRouteSaveData.getInstance(SingleModeActivity.this) != null) {
                TrackRouteSaveData.getInstance(SingleModeActivity.this).addLatLongD(latLng);
                TrackRouteSaveData.getInstance(getApplicationContext()).saveData();

            }
            drawCoverRoutePolyLineOnMapD();
        } else {
            coverLatLongsD.clear();
        }
    }

    private void cancelUserUpdateApiCall() {
        if (updateDriverLocation != null) {
            updateDriverLocation.cancel();
            updateDriverLocation = null;
        }
    }

    private void updateDriverProfileApi(final double lat, final double lng) {
        cancelUserUpdateApiCall();
        controller.pref.setDriver_Lat(String.valueOf(lat));
        controller.pref.setDriver_Lng(String.valueOf(lng));
        controller.getLoggedDriver().setD_lat(Double.toString(lat));
        controller.getLoggedDriver().setD_lng(Double.toString(lng));
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        //Log.("updateDriver", "true0");
        updateDriverLocation = apiInterface.updateDriverLocation(controller.getLoggedDriver().getApiKey(), controller.getLoggedDriver().getDriverId(), Double.toString(lat), Double.toString(lng));
        updateDriverLocation.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
        if (!isOnPauseMethodCalled) {
            // repeatTimerManagerUpdateLocation.setTimerForRepeat();
        }
    }

    private void setAndUpdateDriverLocation(Location location) {
        controller.setD_lat(location.getLatitude());
        controller.setD_lng(location.getLongitude());
        controller.setDriverLocation(location);
    }

    private void checkOffRouteAndRedrwaRoute(final Location location) {
        if (mRouteResponse != null) {
            if (polylines == null) {
                polylines = new ArrayList<>();
            }
            GoogleSDRoute.DataTest nearestLocationTest = mRouteResponse.findNearestLocationTest(location);
            /*TrackRouteSaveData.getInstance(getApplication()).addLatLong(nearestLocationTest.latLng);
            TrackRouteSaveData.getInstance(getApplicationContext()).saveData();*/
            final Location locationNear = new Location("");
            locationNear.setLatitude(nearestLocationTest.latLng.latitude);
            locationNear.setLongitude(nearestLocationTest.latLng.longitude);
            ArrayList<LatLng> lastNearstLoc = nearestLocationTest.latLngsRouteData;
            /*PolylineOptions polylineOptionses = new PolylineOptions();
            if (lastNearstLoc == null)
                return;
            polylineOptionses.addAll(lastNearstLoc);
            polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
            polylineOptionses.color(getResources().getColor(R.color.splash_background));
            polylineOptionses.geodesic(true);
            polylineOptionses.zIndex(2);
            clearGoogleRoutePolyLine();*/
            boolean onOffRoute = mRouteResponse.isOnOffRoute(location);
            //polylines.add(mGoogleMap.addPolyline(polylineOptionses));
            if (onOffRoute) {
                makeRouteAndRedrwaRouteD(location, location);
            } else {
                makeRouteAndRedrwaRouteD(locationNear, location);
            }

//            if (mRouteResponse.srcLoc != null) {
//                LatLng latlng = lastNearstLoc.get(lastNearstLoc.size() - 1);
////                if (markerDriverCarD != null) {
////                    if (latlng != null) {
////                        Location location1 = new Location("");
////                        location1.setLatitude(latlng.latitude);
////                        location1.setLongitude(latlng.longitude);
////                        animateMarkerNew(myCurrentLocation, markerDriverCarD);
////                    }
////                } else {
////                    MarkerOptions markerOptions = null;
////                    markerOptions = new MarkerOptions().position(new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
////                    ;//.anchor(-15.5f, -15.5f);
////                    markerDriverCarD = mGoogleMap.addMarker(markerOptions);
////                }
//            } else {
//                LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
////                if (markerDriverCarD != null) {
////                    if (latLong != null) {
////                        Location location1 = new Location("");
////                        location1.setLatitude(latlng.latitude);
////                        location1.setLongitude(latlng.longitude);
////                        animateMarkerNew(myCurrentLocation, markerDriverCarD);
////                    }
////                } else {
////                    MarkerOptions markerOptions = null;
////                    markerOptions = new MarkerOptions().position(new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
////                    ;//.anchor(-15.5f, -15.5f);
////                    markerDriverCarD = mGoogleMap.addMarker(markerOptions);
////                }
//            }
            // boolean onOffRoute1 = mRouteResponse.isOnOffRoute(location);
            // this is noly for damrie because dont need to reroute the  map if driver goes outside
           /* if (onOffRoute1) {
                try {
                    double lat = myCurrentLocation.getLatitude();
                    double lng = myCurrentLocation.getLongitude();
                    TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                    double destLat = Double.parseDouble(tripModel.trip.getTrip_scheduled_drop_lat());
                    double destLng = Double.parseDouble(tripModel.trip.getTrip_scheduled_drop_lng());
                    googleSDRoute = new GoogleSDRoute(getApplicationContext(), new LatLng(lat, lng), new LatLng(destLat, destLng));
                    googleSDRoute.getRoute(new GoogleSDRoute.GoogleSDRouteCallBack() {
                        @Override
                        public void onCompleteSDRoute(GoogleSDRoute.RouteResponse routeResponse, Exception ex) {
                            if (routeResponse != null) {
                                mRouteResponse = routeResponse;
                                clearGoogleRoutePolyLine();
                                LatLng latlng = routeResponse.srcLoc;
                                if (markerDriverCar != null) {
                                    if (latlng != null) {
                                        Location location1 = new Location("");
                                        location1.setLatitude(latlng.latitude);
                                        location1.setLongitude(latlng.longitude);
                                        animateMarkerNew(myCurrentLocation, markerDriverCar);

                                    }
                                } else {
                                    MarkerOptions markerOptions = null;

                                    markerOptions = new MarkerOptions().position(new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.route_icon));

                                    markerDriverCar = mGoogleMap.addMarker(markerOptions);
                                }
                                showDistanceAndTimeView();
                                polylines = routeResponse.addRouteOnMap(mGoogleMap, false, Constants.Values.BeginTripRouteWidth);
                                if (markerDriverCar == null) {
                                    markerDriverCar = routeResponse.addStartLocation(mGoogleMap, controller.getLoggedDriver().getCategory_id(), new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude()));
                                }
                            }
                        }

                        @Override
                        public void onErrorSDRoute(VolleyError routeResponse) {

                        }
                    });
                } catch (Exception e) {

                }
            }*/
        }
    }

    private void animateCarOnMap(final List<Location> latLngs) {
        if (mGoogleMap != null && latLngs.size() > 0 && controller.getLoggedDriver() != null) {
            Log.d(TAG, "animateCarOnMap: latLngsSize: " + latLngs.size());
            if (markerDriverCarD == null) {
                markerDriverCarD = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latLngs.get(0).getLatitude(), latLngs.get(0).getLongitude()))
                        .flat(true)
                        .rotation(latLngs.get(0).getBearing())
                );

                if (controller.getLoggedDriver().getCategory_id() != null) {
                    if (controller.getLoggedDriver().getCategory_id().equals("4") || controller.getLoggedDriver().getCategory_id().equals("11"))
                        markerDriverCarD.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.bike_top_view_1));
                    else
                        markerDriverCarD.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_top_view_1));
                } else
                    markerDriverCarD.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_top_view_1));
                markerDriverCarD.setAnchor(0.5f, 0.5f);
            }
            markerDriverCarD.setPosition(new LatLng(latLngs.get(0).getLatitude(), latLngs.get(0).getLongitude()));
            markerDriverCarD.setRotation(latLngs.get(0).getBearing());
            if (latLngs.size() == 2) {
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(500);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        v = valueAnimator.getAnimatedFraction();
                        double lng = v * latLngs.get(1).getLongitude() + (1 - v)
                                * latLngs.get(0).getLongitude();
                        double lat = v * latLngs.get(1).getLatitude() + (1 - v)
                                * latLngs.get(0).getLatitude();
                        LatLng newPos = new LatLng(lat, lng);

                        if (markerDriverCarD != null) {
                            markerDriverCarD.setPosition(newPos);
                            markerDriverCarD.setAnchor(0.5f, 0.5f);
//                            markerDriverCarD.setRotation(getBearing(new LatLng(latLngs.get(0).getLatitude(), latLngs.get(0).getLongitude()) , newPos));
                            markerDriverCarD.setRotation(latLngs.get(1).getBearing());
                            if (isZoomEnabled || isZoomEnabledFirst) {
                                isZoomEnabledFirst = false;
                                mGoogleMap.animateCamera(
                                        CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                                .target(newPos)
                                                .bearing(latLngs.get(1).getBearing())
                                                .zoom(16).build()));
                            }
                        }

                    }
                });
                valueAnimator.start();
            }
        }
    }

    private void makeRouteAndRedrwaRouteD(Location locationNear, Location location) {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            if (tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
                clearCoveredRoute();
                LatLng latLngNear = new LatLng(locationNear.getLatitude(), locationNear.getLongitude());
                coverLatLongs.add(latLngNear);
                TrackRouteSaveData.getInstance(this).addLatLongD(latLngNear);
                TrackRouteSaveData.getInstance(getApplicationContext()).saveData();

                drawCoverRoutePolyLineOnMap();
            } else {
                coverLatLongs.clear();
            }
        } else {
            coverLatLongs.clear();
        }
    }

    private void clearCoveredRoute() {
        if (coverRoutePolyLine == null) {
            coverRoutePolyLine = new ArrayList<>();
        }
        for (Polyline polyline : coverRoutePolyLine) {
            polyline.remove();
        }
        coverRoutePolyLine.clear();
    }

    private String getcurrenttime() {
        DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        Calendar calobj = Calendar.getInstance();
        return df.format(calobj.getTime());
    }

    private void showDistanceAndTimeView() {
        try {
            customHandler.removeCallbacks(updateTimerThread, 0);
            customHandler.postDelayed(updateTimerThread, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getcategory();

    }

    private boolean getDriver_rejected() {
        return driver_rejected;
    }

    private boolean isCanMakeApiCall(Location preLocation, Location myCurrentLocation) {
        if (preLocation == null) {
            return true;
        }
        if (myCurrentLocation == null) {
            return true;
        }
        Log.d(TAG, "isCanMakeApiCall: " + preLocation.toString());
        Log.d(TAG, "isCanMakeApiCall: " + myCurrentLocation.toString());
        Log.d(TAG, "isCanMakeApiCall: distance: " + preLocation.distanceTo(myCurrentLocation));
        return preLocation.distanceTo(myCurrentLocation) > 10;
//        return true;
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    public class HandleItemClick implements AdapterView.OnItemClickListener {
        private EditText editText;

        public HandleItemClick(EditText editText) {

            this.editText = editText;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (predictions.getPlaces().get(position).getPlaceID() != null) {
                if (search_dropoff.getText().length() > 0) {
                    callPlaceDetailsApi(predictions.getPlaces().get(position).getPlaceID(), search_dropoff);
                } else {
                    Toast.makeText(SingleModeActivity.this, R.string.please_enter_source_first, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class MyLocationListener implements LocationListener {

        public void onLocationChanged(Location location) {
        }

        public void onStatusChanged(String s, int i, Bundle b) {

        }

        public void onProviderDisabled(String s) {

        }

        public void onProviderEnabled(String s) {

        }

    }

    private class LocationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != intent && intent.getAction().equals(LOACTION_ACTION)) {
                String locationData = intent.getStringExtra(LOCATION_MESSAGE);
                float c_lat = Float.parseFloat(intent.getStringExtra("c_lat"));
                float c_lng = Float.parseFloat(intent.getStringExtra("c_lng"));
                float c_bearing = Float.parseFloat(intent.getStringExtra("c_bearing"));
                boolean isFromBearing = intent.getBooleanExtra("isFromBearing", false);

//                myCurrentLocation = location;

                if (locationData != null && !locationData.equals("") && !locationData.equals("null")) {
                    distanceCoverInKm = Double.parseDouble(locationData) / 1000.0;
                    Log.e("distancecocverd", "" + distanceCoverInKm);
                    controller.pref.setCalculatedDistance(distanceCoverInKm);

                    if (Controller.isActivityVisible()) {
                        try {
                            if (mGoogleMap != null) {
                                TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                                int zoomValue = 16;
                                if (tripModel != null) {
                                    if (tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                                        zoomValue = 16;
                                        getTime();
                                        getcategory();
                                    }
                                }
//                                if (isZoomEnabled) {
//                                    LatLng target = new LatLng(c_lat, c_lng);
//                                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                                            .target(target)             // Sets the center of the map to current location
//                                            .zoom(zoomValue)
////                                    .bearing(location.getBearing()) // Sets the orientation of the camera to east
////                                    .tilt(0)                   // Sets the tilt of the camera to 0 degrees
//                                            .build();                   // Creates a CameraPosition from the builder
//                                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                                }


                                if (!isFromBearing) {
                                    if (latLngPublishRelay.size() <= 1) {
                                        if (latLngPublishRelay.size() == 0) {
                                            myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                            myLastCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                            latLngPublishRelay.add(myLastCurrentLocation);
                                            animateCarOnMap(latLngPublishRelay);
                                        } else {
                                            myLastCurrentLocation = setLocationData(latLngPublishRelay.get(0).getLatitude(), latLngPublishRelay.get(0).getLongitude(), latLngPublishRelay.get(0).getBearing());
                                            myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                            if (isCanMakeApiCall(myLastCurrentLocation, myCurrentLocation)) {
                                                latLngPublishRelay.add(myCurrentLocation);
                                                animateCarOnMap(latLngPublishRelay);
                                            }
                                        }
                                    } else {
                                        myLastCurrentLocation = setLocationData(latLngPublishRelay.get(1).getLatitude(), latLngPublishRelay.get(1).getLongitude(), latLngPublishRelay.get(1).getBearing());
                                        myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                        if (isCanMakeApiCall(myLastCurrentLocation, myCurrentLocation)) {
                                            latLngPublishRelay.set(0, latLngPublishRelay.get(1));
                                            latLngPublishRelay.set(1, myCurrentLocation);
                                            animateCarOnMap(latLngPublishRelay);
                                        }
                                    }
                                }

                                if (tripModel != null && tripModel.trip != null && tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
                                    checkOffRouteAndRedrwaRoute(myCurrentLocation);
                                } else {
                                    drawRouteWithoutDestination(myCurrentLocation);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                    if (!isFromBearing) {
                        if (latLngPublishRelay.size() <= 1) {
                            if (latLngPublishRelay.size() == 0) {
                                myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                myLastCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                latLngPublishRelay.add(myLastCurrentLocation);
                                animateCarOnMap(latLngPublishRelay);
                            } else {
                                myLastCurrentLocation = setLocationData(latLngPublishRelay.get(0).getLatitude(), latLngPublishRelay.get(0).getLongitude(), latLngPublishRelay.get(0).getBearing());
                                myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                if (isCanMakeApiCall(myLastCurrentLocation, myCurrentLocation)) {
                                    latLngPublishRelay.add(myCurrentLocation);
                                    animateCarOnMap(latLngPublishRelay);
                                }
                            }
                        } else {
                            myLastCurrentLocation = setLocationData(latLngPublishRelay.get(1).getLatitude(), latLngPublishRelay.get(1).getLongitude(), latLngPublishRelay.get(1).getBearing());
                            myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                            if (isCanMakeApiCall(myLastCurrentLocation, myCurrentLocation)) {
                                latLngPublishRelay.set(0, latLngPublishRelay.get(1));
                                latLngPublishRelay.set(1, myCurrentLocation);
                                animateCarOnMap(latLngPublishRelay);
                            }
                        }
                    }

//                    if (net_connection_check()) {
//                        LatLng target = new LatLng(location.getLatitude(), location.getLongitude());
//                        if (location.hasBearing() && mGoogleMap != null && isZoomEnabled) {
//                            CameraPosition cameraPosition = new CameraPosition.Builder()
//                                    .target(target)             // Sets the center of the map to current location
//                                    .zoom(16)
//                                    // .bearing(location.getBearing()) // Sets the orientation of the camera to east
////                            .tilt(0)                   // Sets the tilt of the camera to 0 degrees
//                                    .build();                   // Creates a CameraPosition from the builder
//                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                        }
//                    }
                }
            }
        }
    }
}