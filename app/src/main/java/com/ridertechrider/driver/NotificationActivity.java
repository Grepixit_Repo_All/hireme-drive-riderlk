package com.ridertechrider.driver;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.NotificationAdapter;
import com.ridertechrider.driver.adaptor.NotificationResponse;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationActivity extends AppCompatActivity {

    ArrayList<NotificationResponse> notificationResponses;
    private Controller controller;
    private RecyclerView recyclerView;
    private CustomProgressDialog progressDialog;
    private NotificationAdapter tripDetailsAdapter;
    private ImageButton noti_Back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        recyclerView = findViewById(R.id.not_rec);
        noti_Back = findViewById(R.id.noti_Back);
        controller = (Controller) getApplicationContext();
        progressDialog = new CustomProgressDialog(NotificationActivity.this);


        noti_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getNotification();
        setLocalizerString();
    }

    private void setLocalizerString() {
        TextView textView14;
        textView14 = findViewById(R.id.textView14);
        textView14.setText(Localizer.getLocalizerString("k_15_s4_a1_notifications"));
    }


    private void getNotification() {
        progressDialog.showDialog();
        HashMap<String, String> prams = new HashMap<>();
        prams.put("to_type", "driver");


        ServerApiCall.callWithApiKeyAndDriverId(this, prams, Constants.Urls.URL_USER_GET_NOTIFICATION, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {

                    try {
                        notificationResponses = new ArrayList<>();
                        JSONObject result = new JSONObject(data.toString());
                        String status = result.getString("status");
                        if (status.equalsIgnoreCase("OK")) {

                            JSONArray response = result.getJSONArray("response");
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject1 = response.getJSONObject(i);
                                NotificationResponse notificationResponse = new NotificationResponse();
                                notificationResponse.setId(jsonObject1.getString("id"));
                                notificationResponse.setFrom_type(jsonObject1.getString("from_type"));
                                notificationResponse.setTo_type(jsonObject1.getString("to_type"));
                                notificationResponse.setFrom_id(jsonObject1.getString("from_id"));
                                notificationResponse.setTo_id(jsonObject1.getString("to_id"));
                                notificationResponse.setRef_type(jsonObject1.getString("ref_type"));
                                notificationResponse.setRef_id(jsonObject1.getString("ref_id"));
                                notificationResponse.setRef_status(jsonObject1.getString("ref_status"));
                                notificationResponse.setTitle(jsonObject1.getString("title"));
                                notificationResponse.setUrl(jsonObject1.getString("url"));
                                notificationResponse.setMessage(jsonObject1.getString("message"));
                                notificationResponse.setStatus(jsonObject1.getString("status"));
                                notificationResponse.setCreated(jsonObject1.getString("created"));

                                notificationResponses.add(notificationResponse);
                            }

                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotificationActivity.this, LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            tripDetailsAdapter = new NotificationAdapter(notificationResponses, getApplicationContext(), NotificationActivity.this);
                            recyclerView.setAdapter(tripDetailsAdapter);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
