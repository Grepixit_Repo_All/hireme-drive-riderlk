package com.ridertechrider.driver.MapHelper.google_place_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GooglePlaceDetailsGeometry {

    @SerializedName("location")
    @Expose
    private GooglePlaceDetailsLocation location;
    @SerializedName("viewport")
    @Expose
    private GooglePlaceDetailsViewport viewport;

    public GooglePlaceDetailsLocation getLocation() {
        return location;
    }

    public void setLocation(GooglePlaceDetailsLocation location) {
        this.location = location;
    }

    public GooglePlaceDetailsViewport getViewport() {
        return viewport;
    }

    public void setViewport(GooglePlaceDetailsViewport viewport) {
        this.viewport = viewport;
    }

}
