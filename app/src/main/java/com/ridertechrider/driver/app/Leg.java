package com.ridertechrider.driver.app;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by grepixinfotech on 14/11/17.
 */

class Leg {
    final ArrayList<Step> steps = new ArrayList<>();

    int distanceValue;
    int durationValue;

    public Leg(JSONObject jsonObject) throws JSONException {
        LatLng startLocation = new LatLng(jsonObject.getJSONObject("start_location").getDouble("lat"), jsonObject.getJSONObject("start_location").getDouble("lng"));
        LatLng endLocation = new LatLng(jsonObject.getJSONObject("end_location").getDouble("lat"), jsonObject.getJSONObject("end_location").getDouble("lng"));
        String distanceText = jsonObject.getJSONObject("distance").getString("text");
        distanceValue = jsonObject.getJSONObject("distance").getInt("value");
        String durationText = jsonObject.getJSONObject("duration").getString("text");
        durationValue = jsonObject.getJSONObject("duration").getInt("value");

        JSONArray stepsJson = jsonObject.getJSONArray("steps");
        for (int i = 0; i < stepsJson.length(); i++) {
            steps.add(new Step(stepsJson.getJSONObject(i)));
        }
    }
}
