package com.ridertechrider.driver;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class NotificationDetails extends AppCompatActivity {

    private String title;
    private String url;
    private TextView title_not;
    private WebView not_details;
    private ImageButton noti_detail_Back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        url = intent.getStringExtra("url");

        title_not = findViewById(R.id.title_not);
        not_details = findViewById(R.id.not_details);
        noti_detail_Back = findViewById(R.id.noti_detail_Back);

        title_not.setText(title);

        noti_detail_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        not_details.getSettings().setLoadsImagesAutomatically(true);
        not_details.setWebViewClient(new WebViewClient());
        not_details.getSettings().setJavaScriptEnabled(true);
        not_details.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        not_details.loadUrl(url);
    }


}
