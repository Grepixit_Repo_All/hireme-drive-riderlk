package com.ridertechrider.driver.DeliverResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Driver {

    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("fire_id")
    @Expose
    private Object fireId;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("d_name")
    @Expose
    private String dName;
    @SerializedName("d_fname")
    @Expose
    private String dFname;
    @SerializedName("d_lname")
    @Expose
    private String dLname;
    @SerializedName("d_email")
    @Expose
    private String dEmail;
    @SerializedName("text_password")
    @Expose
    private String textPassword;
    @SerializedName("d_password")
    @Expose
    private String dPassword;
    @SerializedName("d_phone")
    @Expose
    private String dPhone;
    @SerializedName("d_street")
    @Expose
    private String dStreet;
    @SerializedName("d_address")
    @Expose
    private String dAddress;
    @SerializedName("d_address_2")
    @Expose
    private String dAddress2;
    @SerializedName("d_city")
    @Expose
    private String dCity;
    @SerializedName("d_state")
    @Expose
    private String dState;
    @SerializedName("d_country")
    @Expose
    private String dCountry;
    @SerializedName("d_zip")
    @Expose
    private String dZip;
    @SerializedName("skill")
    @Expose
    private String skill;
    @SerializedName("d_lat")
    @Expose
    private String dLat;
    @SerializedName("d_lng")
    @Expose
    private String dLng;
    @SerializedName("d_degree")
    @Expose
    private String dDegree;
    @SerializedName("d_age")
    @Expose
    private Object dAge;
    @SerializedName("d_sex")
    @Expose
    private String dSex;
    @SerializedName("d_lang")
    @Expose
    private String dLang;
    @SerializedName("d_miles")
    @Expose
    private String dMiles;
    @SerializedName("image_id")
    @Expose
    private Object imageId;
    @SerializedName("d_profile_image")
    @Expose
    private String dProfileImage;
    @SerializedName("d_profile_image_url")
    @Expose
    private String dProfileImageUrl;
    @SerializedName("d_license_id")
    @Expose
    private Object dLicenseId;
    @SerializedName("d_insurance_id")
    @Expose
    private String dInsuranceId;
    @SerializedName("d_rc")
    @Expose
    private Object dRc;
    @SerializedName("car_id")
    @Expose
    private String carId;
    @SerializedName("d_device_type")
    @Expose
    private String dDeviceType;
    @SerializedName("d_device_token")
    @Expose
    private String dDeviceToken;
    @SerializedName("d_rating")
    @Expose
    private String dRating;
    @SerializedName("d_rating_count")
    @Expose
    private String dRatingCount;
    @SerializedName("operation_hours")
    @Expose
    private String operationHours;
    @SerializedName("d_is_available")
    @Expose
    private String dIsAvailable;
    @SerializedName("d_is_verified")
    @Expose
    private String dIsVerified;
    @SerializedName("d_wallet")
    @Expose
    private String dWallet;
    @SerializedName("d_active")
    @Expose
    private String dActive;
    @SerializedName("d_created")
    @Expose
    private String dCreated;
    @SerializedName("d_modified")
    @Expose
    private String dModified;
    @SerializedName("d_profile_image_path")
    @Expose
    private Object dProfileImagePath;
    @SerializedName("d_license_image_path")
    @Expose
    private Object dLicenseImagePath;
    @SerializedName("d_insurance_image_path")
    @Expose
    private Object dInsuranceImagePath;
    @SerializedName("d_rc_image_path")
    @Expose
    private Object dRcImagePath;
    @SerializedName("car_name")
    @Expose
    private String carName;
    @SerializedName("car_desc")
    @Expose
    private String carDesc;
    @SerializedName("car_reg_no")
    @Expose
    private String carRegNo;
    @SerializedName("car_model")
    @Expose
    private String carModel;
    @SerializedName("car_make")
    @Expose
    private String carMake;
    @SerializedName("car_currency")
    @Expose
    private String carCurrency;
    @SerializedName("car_fare_per_km")
    @Expose
    private String carFarePerKm;
    @SerializedName("car_fare_per_min")
    @Expose
    private String carFarePerMin;
    @SerializedName("car_profile_image")
    @Expose
    private String carProfileImage;
    @SerializedName("car_profile_image_url")
    @Expose
    private String carProfileImageUrl;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("car_created")
    @Expose
    private String carCreated;
    @SerializedName("car_modified")
    @Expose
    private String carModified;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Object getFireId() {
        return fireId;
    }

    public void setFireId(Object fireId) {
        this.fireId = fireId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getDName() {
        return dName;
    }

    public void setDName(String dName) {
        this.dName = dName;
    }

    public String getDFname() {
        return dFname;
    }

    public void setDFname(String dFname) {
        this.dFname = dFname;
    }

    public String getDLname() {
        return dLname;
    }

    public void setDLname(String dLname) {
        this.dLname = dLname;
    }

    public String getDEmail() {
        return dEmail;
    }

    public void setDEmail(String dEmail) {
        this.dEmail = dEmail;
    }

    public String getTextPassword() {
        return textPassword;
    }

    public void setTextPassword(String textPassword) {
        this.textPassword = textPassword;
    }

    public String getDPassword() {
        return dPassword;
    }

    public void setDPassword(String dPassword) {
        this.dPassword = dPassword;
    }

    public String getDPhone() {
        return dPhone;
    }

    public void setDPhone(String dPhone) {
        this.dPhone = dPhone;
    }

    public String getDStreet() {
        return dStreet;
    }

    public void setDStreet(String dStreet) {
        this.dStreet = dStreet;
    }

    public String getDAddress() {
        return dAddress;
    }

    public void setDAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public String getDAddress2() {
        return dAddress2;
    }

    public void setDAddress2(String dAddress2) {
        this.dAddress2 = dAddress2;
    }

    public String getDCity() {
        return dCity;
    }

    public void setDCity(String dCity) {
        this.dCity = dCity;
    }

    public String getDState() {
        return dState;
    }

    public void setDState(String dState) {
        this.dState = dState;
    }

    public String getDCountry() {
        return dCountry;
    }

    public void setDCountry(String dCountry) {
        this.dCountry = dCountry;
    }

    public String getDZip() {
        return dZip;
    }

    public void setDZip(String dZip) {
        this.dZip = dZip;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getDLat() {
        return dLat;
    }

    public void setDLat(String dLat) {
        this.dLat = dLat;
    }

    public String getDLng() {
        return dLng;
    }

    public void setDLng(String dLng) {
        this.dLng = dLng;
    }

    public String getDDegree() {
        return dDegree;
    }

    public void setDDegree(String dDegree) {
        this.dDegree = dDegree;
    }

    public Object getDAge() {
        return dAge;
    }

    public void setDAge(Object dAge) {
        this.dAge = dAge;
    }

    public String getDSex() {
        return dSex;
    }

    public void setDSex(String dSex) {
        this.dSex = dSex;
    }

    public String getDLang() {
        return dLang;
    }

    public void setDLang(String dLang) {
        this.dLang = dLang;
    }

    public String getDMiles() {
        return dMiles;
    }

    public void setDMiles(String dMiles) {
        this.dMiles = dMiles;
    }

    public Object getImageId() {
        return imageId;
    }

    public void setImageId(Object imageId) {
        this.imageId = imageId;
    }

    public String getDProfileImage() {
        return dProfileImage;
    }

    public void setDProfileImage(String dProfileImage) {
        this.dProfileImage = dProfileImage;
    }

    public String getDProfileImageUrl() {
        return dProfileImageUrl;
    }

    public void setDProfileImageUrl(String dProfileImageUrl) {
        this.dProfileImageUrl = dProfileImageUrl;
    }

    public Object getDLicenseId() {
        return dLicenseId;
    }

    public void setDLicenseId(Object dLicenseId) {
        this.dLicenseId = dLicenseId;
    }

    public String getDInsuranceId() {
        return dInsuranceId;
    }

    public void setDInsuranceId(String dInsuranceId) {
        this.dInsuranceId = dInsuranceId;
    }

    public Object getDRc() {
        return dRc;
    }

    public void setDRc(Object dRc) {
        this.dRc = dRc;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getDDeviceType() {
        return dDeviceType;
    }

    public void setDDeviceType(String dDeviceType) {
        this.dDeviceType = dDeviceType;
    }

    public String getDDeviceToken() {
        return dDeviceToken;
    }

    public void setDDeviceToken(String dDeviceToken) {
        this.dDeviceToken = dDeviceToken;
    }

    public String getDRating() {
        return dRating;
    }

    public void setDRating(String dRating) {
        this.dRating = dRating;
    }

    public String getDRatingCount() {
        return dRatingCount;
    }

    public void setDRatingCount(String dRatingCount) {
        this.dRatingCount = dRatingCount;
    }

    public String getOperationHours() {
        return operationHours;
    }

    public void setOperationHours(String operationHours) {
        this.operationHours = operationHours;
    }

    public String getDIsAvailable() {
        return dIsAvailable;
    }

    public void setDIsAvailable(String dIsAvailable) {
        this.dIsAvailable = dIsAvailable;
    }

    public String getDIsVerified() {
        return dIsVerified;
    }

    public void setDIsVerified(String dIsVerified) {
        this.dIsVerified = dIsVerified;
    }

    public String getDWallet() {
        return dWallet;
    }

    public void setDWallet(String dWallet) {
        this.dWallet = dWallet;
    }

    public String getDActive() {
        return dActive;
    }

    public void setDActive(String dActive) {
        this.dActive = dActive;
    }

    public String getDCreated() {
        return dCreated;
    }

    public void setDCreated(String dCreated) {
        this.dCreated = dCreated;
    }

    public String getDModified() {
        return dModified;
    }

    public void setDModified(String dModified) {
        this.dModified = dModified;
    }

    public Object getDProfileImagePath() {
        return dProfileImagePath;
    }

    public void setDProfileImagePath(Object dProfileImagePath) {
        this.dProfileImagePath = dProfileImagePath;
    }

    public Object getDLicenseImagePath() {
        return dLicenseImagePath;
    }

    public void setDLicenseImagePath(Object dLicenseImagePath) {
        this.dLicenseImagePath = dLicenseImagePath;
    }

    public Object getDInsuranceImagePath() {
        return dInsuranceImagePath;
    }

    public void setDInsuranceImagePath(Object dInsuranceImagePath) {
        this.dInsuranceImagePath = dInsuranceImagePath;
    }

    public Object getDRcImagePath() {
        return dRcImagePath;
    }

    public void setDRcImagePath(Object dRcImagePath) {
        this.dRcImagePath = dRcImagePath;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarDesc() {
        return carDesc;
    }

    public void setCarDesc(String carDesc) {
        this.carDesc = carDesc;
    }

    public String getCarRegNo() {
        return carRegNo;
    }

    public void setCarRegNo(String carRegNo) {
        this.carRegNo = carRegNo;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public String getCarCurrency() {
        return carCurrency;
    }

    public void setCarCurrency(String carCurrency) {
        this.carCurrency = carCurrency;
    }

    public String getCarFarePerKm() {
        return carFarePerKm;
    }

    public void setCarFarePerKm(String carFarePerKm) {
        this.carFarePerKm = carFarePerKm;
    }

    public String getCarFarePerMin() {
        return carFarePerMin;
    }

    public void setCarFarePerMin(String carFarePerMin) {
        this.carFarePerMin = carFarePerMin;
    }

    public String getCarProfileImage() {
        return carProfileImage;
    }

    public void setCarProfileImage(String carProfileImage) {
        this.carProfileImage = carProfileImage;
    }

    public String getCarProfileImageUrl() {
        return carProfileImageUrl;
    }

    public void setCarProfileImageUrl(String carProfileImageUrl) {
        this.carProfileImageUrl = carProfileImageUrl;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCarCreated() {
        return carCreated;
    }

    public void setCarCreated(String carCreated) {
        this.carCreated = carCreated;
    }

    public String getCarModified() {
        return carModified;
    }

    public void setCarModified(String carModified) {
        this.carModified = carModified;
    }

}
