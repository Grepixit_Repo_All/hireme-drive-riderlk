package com.ridertechrider.driver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.RSRTripListAdaptor;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.DriverConstants;
import com.grepix.grepixutils.WebServiceUtil;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RideSharingRequestFragment extends Fragment {

    private static RideSharingRequestFragment requestFragment;
    private final int limit = 10;
    private final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    public TextView listEmptyTxt;
    private ListView listView;
    private Activity activity;
    private RSRTripListAdaptor adaptor;
    private int offSet = 0;
    private ArrayList<TripModel> tripHistoryList;
    private Controller controller;
    private RequestFragment.ClickListenerCallback mCallback;
    private SwipeRefreshLayout pullToRefresh;
    private BroadcastReceiver requestPageChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int page = intent.getIntExtra("page", -1);
            if (page == 1) {
                refresh(false);
            }
        }
    };

    public static RideSharingRequestFragment getInstance() {

        if (requestFragment == null) {
            requestFragment = new RideSharingRequestFragment();
        }
        return requestFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        repeatTimerManager.startRepeatCall();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        repeatTimerManager.stopRepeatCall();
//    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_layout, container, false);
        pullToRefresh = view.findViewById(R.id.pullToRefresh);
        listEmptyTxt = view.findViewById(R.id.list_empty_txt);
        listView = view.findViewById(R.id.req_listView);
        // listView.setPullLoadEnable(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), Fonts.KARLA);
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        tripHistoryList = new ArrayList<>();
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        CategoryActors driverCat = null;
        for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
            if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                driverCat = categoryActors;
            }
        }
        adaptor = new RSRTripListAdaptor(activity, tripHistoryList, typeface, mCallback, driverCat, this);
        // listView.setXListViewListener(this);
        // listView.setPullLoadEnable(false);
        //setting an setOnRefreshListener on the SwipeDownLayout
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                adaptor.clear();
                refresh(false);
            }
        });
        listView.setAdapter(adaptor);
        controller = (Controller) getActivity().getApplicationContext();

        refresh(false);
    }

    public void setCallback(RequestFragment.ClickListenerCallback mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (RequestFragment.ClickListenerCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ClickListenerCallback");
        }

        Controller.getInstance().registerReceiver(requestPageChangedReceiver, new IntentFilter("refresh_request_list1"));
    }

    public void refresh(final boolean isLoadMore) {
        if (!isLoadMore) {
            offSet = 0;
            if (adaptor != null)
                adaptor.clear();
        }
        if (Controller.isActivityVisible() && controller != null) {
            tripHistoryList.clear();
            if (controller.getLoggedDriver() != null) {
                String availability = controller.getLoggedDriver().getD_is_available();
                if ((availability.equalsIgnoreCase("1") || availability.equalsIgnoreCase("2"))
                        && controller.getLoggedDriver().getD_is_verified())
                    getTripRequestList(offSet, isLoadMore);
                else cantMakeRequest();
            } else {
                cantMakeRequest();
            }
        }
        if (pullToRefresh != null)
            pullToRefresh.setRefreshing(false);
    }

    private void cantMakeRequest() {
        Controller.getInstance().stopNotificationSound();
        if (adaptor != null) {
            CategoryActors driverCat = null;
            for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                    driverCat = categoryActors;
                }
            }
            adaptor.setDriverCat(driverCat);
            adaptor.notifyDataSetChanged();
        }
        listEmptyTxt.setVisibility(View.GONE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Controller.getInstance().unregisterReceiver(requestPageChangedReceiver);
    }


    public void getTripRequestList(final int nextoffset, final boolean isLoadMore) {
        if (getActivity() != null) {
            if (!((SlideMainActivity) getActivity()).hasLocationPermission)
                return;

            Controller controller = (Controller) (getActivity()).getApplication();
            if (controller.getConstantsList() != null && controller.getConstantsList().size() > 0) {
                String categoryId = controller.getLoggedDriver().getCategory_id();
                String d_lat = controller.pref.getDriver_Lat();
                String d_lng = controller.pref.getDriver_Lng();


                if (d_lat != null && Double.valueOf(d_lat) != 0 && d_lng != null && Double.valueOf(d_lng) != 0) {

                    controller.stopNotificationSound();
                    String radius = "";
                    for (DriverConstants driverConstants : controller.getConstantsList()) {
                        if (driverConstants.getConstant_key().equalsIgnoreCase("driver_radius")) {
                            radius = driverConstants.getConstant_value();
                        }
                    }
                    Call<ResponseBody> request = apiInterface.getRevisedTrips(controller.getLoggedDriver().getApiKey(), categoryId, "1", d_lat, d_lng, radius, "request", String.valueOf(nextoffset), String.valueOf(limit), controller.getLoggedDriver().getDriverId(), "1");
                    controller.setRequestTripResuest(request);
                    request.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                try {
                                    String string = response.body().string();
                                    //listView.stopLoadMore();
                                    //listView.stopRefresh();
                                    handleHistoryResponse(string);

                                } catch (IOException e) {
                                    e.printStackTrace();

                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            if (call.isCanceled()) {
                                listView.setVisibility(View.GONE);
                                listEmptyTxt.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            } else {
                getConstantApi(nextoffset, isLoadMore);
            }
        }
    }

    private void getConstantApi(final int nextoffset, final boolean isLoadMore) {
        ServerApiCall.callWithApiKey(getActivity(), null, Constants.Urls.GET_CONSTANTS_API, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

                if (isUpdate) {
                    String response = data.toString();
                    controller.setConstantsList(response);
                    getTripRequestList(nextoffset, isLoadMore);
                }
            }
        });

    }

    private void handleHistoryResponse(String s) {
        try {
            JSONObject jsonRootObject = new JSONObject(s);
            if (jsonRootObject.get(Constants.Keys.RESPONSE) instanceof JSONArray) {
                JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
                tripHistoryList.clear();

                for (int i = 0; i < response.length(); i++) {
                    JSONObject childObject = response.getJSONObject(i);
                    TripModel tripModel = TripModel.parseJson(childObject.toString());
                    tripHistoryList.add(tripModel);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (tripHistoryList.size() == 0) {
            listView.setVisibility(View.GONE);
            listEmptyTxt.setVisibility(View.VISIBLE);
            listEmptyTxt.setText(Localizer.getLocalizerString("k_52_s4_waiting_new_ride_req"));

        } else {
            listView.setVisibility(View.VISIBLE);
            listEmptyTxt.setVisibility(View.GONE);
        }
        try {
            if (adaptor != null) {
                CategoryActors driverCat = null;
                for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                    if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                        driverCat = categoryActors;
                    }
                }
                adaptor.setDriverCat(driverCat);
                adaptor.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

