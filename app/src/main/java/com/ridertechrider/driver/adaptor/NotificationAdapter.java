package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.ridertechrider.driver.NotificationActivity;
import com.ridertechrider.driver.NotificationDetails;
import com.ridertechrider.driver.R;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    Context context;
    NotificationActivity notificationActivity;
    private ArrayList<NotificationResponse> notificationResponses;

    public NotificationAdapter(ArrayList<NotificationResponse> notificationResponses, Context context, NotificationActivity tri) {
        this.notificationResponses = notificationResponses;
        this.context = context;
        this.notificationActivity = tri;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.MyViewHolder holder, final int position) {

        holder.heading.setText(notificationResponses.get(position).getTitle());

        holder.desc.setText(notificationResponses.get(position).getMessage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notificationResponses.get(position).getUrl() != null && !notificationResponses.get(position).getUrl().equalsIgnoreCase("")) {
                    Intent intent = new Intent(context, NotificationDetails.class);
                    intent.putExtra("title", notificationResponses.get(position).getTitle());
                    intent.putExtra("url", notificationResponses.get(position).getUrl());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, R.string.no_details_available, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationResponses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView heading, desc;

        public MyViewHolder(View itemView) {
            super(itemView);
            heading = itemView.findViewById(R.id.heading);
            desc = itemView.findViewById(R.id.desc);
        }
    }
}
