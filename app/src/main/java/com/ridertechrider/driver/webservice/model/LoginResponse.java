package com.ridertechrider.driver.webservice.model;

import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.webservice.Constants;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends BaseResponse {
    /*@SerializedName(Constants.Keys.RESPONSE)
    private Driver driver;

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Driver  getDriver() {
        return driver;
    }

*/
    @SerializedName(Constants.Keys.RESPONSE)
    private Driver driver;

    public Driver getDriver() {
        return driver;
    }


}
