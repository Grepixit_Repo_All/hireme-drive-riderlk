package com.ridertechrider.driver.utils;

import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.GetPriceBeanClass;
import com.ridertechrider.driver.GetPriceJson;
import com.ridertechrider.driver.webservice.CategoryActors;

import java.util.Date;

public class TripFareCalculator {
    private final Controller controller;
    private final Double distanceCoverInKm;
    private final String jsonSpecial;
    private double minutesTotal = 1;

    public TripFareCalculator(Controller controller, Double distanceCoverInKm, String jsonSpecial) {
        this.controller = controller;
        this.distanceCoverInKm = distanceCoverInKm;
        this.jsonSpecial = jsonSpecial;
    }

    public TripFare calculateFare() {
        return calculateFare((int) 0);
    }

    public TripFare calculateFare(int waitingTime) {
        try {
            double minutes = 1;
            Date tripStartDate = AppUtil.convertStringDateToAppTripDate(controller.pref.getTripStartTime());
            Date tripEndDate = new Date();
            if (tripStartDate != null) {
                minutes = AppUtil.getTimeBetweenTwoDateDifference(tripStartDate, tripEndDate);
                if ((minutes - (int) minutes) > 0.0) {
                    minutes = (int) minutes + 1;
                }
            }
            minutesTotal = minutes;
            if (waitingTime > 0 && waitingTime < minutes)
                minutes = minutes - waitingTime;

            String categoryId = controller.getLoggedDriver().getCategory_id();
            CategoryActors driverCat = null;
            for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                if (categoryActors.getCategory_id().equalsIgnoreCase(categoryId)) {
                    driverCat = categoryActors;
                    break;
                }
            }

            TripFare tripFare = new TripFare();
            if (driverCat != null) {

                double distanceUnit = controller.convertDistanceKmToUnit(distanceCoverInKm);

                double distance = 0;
                double basePrice = Double.parseDouble(driverCat.getCat_base_price());
                double baseIncludes = Double.parseDouble(driverCat.getCat_base_includes());
                if (distanceUnit > baseIncludes) {
                    distance = distanceUnit - baseIncludes;
                }
                double kmPrice = distance * Double.parseDouble(driverCat.getCat_fare_per_km());
                double minutesPrice = ((int) minutes * Double.parseDouble(driverCat.getCat_fare_per_min()));
                double totalPrice = (basePrice + kmPrice + minutesPrice);

                double waitFare = 0;
                if (waitingTime > 0)
                    waitFare = (driverCat.getWait_per_min() * waitingTime);

                totalPrice = waitFare + totalPrice;

                double serviceTaxPercent = controller.getCityTax();

                double tripTax = totalPrice * serviceTaxPercent / 100.0f;
                tripFare.tripTax = tripTax;
                totalPrice = totalPrice + tripTax + calculateFare(totalPrice);
                if (driverCat.getCat_is_fixed_price().equals("0")) {
                    double primepercentage = (totalPrice * Double.parseDouble(driverCat.getCat_prime_time_percentage())) / 100.0;
                    tripFare.totalPrice = totalPrice + primepercentage;
                } else {
                    tripFare.totalPrice = totalPrice;
                }
            }
            return tripFare;
        } catch (Exception e) {
            e.printStackTrace();
            return new TripFare();
        }
    }

    private double calculateFare(double totalPrice) {

        GetPriceJson getPriceJson = new GetPriceJson(controller, "8", jsonSpecial);
        getPriceJson.parseJsonFile();
        GetPriceBeanClass price = getPriceJson.getPrice(new Date());
        double total = 0;
        if (price != null) {
            if (price.getPrice_per_km() != null) {
                float f1 = Float.parseFloat(price.getPrice_per_km());
                total = ((totalPrice * f1) / 100);
                return total;

            } else {
                return total;
            }
        } else {
            return total;
        }

    }

    public double getTotalMinutes() {
        return minutesTotal;
    }

    public class TripFare {
        private double tripTax;
        private double totalPrice;

/*
        public void setTripTax(double tripTax) {
            this.tripTax = tripTax;
        }
*/

        public double getTripTax() {
            return tripTax;
        }


        /* public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
        }*/

        public double getTotalPrice() {
            return totalPrice;
        }
    }
}
