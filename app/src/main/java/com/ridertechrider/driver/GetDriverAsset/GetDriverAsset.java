package com.ridertechrider.driver.GetDriverAsset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDriverAsset {

@SerializedName("status")
@Expose
private String status;
@SerializedName("code")
@Expose
private Integer code;
@SerializedName("message")
@Expose
private Object message;
@SerializedName("next_offset")
@Expose
private Object nextOffset;
@SerializedName("last_offset")
@Expose
private Object lastOffset;
@SerializedName("response")
@Expose
private List<GetDriverAssetResponse> getDriverAssetResponse = null;


public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

public Object getMessage() {
return message;
}

public void setMessage(Object message) {
this.message = message;
}

public Object getNextOffset() {
return nextOffset;
}

public void setNextOffset(Object nextOffset) {
this.nextOffset = nextOffset;
}

public Object getLastOffset() {
return lastOffset;
}

public void setLastOffset(Object lastOffset) {
this.lastOffset = lastOffset;
}

public List<GetDriverAssetResponse> getGetDriverAssetResponse() {
return getDriverAssetResponse;
}

public void setGetDriverAssetResponse(List<GetDriverAssetResponse> getDriverAssetResponse) {
this.getDriverAssetResponse = getDriverAssetResponse;
}




}