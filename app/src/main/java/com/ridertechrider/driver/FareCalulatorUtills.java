package com.ridertechrider.driver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

class FareCalulatorUtills {

    private static boolean isBetweenTwowDate(Date endJobTime, Date startJobTime, String sD, String eD) {
        Date sDate = dateToString(sD);
        Date eDate = dateToString(eD);
        if (eDate.before(sDate)) {
            eDate = new Date(eDate.getTime() + 24 * 60 * 60 * 1000);
        }
        endJobTime = convertDateToTime(endJobTime);
        startJobTime = convertDateToTime(startJobTime);
        if (Objects.requireNonNull(endJobTime).after(sDate) && endJobTime.before(eDate)) {
            return true;
        }
        return Objects.requireNonNull(startJobTime).after(sDate) && startJobTime.before(eDate);
    }

    private static Date convertDateToTime(Date date) {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        String format = simpleDateFormat.format(date);
        try {
            return simpleDateFormat.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Date dateToString(String s) {
        if (s != null && s.length() > 0) {
            s = s.trim();
            SimpleDateFormat simpleDateFormat;
            simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
            try {
                return simpleDateFormat.parse(s);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static boolean canApplyNightCharge(Controller controller, Date endJobTime, Date startDate) {
        String startTime = controller.getConstantsValueForKey("night_starts_at");
        String endTime = controller.getConstantsValueForKey("night_ends_at");
        // if (categoryActors.getCategory_id().equalsIgnoreCase("1")  ) {//||categoryActors.getParentId().equalsIgnoreCase("1")
        return isBetweenTwowDate(endJobTime, startDate, startTime, endTime);
        // }
        // return false;
    }

}
