package com.ridertechrider.driver;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.ridertechrider.driver.GetDriverAsset.GetDriverAsset;
import com.ridertechrider.driver.Model.Agency;
import com.ridertechrider.driver.Model.City;
import com.ridertechrider.driver.adaptor.AppController;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.service.LocationService;
import com.ridertechrider.driver.service.PreferencesUtils;
import com.ridertechrider.driver.service.TrackRecordingServiceConnectionUtils;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.DriverConstants;
import com.ridertechrider.driver.webservice.DriverSettings;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.gson.Gson;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

import static com.ridertechrider.driver.webservice.Constants.Values.USE_EMAIL_AUTH;

public class Controller extends AppController implements Application.ActivityLifecycleCallbacks {

    private static final String TAG = Controller.class.getSimpleName();
    private static Controller mInstance;
    private static boolean isActive;
    private static Map<String, Double> currencyRates = new HashMap<>();
    public Pref pref;
    public boolean isFromBackground = false;
    public boolean isUpdaingLocation = false;
    public boolean isRequestRejected = false;
    private String currencyJSON = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private String tripDistance;
    private String tripDuration;
    private boolean isDocUpdate;
    private String fareReviewCalled;
    private boolean isNotificationCome;
    //    private TripModel currentTripModel;
    private boolean isShowYesNowDialog;
    private MediaPlayer notificationSound;
    private boolean isAlreadyPlay = false;
    private ArrayList<CategoryActors> categoryResponseList;
    private Call<ResponseBody> requestTripResuest;
    private Call<ResponseBody> requestTripResuestAssigned;
    private double d_lat, d_lng;
    private Location driverLocation;
    private ArrayList<DriverConstants> constantsList;
    private ArrayList<City> cities;
    private ArrayList<Agency> agencies;
    private GetDriverAsset getDriverAsset;
    private ArrayList<DriverSettings> settingsList;

    public ArrayList<DriverSettings> getSettingsList() {
        if (settingsList == null || settingsList.size() == 0) {
            if (pref.getSettingsResponse() != null) {
                String response = pref.getSettingsResponse();
                this.settingsList = DriverSettings.parseDriverSettingsReponse(response);
            }
            if (settingsList == null)
                settingsList = new ArrayList<>();
        }
        return settingsList;
    }

    public static synchronized Controller getInstance() {
        return mInstance;
    }

    public static boolean isActivityVisible() {
        return isActive;
    }

    private static String getCurrencyJSONFromRaw(Context context, int rawResId) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().openRawResource(rawResId);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception ignore) {
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception ignore) {
            }
        }
        return returnString.toString();
    }

    public static String getSaveDiceToken() {
        SharedPreferences pref = Controller.getInstance().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("regId", null);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = new Pref(this);
        mInstance = this;

        Utils.applyAppLanguage(getApplicationContext());
        // MultiDex.install(this);
        registerActivityLifecycleCallbacks(this);

        constantsList = getConstantsList();

        String login_response = pref.getString("login_response");
        Log.d(TAG, "onCreate: login_response: " + login_response);
        if (login_response != null) {
            Driver driver = new Gson().fromJson(login_response, Driver.class);
            AppData.getInstance(getApplicationContext()).setDriver(driver);
            setDriver(driver, this);
        }
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm != null) {
            PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp::MyWakelockTag");
            mWakeLock.acquire();
        }

        getCityApi();
        getAgencyApi();


        setupCurrencyRates();
    }

    public Location getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(Location driverLocation) {
        this.driverLocation = driverLocation;
    }

    public double getD_lat() {
        return d_lat;
    }

    public void setD_lat(double d_lat) {
        this.d_lat = d_lat;
    }

    public double getD_lng() {
        return d_lng;
    }

    public void setD_lng(double d_lng) {
        this.d_lng = d_lng;
    }

    public Call<ResponseBody> getRequestTripResuest() {
        return requestTripResuest;
    }

    public void setRequestTripResuest(Call<ResponseBody> requestTripResuest) {
        this.requestTripResuest = requestTripResuest;
    }

    public Call<ResponseBody> getRequestTripResuestAssigned() {
        return requestTripResuestAssigned;
    }

    public void setRequestTripResuestAssigend(Call<ResponseBody> requestTripResuest) {
        this.requestTripResuestAssigned = requestTripResuest;
    }

    public boolean isShowYesNowDialog() {
        return isShowYesNowDialog;
    }

    public void setShowYesNowDialog(boolean showYesNowDialog) {
        isShowYesNowDialog = showYesNowDialog;
    }

    public void playNotificationSound(int rawId) {
        //if (!isAlreadyPlay) {
        try {
            if (notificationSound.isPlaying()) {
                notificationSound.setLooping(false);
                stopNotificationSound();
                playSoundNow(rawId);
            } else {
                playSoundNow(rawId);
            }
        } catch (Exception e) {
            playSoundNow(rawId);
        }
        // }


    }

    private void playSoundNow(int rawId) {
        notificationSound = MediaPlayer.create(this, rawId);
        notificationSound.start();
        isAlreadyPlay = true;
    }

    public void stopNotificationSound() {
        try {
            if (notificationSound != null && notificationSound.isPlaying()) {
                notificationSound.stop();
                notificationSound.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isAlreadyPlay = false;

    }

    public boolean isDocUpdate() {
        return isDocUpdate;
    }

    public void setDocUpdate(boolean docUpdate) {
        isDocUpdate = docUpdate;
    }

    public String checkCityDistanceUnit() {
        String distanceUnit = "km";
        String city_id = getLoggedDriver() != null ? getLoggedDriver().getCity_id() : "";
        if (city_id != null && !city_id.equalsIgnoreCase("")) {
            for (City city : getCities()) {
                if (city.getCity_id() != null && city.getCity_id().equalsIgnoreCase(city_id)) {
                    if (city.getCity_dist_unit() != null)
                        distanceUnit = city.getCity_dist_unit();
                    break;
                }
            }
        }
        return distanceUnit;
    }

    public String currencyUnit() {
        String currency = "";//getConstantsValueForKeyEmpty("currency");
        if (getLoggedDriver() != null)
            for (City city : getCities()) {
                if (city.getCity_id() != null && city.getCity_id().equalsIgnoreCase(getLoggedDriver().getCity_id())) {
                    if (city.getCity_cur() != null)
                        currency = city.getCity_cur();
                    break;
                }
            }
        return currency;
    }

    public double getCityTax() {
        double tax = 0.0;
        if (getLoggedDriver() != null) {
            if (getCities() != null && cities.size() > 0) {
                for (City city : getCities()) {
                    if (city.getCity_id() != null && city.getCity_id().equalsIgnoreCase(getLoggedDriver().getCity_id())) {
                        if (city.getCity_tax() != null)
                            tax = Double.parseDouble(city.getCity_tax());
                        break;
                    }
                }
            }
//            else {
//                for (DriverConstants driverConstants : getConstantsList()) {
//                    if (driverConstants.getConstant_key() != null && driverConstants.getConstant_key().equalsIgnoreCase("service_tax")) {
//                        tax = Double.parseDouble(driverConstants.getConstant_value());
//                    }
//                }
//            }
        }
        return tax;
    }

    public String deliveryStatus() {
        String rideShare = "0";
        if (getConstantsList() != null && getConstantsList().size() != 0) {
            for (DriverConstants driverConstants : getConstantsList()) {
                if (driverConstants.getConstant_key().equalsIgnoreCase("enable_delivery")) {
                    rideShare = driverConstants.getConstant_value();
                }
            }
        }
        return rideShare;
    }

    public String checkPrivacyPolicy() {
        String pp = "0";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("enable_pp")) {
                pp = driverConstants.getConstant_value();
                break;
            }

        }
        return pp;
    }

    public String checkAboutUs() {
        String aboutUs = "0";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("enable_aboutus")) {
                aboutUs = driverConstants.getConstant_value();
                break;
            }

        }
        return aboutUs;
    }

    public String checkWallet() {
        String aboutUs = "0";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("enable_wallet")) {
                aboutUs = driverConstants.getConstant_value();
                break;
            }

        }
        return aboutUs;
    }

    public String checkSingleMode() {
        String aboutUs = "0";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("single_ride")) {
                aboutUs = driverConstants.getConstant_value();
                break;
            }

        }
        return aboutUs;
    }

    public String checkOtpStart() {
        String aboutUs = "0";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("otp_start")) {
                aboutUs = driverConstants.getConstant_value();
                break;
            }

        }
        return aboutUs;
    }

    public String checkOtpEnd() {
        String aboutUs = "0";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("otp_end")) {
                aboutUs = driverConstants.getConstant_value();
                break;
            }

        }
        return aboutUs;
    }

    public String getAboutUsData() {
        String aboutUs = "";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("enable_aboutus")) {
                aboutUs = driverConstants.getData();
                break;
            }

        }
        return aboutUs;
    }

    public String getSupportEmail() {
        String aboutUs = "";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("support_email")) {
                aboutUs = driverConstants.getData();
            }

        }
        return aboutUs;
    }

    public String getSupportEmailValue() {
        String aboutUs = "";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("support_email")) {
                aboutUs = driverConstants.getConstant_value();
            }

        }
        return aboutUs;
    }

    public String getShareText() {
        String aboutUs = "";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("share_text_driver")) {
                aboutUs = driverConstants.getConstant_value();
                break;
            }

        }
        return aboutUs;
    }

    public String getPrivacyPolicyData() {
        String privacyPolicy = "";
        for (DriverConstants driverConstants : getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("enable_pp")) {
                privacyPolicy = driverConstants.getData();
                break;
            }

        }
        return privacyPolicy;
    }

    public String formatMeterToDistanceWithUnit(double distanceInMeter) {
//        return distanceUnit.equalsIgnoreCase("km") ? String.format(Locale.ENGLISH, "%.01f km", distanceInMeter / 1000.0) : String.format(Locale.ENGLISH, "%.01f mi", distanceInMeter * METER_TO_MILE);
        return String.format(Locale.ENGLISH, "%.01f %s", distanceInMeter, checkCityDistanceUnit());
    }

    /**
     * @param distance this is distance double value of distance of trip
     * @return formatted string with the unit  km / mi etc according to admin server
     */

    private String formatDistanceWithUnit(double distance) {
        return String.format(Locale.ENGLISH, "%.02f %s", (float) distance, checkCityDistanceUnit());
    }

    /**
     * @param distanceStringValue this is distance double string value of distance of trip
     * @return formatted string with the unit  km / mi etc according to admin server
     */
    public String formatDistanceWithUnit(String distanceStringValue) {
        if (distanceStringValue == null || distanceStringValue.equalsIgnoreCase("null")) {
            return formatDistanceWithUnit(0);
        }
        try {
            return formatDistanceWithUnit(Float.parseFloat(distanceStringValue.trim()));
        } catch (Exception e) {
            return formatDistanceWithUnit(0);
        }
    }

    public String formatAmountWithCurrencyUnit(String amount) {
        if (amount == null || amount.equalsIgnoreCase("null")) {
            return formatAmountWithCurrencyUnit(0);
        }
        try {
            return formatAmountWithCurrencyUnit(Float.parseFloat(amount.trim()));
        } catch (Exception Ignored) {
            return amount;
        }
    }

    public String formatAmountWithCurrencyUnit(float amount) {
//        amount = (float) getPriceAfterCurrencyRateApplied(amount);
        return String.format(Locale.ENGLISH, currencyUnit() + "%.02f", amount);
    }

    public String formatAmountWithCurrencyUnit(String amount, String negPlus) {
        if (amount == null || amount.equalsIgnoreCase("null")) {
            return formatAmountWithCurrencyUnit(0, negPlus);
        }
        return formatAmountWithCurrencyUnit(Float.parseFloat(amount.trim()), negPlus);
    }

    public String formatAmountWithCurrencyUnit(float amount, String negPlus) {
//        amount = (float) getPriceAfterCurrencyRateApplied(amount);
        return String.format(Locale.ENGLISH, negPlus + currencyUnit() + "%.02f", amount);
    }

    private void setupCurrencyRates() {
        //        currencyJSON = getConstantsValueForKeyEmpty("currency_conversion");
        currencyJSON = getCurrencyJSONFromRaw(this, R.raw.currency_rate);
        if (!currencyJSON.equals("") && !currencyJSON.equals("null")) {
            try {
                JSONObject object = new JSONObject(currencyJSON);
                JSONArray objArray = object.names();
                for (int i = 0; i < objArray.length(); i++) {
                    if (!object.isNull(objArray.getString(i))) {
                        currencyRates.put(objArray.getString(i), object.getDouble(objArray.getString(i)));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "setupCurrencyRates: currencyJSON: " + currencyJSON);
        Log.d(TAG, "setupCurrencyRates: currencyRates: " + currencyRates);
    }

    public double getPriceAfterCurrencyRateApplied(double amount) {
        String myCurrency = getMyCityCurrency();
        String baseCurrency = getConstantsValueForKeyEmpty("currency");

        if (currencyRates.containsKey(baseCurrency + myCurrency)) {
            amount = amount * currencyRates.get(baseCurrency + myCurrency);
        }
        return amount;
    }

    public void getCityApi() {
        WebServiceUtil.excuteRequest(this, null, Constants.Urls.URL_GET_CITIES, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    pref.setCitiesResponse(response);
                    cities = City.parseCities(response);

                } else {
                    if (error instanceof ServerError) {
                        String s = new String(error.networkResponse.data);
                        try {
                            JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }


//    public TripModel getCurrentTripModel() {
//        return currentTripModel;
//    }

//    public void setCurrentTripModel(TripModel currentTripModel) {
//        this.currentTripModel = currentTripModel;
//        this.pref.saveTripData(currentTripModel);
//    }

    public void getAgencyApi() {
        Map<String, String> params = new HashMap<>();
//        params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey());
        WebServiceUtil.excuteRequest(this, params, Constants.Urls.URL_GET_AGENCIES, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
                if (isUpdate) {
                    String response = data.toString();
                    agencies = Agency.parseAgencies(response);
                    agencies.add(0, new Agency(getString(R.string.select)));
                    pref.setAgenciesResponse(response);
                } else {
                    if (error instanceof ServerError) {
                        String s = new String(error.networkResponse.data);
                        try {
                            JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public String getConstantsValueForKey(String key) {
        ArrayList<DriverConstants> constantsList = getConstantsList();
        if (constantsList != null && constantsList.size() != 0) {
            for (DriverConstants driverConstants : constantsList) {
                if (driverConstants.getConstant_key().equalsIgnoreCase(key)) {
                    return driverConstants.getConstant_value();
                }
            }
        }
        return "0";
    }

    public String getConstantsValueForKeyEmpty(String key) {
        ArrayList<DriverConstants> constantsList = getConstantsList();
        if (constantsList != null && constantsList.size() != 0) {
            for (DriverConstants driverConstants : constantsList) {
                if (driverConstants.getConstant_key().equalsIgnoreCase(key)) {
                    return driverConstants.getConstant_value();
                }
            }
        }
        return "";
    }



   /* public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }*/

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    protected RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public ArrayList<DriverConstants> getConstantsList() {
        if (constantsList == null || constantsList.size() == 0) {
            if (pref.getConstantResponse() != null) {
                String response = pref.getConstantResponse();
                this.constantsList = DriverConstants.parseDriverConstantsReponse(response);
            }
            if (constantsList == null)
                constantsList = new ArrayList<>();
        }
        return constantsList;
    }

    public void setConstantsList(String response) {

        pref.setConstantResponse(response);
        this.constantsList = DriverConstants.parseDriverConstantsReponse(response);
        setupCurrencyRates();
    }

    public void setDriver(Driver driver, Context context) {
        Log.d(TAG, "setDriver: driverisnull: " + (driver == null));

        if (driver == null) {
            String login_response = pref.getString("login_response");
            Log.d(TAG, "setDriver: login_response: " + login_response);
            if (login_response != null) {
                driver = new Gson().fromJson(login_response, Driver.class);
                AppData.getInstance(context).setDriver(driver);
                String trip_response = pref.getString("trip_response");
                if (trip_response != null) {
                    Log.e("saveTripResponse", "" + trip_response);
                    TripModel tripModel = new Gson().fromJson(trip_response, TripModel.class);
                    AppData.getInstance(context).setTripModel(tripModel);
                }
            }
        }
        AppData.getInstance(getApplicationContext()).setDriver(driver);
    }

    public ArrayList<City> getCities() {
        if (cities == null || cities.size() == 0) {
            if (pref.getCitiesResponse() != null) {
                String response = pref.getCitiesResponse();
                this.cities = City.parseCities(response);
            }
            if (cities == null)
                cities = new ArrayList<>();
        }
        return cities;
    }

    public ArrayList<Agency> getAgencies() {
        if (pref.getCitiesResponse() != null) {
            String response = pref.getAgenciesResponse();
            this.agencies = Agency.parseAgencies(response);
        }
        if (agencies == null)
            agencies = new ArrayList<>();
        return agencies;
    }

    public String getCityCommission(String city_id) {
        String distanceUnit = getConstantsValueForKeyEmpty("appicial_commission");

        if (city_id != null && !city_id.equalsIgnoreCase("")) {
            for (City city : getCities()) {
                if (city.getCity_id() != null && city.getCity_id().equalsIgnoreCase(city_id)) {
                    if (city.getCity_comm() != null)
                        distanceUnit = city.getCity_comm();
                    break;
                }
            }
        }
        if (distanceUnit.equals(""))
            return "0";
        return distanceUnit;
    }

    public String getCityName(String cityId) {
        for (City city : getCities()) {
            if (city.getCity_id() != null && cityId != null && city.getCity_id().equals(cityId))
                return city.getCity_name();
        }
        return "";
    }

    public String getAgencyName(String user_id) {
        for (Agency ag : getAgencies()) {
            if (ag.getUser_id() != null && user_id != null && ag.getUser_id().equals(user_id))
                return ag.getU_name();
        }
        return "";
    }

    public String getCityCurrency(String cityId) {
        for (City city : getCities()) {
            if (city.getCity_id() != null && cityId != null && city.getCity_id().equals(cityId))
                return city.getCity_cur();
        }
        return "";
    }

    public String getMyCityCurrency() {
        if (getLoggedDriver() != null) {
            String cityId = getLoggedDriver().getCity_id();
            for (City city : getCities()) {
                if (city.getCity_id() != null && cityId != null && city.getCity_id().equals(cityId))
                    return city.getCity_cur();
            }
        }
        return currencyUnit();
    }

    public String getCountryCodeFromCity(String cityId) {
        for (City city : getCities()) {
            if (city.getCity_id() != null && cityId != null && city.getCity_id().equals(cityId))
                return city.getCountry_code();
        }
        return "";
    }

    public String getCityCountryCode(String cityId) {
        for (City city : getCities()) {
            if (city.getCity_id() != null && cityId != null && city.getCity_id().equals(cityId))
                return city.getCountry_code();
        }
        return "";
    }

    public String getTripDistance() {
        return tripDistance;
    }

    public void setTripDistance(String tripDistance) {
        this.tripDistance = tripDistance;
    }

    public String getTripDuration() {
        return tripDuration;
    }

    public void setTripDuration(String tripDuration) {
        this.tripDuration = tripDuration;
    }

    public boolean getNotificationCome() {
        return isNotificationCome;
    }

    public void setIsNotificationCome(boolean isNotificationCome) {
        this.isNotificationCome = isNotificationCome;
    }

    public String getFareReviewCalled() {
        return fareReviewCalled;
    }

    public void setFareReviewCalled(String fareReviewCalled) {
        this.fareReviewCalled = fareReviewCalled;
    }

    // Notifies UI to display a message.
    public void displayMessageOnScreen(Context context, String message, String tripStatus, String tripId) {

        Intent intent = new Intent(Config.DISPLAY_MESSAGE_ACTION);
        intent.putExtra(Config.EXTRA_MESSAGE, message);
        intent.putExtra(Config.EXTRA_TRIP_STATUS, tripStatus);
        intent.putExtra(Config.EXTRA_TRIP_ID, tripId);

        //System.out.println("Message send to Broad Cast from controller" + message);
        // Send Broadcast to Broadcast receiver with message
        context.sendBroadcast(intent);

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        isActive = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isActive = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

//    private InterfaceRefreshProfile refreshInterface;
//
//    public void setRefreshInterface(InterfaceRefreshProfile refreshInterface) {
//        this.refreshInterface = refreshInterface;
//    }
//
//    public InterfaceRefreshProfile getRefreshInterface() {
//        return refreshInterface;
//    }

    /* public double formatDoubleTo2DecimalPlaces(double value) {
         try {
             return Double.parseDouble(String.format(Locale.ENGLISH, "%,.02f", value));

         } catch (Exception e) {
             e.printStackTrace();
         }
         return 0.00;
     }*/
    public void setDriverAsset(GetDriverAsset getDriverAsset) {
        this.getDriverAsset = getDriverAsset;
    }
    public GetDriverAsset getGetDriverAsset() {
        return getDriverAsset;
    }


    public Driver getLoggedDriver() {
        return AppData.getInstance(this).getDriver();
    }

    public void setLoggedDriver(Driver driver) {
        Gson gson = new Gson();
        String s = gson.toJson(driver);
        Log.d(TAG, "setLoggedDriver: " + s);
        if (s == null || s.isEmpty()) {
            Log.d(TAG, "setLoggedDriver: driverisnull: " + (driver == null));
        }
        pref.setString("login_response", s);
        AppData.getInstance(this).setDriver(driver);
    }

    public void logout() {

        pref.setSingleMode(false);
        pref.setTripId("");
        pref.setString("trip_response", null);
        pref.setTripIds("");
        pref.setTripStartTime("");
        pref.setCalculateDistance(true);
        pref.setCalculatedDistance(0);
        PreferencesUtils.setFloat(this, R.string.recorded_distance, 0);
        pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(0));
        pref.setTRIP_DISTANCE(String.valueOf(0));
        pref.setTRIP_DISTANCE_CHANGEABLE(String.valueOf(0));
        pref.setTRIP_DURATION_CHANGEABLE_INT(0);
        pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(0));
        pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(0));

        pref.setIS_FARE_SUMMARY_OPEN("not_open");

        pref.setIsLogin(false);
        pref.setString("login_response", null);
        AppData.getInstance(this).clearData();
        stopLocationRecordService();
    }

    private void stopLocationRecordService() {
        if (TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class)) {
            Intent serviceIntent = new Intent(this, LocationService.class);
            stopService(serviceIntent);
        }
    }

    public boolean isLoggedIn() {
        return getLoggedDriver() != null;
    }

    public ArrayList<CategoryActors> getCategoryResponseList() {
        if (categoryResponseList == null) {
            categoryResponseList = new ArrayList<>();
        }
        return categoryResponseList;
    }

    public void setCategoryResponseList(ArrayList<CategoryActors> categoryResponseList) {
        this.categoryResponseList = categoryResponseList;
    }

    public double convertDistanceKmToUnit(Double distanceCoverInKm) {
        return checkCityDistanceUnit().equalsIgnoreCase("km") ? (distanceCoverInKm) : (distanceCoverInKm * 0.621371);
    }

    public void setLocalizeData(String data) {
        pref.setLocalizeData(data);
        setLocalizeData();
    }

    public void setLocalizeLang(String data) {
        pref.setLocalizeLang(data);
        //setLocalizeLang();
    }

    public void setLocalizeData() {
        String localizeData = pref.getLocalizeData();

        if (localizeData != null) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(localizeData);
                if (jsonObject != null && jsonObject.has("response"))
                    Localizer.localizeJson = jsonObject.getJSONObject("response");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String formatAmountWithCurrencyUnitInt(int amount) {
//        amount = (float) getPriceAfterCurrencyRateApplied(amount);
        return String.format(Locale.ENGLISH, currencyUnit() + " %d", amount);
    }

    public void reSignIn(final WebServiceUtil.DeviceTokenServiceListener listener) {
        reSignIn(listener, false, null);
    }

    public void reSignIn(final WebServiceUtil.DeviceTokenServiceListener listener, boolean isChangePassword, String newPassword) {
        if (getLoggedDriver() != null && net_connection_check()) {
            Driver driverInfo = getLoggedDriver();
            final Map<String, String> params = new HashMap<>();
            String url;
            if (USE_EMAIL_AUTH) {
                params.put(Constants.Keys.EMAIL, driverInfo.getD_email());
                if (isChangePassword)
                    params.put(Constants.Keys.PASSWORD, newPassword);
                else
                    params.put(Constants.Keys.PASSWORD, pref.getPASSWORD());
                url = Constants.Urls.URL_DRIVER_LOGIN;
            } else {
                params.put(Constants.Keys.PHONE, driverInfo.getD_phone());
                if (isChangePassword)
                    params.put(Constants.Keys.PASSWORD, newPassword);
                else
                    params.put(Constants.Keys.PASSWORD, pref.getPASSWORD());
                params.put(Constants.Keys.COUNTRY_CODE, driverInfo.getC_code());
                url = Constants.Urls.URL_DRIVER_PHONE_SIGN_IN;
            }
            WebServiceUtil.excuteRequest(this, params, url, listener);
        }
    }

    public boolean net_connection_check() {
        ConnectionManager cm = new ConnectionManager(this);
        boolean connection = cm.isConnectingToInternet();
        if (!connection) {
            Toast toast = Toast.makeText(this, Localizer.getLocalizerString("k_39_s4_no_mob_net"), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 70);
            toast.show();
        }
        return connection;
    }
}
