package com.ridertechrider.driver;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.utils.Utils;

public class BaseActivity extends Activity {
    private CustomProgressDialog progressDialog;

    void showProgressBar() {
        hideProgressBar();
        progressDialog = new CustomProgressDialog(BaseActivity.this);
        progressDialog.showDialog();
    }

    void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    void showToast() {
        Toast.makeText(getApplicationContext(), R.string.trip_udpated_successfully, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.applyAppLanguage(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Utils.applyAppLanguage(this);

    }
}
