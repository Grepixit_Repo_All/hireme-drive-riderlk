package com.ridertechrider.driver;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;

import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.service.NotificationUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeActivity extends BaseActivity {
    protected static final String TAG = "HomeActivity";
    private Controller controller;
    private TripModel tripModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        controller = (Controller) getApplicationContext();
        new GetDeviceID().execute();
        checkAutoLogin();
    }

    private void checkAutoLogin() {
        if (controller.isLoggedIn()) {
            if (controller.getLoggedDriver().isDocVerified()) {

                Intent i = new Intent(getApplicationContext(), SlideMainActivity.class);
                startActivity(i);
                finish();
            }
        }
    }


    @OnClick(R.id.signin)
    public void openSignInScreen() {
        Intent signin = new Intent(getApplicationContext(), SignInActivity.class);
        signin.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        startActivity(signin);
    }

    @OnClick(R.id.register)
    public void openRegisterScreen() {
        Intent register = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(register);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("NewApi")
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    private class GetDeviceID extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            int currentapiVersion = Build.VERSION.SDK_INT;
            //System.out.println("Current API Version is " + currentapiVersion);
            //System.out.println("Check API version is" + Build.VERSION_CODES.M);
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
                int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
                int hasAccessLocation = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
                int hasGetAccounts = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
                int hasInternet = checkSelfPermission(Manifest.permission.INTERNET);
                int hasAccessNetwork = checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);
                int hasAccounts = checkSelfPermission(Manifest.permission.ACCOUNT_MANAGER);
                int hasCamera = checkSelfPermission(Manifest.permission.CAMERA);
                int hasVibrate = checkSelfPermission(Manifest.permission.VIBRATE);
                int hasReadContacts = checkSelfPermission(Manifest.permission.READ_CONTACTS);
                int hasWriteContacts = checkSelfPermission(Manifest.permission.WRITE_CONTACTS);
                int hasReadStorage = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                int hasWriteStorage = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                int hasWakeLock = checkSelfPermission(Manifest.permission.WAKE_LOCK);
                int hasphonestate = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
                //System.out.println("HAS PHONE STATE" + hasphonestate);

                int hasChangeNetwork = checkSelfPermission(Manifest.permission.CHANGE_NETWORK_STATE);


                List<String> permissions = new ArrayList<>();

                if (hasCamera != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.CAMERA);
                }
                if (hasphonestate != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.READ_PHONE_STATE);
                }

                if (hasVibrate != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.VIBRATE);
                }
                if (hasReadContacts != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.READ_CONTACTS);
                }
                if (hasReadStorage != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
                if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                }

                if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                }

                if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.SEND_SMS);
                }

                if (hasAccessLocation != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                }
                if (hasGetAccounts != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.GET_ACCOUNTS);
                }

                if (hasInternet != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.INTERNET);
                }

                if (hasAccessNetwork != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.ACCESS_NETWORK_STATE);
                }

                if (hasAccounts != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.ACCOUNT_MANAGER);
                }

                if (hasChangeNetwork != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.CHANGE_NETWORK_STATE);
                }

                if (hasWakeLock != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.WAKE_LOCK);
                }
                if (hasWriteStorage != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }


                if (!permissions.isEmpty()) {
                    int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 1;
                    requestPermissions(permissions.toArray(new String[0]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
                }
                //System.out.println("HAS PHONE STATE after Give Permission" + hasphonestate);
            }
        }

    }


}

