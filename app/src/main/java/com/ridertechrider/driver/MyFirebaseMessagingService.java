package com.ridertechrider.driver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.grepix.grepixutils.WebServiceUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import static com.ridertechrider.driver.webservice.Constants.TripStatus.CANCEL;
import static com.ridertechrider.driver.webservice.Constants.TripStatus.CANCEL_RIDER;


/**
 * Created by Grepix Infotech on 08/08/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private static final String channelId = "channelid_001";
    private static NotificationManager mNotificationManager;
    private static NotificationChannel mChannel;
    private Controller controller;
    private int count;

    public static void driverTokenUpdateProfile(final Context context, String token) {
        Map<String, String> params = new HashMap<>();
//        String trip_response = Controller.getInstance().pref.getString("trip_response");
//        TripModel tripModel = null;
//        if (trip_response != null) {
//            Log.e("saveTripResponse", "" + trip_response);
//            tripModel = new Gson().fromJson(trip_response, TripModel.class);
//        }
//        if (tripModel == null)
//            params.put(Constants.Keys.D_IS_AVAILABLE, "1");
//        else
//            params.put(Constants.Keys.D_IS_AVAILABLE, "0");
        params.put(Constants.Keys.DEVICE_TOKEN, token);
        params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);
        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverIdWithNoAlert(context, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                //                        ServerApiCall.handleError(context, error);
            }
        });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived: " + remoteMessage.getNotification());
        Log.e(TAG, "RemoteMessage : " + remoteMessage.getData());
        controller = (Controller) getApplicationContext();
        Map<String, String> data = remoteMessage.getData();
        String message = "";

        message = data.get("price");
        String tripStatus = data.get("trip_status");
        String isShare = data.get("is_share");
        if (isShare == null) isShare = "0";
        String tripId = data.get("trip_id");
        String type = data.get("type");

        String localTripId = controller.pref.geTripIds();
        Log.d(TAG, "onMessageReceived: " + localTripId);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (tripStatus == null)
            return;
        if (tripStatus.equalsIgnoreCase("paid_cancel")) {
            String title = getString(R.string.app_name);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            inboxStyle.addLine(message);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            mBuilder.setLargeIcon(largeIcon);
            mBuilder.setContentTitle(title);
            mBuilder.setContentText(message);
            mBuilder.setLights(Color.RED, 3000, 3000);
            mBuilder.setSound(uri);
            mBuilder.setAutoCancel(true);
            mBuilder.setPriority(Notification.PRIORITY_HIGH);

            mBuilder.build().flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;

// Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(this, TripDetailsActivity.class);
            resultIntent.putExtra("isFromRiderCancel", true);
            resultIntent.putExtra("tripId", tripId);

            if (!Controller.isActivityVisible()) {
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                // your application to the Home screen.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(SlideMainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(resultIntent);
            }

            final PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_CANCEL_CURRENT
                    );

            mBuilder.setContentIntent(resultPendingIntent);
            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                mNotificationManager.createNotificationChannel(mChannel);
            }
// mId allows you to update the notification later on.
            mNotificationManager.notify(1003, mBuilder.build());

        }

        boolean alreadyHasTrip = localTripId != null && !localTripId.trim().equalsIgnoreCase("");

        if (data.containsKey("type") && data.get("type") != null && data.get("type").equalsIgnoreCase("notif")) {
            final int NOTIFICATION_ID = 143; //this can be any int
            String title = getString(R.string.app_name);
            //Building the Notification
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
            Bitmap smallIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            builder.setSmallIcon(R.mipmap.ic_launcher);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            builder.setContentTitle(title);
            builder.setLargeIcon(largeIcon);
            builder.setContentText(message);
            builder.setLights(Color.RED, 3000, 3000);
            builder.setSound(uri);
            builder.setAutoCancel(true);
            builder.setPriority(Notification.PRIORITY_HIGH);
            builder.build().flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;

            PendingIntent intent = PendingIntent.getActivity(this, 0, new Intent(this, SlideMainActivity.class), 0);
            if (controller.getLoggedDriver() == null)
                intent = PendingIntent.getActivity(this, 0, new Intent(this, SignInSignUpMainActivity.class), 0);
            builder.setContentIntent(intent);
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                mNotificationManager.createNotificationChannel(mChannel);
            }
            mNotificationManager.notify(NOTIFICATION_ID, builder.build());

            final Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mNotificationManager.cancel(NOTIFICATION_ID);
                    timer.cancel();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mNotificationManager.deleteNotificationChannel(mChannel.getId());
                    }

                }
            }, 10000, 1000);
        } else {


            if (alreadyHasTrip && tripStatus.equalsIgnoreCase("request") && isShare.equalsIgnoreCase("1")) {

            } else {
                if (alreadyHasTrip && tripStatus.equalsIgnoreCase("request"))
                    return;
                else if (alreadyHasTrip && tripId != null && !tripId.equals(localTripId))
                    return;
                else if (!alreadyHasTrip && !tripStatus.equalsIgnoreCase("request"))
                    return;
            }

            if (!Controller.isActivityVisible()) {
                //Some Vars
                final int NOTIFICATION_ID = 1; //this can be any int
                String title = getString(R.string.app_name);
                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                inboxStyle.addLine(message);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId);
                mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mBuilder.setLargeIcon(largeIcon);
                mBuilder.setContentTitle(title);
                mBuilder.setContentText(message);
                mBuilder.setLights(Color.RED, 3000, 3000);
                mBuilder.setSound(uri);
                mBuilder.setAutoCancel(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mBuilder.setPriority(Notification.PRIORITY_HIGH);
                }

                mBuilder.build().flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;

// Creates an explicit intent for an Activity in your app
                if (Objects.requireNonNull(tripStatus).equalsIgnoreCase("Cash") || tripStatus.equalsIgnoreCase("accept_payment_promo")) {
                    Intent resultIntent = new Intent(this, FareSummaryActivity.class);
                    resultIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                    // your application to the Home screen.
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    // Adds the back stack for the Intent (but not the Intent itself)
                    stackBuilder.addParentStack(SlideMainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
                    stackBuilder.addNextIntent(resultIntent);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    final PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(
                                    this,
                                    0,
                                    resultIntent,
                                    PendingIntent.FLAG_CANCEL_CURRENT
                            );

                    mBuilder.setContentIntent(resultPendingIntent);
                    mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                        mNotificationManager.createNotificationChannel(mChannel);
                    }
// mId allows you to update the notification later on.
                    final int id = 0;
                    mNotificationManager.notify(id, mBuilder.build());

//                if (tripStatus.equalsIgnoreCase("hide_alert")) {
//                    controller.stopNotificationSound();
//                    if (controller != null && tripId != null && tripId.equalsIgnoreCase(controller.pref.geTripIds())) {
//                        controller.displayMessageOnScreen(this, tripStatus, tripId, message);
//                    }
//                    return;
//                }
//                if (tripStatus.equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
//                    controller.isFromBackground = true;
//                    controller.playNotificationSound();
//                }
                } else if (tripStatus.equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
                    String trip_response = Controller.getInstance().pref.getString("trip_response");
                    TripModel tripModel = null;
                    if (trip_response != null) {
                        Log.e("saveTripResponse", "" + trip_response);
                        tripModel = new Gson().fromJson(trip_response, TripModel.class);
                    }
                    if (tripModel == null || (tripModel.trip != null && tripModel.trip.getIs_share().equalsIgnoreCase("1"))) {

                        Intent resultIntent = new Intent(this, SlideMainActivity.class);

// your application to the Home screen.
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
                        stackBuilder.addParentStack(SlideMainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
                        stackBuilder.addNextIntent(resultIntent);
                        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        final PendingIntent resultPendingIntent =
                                PendingIntent.getActivity(
                                        this,
                                        0,

                                        resultIntent,
                                        PendingIntent.FLAG_CANCEL_CURRENT
                                );

                        mBuilder.setContentIntent(resultPendingIntent);
                        mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            int importance = NotificationManager.IMPORTANCE_HIGH;
                            mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                            mNotificationManager.createNotificationChannel(mChannel);
                        }
// mId allows you to update the notification later on.
                        final int id = 0;

                        mNotificationManager.notify(id, mBuilder.build());
                        controller.isFromBackground = true;
                        controller.playNotificationSound(R.raw.trip_request);
                    }

                } else {
//                if (tripStatus.equalsIgnoreCase(Constants.TripStatus.REQUEST) && controller.pref.get) {
//                }
                    Intent resultIntent = new Intent(this, SlideMainActivity.class);

// your application to the Home screen.
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
                    stackBuilder.addParentStack(SlideMainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
                    stackBuilder.addNextIntent(resultIntent);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    final PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(
                                    this,
                                    0,

                                    resultIntent,
                                    PendingIntent.FLAG_CANCEL_CURRENT
                            );

                    mBuilder.setContentIntent(resultPendingIntent);
                    mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                        mNotificationManager.createNotificationChannel(mChannel);
                    }
// mId allows you to update the notification later on.
                    final int id = 0;

                    String trip_response = Controller.getInstance().pref.getString("trip_response");
                    TripModel tripModel = null;
                    if (trip_response != null) {
                        Log.e("saveTripResponse", "" + trip_response);
                        tripModel = new Gson().fromJson(trip_response, TripModel.class);
                        AppData.getInstance(Controller.getInstance()).setTripModel(tripModel);
                    }
                    if (tripStatus.equalsIgnoreCase("request")) {
                        if (tripModel == null || (tripModel.trip != null && tripModel.trip.getIs_share().equalsIgnoreCase("1"))) {
                            mNotificationManager.notify(id, mBuilder.build());
                            if (tripStatus.equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
                                controller.isFromBackground = true;
                                controller.playNotificationSound(R.raw.trip_request);
                            }
                        }
                    } else {

                        mNotificationManager.notify(id, mBuilder.build());
                        if (tripStatus.equalsIgnoreCase("hide_alert")) {
                            controller.stopNotificationSound();
                            if (controller != null && tripId != null && tripId.equalsIgnoreCase(controller.pref.geTripIds())) {
                                controller.displayMessageOnScreen(this, tripStatus, tripId, message);
                            }
                            return;
                        }
                        if (tripStatus.equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
                            controller.isFromBackground = true;
                            controller.playNotificationSound(R.raw.trip_request);
                        }
                        playVoice(tripStatus);
                    }
                }


                count = 7;
                controller.setIsNotificationCome(true);
                final Timer timer = new Timer();
                timer.schedule(new TimerTask() {


                    @Override
                    public void run() {
                        count++;
                        try {
                            if (mNotificationManager != null) {
                                mNotificationManager.cancel(0);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    mNotificationManager.deleteNotificationChannel(mChannel.getId());
                                }
                            }
                            if (count < 35) {
                                controller.setIsNotificationCome(true);

                                float cont = count;
                            } else {
                                controller.setIsNotificationCome(false);
                                if (mNotificationManager != null) {
                                    mNotificationManager.cancel(0);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        mNotificationManager.deleteNotificationChannel(mChannel.getId());
                                    }
                                }
                                timer.cancel();

                            }

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //  timer.cancel();
                }, 35000, 1000);
//            if (controller == null) {
                controller.displayMessageOnScreen(this, message, tripStatus, tripId);
//            }
            } else {
                generateNotification(this, message, tripStatus, tripId, controller);
            }

        }
    }

    private void generateNotification(Context context, String message, String
            status, String tripId, Controller controller) {
        //Some Vars
        Log.e("tripGenrateNotification", "" + tripId + "," + controller.pref.geTripIds() + status);
        final int NOTIFICATION_ID = 1; //this can be any int
        String title = context.getString(R.string.app_name);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String trip_response = Controller.getInstance().pref.getString("trip_response");
        TripModel tripModel = null;
        if (trip_response != null) {
            Log.e("saveTripResponse", "" + trip_response);
            tripModel = new Gson().fromJson(trip_response, TripModel.class);
            AppData.getInstance(Controller.getInstance()).setTripModel(tripModel);
        }
        Log.d(TAG, "generateNotification: " + AppData.getInstance(Controller.getInstance()).getTripModel());

        //Building the Notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId);
        Bitmap smallIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        builder.setContentTitle(title);
        builder.setLargeIcon(largeIcon);
        builder.setContentText(message);
        builder.setLights(Color.RED, 3000, 3000);
        builder.setSound(uri);
        builder.setAutoCancel(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(Notification.PRIORITY_HIGH);
        }
        builder.build().flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        if (status.equalsIgnoreCase("request")) {
            if (tripModel == null || (tripModel.trip != null && tripModel.trip.getIs_share().equalsIgnoreCase("1"))) {

                Intent notificationIntent = new Intent(context, SlideMainActivity.class);
                notificationIntent.putExtra(Config.EXTRA_TRIP_STATUS, status);
                notificationIntent.putExtra(Config.EXTRA_TRIP_ID, tripId);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
                builder.setContentIntent(intent);
                Intent i = new Intent();
                String mPackage = context.getPackageName();
                String mClass = ".SlideMainActivity";
                i.setComponent(new ComponentName(mPackage, mPackage + mClass));
                i.putExtra("message", message);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                controller.isFromBackground = false;
                controller.playNotificationSound(R.raw.trip_request);
                context.startActivity(i);
                Intent intent1 = new Intent();
                intent1.setAction(Config.DISPLAY_MESSAGE_ACTION);
                intent1.putExtra(Config.EXTRA_TRIP_STATUS, status);
                intent1.putExtra(Config.EXTRA_TRIP_ID, tripId);
                context.sendBroadcast(intent1);
                mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                    mNotificationManager.createNotificationChannel(mChannel);
                }
                mNotificationManager.notify(NOTIFICATION_ID, builder.build());
            }
        } else {
            if (status.equalsIgnoreCase("Cash") || status.equalsIgnoreCase("accept_payment_promo") || status.equalsIgnoreCase(Constants.TripStatus.END) || status.equalsIgnoreCase("Paid")) {
                Intent notificationIntent = new Intent(context, FareSummaryActivity.class);
                notificationIntent.putExtra(Config.EXTRA_TRIP_STATUS, status);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                notificationIntent.putExtra(Config.EXTRA_TRIP_ID, tripId);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
                builder.setContentIntent(intent);
                Intent i = new Intent();
                String mPackage = context.getPackageName();
                String mClass = ".FareSummaryActivity";
                i.setComponent(new ComponentName(mPackage, mPackage + mClass));
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                Intent intent1 = new Intent();
                intent1.setAction(Config.DISPLAY_MESSAGE_ACTION);
                intent1.putExtra(Config.EXTRA_TRIP_STATUS, status);
                intent1.putExtra(Config.EXTRA_TRIP_ID, tripId);
                context.sendBroadcast(intent1);
                mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                    mNotificationManager.createNotificationChannel(mChannel);
                }
                mNotificationManager.notify(NOTIFICATION_ID, builder.build());

            } /*else if (message.equalsIgnoreCase("manualpay")) {

        } */ else if (status.equalsIgnoreCase("promopay")) {
                Intent notificationIntent = new Intent(context, FareSummaryActivity.class);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                notificationIntent.putExtra(Config.EXTRA_TRIP_STATUS, status);
                notificationIntent.putExtra(Config.EXTRA_TRIP_ID, tripId);
                notificationIntent.putExtra(Config.EXTRA_MESSAGE, message);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
                builder.setContentIntent(intent);

                Intent i = new Intent();
                String mPackage = context.getPackageName();
                String mClass = ".FareSummaryActivity";
                i.setComponent(new ComponentName(mPackage, mPackage + mClass));
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                Intent intent1 = new Intent();
                intent1.setAction(Config.DISPLAY_MESSAGE_ACTION);
                intent1.putExtra(Config.EXTRA_TRIP_STATUS, status);
                intent1.putExtra(Config.EXTRA_TRIP_ID, tripId);
                context.sendBroadcast(intent1);
                mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                    mNotificationManager.createNotificationChannel(mChannel);

                }
                mNotificationManager.notify(NOTIFICATION_ID, builder.build());

            } else if (status.equalsIgnoreCase("hide_alert")) {
                Log.e("tripId", "" + tripId);

                if (tripId != null && controller.pref.geTripIds().equals(tripId)) {
                    Log.e("prefTripId", "" + controller.pref.geTripIds());
                    Intent notificationIntent = new Intent(context, SlideMainActivity.class);
                    notificationIntent.putExtra(Config.EXTRA_TRIP_STATUS, status);
                    notificationIntent.putExtra(Config.EXTRA_TRIP_ID, tripId);
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                    builder.setContentIntent(intent);
                    Intent i = new Intent();
                    String mPackage = context.getPackageName();
                    String mClass = ".SlideMainActivity";
                    i.setComponent(new ComponentName(mPackage, mPackage + mClass));
                    i.putExtra("message", message);
                    i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (status.equalsIgnoreCase(CANCEL) || status.equalsIgnoreCase(CANCEL_RIDER)) {
                        Intent intentBroadcast = new Intent("some_custom_id");
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intentBroadcast);
                    } else {
                        controller.isFromBackground = false;
                        controller.playNotificationSound(R.raw.trip_request);
                    }
                    context.startActivity(i);
                    Intent intent1 = new Intent();
                    intent1.setAction(Config.DISPLAY_MESSAGE_ACTION);
                    intent1.putExtra(Config.EXTRA_TRIP_STATUS, status);
                    intent1.putExtra(Config.EXTRA_TRIP_ID, tripId);
                    context.sendBroadcast(intent1);
                    mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                        mNotificationManager.createNotificationChannel(mChannel);

                    }
                    mNotificationManager.notify(NOTIFICATION_ID, builder.build());
                }
            } else if (!status.equalsIgnoreCase("hide_alert")) {
                String tripIds = controller.pref.geTripIds();
                Log.d(TAG, "generateNotification: " + controller.pref.geTripIds());

                Intent notificationIntent = new Intent(context, SlideMainActivity.class);
                notificationIntent.putExtra(Config.EXTRA_TRIP_STATUS, status);
                notificationIntent.putExtra(Config.EXTRA_TRIP_ID, tripId);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
                builder.setContentIntent(intent);
                Intent i = new Intent();
                String mPackage = context.getPackageName();
                String mClass = ".SlideMainActivity";
                i.setComponent(new ComponentName(mPackage, mPackage + mClass));
                i.putExtra("message", message);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                if (status.equalsIgnoreCase(CANCEL) || status.equalsIgnoreCase(CANCEL_RIDER)) {
                    controller.isFromBackground = false;
                    Intent intentBroadcast = new Intent("some_custom_id");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intentBroadcast);
                }
//                else {
//                    controller.isFromBackground = false;
//                    controller.playNotificationSound(R.raw.trip_request);
//                }
                context.startActivity(i);
                Intent intent1 = new Intent();
                intent1.setAction(Config.DISPLAY_MESSAGE_ACTION);
                intent1.putExtra(Config.EXTRA_TRIP_STATUS, status);
                intent1.putExtra(Config.EXTRA_TRIP_ID, tripId);
                context.sendBroadcast(intent1);
                mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
                    mNotificationManager.createNotificationChannel(mChannel);
                }
                mNotificationManager.notify(NOTIFICATION_ID, builder.build());
            }
//            playVoice(status);
        }
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                mNotificationManager.cancel(NOTIFICATION_ID);
                timer.cancel();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    mNotificationManager.deleteNotificationChannel(mChannel.getId());
                }

            }
        }, 10000, 1000);
    }

    public void playVoice(String tripStatus) {
        Log.d(TAG, "playVoice: " + tripStatus);
        if (tripStatus != null)
//            if (tripStatus.equalsIgnoreCase(BEGIN)){
//                Controller.getInstance().playNotificationSound(R.raw.begin);
//
//            } else if (tripStatus.equalsIgnoreCase(END)){
//                Controller.getInstance().playNotificationSound(R.raw.ended_succ);
//
//            }else if (tripStatus.equalsIgnoreCase(ACCEPT)){
//                Controller.getInstance().playNotificationSound(R.raw.accepted);
//
//            }else if (tripStatus.equalsIgnoreCase(ARRIVE)){
//                Controller.getInstance().playNotificationSound(R.raw.arrive);
//
//            }else
            if (tripStatus.equalsIgnoreCase(CANCEL) || tripStatus.equalsIgnoreCase(CANCEL_RIDER)) {
                Controller.getInstance().playNotificationSound(R.raw.trip_cancelled);
//
//            }else if (tripStatus.equalsIgnoreCase("Cash")){
//                Controller.getInstance().playNotificationSound(R.raw.collect_cash);
//
//            }else if (tripStatus.equalsIgnoreCase("Paid")){
//                Controller.getInstance().playNotificationSound(R.raw.collect_cash);
//
//            }else if (tripStatus.equalsIgnoreCase("accept_payment_promo")){
//                Controller.getInstance().playNotificationSound(R.raw.promo_applied);

            }
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Controller controller = (Controller) getApplicationContext();
        if (controller.pref.getIsLogin()) {
            driverTokenUpdateProfile(getApplicationContext(), token);
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        storeRegIdInPref(s);
        // sending reg id to your server
        sendRegistrationToServer(s);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
    }


    private void storeRegIdInPref(String token) {
        Log.d("my tokn=====6", token);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
        //send device toke to server  if user allready logedin
        sendRegistrationToServer(token);
    }

}
