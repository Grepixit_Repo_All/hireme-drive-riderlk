package com.ridertechrider.driver.webservice;

import com.ridertechrider.driver.BuildConfig;
import com.ridertechrider.driver.Localizer;

/**
 * Created by grepixinfotech on 22/07/16.
 */
public interface Constants {
    //    http://104.154.82.146/riderlk/webservices/1.0.5/index.php
    String SETTINGS_NAME = "fhjadfjksahdfssdsa.czxch";
    String HOST = "http://35.222.125.91/";
    //String HOST_PATH = "riderlk/webservices/1.0.5/index.php";
    String D_LICENSE_IMAGE_PATH = "driver_license";
    String D_PROFILE_IMAGE_PATH = "driver_image";
    String D_RC_IMAGE_PATH = "driver_rc";
    String D_INSURANCE_IMAGE_PATH = "driver_insurance";
    String D_CAR_IMAGE = "car_image";
    String D_ASSET_TYPE_INSURANCE = "INSURANCE";
    String D_ASSET_TYPE_RC = "RC";
    String D_ASSET_TYPE_LICENSE = "LICENSE";
    String IMAGE_BASE_URL = HOST + "riderlk/webservices/images/originals/";
    float METER_TO_MILE = 0.000621371f;

    interface DeliveryStatus {

        String PAID = "paid";
        String Completed = "Completed";

    }

    interface Urls {
//        String URL_BASE = "http://104.154.82.146/riderlk/webservices/1.0.5/index.php";
        String URL_BASE = "http://35.222.125.91/riderlk/webservices/1.0.5/index.php";

        String URL_LOCALIZER_KEY = URL_BASE + "/localisationapi/getlocalisations?";
        String URL_LOCALIZE_LANG = URL_BASE + "/localisationapi/getlanguages?";
        String URL_DRIVER_PHONE_REGISTRATION = URL_BASE + "/driverapi/phregistration?";
        String URL_DRIVER_PHONE_SIGN_IN = URL_BASE + "/driverapi/phlogin?";
        String URL_DRIVER_PHONE_VALIDATE = URL_BASE + "/driverapi/phdrivervalidate?";

        String UPDATE_PROFILE = URL_BASE + "/driverapi/updatedriverprofile?";
        String UPDATE_USER_PROFILE = URL_BASE + "/userapi/updateuserprofile?";
        String UPDATE_DELIVERY_URL = URL_BASE + "/deliveryapi/updatedelivery";
        String FORGET_PASSWORD = URL_BASE + "/driverapi/forgetpassword?";
        String URL_VALIDATE_REFERAL_CODE = URL_BASE + "/constantapi/validatereferral?";
        String DRIVER_TRIP_HISTORY = URL_BASE + "/tripapi/gettrips?";
        String DRIVER_PENDING_TRIPS = URL_BASE + "/tripapi/getpendingtrips?";
        String GET_CAR_CATEGORY = URL_BASE + "/categoryapi/getcategories?";
        String UPDATE_TRIP_URL = URL_BASE + "/tripapi/updatetrip?";
        String GET_CONSTANTS_API = URL_BASE + "/constantapi/getconstants?";
        String URL_DRIVER_REGISTER = URL_BASE + "/driverapi/registration?";
        String URL_DRIVER_LOGIN = URL_BASE + "/driverapi/login?";
        String URL_ACCEPT_TRIP_REQUEST = URL_BASE + "/tripapi/tripaccept?";
        String URL_GET_MTRIPS_TRIP_REQUEST = URL_BASE + "/mtripapi/getmtrips";
        String URL_ASSIGN_TRIP_REQUEST = URL_BASE + "/tripapi/tripassigned";
        String URL_TO_SHARE = "http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "&hl=en";
        String EMAIL_FOR_SUPPORT = "info@appicial.com";
        String URL_USER_UPDATE_TRIP = URL_BASE + "/tripapi/updatetrip?";
        String URL_PAYMENT_SAVE = URL_BASE + "/paymentapi/save?";
        String URL_COMMAN_NOTIFICATION = HOST + "riderlk/webservices/push/RiderAndroidIosPushNotification.php";
        String URL_USER_GET_DRIVER = URL_BASE + "/driverapi/getdrivers?";
        String ADD_TRANS_WITHOUT_TRIP = URL_BASE + "/transactionapi/addtranswithouttrip";
        String GET_DRIVER_TRANSACTIONS_DETAILS = URL_BASE + "/transdrvapi/getdrvtrans";
        String ADD_TRANS_WITH_TRIP = URL_BASE + "/transactionapi/addtriptrans";
        String URL_USER_CREATE_TRIP = URL_BASE + "/tripapi/save?";
        String URL_USER_GET_NOTIFICATION = URL_BASE + "/notificationapi/getnotification";
        String URL_GET_CITIES = URL_BASE + "/cityapi/getcities";
        String URL_GET_AGENCIES = URL_BASE + "/userapi/getagencies";
        String UPDATE_TRIP_GET_ROUTE_URL = URL_BASE + "/triprouteapi/gettriproutes";

        String URL_ADD_DRIVER_ASSET = URL_BASE + "/driverassetapi/adddriverasset";
        String URL_GET_DRIVER_ASSET = URL_BASE + "/driverassetapi/getdriverassets";

        String URL_REJECT_REQUEST = URL_BASE + "/triprequestapi/rejecttriprequest";
    }

    interface Language {
        String ENGLISH = "en";
        String PORTUGEUESE = "ar";
    }

    interface Keys {
        String TRIP_ID = "trip_id";
        String DRIVER_ID = "driver_id";
        String TRIP_STATUS = "trip_status";
        String ANDROID = "android";
        String IOS = "ios";
        boolean IS_YSE_NO_DIALOG = true;
        String API_KEY = "api_key";
        String TRIP_PAY_MODE = "trip_pay_mode";
        String TRIP_PAY_STATUS = "trip_pay_status";
        String RESPONSE = "response";
        String LAT = "lat";
        String LNG = "lng";
        String D_LAT = "d_lat";
        String D_LNG = "d_lng";
        String DEVICE_TYPE = "d_device_type";
        String DEVICE_TOKEN = "d_device_token";
        String D_IS_AVAILABLE = "d_is_available";
        String EMAIL = "d_email";
        String PHONE = "d_phone";
        String CITY_ID = "city_id";
        String COMPANY_ID = "company_id";
        String COUNTRY_CODE = "c_code";
        String PASSWORD = "d_password";
        String APP_LANGUAGE = "localeString";

        String FNAME = "d_fname";
        String LNAME = "d_lname";

        /* String TRIP_ID = "trip_id";
         String DRIVER_ID = "driver_id";
         String TRIP_STATUS = "trip_status";*/
        // String GROUP_ID = "group_id";
        String USER_ID = "user_id";
        String SCHEDULED_DROP_LAT = "trip_scheduled_drop_lat";
        String SCHEDULED_DROP_LNG = "trip_scheduled_drop_lng";
        String SCHEDULED_PICK_LAT = "trip_scheduled_pick_lat";
        String SCHEDULED_PICK_LNG = "trip_scheduled_pick_lng";
        String SEARCH_RESULT_ADDR = "trip_search_result_addr";
        String SEARCH_ADDR = "trip_searched_addr";
        String TRIP_DATE = "trip_date";
        String TRIP_PICK_LOC = "trip_from_loc";
        String TRIP_DEST_LOC = "trip_to_loc";
        String TRIP_PAY_AMOUNT = "trip_pay_amount";
        String TRIP_DISTANCE = "trip_distance";
        String TRIP_DISTANCE_UNIT = "trip_dunit";
        String TRIP_ACTUAL_PICK_LAT = "trip_actual_pick_lat";
        String TRIP_ACTUAL_PICK_LNG = "trip_actual_pick_lng";
        String TRIP_ACTUAL_DROP_LAT = "trip_actual_drop_lat";
        String TRIP_ACTUAL_DROP_LNG = "trip_actual_drop_lng";
        String TRIP_ACTULA_TO_LOC = "actual_to_loc";
        String TRIP_ACTUAL_FROM_LOC = "actual_from_loc";
        String OTP = "otp";
        String USERNAME = "username";


        String APP_VERSION = "app_ver";
        String DEVICE_VERSION = "os_ver";
        String DEV_TYPE = "dev_type";
        String LIMIT = "limit";

        int BOOKING_TIME_RIDE_LATER = 120;

    }

    interface Values {
        String DEVICE_TYPE_ANDROID = "Android";
        String DRIVER_AVAILABLE = "1";
        int BeginTripRouteWidth = 16;
        int WALLET_BALANCE_THRESHOLD = 50;
        boolean USE_EMAIL_AUTH = true;
    }

    interface TripStatus {
        String REQUEST = "request";
        String ACCEPT = "accept";
        String ARRIVE = "arrive";
        String BEGIN = "begin";
        String Hide_Alert = "hide_alert";
        String ASSIGN = "assigned";
        String END = "completed";
        String PAID = "paid";
        String CANCEL = "cancel";
        String DRIVER_CANCEL_AT_DROP = "p_cancel_drop";
        String DRIVER_CANCEL_AT_PICKUP = "p_cancel_pickup";
        String CANCEL_RIDER = "paid_cancel";
    }

    interface Message {
        String ACCEPTED = Localizer.getLocalizerString("k_1_s14_trip_confirm");
        String END = Localizer.getLocalizerString("k_2_s14_trip_conplete");
        String ARRIVE = Localizer.getLocalizerString("k_3_s14_arrive_soon");
        String BEGIN = Localizer.getLocalizerString("k_4_s14_trip_started");
        String DRIVER_CANCEL = Localizer.getLocalizerString("k_5_s14_trip_cancelled_by_driver");
        String DRIVER_ARIVED = Localizer.getLocalizerString("k_6_s14_driver_arrived");
        String DRIVER_REQUEST_ACCEPT = Localizer.getLocalizerString("k_7_s14_request_accept");
        String SOME_ONE_SOMETHING = Localizer.getLocalizerString("k_8_s14_sent_something");
        String PAID = Localizer.getLocalizerString("k_9_s14_paid_successfully");
        String AASSIGNED = Localizer.getLocalizerString("k_10_s14_trip_request_assign");

        String ACCEPTED_DELIVERY = Localizer.getLocalizerString("k_11_s14_delivery_request_confirm");
        String END_DELIVERY = Localizer.getLocalizerString("k_12_s14_delivery_complete_success");
        String ARRIVE_DELIVERY = Localizer.getLocalizerString("k_3_s14_arrive_soon");
        String BEGIN_DELIVERY = Localizer.getLocalizerString("k_14_s14_delivery_started");
        String DRIVER_CANCEL_DELIVERY = Localizer.getLocalizerString("k_15_s14_driver_cancel_delivery");
        String DRIVER_ARIVED_DELIVERY = Localizer.getLocalizerString("k_6_s14_driver_arrived");
        String DRIVER_REQUEST_ACCEPT_DELIVERY = Localizer.getLocalizerString("k_17_s14_delivery_request_acept");
        String SOME_ONE_SOMETHING_DELIVERY = Localizer.getLocalizerString("k_8_s14_sent_something");
        String PAID_DELIVERY = Localizer.getLocalizerString("k_9_s14_paid_successfully");
    }

    interface Message_ar {
        String ACCEPTED = "واو ، تم تأكيد طلب رحلتك. إستعد";
        String END = "اكتملت رحلتك بنجاح. شكر";
        String ARRIVE = "مهلا ، سوف الكابينة تصل قريبا. كن جاهزا";
        String BEGIN = "لقد بدأت رحلتك. حظا سعيدا";
        String DRIVER_CANCEL = "ألغى السائق الرحلة. اسأله";
        String DRIVER_ARIVED = "وصل السائق. يرجى التحقق من التسليم!";
        String DRIVER_REQUEST_ACCEPT = "قبول طلبك";
        String SOME_ONE_SOMETHING = "أرسل شخص ما شيئا لك. الرجاء المساعدة للتحقق";
        String PAID = "لقد دفعت بنجاح";
        String AASSIGNED = "Wow, Your Trip Request has been assigned. Get Ready";

        String ACCEPTED_DELIVERY = "نجاح باهر ، تم تأكيد طلب التسليم الخاص بك. إستعد";
        String END_DELIVERY = "تم تسليم التوصيل بنجاح شكر";
        String ARRIVE_DELIVERY = "مهلا ، سوف الكابينة تصل قريبا. كن جاهزا";
        String BEGIN_DELIVERY = "لقد بدأ التسليم الخاص بك. حظا سعيدا";
        String DRIVER_CANCEL_DELIVERY = "ألغى السائق التسليم. اسأله";
        String DRIVER_ARIVED_DELIVERY = "وصل السائق. يرجى التحقق من التسليم!";
        String DRIVER_REQUEST_ACCEPT_DELIVERY = "قبول طلب التسليم الخاص بك";
        String SOME_ONE_SOMETHING_DELIVERY = "أرسل شخص ما شيئا لك. الرجاء المساعدة للتحقق";
        String PAID_DELIVERY = "لقد دفعت بنجاح";
    }
}
