package com.ridertechrider.driver.webservice;

import com.ridertechrider.driver.CategoryResponse;
import com.ridertechrider.driver.webservice.model.LoginResponse;
import com.ridertechrider.driver.webservice.model.NearUserResponse;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface APIInterface {
    //@POST(HOST_PATH + "/userapi/getnearbyuserlists")
//    riderlk/webservices/1.0.5/index.php
    @POST("riderlk/webservices/1.0.5/index.php/userapi/getnearbyuserlists")
    Call<NearUserResponse> getNearUsers(@Query(Constants.Keys.API_KEY) String api_key, @Query(Constants.Keys.LAT) String lat, @Query(Constants.Keys.LNG) String lng, @Query("miles") String miles);

    @POST("riderlk/webservices/1.0.5/index.php/tripapi/gettrips")
    Call<ResponseBody> getTripById(@Query(Constants.Keys.API_KEY) String api_key, @Query(Constants.Keys.TRIP_ID) String tripId);

    @POST("riderlk/webservices/1.0.5/index.php/tripapi/gettrips")
    Call<ResponseBody> getTripRequestList(@Query(Constants.Keys.API_KEY) String api_key, @Query("category_id") String categoryId, @Query("hours") String hours, @Query("lat") String lat, @Query("lng") String lng, @Query("miles") String miles, @Query("trip_status") String trip_status, @Query("next_offset") String next_offset, @Query("limit") String limit, @Query("city_id") String city_id);

    @POST("riderlk/webservices/1.0.5/index.php/tripapi/getrevisedtrips")
    Call<ResponseBody> getRevisedTrips(@Query(Constants.Keys.API_KEY) String api_key,
                                       @Query("category_id") String categoryId, @Query("hours") String hours,
                                       @Query("lat") String lat, @Query("lng") String lng, @Query("miles") String miles,
                                       @Query("trip_status") String trip_status, @Query("next_offset") String next_offset,
                                       @Query("limit") String limit, @Query("driver_id") String driver_id);

    @POST("riderlk/webservices/1.0.5/index.php/tripapi/getrevisedtrips")
    Call<ResponseBody> getRevisedTrips(@Query(Constants.Keys.API_KEY) String api_key,
                                       @Query("category_id") String categoryId, @Query("hours") String hours,
                                       @Query("lat") String lat, @Query("lng") String lng, @Query("miles") String miles,
                                       @Query("trip_status") String trip_status, @Query("next_offset") String next_offset,
                                       @Query("limit") String limit, @Query("driver_id") String driver_id, @Query("is_share") String isRideShare);

    @POST("riderlk/webservices/1.0.5/index.php/tripapi/getpendingtrips")
    Call<ResponseBody> getPendingTrips(@Query(Constants.Keys.API_KEY) String api_key, @Query("driver_id") String driver_id, @Query("limit") String limit);

    @POST("riderlk/webservices/1.0.5/index.php/tripapi/gettrips")
    Call<ResponseBody> getAsignedTripRequestList(@Query(Constants.Keys.API_KEY) String api_key, @Query("driver_id") String driver_id, @Query("trip_status") String trip_status);

    /*   @POST("riderlk/webservices/1.0.5/index.php/driverapi/login?")
       Call<LoginResponse> driverLogin(@Query(Constants.Keys.EMAIL) String email, @Query(Constants.Keys.PASSWORD) String tripId, @Query("d_is_available") String d_is_available);
   */
    @POST("riderlk/webservices/1.0.5/index.php/driverapi/updatedriverprofile?")
    Call<LoginResponse> updateDriverLocation(@Query(Constants.Keys.API_KEY) String api_key, @Query(Constants.Keys.DRIVER_ID) String driverId, @Query(Constants.Keys.D_LAT) String dLat, @Query(Constants.Keys.D_LNG) String dLng);

    /*@POST("riderlk/webservices/1.0.5/index.php/driverapi/updatedriverprofile?")
    Call<LoginResponse> updateDriverProfile(@Query(Constants.Keys.API_KEY) String api_key, @Query(Constants.Keys.DRIVER_ID) String driverId, @Query("d_is_available") String d_is_available, @Query("d_device_type") String d_device_type, @Query("d_degree") String d_degree);
*/
 /*   @POST("riderlk/webservices/1.0.5/index.php/driverapi/updatedriverprofile?")
    Call<ResponseBody> addDeviceToken(@Query(Constants.Keys.API_KEY) String api_key, @Query(Constants.Keys.PASSWORD) String tripId, @Query("d_is_available") String d_is_available);
*/
    @POST("riderlk/webservices/1.0.5/index.php/deliveryapi/getdeliveries")
    Call<ResponseBody> getTripDeliveryResponseList(@Query(Constants.Keys.API_KEY) String api_key, @Query("trip_id") String tripId);

    @POST("riderlk/webservices/push/RiderAndroidIosPushNotification.php")
    Call<Void> sendRiderNotrification(@QueryMap Map<String, String> map);

    /*@POST("riderlk/webservices/push/RiderAndroidIosPushNotification.php?")
    Call<Void> sendNotificationToUser(@QueryMap Map<String, String> map);
*/
    @POST("riderlk/webservices/1.0.5/index.php/categoryapi/getcategories?")
    Call<CategoryResponse> getCategories(@Query("api_key") String api_key);

  /*  @POST("riderlk/webservices/1.0.5/index.php/driverapi/registration?")
    Call<ResponseBody> registerUser(@QueryMap Map<String, String> map);
*/
    /*@POST("riderlk/webservices/1.0.5/index.php/driverapi/registration?")
    Call<ResponseBody> registerUser(@Query("d_email") String dEmail,@Query("d_phone") String dPhone,@Query("d_password") String dPassword,@Query("d_device_type") String d_type,@Query("d_device_token") String d_token,@Query("d_fname") String firstname,@Query("d_lname") String lastname);
*/

    @POST("riderlk/webservices/tw_sms")
    Call<ResponseBody> sendOtp(@Query("msg") String message, @Query("ph") String phoneNumber);


}
