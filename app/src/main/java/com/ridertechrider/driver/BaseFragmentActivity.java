package com.ridertechrider.driver;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.utils.Utils;

public class BaseFragmentActivity extends FragmentActivity {
    private CustomProgressDialog progressDialog;

    void showProgressBar() {
        hideProgressBar();
        progressDialog = new CustomProgressDialog(BaseFragmentActivity.this);
        progressDialog.showDialog();
    }

    void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    void showToast() {
        Toast.makeText(getApplicationContext(), R.string.trip_udpated_successfully, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.applyAppLanguage(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Utils.applyAppLanguage(this);
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Configuration config = newBase.getResources().getConfiguration();
//            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(newBase);
//            String language = prefs.getString(Constants.Keys.APP_LANGUAGE, Constants.Language.ENGLISH);
//            Locale locale = new Locale(language);
//            Locale.setDefault(locale);
//            config.setLocale(locale);
//            newBase = newBase.createConfigurationContext(config);
//        }
//        super.attachBaseContext(newBase);
//    }
}
