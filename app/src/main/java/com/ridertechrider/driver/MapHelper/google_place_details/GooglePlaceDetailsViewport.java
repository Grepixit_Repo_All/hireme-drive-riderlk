package com.ridertechrider.driver.MapHelper.google_place_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GooglePlaceDetailsViewport {

    @SerializedName("northeast")
    @Expose
    private GooglePlaceDetailsNortheast northeast;
    @SerializedName("southwest")
    @Expose
    private GooglePlaceDetailsSouthwest southwest;

    public GooglePlaceDetailsNortheast getNortheast() {
        return northeast;
    }

    public void setNortheast(GooglePlaceDetailsNortheast northeast) {
        this.northeast = northeast;
    }

    public GooglePlaceDetailsSouthwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(GooglePlaceDetailsSouthwest southwest) {
        this.southwest = southwest;
    }

}
