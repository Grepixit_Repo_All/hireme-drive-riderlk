package com.ridertechrider.driver.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ridertechrider.driver.Localizer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.android.volley.Request.Method.POST;

public class WebService {
    private static final String TAG = WebService.class.getSimpleName();

    public static void excuteRequest(final Context contaxt, final Map<String, String> params, String url, final DeviceTokenServiceListener listener) {
        excuteRequest(contaxt, POST, params, url, listener);
    }

    public static void excuteRequest(final Context contaxt, int method, final Map<String, String> params, String url, final DeviceTokenServiceListener listener) {

        Log.d(TAG, "excuteRequest: url: " + url);
        if (params != null) {
            Log.d(TAG, "excuteRequest: params: " + params.toString());
            String paramsString = "excuteRequest: params: ";
            for (String key : params.keySet()) {
                paramsString += "\n" + key + ":" + params.get(key);
            }
            Log.d(TAG, paramsString);
        }
        StringRequest stringRequest = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onUpdateDeviceTokenOnServer(response, response != null, null);
                        //System.out.println("Success : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //System.out.println("VolleyError : " + error);
                        if (error instanceof NoConnectionError)
                            Toast.makeText(contaxt, Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_SHORT).show();
                        else if (error instanceof ServerError) {

                            String d = new String(error.networkResponse.data);
                            try {
                                JSONObject jso = new JSONObject(d);
                                Toast.makeText(contaxt, jso.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                listener.onUpdateDeviceTokenOnServer(null, false, error);
                            }

                        }
                        listener.onUpdateDeviceTokenOnServer(null, false, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(contaxt);
        requestQueue.add(stringRequest);
    }

    public interface DeviceTokenServiceListener {
        void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error);
    }
/*
    public static void excuteRequestWithNoAlert(final Context contaxt, final Map<String, String> params, String url, final DeviceTokenServiceListener listener) {
        excuteRequest(contaxt, POST, params, url, listener);
    }
*/
   /* public static void callWithApiKeyAndUserrId(Context context, Map<String, String> params, String url, DeviceTokenServiceListener deviceTokenServiceListener) {
        excuteRequest(context,POST, params, url, deviceTokenServiceListener);
    }
*/
/*
    public static void excuteRequestWithNoAlert(final Context contaxt, int method, final Map<String, String> params, String url, final DeviceTokenServiceListener listener) {

        StringRequest stringRequest = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onUpdateDeviceTokenOnServer(response, response != null, null);
                        //System.out.println("Success : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        //System.out.println("VolleyError : " + error);
//                        if (error instanceof NoConnectionError)
//                            Toast.makeText(contaxt, "No internet available", Toast.LENGTH_SHORT).show();
//                        else if (error instanceof ServerError) {
//
//                            String d = new String(error.networkResponse.data);
//                            try {
//                                JSONObject jso = new JSONObject(d);
//                                Toast.makeText(contaxt, jso.getString("message"), Toast.LENGTH_LONG).show();
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                                listener.onUpdateDeviceTokenOnServer(null, false, error);
//                            }
//
//                        }
                        listener.onUpdateDeviceTokenOnServer(null, false, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(contaxt);
        requestQueue.add(stringRequest);
    }
*/
/*
    public static void excuteRequestGet(final Context contaxt, final Map<String, String> params, String url, final DeviceTokenServiceListener listener) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onUpdateDeviceTokenOnServer(response, response != null, null);
                        //System.out.println("Success : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //System.out.println("VolleyError : " + error);
                        if (error instanceof NoConnectionError)
                            Toast.makeText(contaxt, "No internet available", Toast.LENGTH_SHORT).show();
                        else if (error instanceof ServerError) {

                            String d = new String(error.networkResponse.data);
                            try {
                                JSONObject jso = new JSONObject(d);
                                Toast.makeText(contaxt, jso.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                listener.onUpdateDeviceTokenOnServer(null, false, error);
                            }

                        }
                        listener.onUpdateDeviceTokenOnServer(null, false, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(contaxt);
        requestQueue.add(stringRequest);
    }
*/
}
