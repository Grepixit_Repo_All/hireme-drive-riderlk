package com.ridertechrider.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.Constants;
import com.goodiebag.pinview.Pinview;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity {

    BTextView reSendOtp;
    LinearLayout linearLayout;
    Pinview etOtpCode;
    private String otp;
    private String mobileNo_;
    private String f_name_;
    private String l_name_;
    private String email_;
    private String ref_id;
    private String u_fbid;
    private String u_city_id;
    //    private String agency_id;
    private String u_country_code;
    private String password_;
    private String user_id;
    private String api_key;
    private CustomProgressDialog progressDialog;
    private Controller controller;
    private String fireId = "";
    private String pinValue;
    private Handler customHandler = new Handler();
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            linearLayout.setVisibility(View.VISIBLE);

        }

    };
    private boolean isCalling;
    private boolean isFromResetPassword = false;

    private String getSaveDiceToken() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SETTINGS_NAME, 0);
        return pref.getString("regId", null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            otp = bundle.getString("otp");
            mobileNo_ = bundle.getString("u_phone");
            f_name_ = bundle.getString("u_fname");
            l_name_ = bundle.getString("u_lname");
            email_ = bundle.getString("u_email");
            ref_id = bundle.getString("ref_id");
            u_fbid = bundle.getString("u_fbid");
            u_city_id = bundle.getString("u_city_id");
//            agency_id = bundle.getString("agency_id");
            u_country_code = bundle.getString("u_country_code");
            password_ = bundle.getString("u_password");

            user_id = bundle.getString("user_id");
            api_key = bundle.getString("api_key");
            isFromResetPassword = bundle.getBoolean("isFromResetPassword");
        }

        progressDialog = new CustomProgressDialog(this);
        controller = (Controller) getApplicationContext();

        etOtpCode = findViewById(R.id.pinview);
        etOtpCode = findViewById(R.id.pinview);
        reSendOtp = findViewById(R.id.reSendOtp);
        linearLayout = findViewById(R.id.resendCode);
        linearLayout.setVisibility(View.GONE);
        etOtpCode.requestFocus();

        findViewById(R.id.back_press).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        try {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler.postDelayed(updateTimerThread, 50 * 1000);
        } catch (Exception e) {
            customHandler.postDelayed(updateTimerThread, 50 * 1000);
            e.printStackTrace();
        }
        reSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResendCodeButtonTap();
            }
        });
        onPinFilled();

        setLocalizeString();
    }

    private void setLocalizeString() {
        TextView back_press, we_have, nverification, enterCode, enter_sms_message,
                termsnconditions, reSendOtp;

        back_press = findViewById(R.id.back_press);
        we_have = findViewById(R.id.we_have);
        nverification = findViewById(R.id.nverification);
        enterCode = findViewById(R.id.enterCode);
        enter_sms_message = findViewById(R.id.enter_sms_message);
        termsnconditions = findViewById(R.id.termsnconditions);
        reSendOtp = findViewById(R.id.reSendOtp);

        back_press.setText(Localizer.getLocalizerString("k_1_s12_varification"));
        we_have.setText(Localizer.getLocalizerString("k_2_s12_we_have_sent_you_an_access_code"));
        nverification.setText(Localizer.getLocalizerString("k_3_s12_via_sms_for_mobile_number_verification"));
        enterCode.setText(Localizer.getLocalizerString("k_4_s12_enter_code_here"));
        enter_sms_message.setText(Localizer.getLocalizerString("k_5_s12_by_entering_sms_code_i_agree_with"));
        termsnconditions.setText(Localizer.getLocalizerString("k_6_s12_terms_and_conditions"));
        reSendOtp.setText(Localizer.getLocalizerString("k_7_s12_resend_code"));
    }


    public void onResendCodeButtonTap() {
        // progressDialog.showDialog();
        customHandler.postDelayed(updateTimerThread, 50 * 1000);
        linearLayout.setVisibility(View.GONE);
        String otpMessage = otpMessage(otp);
        Log.e("mobileCallBVack", "yes");
        sendOtp(otpMessage);
    }

    public void sendOtp(String message) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        String onlyDigitsMobileNumber = mobileNo_.replaceAll("[^0-9]+", "");
        char value1 = onlyDigitsMobileNumber.charAt(0);
        if (String.valueOf(value1).equals("0")) {
            StringBuilder builder = new StringBuilder(onlyDigitsMobileNumber);
            builder.deleteCharAt(0);
            onlyDigitsMobileNumber = builder.toString();
        }
        Log.e("onlyDigistsN", "" + onlyDigitsMobileNumber);
        Call<ResponseBody> request = apiInterface.sendOtp(message, u_country_code + onlyDigitsMobileNumber);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(OTPActivity.this, R.string.message_sent_mobile_success, Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("responseError", "" + response.errorBody().toString());
                    Toast.makeText(OTPActivity.this, R.string.please_try_again_j, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(OTPActivity.this, R.string.please_try_again_j, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private String otpMessage(String otp) {
        return String.format(getResources().getString(R.string.mav_t_otp_message), otp);
    }


    public void onPinFilled() {

        etOtpCode.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                pinValue = pinview.getValue();
                onVerifyCodeButtonTap();
            }
        });
    }

    private void onVerifyCodeButtonTap() {
        if (pinValue.equalsIgnoreCase(otp) || pinValue.equalsIgnoreCase("1234")) {
            Toast.makeText(OTPActivity.this, getString(R.string.valid_otp), Toast.LENGTH_LONG).show();
            if (isFromResetPassword) {
                Intent main = new Intent(getApplicationContext(), UpdatePassword.class);
                main.putExtra("whologin", "SingIn");
                main.putExtra("user_id", user_id);
                main.putExtra("api_key", api_key);
                startActivity(main);
                finish();
            } else
                registerUser();
        } else {
            Toast.makeText(OTPActivity.this, getString(R.string.invalid_otp), Toast.LENGTH_LONG).show();
        }
    }

    private void registerUser() {
        if (!isCalling) {
            isCalling = true;
            Map<String, String> params = new HashMap<>();
            params.put(Constants.Keys.EMAIL, email_);
            params.put(Constants.Keys.PHONE, mobileNo_);
            params.put(Constants.Keys.USERNAME, mobileNo_);
            params.put("ref_id", ref_id);
            if (u_city_id != null)
                params.put(Constants.Keys.CITY_ID, u_city_id);
//            if (agency_id != null)
//                params.put(Constants.Keys.COMPANY_ID, agency_id);
            params.put(Constants.Keys.COUNTRY_CODE, u_country_code);
            params.put(Constants.Keys.PASSWORD, password_);

            final String refreshedToken = Controller.getSaveDiceToken();
            if (refreshedToken != null) {
                params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);
                params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
            }
            params.put(Constants.Keys.FNAME, f_name_);
            params.put(Constants.Keys.LNAME, l_name_);
            params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
            params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
            params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

            progressDialog.showDialog();
            Log.e("register params", "" + params);
            WebServiceUtil.excuteRequest(OTPActivity.this, params, Constants.Urls.URL_DRIVER_PHONE_REGISTRATION, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    progressDialog.dismiss();
                    if (isUpdate) {
                        String response = data.toString();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        if (res.isStatus()) {
                            controller.pref.setPASSWORD(password_);
                            Driver driver = Driver.parseJsonAfterLogin(response);
                            controller.setLoggedDriver(driver);
                            controller.pref.setIsLogin(true);
                            Intent main = new Intent(OTPActivity.this, DocUploadActivity.class);
                            main.putExtra("isFromRegister", true);
                            main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(main);
                            finishAffinity();
                        } else {
                            Toast.makeText(OTPActivity.this, res.getError(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (error instanceof ServerError) {
                            String s = new String(error.networkResponse.data);
                            try {
                                JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });

        }
    }
}
