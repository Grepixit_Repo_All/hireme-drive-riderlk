package com.ridertechrider.driver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GetPriceBeanClass {
    private Date startDate;
    private Date endDate;
    private String day;
    private String time;
    private String price_per_km;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
        if (time != null && time.length() > 0) {
            String[] split = time.split("-");
            startDate = dateToString(split[0]);
            endDate = dateToString(split[1]);
        }
    }

    private Date dateToString(String s) {
        if (s != null && s.length() > 0) {
            s = s.trim();
            SimpleDateFormat simpleDateFormat;
            simpleDateFormat = new SimpleDateFormat("hh:mma", Locale.ENGLISH);
            try {
                return simpleDateFormat.parse(s);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getPrice_per_km() {
        return price_per_km;
    }

    public void setPrice_per_km(String price_per_km) {
        this.price_per_km = price_per_km;
    }

    public boolean isbetween(Date currentDate) {
        String time = stringTodATE(currentDate);
        Date dateOnlyTime = dateToString(time);

        if (startDate != null && endDate != null) {
            if ((startDate.getTime() == dateOnlyTime.getTime()) || (endDate.getTime() == dateOnlyTime.getTime())) {
                return true;
            }
            return startDate.before(dateOnlyTime) && endDate.after(dateOnlyTime);
        }

        return false;
    }

    private String stringTodATE(Date currentDate) {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("hh:mma", Locale.ENGLISH);
        return simpleDateFormat.format(currentDate);
    }
}
