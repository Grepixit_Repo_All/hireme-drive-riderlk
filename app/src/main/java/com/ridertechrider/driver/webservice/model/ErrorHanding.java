package com.ridertechrider.driver.webservice.model;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.ResponseBody;

public class ErrorHanding extends BaseResponse {
    public static ErrorHanding parse(ResponseBody responseBody) {
        String string;
        try {
            string = responseBody.string();
            Gson gson = new Gson();
            return gson.fromJson(string, ErrorHanding.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ErrorHanding();
    }
}
