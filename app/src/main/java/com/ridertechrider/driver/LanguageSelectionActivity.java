package com.ridertechrider.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.CustomLangAdapter;
import com.ridertechrider.driver.adaptor.LangModel;
import com.ridertechrider.driver.adaptor.RecyclerItemClickListener;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.WebServiceUtil.DeviceTokenServiceListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LanguageSelectionActivity extends BaseActivity {
    @BindView(R.id.ln_rg_language)
    RadioGroup rgLanguage;
    @BindView(R.id.ln_rbt_english)
    RadioButton rbEnglish;
    //    @BindView(R.id.ln_rbt_portugeuese)
//    RadioButton rbPortugeuese;
    @BindView(R.id.ln_alert_language)
    RelativeLayout rltAlertLanguage;
    @BindView(R.id.ln_alert_language_container)
    LinearLayout lltAlertLanguageConatiner;
    int firstPos;
    private ArrayList<LangModel> langList;
    private Controller controller;
    private int selectedButtonId = 0;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String language = null;
            if (view.getId() == R.id.ln_rbt_english) {
                language = "en";
            }
//            else {
//                language = "ar";
//            }
            Controller controller = (Controller) getApplicationContext();
            if (language != null && !language.equalsIgnoreCase(controller.pref.getSelectedLanguage())) {
                selectedButtonId = view.getId();
                okLanguageSelect();
               /* rltAlertLanguage.setVisibility(View.VISIBLE);
                lltAlertLanguageConatiner.setVisibility(View.VISIBLE);*/
            }

        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Configuration config = newBase.getResources().getConfiguration();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(newBase);
            String language = prefs.getString(Constants.Keys.APP_LANGUAGE, Constants.Language.ENGLISH);
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            config.setLocale(locale);
            newBase = newBase.createConfigurationContext(config);
        }
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);
        langList = new ArrayList<>();
        BTextView tv_rating = findViewById(R.id.tv_rating);
        tv_rating.setText(Localizer.getLocalizerString("k_12_s4_a1_language"));
        controller = (Controller) getApplicationContext();
        if (Build.VERSION.SDK_INT >= 9) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.theme));
            }
        }
        ButterKnife.bind(this);
        rbEnglish.setOnClickListener(onClickListener);
//        rbPortugeuese.setOnClickListener(onClickListener);
        rltAlertLanguage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        Controller controller = (Controller) getApplicationContext();
        if (controller.pref.getSelectedLanguage().equalsIgnoreCase("en")) {
            selectedButtonId = R.id.ln_rbt_english;
        } else {
            selectedButtonId = R.id.ln_rbt_portugeuese;
        }
        changeButtonDrawable(selectedButtonId);
        setLang();
    }

    public void okLanguageSelect() {
        Controller controller = (Controller) getApplicationContext();
        String language = null;
        if (selectedButtonId == R.id.ln_rbt_english) {
            language = "en";
//            rbPortugeuese.setChecked(true);
            rbEnglish.setChecked(false);
        }
//        else {
//            language = "ar";
//            rbPortugeuese.setChecked(false);
//            rbEnglish.setChecked(true);
//        }
        updateLanguageToServer(language);
        rltAlertLanguage.setVisibility(View.GONE);
        changeButtonDrawable(selectedButtonId);
    }

    @OnClick(R.id.ln_bt_back)
    public void onBackButtonTap(View view) {
        onBackPressed();
    }

    private void changeButtonDrawable(int id) {
        if (id == R.id.ln_rbt_english) {
            rbEnglish.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.bg_radio_selected, 0);
//            rbPortugeuese.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.bg_radio_normal, 0);
        }
//        else {
//            rbEnglish.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.bg_radio_normal, 0);
//            rbPortugeuese.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.bg_radio_selected, 0);
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // alert Handle
    @OnClick(R.id.ln_alt_bt_cancel)
    public void onAlertCancelButtonTap(View view) {
        rltAlertLanguage.setVisibility(View.GONE);
    }

    @OnClick(R.id.ln_alt_bt_ok)
    public void onAlertOKButtonTap(View view) {
        Controller controller = (Controller) getApplicationContext();
        String language = null;
        if (selectedButtonId == R.id.ln_rbt_english) {
            language = "en";
//            rbPortugeuese.setChecked(true);
            rbEnglish.setChecked(false);
        }
//        else {
//            language = "ar";
//            rbPortugeuese.setChecked(false);
//            rbEnglish.setChecked(true);
//        }
        updateLanguageToServer(language);
        rltAlertLanguage.setVisibility(View.GONE);
        changeButtonDrawable(selectedButtonId);
    }

    private void updateLanguageToServer(final String language) {
        HashMap<String, String> params = new HashMap<>();
        params.put("d_lang", language);
        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        showProgressBar();
        ServerApiCall.callWithApiKeyAndDriverId(getApplicationContext(), params, Constants.Urls.UPDATE_PROFILE, new DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                hideProgressBar();
                if (isUpdate) {
                    onSusscessSavedLanguageToServer(language);
                } else {
                    ServerApiCall.handleError(getApplicationContext(), error);
                }
            }
        });
    }

    private void onSusscessSavedLanguageToServer(String language) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.edit().putString(Constants.Keys.APP_LANGUAGE, language).commit();
        Controller controller = (Controller) getApplicationContext();
        controller.pref.setSelectedLanguage(language);
        Utils.applyAppLanguage(getApplicationContext());
        Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }

    private void setLang() {
        String localizeLang = controller.pref.getLocalizeLang();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(controller);
        String lang = prefs.getString(Constants.Keys.APP_LANGUAGE, Constants.Language.ENGLISH);
        if (localizeLang != null) {
            JSONObject jsonObject, jsonParseData;
            JSONArray localizeLangJson;
            try {
                jsonObject = new JSONObject(localizeLang);
                if (jsonObject != null && jsonObject.has("response")) {
                    localizeLangJson = jsonObject.getJSONArray("response");
                    for (int i = 0; i < localizeLang.length(); i++) {
                        LangModel langModel = new LangModel();
                        jsonParseData = localizeLangJson.getJSONObject(i);
                        if (lang.equals(jsonParseData.getString("code"))) {
                            langModel.setisChecked(true);
                            firstPos = i;
                        } else
                            langModel.setisChecked(false);
                        langModel.setName(jsonParseData.getString("name"));
                        langModel.setCode(jsonParseData.getString("code"));
                        langList.add(langModel);
                        Log.e("LangData", "" + i + " " + langList);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        RecyclerView recyclerView = findViewById(R.id.lang_list);
        final CustomLangAdapter adapter = new CustomLangAdapter(this, langList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (firstPos != position) {
                    for (int x = 0; x < langList.size(); x++) {
                        if (langList.get(x).getisChecked()) {
                            langList.get(x).setisChecked(false);
                            langList.get(position).setisChecked(true);
                        }
                    }
                    adapter.notifyDataSetChanged();
                    setLanguage(position);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {
                if (firstPos != position)
                    setLanguage(position);
            }
        }));
    }

    void setLanguage(int position) {
        if (position >= 0) {
            LangModel model = langList.get(position);
            Log.e("LanguageSelected", "" + model.getCode());
            updateLanguageToServer(model.getCode());
        }
    }
}
