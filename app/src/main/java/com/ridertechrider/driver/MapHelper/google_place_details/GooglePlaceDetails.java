package com.ridertechrider.driver.MapHelper.google_place_details;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GooglePlaceDetails {

    @SerializedName("html_attributions")
    @Expose
    private List<Object> htmlAttributions = null;
    @SerializedName("result")
    @Expose
    private GooglePlaceDetailsResult result;
    @SerializedName("status")
    @Expose
    private String status;

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public GooglePlaceDetailsResult getResult() {
        return result;
    }

    public void setResult(GooglePlaceDetailsResult result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GooglePlaceDetails parseGooglePlaceDetails(String response) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(response, GooglePlaceDetails.class);
    }


}
