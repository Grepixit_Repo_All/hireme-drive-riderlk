package com.ridertechrider.driver;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ridertechrider.driver.adaptor.TripModel;

/**
 * Created by devin on 2017-10-10.
 */

class UserInfoView {

    private final Context context;
    private final View view;
    private final TripModel tripModel;
    private TextView tvUserMobile;

    public UserInfoView(Context context, View view, TripModel tripModel) {
        this.context = context;
        this.view = view;
        this.tripModel = tripModel;
        init();
    }

    public void setUserInfoViewCallBack(UserInfoViewCallBack callback) {
    }

    public void hide() {
        this.view.setVisibility(View.GONE);
    }

    public void show() {
        this.view.setVisibility(View.VISIBLE);
    }

    private void init() {

        TextView tvUserNme = view.findViewById(R.id.tv_user_name);
        tvUserNme.setText(tripModel.user.getU_name());
        tvUserMobile = view.findViewById(R.id.tv_mobile_number);
        tvUserMobile.setText(tripModel.user.getU_phone());

        ImageView accept = view.findViewById(R.id.iv_user_call_number);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone_no = tvUserMobile.getText().toString();
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone_no));
                if (callIntent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(callIntent);
                }
            }
        });

    }

    private interface UserInfoViewCallBack {
        void onCallButtonCliked();
    }

}
