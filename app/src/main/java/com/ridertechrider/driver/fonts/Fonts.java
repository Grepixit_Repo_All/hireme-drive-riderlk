package com.ridertechrider.driver.fonts;

/**
 * Created by grepix on 7/22/2016.
 */
public interface Fonts {
    String HELVETICA_NEUE = "HelveticaNeue.otf";
    // --Commented out by Inspection START (24-07-2019 02:45):
//    // --Commented out by Inspection (24-07-2019 02:45):String MYRIADPRO_REGULAR = "SFUIDisplay-Regular.otf";
//    String ROBOTO_CONDENCE = "RobotoCondensed-Regular.otf";
// --Commented out by Inspection STOP (24-07-2019 02:45)
    String KARLA = "Karla-Regular.ttf";
}
