package com.ridertechrider.driver;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BButton;
import com.ridertechrider.driver.custom.BEditText;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.Validations;
import com.grepix.grepixutils.WebServiceUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.ridertechrider.driver.webservice.Constants.Urls.URL_DRIVER_LOGIN;
import static com.ridertechrider.driver.webservice.Constants.Urls.URL_DRIVER_PHONE_SIGN_IN;
import static com.ridertechrider.driver.webservice.Constants.Values.USE_EMAIL_AUTH;

public class DriverProfileActivity extends BaseCompatActivity {
    private static final int RESULT_Gallery_IMAGE = 323;
    private static final int CAMERA_REQUEST = 751;
    private CustomProgressDialog progressDialog;
    private EditText etfname, etlname;
    private EditText etOldPswd, etNewPswd, etConfirmPswd;
    private TextView etemail;
    private BTextView etMob;
    private EditText edit_mail;
    private EditText bank_name, bank_user_name, bank_account_number, bank_IFSC_code;
    private LinearLayout linearChangePswd, layoutBankInfo;
    private SwitchCompat switchChngPswd, switch_bank_info;
    private Button btnChange;// btnBack;
    //  private static boolean isChangePswd = true;
    private Controller controller;
    private CircleImageView profileimagezoom;
    private TextView tvUploadDoc;
    private RatingBar ratingBar1;
    private BTextView tvCity;
    private final View.OnClickListener changProfile = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (Validations.isValidateUpdateProfile(getApplicationContext(), etfname, etlname, edit_mail, switchChngPswd, etOldPswd, etNewPswd, etConfirmPswd, controller.pref.getPASSWORD()) && validateBankInfo()) {
                updateProfileApi();
            }
        }
    };
    //    private BTextView tvAgency;
    private View sepratorCity, layoutCity;
//    private View sepratorAgency, layoutAgency;

    /*  Image rotate.......................*/
    private static Bitmap rotateImageIfRequired(Bitmap img, Context context, Uri selectedImage) throws IOException {

        if (Objects.requireNonNull(selectedImage.getScheme()).equals("content")) {
            String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
            Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
            if (Objects.requireNonNull(c).moveToFirst()) {
                final int rotation = c.getInt(0);
                c.close();
                return rotateImage(img, rotation);
            }
            return img;
        } else {
            ExifInterface ei = new ExifInterface(selectedImage.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            Log.d("orientation: %s", "->>>>>>>>  " + orientation);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(img, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(img, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(img, 270);
                default:
                    return img;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        return Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
    }

    private static Bitmap getResizedBitmap(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio < 1 && width > 300) {

            width = 300;
            height = (int) (width / bitmapRatio);
        } else if (height > 300) {
            height = 300;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private static String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile_layout);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        profileimagezoom = findViewById(R.id.profile_img);
        tvCity = findViewById(R.id.tvCity);
//        tvAgency = findViewById(R.id.tvAgency);
//        sepratorAgency = findViewById(R.id.sepratorAgency);
//        layoutAgency = findViewById(R.id.layoutAgency);
        sepratorCity = findViewById(R.id.sepratorCity);
        layoutCity = findViewById(R.id.layoutCity);
        ratingBar1 = findViewById(R.id.ratingBar1);
        edit_mail = findViewById(R.id.edit_email1);
        progressDialog = new CustomProgressDialog(DriverProfileActivity.this);
        controller = (Controller) getApplicationContext();
        findViewById(R.id.recancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        bindActivity();

        ((TextView) findViewById(R.id.refId)).setText(String.format(Localizer.getLocalizerString("k_2_s6_referral_id_col") + " DHME%s", controller.getLoggedDriver().getDriverId()));

        if (USE_EMAIL_AUTH)
            edit_mail.setEnabled(false);
        setLocalizerString();

        fillDriverDetails();

        profileimagezoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        switchChngPswd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (switchChngPswd.isChecked()) {
                    // isChangePswd = false;
                    linearChangePswd.setVisibility(View.VISIBLE);
                } else {
                    linearChangePswd.setVisibility(View.GONE);
                    //isChangePswd = true;
                }
            }
        });
        switch_bank_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (switch_bank_info.isChecked()) {
                    layoutBankInfo.setVisibility(View.VISIBLE);
                } else {
                    layoutBankInfo.setVisibility(View.GONE);
                }
            }
        });
        etNewPswd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    etNewPswd.setError(Localizer.getLocalizerString("k_7_s1_psd_space_nt_allowed"));
                    str = str.replaceAll(" ", "");
                    etNewPswd.setText(str);
                }
            }
        });

        etOldPswd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    etOldPswd.setError(Localizer.getLocalizerString("k_7_s1_psd_space_nt_allowed"));
                    str = str.replaceAll(" ", "");
                    etOldPswd.setText(str);
                }
            }
        });


        etConfirmPswd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    etConfirmPswd.setError(Localizer.getLocalizerString("k_7_s1_psd_space_nt_allowed"));
                    str = str.replaceAll(" ", "");
                    etConfirmPswd.setText(str);
                }
            }
        });

        if (USE_EMAIL_AUTH) {
            layoutCity.setVisibility(View.GONE);
            sepratorCity.setVisibility(View.GONE);
        }

        btnChange.setOnClickListener(changProfile);

        tvUploadDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.setDocUpdate(true);
                Intent intent = new Intent(getApplicationContext(), DocUploadActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setLocalizerString() {
        BTextView tv_edit_profile, refId, mobile_title, tv_upload_documents, tv_first_name;
        BTextView tv_last_name, tv_email, tv_mobile_number, et_mobile, tv_city;//, tv_agency;
        BTextView /*tvAgency,*/tv_update_password, tv_current_password, tv_new_password, tv_confirm_password;
        BEditText firstname1, lastname1, edit_email1, et_old_pswd1, et_new_pswd1, et_confirm_pswd1;
        BButton btn_change;

        EditText bank_name, bank_user_name, bank_account_number, bank_IFSC_code;
        TextView tv_bank_name, tv_bank_user_name, tv_bank_account_number, tv_bank_IFSC_code, tv_bank_info;


        tv_edit_profile = findViewById(R.id.textView14);
        refId = findViewById(R.id.refId);
        mobile_title = findViewById(R.id.mobile_title);
        tv_upload_documents = findViewById(R.id.tv_upload_documents);
        tv_first_name = findViewById(R.id.tv_first_name);
        tv_last_name = findViewById(R.id.tv_last_name);
        tv_email = findViewById(R.id.tv_email);
        tv_mobile_number = findViewById(R.id.tv_mobile_number);
        et_mobile = findViewById(R.id.et_mobile);
        tv_city = findViewById(R.id.tv_city);
//        tv_agency = findViewById(R.id.tv_agency);
//        tvAgency = findViewById(R.id.tvAgency);
        tv_update_password = findViewById(R.id.tv_update_password);
        tv_current_password = findViewById(R.id.tv_current_password);
        tv_new_password = findViewById(R.id.tv_new_password);
        tv_confirm_password = findViewById(R.id.tv_confirm_password);

        firstname1 = findViewById(R.id.firstname1);
        lastname1 = findViewById(R.id.lastname1);
        edit_email1 = findViewById(R.id.edit_email1);
        et_old_pswd1 = findViewById(R.id.et_old_pswd1);
        et_new_pswd1 = findViewById(R.id.et_new_pswd1);
        et_confirm_pswd1 = findViewById(R.id.et_confirm_pswd1);

        tv_bank_info = findViewById(R.id.tv_bank_info);
        bank_name = findViewById(R.id.bank_name);
        bank_user_name = findViewById(R.id.bank_user_name);
        bank_account_number = findViewById(R.id.bank_account_number);
        bank_IFSC_code = findViewById(R.id.bank_IFSC_code);
        tv_bank_name = findViewById(R.id.tv_bank_name);
        tv_bank_user_name = findViewById(R.id.tv_bank_user_name);
        tv_bank_account_number = findViewById(R.id.tv_bank_account_number);
        tv_bank_IFSC_code = findViewById(R.id.tv_bank_IFSC_code);

        btn_change = findViewById(R.id.btn_change);

        btn_change.setText(Localizer.getLocalizerString("k_34_s6_save"));

        tv_edit_profile.setText(Localizer.getLocalizerString("k_1_s6_edit_profile"));
        tv_upload_documents.setText(Localizer.getLocalizerString("k_3_s6_upload_doc"));
        tv_first_name.setText(Localizer.getLocalizerString("k_1_s2_fname"));
        tv_last_name.setText(Localizer.getLocalizerString("k_3_s2_lname"));
        tv_email.setText(Localizer.getLocalizerString("k_13_s1_email"));
        tv_mobile_number.setText(Localizer.getLocalizerString("k_2_s1_mobile_number_hint"));
        tv_city.setText(Localizer.getLocalizerString("k_18_s6_city"));
//        tv_agency.setText(Localizer.getLocalizerString("k_9_s2_agency"));
        tv_update_password.setText(Localizer.getLocalizerString("k_19_s6_update_password"));
        tv_current_password.setText(Localizer.getLocalizerString("k_20_s6_current_password"));
        tv_new_password.setText(Localizer.getLocalizerString("k_21_s6_new_password"));
        tv_confirm_password.setText(Localizer.getLocalizerString("k_21_s6_conf_new_password"));


        firstname1.setHint(Localizer.getLocalizerString("k_1_s2_fname"));
        lastname1.setHint(Localizer.getLocalizerString("k_3_s2_lname"));
        edit_email1.setHint(Localizer.getLocalizerString("k_13_s1_email"));
        et_mobile.setHint(Localizer.getLocalizerString("k_2_s1_mobile_number_hint"));
        tvCity.setHint(Localizer.getLocalizerString("k_18_s6_city"));
//        tvAgency.setHint(Localizer.getLocalizerString("k_9_s2_agency"));
        et_old_pswd1.setHint(Localizer.getLocalizerString("k_20_s6_current_password"));
        et_new_pswd1.setHint(Localizer.getLocalizerString("k_21_s6_new_password"));
        et_confirm_pswd1.setHint(Localizer.getLocalizerString("k_21_s6_conf_new_password"));

        bank_name.setHint(Localizer.getLocalizerString("k_2_s2_bank_name_hint"));
        bank_user_name.setHint(Localizer.getLocalizerString("k_2_s2_bank_user_name_hint"));
        bank_account_number.setHint(Localizer.getLocalizerString("k_2_s2_bank_account_number_hint"));
        bank_IFSC_code.setHint(Localizer.getLocalizerString("k_2_s2_bank_ifsc_code_hint"));

        tv_bank_name.setText(Localizer.getLocalizerString("k_2_s2_bank_name"));
        tv_bank_user_name.setText(Localizer.getLocalizerString("k_2_s2_bank_user_name"));
        tv_bank_account_number.setText(Localizer.getLocalizerString("k_2_s2_bank_account_number"));
        tv_bank_IFSC_code.setText(Localizer.getLocalizerString("k_2_s2_bank_ifsc_code"));

        tv_bank_info.setText(Localizer.getLocalizerString("k_2_s2_bank_info"));
    }

    private boolean validateBankInfo() {

        final String bankName = bank_name.getText().toString().trim();
        final String bankUserName = bank_user_name.getText().toString().trim();
        final String bankAccountNumber = bank_account_number.getText().toString().trim();
        final String bankIFSCCode = bank_IFSC_code.getText().toString().trim();

        if (bankName.length() == 0) {
            Toast.makeText(DriverProfileActivity.this, Localizer.getLocalizerString("k_2_s2_bank_name_hint"), Toast.LENGTH_LONG).show();
            bank_name.requestFocus();
            return false;
        } else if (bankUserName.length() == 0) {
            Toast.makeText(DriverProfileActivity.this, Localizer.getLocalizerString("k_2_s2_bank_user_name_hint"), Toast.LENGTH_LONG).show();
            bank_user_name.requestFocus();
            return false;
        } else if (bankAccountNumber.length() == 0) {
            Toast.makeText(DriverProfileActivity.this, Localizer.getLocalizerString("k_2_s2_bank_account_number_hint"), Toast.LENGTH_LONG).show();
            bank_account_number.requestFocus();
            return false;
        } else if (bankIFSCCode.length() == 0) {
            Toast.makeText(DriverProfileActivity.this, Localizer.getLocalizerString("k_2_s2_bank_ifsc_code_hint"), Toast.LENGTH_LONG).show();
            bank_IFSC_code.requestFocus();
            return false;
        } else return true;
    }

    private void fillDriverDetails() {
        Driver driver = controller.getLoggedDriver();
        etfname = findViewById(R.id.firstname1);
        etfname.setText(driver.getD_fname());
        etlname = findViewById(R.id.lastname1);
        etlname.setText(driver.getD_lname());

        etemail = findViewById(R.id.emailadd);
        etemail.setText(driver.getD_fname() + " " + driver.getD_lname());
        etMob = findViewById(R.id.et_mobile);
        if (USE_EMAIL_AUTH) {
            etMob.setText(driver.getD_phone());
        } else
            etMob.setText(driver.getC_code() + driver.getD_phone());
        edit_mail.setText(driver.getD_email());
        tvCity.setText(controller.getCityName(driver.getCity_id()));

        String bankInfo = driver.getD_bank_info();
        if (bankInfo != null) {
            try {
                JSONObject objBankInfo = new JSONObject(bankInfo);
                bank_name.setText(objBankInfo.getString("bank_name"));
                bank_user_name.setText(objBankInfo.getString("user_name"));
                bank_account_number.setText(objBankInfo.getString("account_number"));
                bank_IFSC_code.setText(objBankInfo.getString("ifsc_code"));
            } catch (Exception e) {
                Log.e("TAG", "updateProfileApi: " + e.getMessage(), e);
            }
        }


        String agancyId = driver.getCompany_id();
//        if (agancyId != null && !agancyId.equalsIgnoreCase("null") && !agancyId.isEmpty()) {
//            tvAgency.setText(controller.getAgencyName(controller.getLoggedDriver().getCompany_id()));
//        } else {
//            sepratorAgency.setVisibility(View.GONE);
//            layoutAgency.setVisibility(View.GONE);
//        }
        driverUpdateProfileRating();
        if (driver.isProfileImagePathEmpty()) {
            String fullpath = Constants.IMAGE_BASE_URL + driver.getD_profile_image_path();
            Picasso.get().load(fullpath).error(R.drawable.circleuser).into(profileimagezoom);
        }


    }

    private void driverUpdateProfileRating() {
        progressDialog.showDialog();
        final Controller controller = (Controller) getApplicationContext();
        Map<String, String> params = new HashMap<>();

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverId(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    Driver driver = Driver.parseJsonAfterLogin(data.toString());
                    controller.setLoggedDriver(driver);
                    String driver_rating = Objects.requireNonNull(driver).getD_rating();
                    if (driver_rating != null) {
                        if (driver_rating.length() != 0) {
                            float rating = Float.parseFloat(driver_rating);
                            ratingBar1.setRating(rating);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void updateProfileApi() {
        progressDialog.showDialog();
        final Controller controller = (Controller) getApplicationContext();

        final String bankName = bank_name.getText().toString().trim();
        final String bankUserName = bank_user_name.getText().toString().trim();
        final String bankAccountNumber = bank_account_number.getText().toString().trim();
        final String bankIFSCCode = bank_IFSC_code.getText().toString().trim();

        Map<String, String> params = new HashMap<>();
        params.put("d_fname", etfname.getText().toString().trim());
        params.put("d_lname", etlname.getText().toString().trim());
        params.put("d_email", edit_mail.getText().toString().trim());

        JSONObject objBankInfo = new JSONObject();
        try {
            objBankInfo.put("bank_name", bankName);
            objBankInfo.put("user_name", bankUserName);
            objBankInfo.put("account_number", bankAccountNumber);
            objBankInfo.put("ifsc_code", bankIFSCCode);
        } catch (Exception e) {
            Log.e("TAG", "updateProfileApi: " + e.getMessage(), e);
        }

        params.put("d_bank_info", objBankInfo.toString());

        params.put("is_send_email", String.valueOf(0));
//        params.put("d_phone", etMob.getText().toString());
//        params.put("city_id", "2");
        if (switchChngPswd.isChecked()) {
            params.put("d_password", etNewPswd.getText().toString());
        }
        Log.e("paramas", "" + params);

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverId(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    String response = data.toString();
                    Log.e("update Profile Response", "" + response);
                    if (response.contains("SMTP")) {
//                        Toast.makeText(DriverProfileActivity.this, R.string.email_is_not_valid, Toast.LENGTH_SHORT).show();
                        reLogin();
                    } else {
                        if (switchChngPswd.isChecked()) {
                            controller.pref.setPASSWORD(etNewPswd.getText().toString());
                            etNewPswd.setText("");
                            etOldPswd.setText("");
                            etConfirmPswd.setText("");
                        }

                        Driver driver = Driver.parseJsonAfterLogin(response);
                        if (driver != null) {
                            controller.setLoggedDriver(driver);
                            fillDriverDetails();
                            Toast.makeText(getApplication(), Localizer.getLocalizerString("k_24_s6_profile_updated_sucessfully"), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
    }

    private void reLogin() {
        if (controller.getLoggedDriver() != null && controller.getCities() != null && controller.getCities().size() > 0) {
            HashMap<String, String> params = new HashMap<>();
            String loginUrl = "";
            if (USE_EMAIL_AUTH) {
                com.ridertechrider.driver.app.Request.Login login = new com.ridertechrider.driver.app.Request.Login();
                if (switchChngPswd.isChecked()) {
                    params = login.addEmail(controller.getLoggedDriver().getD_email())
                            .addPassword(etNewPswd.getText().toString())
                            .addIsAvailable(String.valueOf(1))
                            .build();
                } else {
                    params = login.addEmail(controller.getLoggedDriver().getD_email())
                            .addPassword(controller.getLoggedDriver().getText_password())
                            .addIsAvailable(String.valueOf(1))
                            .build();
                }
                loginUrl = URL_DRIVER_LOGIN;
            } else {
                params.put("username", controller.getLoggedDriver().getD_phone());
                if (switchChngPswd.isChecked()) {
                    params.put("d_password", etNewPswd.getText().toString());
                } else {
                    params.put("d_password", controller.getLoggedDriver().getText_password());
                }
                params.put("c_code", controller.getLoggedDriver().getC_code());
                loginUrl = URL_DRIVER_PHONE_SIGN_IN;
            }
            progressDialog.showDialog();
            Log.e("loginPara,s", "" + params);
            WebServiceUtil.excuteRequest(getApplication(), params, loginUrl, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    if (isUpdate) {
                        String response = data.toString();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        if (res.isStatus()) {

                            if (switchChngPswd.isChecked()) {
                                controller.pref.setPASSWORD(etNewPswd.getText().toString());
                                etNewPswd.setText("");
                                etOldPswd.setText("");
                                etConfirmPswd.setText("");
                            }

                            final Driver driver = Driver.parseJsonAfterLogin(response);
                            if (driver != null) {

                                controller.setLoggedDriver(driver);
                                fillDriverDetails();
                                Toast.makeText(getApplication(), Localizer.getLocalizerString("k_24_s6_profile_updated_sucessfully"), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        progressDialog.dismiss();
                    }
                }
            });
        }
    }

    private void bindActivity() {

        linearChangePswd = findViewById(R.id.linear_change_pswd);
        layoutBankInfo = findViewById(R.id.layoutBankInfo);

        etOldPswd = findViewById(R.id.et_old_pswd1);
        etNewPswd = findViewById(R.id.et_new_pswd1);
        etConfirmPswd = findViewById(R.id.et_confirm_pswd1);
        switchChngPswd = findViewById(R.id.switch_chng_pswd);
        switch_bank_info = findViewById(R.id.switch_bank_info);
        btnChange = findViewById(R.id.btn_change);
        //  btnBack = findViewById(R.id.back_btn);
        tvUploadDoc = findViewById(R.id.tv_upload_documents);


        bank_name = findViewById(R.id.bank_name);
        bank_user_name = findViewById(R.id.bank_user_name);
        bank_account_number = findViewById(R.id.bank_account_number);
        bank_IFSC_code = findViewById(R.id.bank_IFSC_code);

    }

    /*........................camera runtime permisstion......................*/
    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(android.Manifest.permission.CAMERA, getPackageName());
            if (ContextCompat.checkSelfPermission(DriverProfileActivity.this, android.Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setMessage(Localizer.getLocalizerString("k_25_s6_please_allow_camera_permission"))
                        .setTitle(Localizer.getLocalizerString("k_26_s6_permission_required_j"));

                builder.setPositiveButton(Localizer.getLocalizerString("k_18_s4_Ok"), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        makeRequest();
                    }
                });

                androidx.appcompat.app.AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                    final CharSequence[] options = {Localizer.getLocalizerString("k_28_s6_camera_j")
                            , Localizer.getLocalizerString("k_29_s6_gallery_j"),
                            Localizer.getLocalizerString("k_30_s6_cancel_j")};

                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(DriverProfileActivity.this);
                    builder.setTitle(Localizer.getLocalizerString("k_r18_s5_chse_img"));
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals(Localizer.getLocalizerString("k_28_s6_camera_j"))) {
                                dialog.dismiss();
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                            } else if (options[item].equals(Localizer.getLocalizerString("k_29_s6_gallery_j"))) {
                                dialog.dismiss();
                                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, RESULT_Gallery_IMAGE);
                            } else if (options[item].equals(Localizer.getLocalizerString("k_30_s6_cancel_j"))) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();
                } else
                    Toast.makeText(this, Localizer.getLocalizerString("k_31_s6_please_provide_camera_permission"), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, Localizer.getLocalizerString("k_32_s6_camera_permission_error"), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void makeRequest() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 160);
    }

    /*........................Image Operation.....end...........*/

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            profileimagezoom.setImageBitmap(photo);
            profileImageUpload(photo);
        } else if (requestCode == RESULT_Gallery_IMAGE && resultCode == RESULT_OK && null != data) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(Objects.requireNonNull(imageUri));
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                profileimagezoom.setImageBitmap(getResizedBitmap(rotateImageIfRequired(selectedImage, DriverProfileActivity.this, imageUri)));
                profileImageUpload(getResizedBitmap(rotateImageIfRequired(selectedImage, DriverProfileActivity.this, imageUri)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(DriverProfileActivity.this, Localizer.getLocalizerString("k_33_s6_something_went_wrong_j"), Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void profileImageUpload(Bitmap bitmapValue) {
        progressDialog.showDialog();
        final String bitmap64 = convert(bitmapValue);
        Map<String, String> params = new HashMap<>();
        params.put("image_type", "jpg");
        params.put(Constants.D_PROFILE_IMAGE_PATH, bitmap64);

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverId(DriverProfileActivity.this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    progressDialog.dismiss();
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        Driver driver = Driver.parseJsonAfterLogin(response);
                        controller.setLoggedDriver(driver);
                        if (Objects.requireNonNull(driver).isProfileImagePathEmpty()) {
                            String fullpath = Constants.IMAGE_BASE_URL + driver.getD_profile_image_path();
                            Picasso.get().load(fullpath).error(R.drawable.circleuser).into(profileimagezoom);
                        }

                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        bindActivity();
    }
}
