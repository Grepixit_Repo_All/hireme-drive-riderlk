package com.ridertechrider.driver.webservice;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DriverSettings {
    private String setting_id;
    private String stitle;
    private String skey;
    private String svalue;
    private String active;
    private String created;
    private String modified;

    public static ArrayList<DriverSettings> parseDriverSettingsReponse(String s) {
        ArrayList<DriverSettings> settings = new ArrayList<>();
        try {
            JSONObject rootObject = new JSONObject(s);
            JSONArray jsonArray = rootObject.getJSONArray(Constants.Keys.RESPONSE);
            Gson gson = new Gson();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject childObject = jsonArray.getJSONObject(i);
                settings.add(gson.fromJson(childObject.toString(), DriverSettings.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return settings;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSetting_id() {
        return setting_id;
    }

    public void setSetting_id(String setting_id) {
        this.setting_id = setting_id;
    }

    public String getStitle() {
        return stitle;
    }

    public void setStitle(String stitle) {
        this.stitle = stitle;
    }

    public String getSkey() {
        if (skey == null)
            return "";
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public String getSvalue() {
        return svalue;
    }

    public void setSvalue(String svalue) {
        this.svalue = svalue;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }


}
