package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.Model.PlaceAutoComplete;
import com.ridertechrider.driver.R;

import java.util.List;
import java.util.StringTokenizer;


public class AutoCompleteAdapter extends ArrayAdapter<PlaceAutoComplete> {

    private final Context context;
    private final List<PlaceAutoComplete> Places;


    public AutoCompleteAdapter(Context context, List<PlaceAutoComplete> modelsArrayList) {
        super(context, R.layout.autocomplete_row, modelsArrayList);
        this.context = context;
        this.Places = modelsArrayList;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.autocomplete_row, parent, false);
            holder = new ViewHolder();
            holder.location = rowView.findViewById(R.id.place_detail);
            rowView.setTag(holder);

        } else
            holder = (ViewHolder) rowView.getTag();
        holder.Place = Places.get(position);
        StringTokenizer st = new StringTokenizer(holder.Place.getPlaceDesc(), ",");


        StringBuilder desc_detail = new StringBuilder();
        for (int i = 1; i < st.countTokens(); i++) {
            if (i == st.countTokens() - 1) {
                desc_detail.append(st.nextToken());
            } else {
                desc_detail.append(st.nextToken()).append(",");
            }
        }

        try {
            holder.location.setText(Places.get(position).getPlaceDesc());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }

    @Override
    public int getCount() {
        return Places.size();
    }

    class ViewHolder {
        PlaceAutoComplete Place;
        TextView location;
    }
}