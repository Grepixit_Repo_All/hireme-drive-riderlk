package com.ridertechrider.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.ridertechrider.driver.adaptor.TripListAdaptor;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TripRequestFragment extends Fragment {

    private static TripRequestFragment tripRequestFragment;
    private final int limit = 10;
    private final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    private ListView listView;
    private TextView listEmptyTxt;
    private Activity activity;
    private TripListAdaptor adaptor;
    private int offSet = 0;
    private ArrayList<TripModel> tripHistoryList;
    private Controller controller;
    private SwipeRefreshLayout pullToRefresh;

    private TabLayout tab;

    public static TripRequestFragment getInstance() {

        if (tripRequestFragment == null) {
            tripRequestFragment = new TripRequestFragment();
        }
        return tripRequestFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_layout_trips, container, false);
      /*  pullToRefresh = view.findViewById(R.id.pullToRefresh);
        listEmptyTxt = view.findViewById(R.id.list_empty_txt);
        listView = view.findViewById(R.id.req_listView);*/
        ViewPager viewPager = view.findViewById(R.id.viewpager);
        tab = view.findViewById(R.id.tabs);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Intent intent = new Intent("refresh_request_list");
                intent.putExtra("page", i);
                controller.sendBroadcast(intent);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        // listView.setPullLoadEnable(true);
        return view;
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());

        adapter.addFragment(new RequestFragment(), Localizer.getLocalizerString("k_62_s4_request"));
        adapter.addFragment(new AsignedRequestFragment(), Localizer.getLocalizerString("k_63_s4_ongoing"));
        tab.setVisibility(View.VISIBLE);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), Fonts.KARLA);
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        tripHistoryList = new ArrayList<>();
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        // adaptor = new TripListAdaptor(activity, tripHistoryList, typeface, mCallback, this);
        // listView.setXListViewListener(this);
        // listView.setPullLoadEnable(false);
        //setting an setOnRefreshListener on the SwipeDownLayout
        controller = (Controller) getActivity().getApplicationContext();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ClickListenerCallback");
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);

        }
    }

}