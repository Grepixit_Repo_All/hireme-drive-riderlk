package com.grepix.grepixutils;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SwitchCompat;

import android.widget.EditText;
import android.widget.Switch;

import java.util.regex.Pattern;

/**
 * Created by devin on 2017-10-10.
 */

public class Validations {


/*
    public static boolean isValidateLogin(Context context, EditText Email, EditText Psw) {
        boolean isValid = true;
        if (Utils.net_connection_check(context)) {



            String Get_Email = Email.getText().toString();
            Get_Email = Get_Email.toLowerCase();

            if (!Email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")) {
                Email.setError(context.getResources().getString(R.string.required));
                isValid = false;
            } else {
                Email.setError(null);
            }
            if (Psw.getText().toString().length() == 0) {
                Psw.setError(context.getResources().getString(R.string.required));
                isValid = false;
            } else {
                Psw.setError(null);
            }



        } else {
          isValid= false;
            Toast.makeText(context, R.string.please_check_network, Toast.LENGTH_SHORT).show();
        }
        return isValid;

    }
*/

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static boolean isValidMobileNumber(Context context, String phone) {
        Pattern pattern = Pattern.compile("\\d+");
        return (phone.length() <= 9 || phone.length() > 12 || !pattern.matcher(phone).matches());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isValidateUpdateProfile(Context context, EditText etfname, EditText etlname, EditText email, SwitchCompat switchChngPswd, EditText etOldPswd, EditText etNewPswd, EditText etConfirmPswd, String oldPassword) {
        boolean isVaild = true;
        if (Utils.net_connection_check(context)) {
            if (etfname.getText().toString().length() == 0) {
                isVaild = false;
                etfname.setError(context.getResources().getString(R.string.please_enter_your_First_Name));
            } else {
                etfname.setError(null);
            }
            if (etlname.getText().toString().length() == 0) {
                isVaild = false;
                etlname.setError(context.getResources().getString(R.string.please_enter_your_last_Name));
            } else {
                etlname.setError(null);
            }

//            if (!isValidMobileNumber(context,etMob.getText().toString())) {
//                etMob.setError(context.getResources().getString(R.string.please_enter_your_valid_number));
//                isVaild = false;
//            }
            if (email.getText().toString().length() > 0 && !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                email.setError(context.getResources().getString(R.string.email1));
                email.requestFocus();
            } else {
                email.setError(null);
            }


            if (switchChngPswd.isChecked()) {
                if (etOldPswd.getText().toString().length() == 0) {
                    etOldPswd.setError(context.getResources().getString(R.string.current_password_hint));
                    // Toast.makeText(getApplicationContext(), "Old Password does not match", Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    etOldPswd.setError(null);
                }
                if (!etOldPswd.getText().toString().equals(oldPassword)) {
                    etOldPswd.setError(context.getResources().getString(R.string.old_Password_is_not_currect));
                    return false;
                } else {
                    etOldPswd.setError(null);
                }
                if (etNewPswd.getText().toString().length() < 6) {
                    etNewPswd.setError(context.getResources().getString(R.string.Password_should_be_at_least_4_char));
                    return false;
                } else {
                    etNewPswd.setError(null);
                }
                if (etConfirmPswd.getText().toString().length() < 6) {
                    etConfirmPswd.setError(context.getResources().getString(R.string.Password_should_be_at_least_4_char));
                    return false;
                } else {
                    etConfirmPswd.setError(null);
                }
                if (!etNewPswd.getText().toString().equals(etConfirmPswd.getText().toString())) {
                    etConfirmPswd.setError(context.getResources().getString(R.string.new_Password_doesnt_match));
                    return false;
                } else {
                    etConfirmPswd.setError(null);
                }
            }
        } else {
            return false;
        }

        return isVaild;
    }

    public static boolean isValidateUpdatePassword(Context context, EditText edit_new_password, EditText edit_confirm_password) {
        if (edit_new_password.getText().toString().length() < 6) {
            edit_new_password.setError(context.getResources().getString(R.string.password_lenght_should_be_alleast));
            return false;
        } else {
            edit_new_password.setError(null);
        }
        if (edit_confirm_password.getText().toString().length() < 6) {
            edit_confirm_password.setError(context.getResources().getString(R.string.password_lenght_should_be_alleast));
            return false;
        } else {
            edit_confirm_password.setError(null);
        }
        if (!edit_new_password.getText().toString().equals(edit_confirm_password.getText().toString())) {
            edit_confirm_password.setError(context.getResources().getString(R.string.password_not_matched));
            return false;
        } else {
            edit_confirm_password.setError(null);
        }
        return true;
    }

}
