package com.ridertechrider.driver;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ridertechrider.driver.DeliverResponse.Receiver;
import com.ridertechrider.driver.DeliverResponse.Sender;
import com.ridertechrider.driver.adaptor.Trip;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.distance_martix.DistanceMatrixResponse;
import com.ridertechrider.driver.distance_martix.Element;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.grepix.grepixutils.Utils;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    static int t = 0;
    ArrayList<com.ridertechrider.driver.DeliverResponse.Response> driverPojos;
    LinearLayout destinationLayout;
    LinearLayout destinationLinear, paymentDetailsLayout;
    LinearLayout pickup_layout;
    View view;
    TripModel tripModel;
    FrameLayout frame_fragment;
    BTextView tripAmount, paid_by;
    Location currentLocation = null;
    boolean canGopickMap = true;
    boolean canGoDropLocation = true;
    String rejectedTripIds = "";
    CustomProgressDialog customProgressDialog;
    int p = 10;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            com.ridertechrider.driver.DeliverResponse.Response response = (com.ridertechrider.driver.DeliverResponse.Response) view.getTag();
            if (response != null && canGoDropLocation) {
                canGoDropLocation = false;
                Intent intent = new Intent(getApplicationContext(), MainActivityFragment.class);
                intent.putExtra("latitude", response.getDeliveryLatitude());
                intent.putExtra("longitude", response.getDeliveryLongitude());
                intent.putExtra("type", "Drop off Details");
                startActivity(intent);
            }
        }
    };
    View.OnClickListener onClickListener1 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (canGoDropLocation) {
                Intent intent = new Intent(getApplicationContext(), MainActivityFragment.class);
                intent.putExtra("latitude", tripModel.trip.getTrip_scheduled_drop_lat());
                intent.putExtra("longitude", tripModel.trip.getTrip_scheduled_drop_lng());
                intent.putExtra("type", "Drop off Details");
                canGoDropLocation = false;
                startActivity(intent);
            }
        }
    };
    private CustomProgressDialog progressDialog;
    private TextView pickupAddress;
    private TextView deliveryAddress;
    private Button accept;
    private Button reject;
    private Controller controller;
    private RequestFragment.ClickListenerCallback mCallback;
    private APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    private SlideMainActivity activity;
    private String url;
    private LocationRequest mLocationRequest;
    private GoogleApiClient googleApiClient;
    private Location myCurrentLocation;
    private String url1;
    private BTextView pickupDistance;
    private RequestQueue requestQueue;
    private LocationListener locationListenerCurrent;
    private boolean isTripStatusUpdating = false;
    private Call<ResponseBody> callTripApi;
    private View.OnClickListener pickup_layoutListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (canGopickMap) {
                canGopickMap = false;
                Intent intent = new Intent(getApplicationContext(), MainActivityFragment.class);
                intent.putExtra("latitude", tripModel.trip.getTrip_scheduled_pick_lat());
                intent.putExtra("longitude", tripModel.trip.getTrip_scheduled_pick_lng());
                intent.putExtra("type", "Pick Up Details");
                startActivity(intent);
            }
        }
    };

    private void setupGoogleClient() {
        // Google Play Services are available
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(GData.UPDATE_INTERVAL_IN_MILLISECONDS);
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(GData.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
        // Note that location updates are off until the user turns them on
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(RequestActivity.this)
                .addOnConnectionFailedListener(RequestActivity.this)
                .build();
        googleApiClient.connect();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        controller = (Controller) getApplicationContext();
        setupGoogleClient();
        getCurrentLocation();
        customProgressDialog = new CustomProgressDialog(RequestActivity.this);
        rejectedTripIds = controller.pref.getRejectedTrip();
        progressDialog = new CustomProgressDialog(RequestActivity.this);
        pickupAddress = findViewById(R.id.pickupAddress);
        pickupDistance = findViewById(R.id.pickupDistance);
        //deliveryAddress = findViewById(R.id.delivery_address);
        destinationLayout = findViewById(R.id.destinationLayout);
        //destinationLinear = findViewById(R.id.destinationLinear);
        paymentDetailsLayout = findViewById(R.id.paymentDetailsLayout);
        frame_fragment = findViewById(R.id.frame_fragment);
        paid_by = findViewById(R.id.paid_by);
        tripAmount = findViewById(R.id.tripAmount);
        pickup_layout = findViewById(R.id.pickup_layout);
        pickup_layout.setOnClickListener(pickup_layoutListner);
        final Intent i = getIntent();
        tripModel = (TripModel) i.getSerializableExtra("trip_history");
//        System.out.print(tripModel);
        if (Build.VERSION.SDK_INT >= 28) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }


        if (tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
            getDeliveryRequests(view);
            paymentDetailsLayout.setVisibility(View.VISIBLE);
            double totalFare = Double.parseDouble(tripModel.trip.getTrip_pay_amount());
            //Log.("totalFare", "" + totalFare);

            int amou = (int) totalFare;
            // delivery.distance.setText(amou+" km");
            tripAmount.setText(controller.formatAmountWithCurrencyUnit(amou));

            if (tripModel.trip.getPickup_notes() != null)
                pickupAddress.setText(String.format("%s, %s", tripModel.trip.getPickup_notes(),
                        tripModel.trip.getTrip_from_loc()));
            else
                pickupAddress.setText(Objects.requireNonNull(tripModel.trip).getTrip_from_loc());

//            pickupAddress.setText(tripModel.trip.getTrip_from_loc());
            // destinationLinear.setVisibility(View.GONE);
            // deliveryAddress.setVisibility(View.GONE);
        } else if (tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
            paymentDetailsLayout.setVisibility(View.GONE);


            if (tripModel.trip.getPickup_notes() != null)
                pickupAddress.setText(String.format("%s, %s", tripModel.trip.getPickup_notes(),
                        tripModel.trip.getTrip_from_loc()));
            else
                pickupAddress.setText(Objects.requireNonNull(tripModel.trip).getTrip_from_loc());

//            pickupAddress.setText(tripModel.trip.getTrip_from_loc());
            Log.e("tripModel.toloc", "" + tripModel.trip.getTrip_to_loc());
            String destination = tripModel.trip.getTrip_scheduled_drop_lat() + "," + tripModel.trip.getTrip_scheduled_drop_lng();
            String origin = tripModel.trip.getTrip_scheduled_pick_lat() + "," + tripModel.trip.getTrip_scheduled_pick_lng();
            url = calculateDistance(origin, destination);
            Dest delivery = new Dest(getApplicationContext(), R.layout.destination_view, onClickListener1);


            parseDistanceMatrixResponse(delivery.distance, url);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 20, 0, 0);
            view = delivery.getView();
            view.setLayoutParams(layoutParams);
            destinationLayout.addView(view);
            if (tripModel.trip.getTrip_to_loc() != null && !tripModel.trip.getTrip_to_loc().equals("null") && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
                delivery.delivery_address.setText(tripModel.trip.getTrip_to_loc());
            } else if (getIntent().hasExtra("destination")) {
                delivery.delivery_address.setText(getIntent().getStringExtra("destination"));
                Log.e("tripModel.toloc", "" + tripModel.trip.getTrip_to_loc());
            }
        } else {
            paymentDetailsLayout.setVisibility(View.GONE);

            if (tripModel.trip.getPickup_notes() != null)
                pickupAddress.setText(String.format("%s, %s", tripModel.trip.getPickup_notes(),
                        tripModel.trip.getTrip_from_loc()));
            else
                pickupAddress.setText(Objects.requireNonNull(tripModel.trip).getTrip_from_loc());

//            pickupAddress.setText(tripModel.trip.getTrip_from_loc());
        }

        accept = findViewById(R.id.accept);
        reject = findViewById(R.id.reject);
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.stopNotificationSound();
                //progressDialog.showDialog();
                Toast.makeText(RequestActivity.this, R.string.you_rejected_trip_success, Toast.LENGTH_SHORT).show();
                //progressDialog.dismiss();
                String rejectedId = tripModel.trip.getTrip_id();
                Log.e("rejectedTripId", "" + rejectedId);
                if (rejectedTripIds != null && rejectedTripIds.trim().length() != 0) {
                    rejectedTripIds = rejectedTripIds + "," + rejectedId;
                } else {
                    rejectedTripIds = rejectedId;
                }

                controller.pref.setRejectedTrip(rejectedTripIds);
                Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                startActivity(intent);
                finish();
                //updateTripStatusApi(Constants.TripStatus.CANCEL, "cancel", 0);
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.stopNotificationSound();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("trip_history", tripModel);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

    }

    public void showImage(String imageUri) {
        Dialog builder = new Dialog(RequestActivity.this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(RequestActivity.this);
        AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(imageUri, imageView);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

    private void getCurrentLocation() {

        //progressDialog.showDialog();
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationListenerCurrent != null) {
            if (locationManager != null) {
                locationManager.removeUpdates(locationListenerCurrent);
                locationListenerCurrent = null;
            }

        }

        locationListenerCurrent = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                //Log.("currentLocation", "" + currentLocation);
                if (location != null) {
                    String origin1 = location.getLatitude() + "," + location.getLongitude();
                    String destination1 = tripModel.trip.getTrip_scheduled_pick_lat() + "," + tripModel.trip.getTrip_scheduled_pick_lng();
                    url1 = calculateDistance(origin1, destination1);
                    parseDistanceMatrixResponse(pickupDistance, url1);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };
        if (locationManager != null) {
            if (Build.VERSION.SDK_INT >= 28) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            } else {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerCurrent);
        }


    }

    public void parseDistanceMatrixResponse(final TextView view, String url23) {
        final String[] distanceValue = new String[1];


        //Log.("url", ""+url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url23, null,
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson = new Gson();
                        DistanceMatrixResponse distanceMatrixResponse = gson.fromJson(response.toString(), DistanceMatrixResponse.class);
                        if (!distanceMatrixResponse.getStatus().equals("INVALID_REQUEST")) {

                            if (distanceMatrixResponse.getStatus().equalsIgnoreCase("OK")) {
                                try {
                                    List<Element> elements;

                                    elements = distanceMatrixResponse.getRows().get(0).getElements();
                                    for (Element e : elements) {
                                        if (e != null) {
                                            if (e.getDistance() != null) {
                                                if (!e.getStatus().equals("NOT_FOUND") && e.getDistance().getValue() != null) {
                                                    Integer getLastDestToOriginDistance = e.getDistance().getValue();
                                                    //Log.("tripModelDelivery", "" + getLastDestToOriginDistance + "dfdcf");
                                                    if (getLastDestToOriginDistance < 1000) {
                                                        view.setText(getLastDestToOriginDistance + " meter");
                                                    } else {
                                                        distanceValue[0] = String.valueOf((int) (getLastDestToOriginDistance * .001));
                                                        view.setText(distanceValue[0] + " km");
                                                    }

                                                }
                                            }
                                        }

                                    }

                                } catch (ArrayIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                    //Log.d("Error", "Size is zero");
                                }
                            }
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(RequestActivity.this);
        }
        requestQueue.add(jsonObjReq);
    }

    private String calculateDistance(String origins, String destinations) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=");
        urlString.append(origins);
        urlString.append("&destinations=");
        urlString.append(destinations);
        urlString.append("&key=" + getResources().getString(R.string.google_api_key));
        return urlString.toString();
    }

    private void updateTripStatusApi(final String trip_status, final String trip_cancel_reason, float distanceCv) {
        if (Utils.net_connection_check(RequestActivity.this)) {
            Map<String, String> params = new HashMap<>();
            params.put("trip_id", "" + tripModel.trip.getTrip_id());
            params.put(Constants.Keys.TRIP_STATUS, Constants.TripStatus.CANCEL);
            ServerApiCall.callWithApiKey(this, params, Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    isTripStatusUpdating = false;
                    Toast.makeText(RequestActivity.this, R.string.rejected_trip_successfully, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                    startActivity(intent);
                    finish();


                }
            });

        }
    }

    private void sendNotificationToUser(String tripStatus) {
        Map<String, String> params = new HashMap<>();
        String notificationMessage = Constants.Message.DRIVER_CANCEL;
        params.put("message", notificationMessage);
        params.put("trip_id", tripModel.trip.getTrip_id());
        params.put("trip_status", tripStatus);
        notificationStatusApi(params, Constants.Urls.URL_COMMAN_NOTIFICATION, tripStatus);

    }

    private void notificationStatusApi(final Map<String, String> params, String noti_url, final String status) {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> driverNOtificationCall;
        driverNOtificationCall = apiInterface.sendRiderNotrification(params);
        driverNOtificationCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
                Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
// call.cancel();

//Log.d("error failed", "" + t.getLocalizedMessage());
//Log.d("error failed", "" + t.getLocalizedMessage());

            }
        });
    }

    private void cancelPreviousCallingTripApi() {
        if (callTripApi != null) {
            callTripApi.cancel();
            callTripApi = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getDeliveryRequests(final View view) {
        customProgressDialog.showDialog();
        Call<ResponseBody> request = apiInterface.getTripDeliveryResponseList(controller.getLoggedDriver().getApiKey(), tripModel.trip.getTrip_id());
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        handleDeliveryResponse(string, view);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    customProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                customProgressDialog.dismiss();
                if (call.isCanceled()) {

                }
            }
        });
    }

    private void handleDeliveryResponse(String s, View view) {
        try {
            Gson gson = new Gson();
            driverPojos = new ArrayList<>();
            JSONObject jsonRootObject = new JSONObject(s);
            JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject1 = response.getJSONObject(i);
                com.ridertechrider.driver.DeliverResponse.Response parseMultiDeliveryResponse = new com.ridertechrider.driver.DeliverResponse.Response();
                parseMultiDeliveryResponse.setDeliveryId(jsonObject1.getString("delivery_id"));
                parseMultiDeliveryResponse.setSenderId(jsonObject1.getString("sender_id"));
                parseMultiDeliveryResponse.setTripId(jsonObject1.getString("trip_id"));
                parseMultiDeliveryResponse.setDeliveryAddress(jsonObject1.getString("delivery_address"));
                parseMultiDeliveryResponse.setDeliveryLatitude(jsonObject1.getString("delivery_latitude"));
                parseMultiDeliveryResponse.setDeliveryLongitude(jsonObject1.getString("delivery_longitude"));
                parseMultiDeliveryResponse.setPayAmount(jsonObject1.getString("pay_amount"));
                parseMultiDeliveryResponse.setDeliveryStatus(jsonObject1.getString("delivery_status"));
                parseMultiDeliveryResponse.setDeliveryNotes(jsonObject1.getString("delivery_notes"));
                parseMultiDeliveryResponse.setDeliveryRemarks(jsonObject1.getString("delivery_remarks"));
                parseMultiDeliveryResponse.setIsPrepaid(jsonObject1.getString("is_prepaid"));
                parseMultiDeliveryResponse.setDeliveryCreated(jsonObject1.getString("delivery_created"));
                parseMultiDeliveryResponse.setDeliveryModified(jsonObject1.getString("delivery_modified"));
                if (jsonObject1.has("Image")) {
                    JSONArray imageArray = jsonObject1.getJSONArray("Image");
                    if (imageArray != null && imageArray.length() != 0) {
                        parseMultiDeliveryResponse.setDeliveryImage(imageArray.get(0).toString());
                    }
                }

                JSONObject jsonObject2 = jsonObject1.optJSONObject("sender");
                Sender sender = gson.fromJson(String.valueOf(jsonObject2), Sender.class);
                JSONObject jsonObject3 = jsonObject1.optJSONObject("receiver");
                Receiver receiver = gson.fromJson(String.valueOf(jsonObject3), Receiver.class);
                parseMultiDeliveryResponse.setSender(sender);
                parseMultiDeliveryResponse.setReceiver(receiver);
                driverPojos.add(parseMultiDeliveryResponse);

                JSONObject jsonObject4 = jsonObject1.optJSONObject("trip");
                Trip trip = gson.fromJson(String.valueOf(jsonObject4), Trip.class);
//                System.out.print(trip);
            }

            if (driverPojos.size() > 0 || driverPojos != null) {
                AppData.getInstance(RequestActivity.this).setDeliveryList(driverPojos).saveDelivery();
                if (driverPojos.get(0).getIsPrepaid() != null && driverPojos.get(0).getIsPrepaid().equals("1")) {
                    paid_by.setText(Localizer.getLocalizerString("k_r11_s9_sender"));
                } else {
                    paid_by.setText(Localizer.getLocalizerString("k_r12_s9_receiver"));
                }
                for (int i = 0; i < driverPojos.size(); i++) {
                    String drop = driverPojos.get(i).getDeliveryAddress();
                    Context context = null;
                    final Dest delivery = new Dest(getApplicationContext(), R.layout.destination_view, onClickListener);
// delivery.add(deliveryLayout1);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, 20, 0, 0);
                    view = delivery.getView();
                    view.setTag(driverPojos.get(i));
//view.setLayoutParams(layoutParams);
                    //Log.("dropfvcvccb", "" + drop);
                    //Log.("dropfvcvccb", "" + driverPojos.get(i).getDeliveryRemarks());
                    delivery.delivery_address.setText(drop);

                    delivery.distance.setText(driverPojos.get(i).getDeliveryRemarks());
//delivery.dest.setTag(view);
                    destinationLayout.addView(view);
                    if (driverPojos.get(i).getDeliveryImage() != null && driverPojos.get(i).getDeliveryImage().trim().length() != 0) {
                        AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + driverPojos.get(i).getDeliveryImage(), delivery.deliveryImage);
                        delivery.deliveryImage.setVisibility(View.GONE);
                        delivery.deliveryImage.setContentDescription(Constants.IMAGE_BASE_URL + driverPojos.get(i).getDeliveryImage());
                        delivery.viewImage.setVisibility(View.GONE);
                    } else {
                        delivery.deliveryImage.setVisibility(View.GONE);
                        delivery.viewImage.setVisibility(View.GONE);
                    }
                    delivery.deliveryImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showImage(delivery.deliveryImage.getContentDescription().toString());
                        }
                    });
                }

            }

            customProgressDialog.dismiss();

        } catch (Exception e) {
            customProgressDialog.dismiss();
            e.printStackTrace();
        }
    }

    public void onResume() {
        super.onResume();
        canGopickMap = true;
        canGoDropLocation = true;

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}