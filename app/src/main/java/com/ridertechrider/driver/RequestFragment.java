package com.ridertechrider.driver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.TripListAdaptor;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.XListView.XListView;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.DriverConstants;
import com.ridertechrider.driver.webservice.RepeatTimerManager;
import com.google.android.gms.maps.model.LatLng;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestFragment extends Fragment implements XListView.IXListViewListener {

    private static RequestFragment requestFragment;
    private final int limit = 10;
    private final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    public TextView listEmptyTxt;
    private ListView listView;
    private Activity activity;
    private TripListAdaptor adaptor;
    private int offSet = 0;
    private ArrayList<TripModel> tripHistoryList;
    private Controller controller;
    private ClickListenerCallback mCallback;
    private SwipeRefreshLayout pullToRefresh;
    public final RepeatTimerManager repeatTimerManager = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
        @Override
        public void onRepeatPerfrom() {
//            System.out.print("onRepeatPerfrom : " + repeatTimerManager + "  requestFragment:" + requestFragment);
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refresh(false);
                    }
                });
            }
        }

        @Override
        public void onStopRepeatPerfrom() {
//            System.out.print("onStopRepeatPerfrom : " + repeatTimerManager + "  requestFragment:" + requestFragment);
            Controller controller = (Controller) Objects.requireNonNull(getActivity()).getApplication();
            if (controller.getRequestTripResuest() != null) {
                controller.getRequestTripResuest().cancel();
                controller.setRequestTripResuest(null);
            }
        }
    }, 30000);
    private BroadcastReceiver availabilityChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refresh(false);
        }
    };
    private BroadcastReceiver requestPageChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int page = intent.getIntExtra("page", -1);
            if (page == 0) {
                refresh(false);
            }
        }
    };

    public static RequestFragment getInstance() {

        if (requestFragment == null) {
            requestFragment = new RequestFragment();
        }
        return requestFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        repeatTimerManager.startRepeatCall();
    }

    @Override
    public void onPause() {
        super.onPause();
        repeatTimerManager.stopRepeatCall();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_layout, container, false);
        pullToRefresh = view.findViewById(R.id.pullToRefresh);
        listEmptyTxt = view.findViewById(R.id.list_empty_txt);
        listView = view.findViewById(R.id.req_listView);
        // listView.setPullLoadEnable(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), Fonts.KARLA);
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        tripHistoryList = new ArrayList<>();
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        CategoryActors driverCat = null;
        for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
            if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                driverCat = categoryActors;
            }
        }
        adaptor = new TripListAdaptor(activity, tripHistoryList, typeface, mCallback, driverCat, this);
        // listView.setXListViewListener(this);
        // listView.setPullLoadEnable(false);
        //setting an setOnRefreshListener on the SwipeDownLayout
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            int Refreshcounter = 1; //Counting how many times user have refreshed the layout

            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                adaptor.clear();
                refresh(false);
            }
        });
        listView.setAdapter(adaptor);
        controller = (Controller) getActivity().getApplicationContext();

    }

    public void setCallback(ClickListenerCallback mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (ClickListenerCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ClickListenerCallback");
        }

        Controller.getInstance().registerReceiver(availabilityChangedReceiver, new IntentFilter("AVAILABILITY_CHANGED_RECEIVER"));
        Controller.getInstance().registerReceiver(requestPageChangedReceiver, new IntentFilter("refresh_request_list"));
    }

    public void refresh(final boolean isLoadMore) {
        if (!isLoadMore) {
            offSet = 0;
            if (adaptor != null)
                adaptor.clear();
        }
        if (Controller.isActivityVisible() && controller != null) {
            if (controller.getLoggedDriver() != null && controller.getLoggedDriver().getD_is_available() != null &&
                    controller.getLoggedDriver().getD_is_available().equalsIgnoreCase("1") && controller.getLoggedDriver().getD_is_verified()) {
                tripHistoryList.clear();
                getTripRequestList(offSet, isLoadMore);
            } else {
                tripHistoryList.clear();
                Controller.getInstance().stopNotificationSound();
                if (adaptor != null) {
                    CategoryActors driverCat = null;
                    for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                        if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                            driverCat = categoryActors;
                        }
                    }
                    adaptor.setDriverCat(driverCat);
                    adaptor.notifyDataSetChanged();
                }
                listEmptyTxt.setVisibility(View.GONE);
            }
        }
        if (pullToRefresh != null)
            pullToRefresh.setRefreshing(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Controller.getInstance().unregisterReceiver(availabilityChangedReceiver);
        Controller.getInstance().unregisterReceiver(requestPageChangedReceiver);
    }


    public void getTripRequestList(final int nextoffset, final boolean isLoadMore) {
        if (getActivity() != null) {
            if (!((SlideMainActivity) getActivity()).hasLocationPermission)
                return;

            Controller controller = (Controller) (getActivity()).getApplication();
            if (controller.getConstantsList() != null && controller.getConstantsList().size() > 0) {
                String categoryId = controller.getLoggedDriver().getCategory_id();
                String d_lat = controller.pref.getDriver_Lat();
                String d_lng = controller.pref.getDriver_Lng();

                if (d_lat != null && Double.valueOf(d_lat) != 0 && d_lng != null && Double.valueOf(d_lng) != 0) {

                    if (controller.getRequestTripResuest() != null) {
                        repeatTimerManager.stopRepeatCall();
                        repeatTimerManager.startRepeatCall();
                        return;
                    }

                    String radius = "";
                    for (DriverConstants driverConstants : controller.getConstantsList()) {
                        if (driverConstants.getConstant_key().equalsIgnoreCase("driver_radius")) {
                            radius = driverConstants.getConstant_value();
                        }

                    }
                    Call<ResponseBody> request = apiInterface.getRevisedTrips(controller.getLoggedDriver().getApiKey(), categoryId, "1", d_lat, d_lng, radius, "request", String.valueOf(nextoffset), String.valueOf(limit), controller.getLoggedDriver().getDriverId());
                    controller.setRequestTripResuest(request);
                    request.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                try {
                                    String string = response.body().string();
                                    //listView.stopLoadMore();
                                    //listView.stopRefresh();
                                    handleHistoryResponse(string);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    if (repeatTimerManager.isRunning()) {
                                        repeatTimerManager.setTimerForRepeat();
                                    }
                                }
                            }
                            if (repeatTimerManager.isRunning()) {
                                repeatTimerManager.setTimerForRepeat();
                            }

                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            if (call.isCanceled()) {
                                listView.setVisibility(View.GONE);
                                listEmptyTxt.setVisibility(View.VISIBLE);
                            } else {
                                if (repeatTimerManager.isRunning()) {

                                    repeatTimerManager.setTimerForRepeat();
                                }
                            }
                        }
                    });
                }
            } else {
                getConstantApi(nextoffset, isLoadMore);
            }
        }
    }

    private void getConstantApi(final int nextoffset, final boolean isLoadMore) {
        ServerApiCall.callWithApiKey(getActivity(), null, Constants.Urls.GET_CONSTANTS_API, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

                if (isUpdate) {
                    String response = data.toString();
                    controller.setConstantsList(response);
                    getTripRequestList(nextoffset, isLoadMore);
                }
            }
        });

    }

    private void handleHistoryResponse(String s) {
        try {
            JSONObject jsonRootObject = new JSONObject(s);
            if (jsonRootObject.get(Constants.Keys.RESPONSE) instanceof JSONArray) {
                JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
                boolean isHasMoreData = response.length() == limit;
                tripHistoryList.clear();
                String rejectedIds = controller.pref.getRejectedTrip();
                String[] ids = null;
                if (rejectedIds != null && rejectedIds.trim().length() != 0) {
                    ids = rejectedIds.split(",");
                }

                for (int i = 0; i < response.length(); i++) {
                    JSONObject childObject = response.getJSONObject(i);
                    TripModel tripModel = TripModel.parseJson(childObject.toString());
                    boolean isRequiredToShow = true;
                    if (ids != null) {
                        for (String id : ids) {
                            if (id.equals(tripModel.trip.getTrip_id())) {
                                Log.e("rid", "" + id);
                                Log.e("tid", "" + id);
                                isRequiredToShow = false;
                                break;
                            }
                        }
                    }

                    if (isRequiredToShow) {
                        Log.e("tripId", "" + tripModel.trip.getTrip_id());
                        tripHistoryList.add(tripModel);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (tripHistoryList.size() == 0) {
            listView.setVisibility(View.GONE);
            listEmptyTxt.setVisibility(View.VISIBLE);
            listEmptyTxt.setText(Localizer.getLocalizerString("k_52_s4_waiting_new_ride_req"));

        } else {
            listView.setVisibility(View.VISIBLE);
            listEmptyTxt.setVisibility(View.GONE);
        }
        try {
            if (adaptor != null) {
                CategoryActors driverCat = null;
                for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                    if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                        driverCat = categoryActors;
                    }
                }
                adaptor.setDriverCat(driverCat);
                adaptor.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRefresh() {
        refresh(false);
    }

    @Override
    public void onLoadMore() {
        adaptor.clear();
        offSet += limit;
        refresh(true);
    }

    public interface ClickListenerCallback {
        void onAcceptTrip(TripModel tripModel, Fragment requestFragment);

        void viewOnMap(String header, LatLng pickLatLng);

        void viewOnMap(String header, LatLng pickLatLng, LatLng dropLatLng);

        void onTripDetail(TripModel tripModel);

        void onAssignTrip(TripModel tripModel, Fragment requestFragment);

        void onRejectTrip(TripModel tripModel, Fragment requestFragment);

        void onAcceptAssignedTrip(TripModel tripModel, Fragment requestFragment);

        void onRejectTripLater(TripModel tripModel, Fragment requestFragment);

        void onCallRider(TripModel tripModel);

        void onCurrentTripChange(TripModel tripModel);

        void showProgress();

        void onAcceptRSTrip(TripModel tripModel);
    }
}

