package com.ridertechrider.driver.adaptor;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ridertechrider.driver.Model.PickDropSelectionModel;
import com.ridertechrider.driver.VolleySingleton;

public class AppController extends Application {

    private static final String TAG = AppController.class.getSimpleName();
    public static VolleySingleton volleyQueueInstance;
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private PickDropSelectionModel pickDropSelectionModel;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        instantiateVolleyQueue();
        pickDropSelectionModel = new PickDropSelectionModel();

    }

    public PickDropSelectionModel getPickDropSelectionModelObserver() {
        return pickDropSelectionModel;
    }

    private void instantiateVolleyQueue() {
        volleyQueueInstance = VolleySingleton.getInstance(getApplicationContext());
    }

    protected RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
}