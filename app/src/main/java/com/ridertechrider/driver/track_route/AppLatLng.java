package com.ridertechrider.driver.track_route;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by grepixinfotech on 14/02/18.
 */

class AppLatLng implements Serializable {
    final double lat;
    final double lng;

    public AppLatLng(LatLng latLng) {
        lat = latLng.latitude;
        lng = latLng.longitude;
    }
}
