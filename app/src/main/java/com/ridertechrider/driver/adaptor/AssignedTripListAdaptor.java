package com.ridertechrider.driver.adaptor;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ridertechrider.driver.AsignedRequestFragment;
import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.Localizer;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.RequestFragment;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class AssignedTripListAdaptor extends BaseAdapter {
    private final ArrayList<TripModel> tripHistories;
    private final Typeface typeface;
    private final Activity activity;
    private final Controller controller;
    private final RequestFragment.ClickListenerCallback mCallback;
    private final AsignedRequestFragment requestFragment;
    private CategoryActors driverCat;

    public AssignedTripListAdaptor(Activity activity, ArrayList<TripModel> tripHistoryList, Typeface typeface, RequestFragment.ClickListenerCallback callback, CategoryActors driverCat, AsignedRequestFragment tripRequestFragment) {
        this.tripHistories = tripHistoryList;
        this.typeface = typeface;
        this.activity = activity;
        controller = (Controller) activity.getApplication();
        this.mCallback = callback;
        this.requestFragment = tripRequestFragment;
        this.driverCat = driverCat;
    }

    public static String formatHoursAndMinutes(int totalMinutes) {
        String minutes = Integer.toString(totalMinutes % 60);
        minutes = minutes.length() == 1 ? "0" + minutes : minutes;
        return (totalMinutes / 60) + " hours " + minutes + " min remaining to begin";
    }

    @Override
    public int getCount() {
        return tripHistories.size();
    }

    @Override
    public Object getItem(int i) {
        if (tripHistories.size() > i) {
            return tripHistories.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_list_items, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        if (getItem(position) != null) {
            final TripModel tripModel = (TripModel) getItem(position);
            mViewHolder.request_reject.setVisibility(View.GONE);
            if(tripModel.trip.getTrip_customer_details() != null)
                mViewHolder.passengerDetails.setVisibility(View.VISIBLE);
            else
                mViewHolder.passengerDetails.setVisibility(View.GONE);

            if (tripModel.user.isProfileImagePathEmpty()) {
                AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripModel.user.getUProfileImagePath(), mViewHolder.thumbNail);
            } else {
                mViewHolder.thumbNail.setImageResource(R.drawable.user);
            }

            mViewHolder.tvName.setText(tripModel.user.getU_name());

            if (tripModel.trip.getPickup_notes() != null) {
                mViewHolder.tvPickAddress.setText(String.format("%s\n\n%s %s", tripModel.trip.getTrip_from_loc(), Localizer.getLocalizerString("k_1_s8_special_notes"), tripModel.trip.getPickup_notes()));
            } else {
                mViewHolder.tvPickAddress.setText(tripModel.trip.getTrip_from_loc());
            }

            mViewHolder.tvDropAddress.setText(tripModel.trip.getTrip_to_loc());
        /*if (tripHistories.get(position).trip.getIs_ride_later().equals("1")) {
            mViewHolder.requestAcceptButton.setText("Assign");
        }*/
        /*if (tripHistories.get(position).trip.getTrip_status().equals("assigned")){
            mViewHolder.requestAcceptButton.setVisibility(View.GONE);
        }*/

            if (tripModel.trip.getTrip_pay_mode() != null && !tripModel.trip.getTrip_pay_mode().trim().isEmpty() && driverCat != null && driverCat.getShow_paymode().equalsIgnoreCase("1")) {
                mViewHolder.trip_pay_mode.setVisibility(View.VISIBLE);
                mViewHolder.trip_pay_mode.setText(String.format("%s %s", Localizer.getLocalizerString("k_1_s8_pay_via"), tripModel.trip.getTrip_pay_mode()));
            } else {
                mViewHolder.trip_pay_mode.setVisibility(View.GONE);
            }

            if (tripModel.trip.getTrip_pay_amount() != null && !tripModel.trip.getTrip_pay_amount().trim().isEmpty() &&
                    !tripModel.trip.getTrip_pay_amount().trim().equalsIgnoreCase("null") &&
                    !tripModel.trip.getTrip_pay_amount().trim().equalsIgnoreCase("0") && driverCat != null && driverCat.getShow_fare().equalsIgnoreCase("1")) {

                mViewHolder.trip_pay_amount.setVisibility(View.VISIBLE);
                mViewHolder.trip_pay_amount.setText(String.format("%s %s", Localizer.getLocalizerString("k_1_s8_est"), controller.formatAmountWithCurrencyUnit(tripModel.trip.getTrip_pay_amount())));
            } else {
                mViewHolder.trip_pay_amount.setVisibility(View.GONE);
            }


            mViewHolder.dateTrip.setText("@" + Utils.convertServerDateToAppLocalDate(tripModel.trip.getTrip_date()));
            long minutes = printDifference(Utils.stringToDate(tripModel.trip.getTrip_date()));
            Log.e("returnes minutes", "" + minutes);
            if (minutes <= 60) {
                Log.e("minutes less than", "" + minutes);
                mViewHolder.requestAcceptButton.setText(controller.getString(R.string.arrive));
                mViewHolder.requestAcceptButton.setVisibility(View.VISIBLE);
                mViewHolder.countDouwnTextView.setVisibility(View.GONE);
            } else {
                mViewHolder.requestAcceptButton.setVisibility(View.GONE);
                mViewHolder.countDouwnTextView.setVisibility(View.VISIBLE);

                mViewHolder.countDouwnTextView.setText(formatHoursAndMinutes((int) minutes));

            }
            mViewHolder.requestAcceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    controller.stopNotificationSound();
                    if (position != -1 && position < tripHistories.size() && tripHistories.size() > 0) {
                        TripModel tripModel1 = tripHistories.get(position);
//                        if (tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
//                            mCallback.onAcceptTrip(tripModel1, requestFragment);
//                        } else
                            mCallback.onAcceptAssignedTrip(tripModel1, requestFragment);
                    }
                }
            });
            mViewHolder.request_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position != -1 && position < tripHistories.size()) {
                        if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("1")) {
                            controller.stopNotificationSound();
                            mCallback.onRejectTripLater(tripHistories.get(position), requestFragment);
                        }
                    }
                }
            });
            mViewHolder.pickupAddressMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position != -1 && position < tripHistories.size()) {
                        mCallback.viewOnMap(controller.getResources().getString(R.string.trip_detail),
                                new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lat()),
                                        Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lng())),
                                new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lat()),
                                        Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lng())));
                    }
                }
            });
            mViewHolder.dropAddressView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position != -1 && position < tripHistories.size()) {
                        mCallback.viewOnMap(controller.getResources().getString(R.string.trip_detail),
                                new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lat()),
                                        Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lng())),
                                new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lat()),
                                        Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lng())));
                    }
                }
            });
            mViewHolder.passengerDetails.setOnClickListener(v -> {
                final Dialog dialogPassengerDetails = new Dialog(activity);
                dialogPassengerDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogPassengerDetails.setCancelable(true);
                dialogPassengerDetails.setContentView(R.layout.someone_else_dialog);
                TextView passengerTitle = dialogPassengerDetails.findViewById(R.id.passengerTitle);
                passengerTitle.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
                BTextView passengerName = dialogPassengerDetails.findViewById(R.id.passengerName);
                BTextView passengerPhone = dialogPassengerDetails.findViewById(R.id.passengerMobile);
                String details = tripModel.trip.getTrip_customer_details();
                try {
                    JSONObject jsonObject = new JSONObject(details);
                    if(jsonObject.has("p_name"))
                        passengerName.setText(jsonObject.getString("p_name"));
                    if(jsonObject.has("p_phone"))
                        passengerPhone.setText(jsonObject.getString("p_phone"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BTextView passengerNameTv = dialogPassengerDetails.findViewById(R.id.passengerNameTv);
                passengerNameTv.setText(Localizer.getLocalizerString("k_s3_passenger_name"));
                BTextView passengerMobileTv = dialogPassengerDetails.findViewById(R.id.passengerMobileTv);
                passengerMobileTv.setText(Localizer.getLocalizerString("k_s3_passenger_phone"));
                ImageView closeButtonDialog = dialogPassengerDetails.findViewById(R.id.close_button);
                closeButtonDialog.setOnClickListener(v1 -> {
                    dialogPassengerDetails.dismiss();
                });

                dialogPassengerDetails.show();

            });

            mViewHolder.callMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position != -1 && position < tripHistories.size()) {
                        mCallback.onCallRider(tripHistories.get(position));
                    }
                }
            });
        }
        setLocalizeData(mViewHolder);
        return convertView;
    }

    private void setLocalizeData(MyViewHolder mViewHolder) {
        mViewHolder.msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
        mViewHolder.msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        mViewHolder.callMe.setText(Localizer.getLocalizerString("s_2_s10_call_me"));
        mViewHolder.passengerDetails.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
    }

    public void clear() {
        this.tripHistories.clear();
        notifyDataSetChanged();
    }

    public long printDifference(Date endDate) {
        //milliseconds
        long different = endDate.getTime() - (new Date()).getTime();
        if (different <= 0) {
            return 0;
        }
        long seconds = different / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        return minutes;
    }

    public void setDriverCat(CategoryActors driverCat) {
        this.driverCat = driverCat;
    }

    private class MyViewHolder {

        final TextView callMe;
        final TextView tvName;
        final TextView tvPickAddress;
        final TextView tvDropAddress;
        final TextView msa_tv_drop, msa_tv_pickup, passengerDetails;
        final TextView requestAcceptButton, dateTrip, countDouwnTextView;
        final CircleImageView thumbNail;
        TextView trip_pay_mode, trip_pay_amount;
        ImageView pickupAddressMap, dropAddressView, request_reject;

        MyViewHolder(View view) {
            this.callMe = view.findViewById(R.id.callMe);
            this.tvName = view.findViewById(R.id.name);
            this.tvName.setTypeface(typeface);
            this.msa_tv_drop = view.findViewById(R.id.msa_tv_drop);
            this.msa_tv_pickup = view.findViewById(R.id.msa_tv_pickup);
            this.tvPickAddress = view.findViewById(R.id.pick_address);
            this.tvPickAddress.setTypeface(typeface);
            this.tvDropAddress = view.findViewById(R.id.drop_address);
            this.tvDropAddress.setTypeface(typeface);
            this.requestAcceptButton = view.findViewById(R.id.request_accept);
            this.thumbNail = view.findViewById(R.id.icon);
            this.pickupAddressMap = view.findViewById(R.id.pickupAddressMap);
            this.dropAddressView = view.findViewById(R.id.dropAddressMap);
            this.dateTrip = view.findViewById(R.id.date_trip);
            this.request_reject = view.findViewById(R.id.request_reject);
            this.countDouwnTextView = view.findViewById(R.id.countDownTime);
            this.trip_pay_mode = view.findViewById(R.id.trip_pay_mode);
            this.trip_pay_amount = view.findViewById(R.id.trip_pay_amount);
            this.passengerDetails = view.findViewById(R.id.passenger_details);
            request_reject.setVisibility(View.GONE);

        }
    }

}
