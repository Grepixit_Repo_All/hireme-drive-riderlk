package com.ridertechrider.driver;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

import com.ridertechrider.driver.custom.BTextView;


public class AboutUsActivity extends BaseCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        BTextView textView14 = findViewById(R.id.textView14);
        textView14.setText(Localizer.getLocalizerString("k_r1_s3_about_us"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        WebView mywebview = findViewById(R.id.webView1);

        WebSettings webSettings = mywebview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        String url = Controller.getInstance().getAboutUsData();
        mywebview.loadUrl(url);

        ImageButton imageButton = findViewById(R.id.recancel);
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }

        });

    }

}