package com.ridertechrider.driver;

import android.animation.Animator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.DeliverResponse.Receiver;
import com.ridertechrider.driver.DeliverResponse.Sender;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.adaptor.Trip;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BButton;
import com.ridertechrider.driver.custom.BEditText;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.service.PreferencesUtils;
import com.ridertechrider.driver.track_route.TrackRouteSaveData;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.RepeatTimerManager;
import com.ridertechrider.driver.webservice.SingleObject;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.gson.Gson;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ridertechrider.driver.utils.Utils.convertServerDateToAppLocalTime;


public class FareSummaryActivity extends BaseActivity {
    private static final String TAG = FareSummaryActivity.class.getSimpleName();
    @BindView(R.id.fare_ratingbar)
    RatingBar fareRatingBar;
    String fr;
    Context context;
    private boolean isDone = false;
    private TextView tvFareReview;
    private LinearLayout linearLayout;
    private RelativeLayout paymentSubLayout, reviewRatingView;
    private View frameRating, ivRatingIcon, comment;
    private Button btnOffline;
    private Button okPaymentbtn, fare_recieved_cash_button;
    private TextView tvPaymentText;
    private TextView alreadyPaid;
    private Controller controller;
    private TextView droploc;
    private TextView tvPromCodeApplied;
    private TripModel updatedTripModel;
    private boolean isDelivery = false;
    private String deliveryId = null;
    private SingleObject object;
    private CustomProgressDialog progressDialog;
    private String apicalled;

    private boolean isStopMethodCalled = false;
    private TextView pickuploc;
    private TextView dropbtn_home_idloc;
    private TextView distance;
    private ArrayList<Receiver> receiverArray;
    private ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList = null;
    private ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList1;

    private boolean isPrepaid = false;
    private com.ridertechrider.driver.DeliverResponse.Response delivery = null;
    private Boolean isFinshed = true;
    private boolean singleMode = false;
    private BButton home;
    private TextView duration;
    private LinearLayout ratingBarDriver;
    private TextView tvRiderAmountValue;
    private RatingBar ratingBarUser;
    private TextView promo_code_description;
    private Call<ResponseBody> callTripApi;
    private int flag = 1;
    private final RepeatTimerManager repeatTimerManager = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
        long prevTime = 0;

        @Override
        public void onRepeatPerfrom() {
            prevTime = System.currentTimeMillis();
            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel != null) {
                getTrip(false, true);
            }
        }

        @Override
        public void onStopRepeatPerfrom() {

        }
    }, 5000);
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getStringExtra(Config.EXTRA_TRIP_STATUS);
            if (newMessage != null && !newMessage.equalsIgnoreCase("request")) {
                TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                if (tripModel == null) {
                    tripModel = new TripModel();
                }
                tripModel.tripStatus = newMessage;
                tripModel.tripId = intent.getStringExtra(Config.EXTRA_TRIP_ID);
                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();

                if (newMessage.equals("Cash")) {
                    try {
                        getTrip(true, false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (newMessage.equals("accept_payment_promo")) {
                    getTrip(true, false);
                } else {
                    getTrip(true, false);
                    paymentSubLayout.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fare_summary_layout);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//        stopPickUpLocationServiceWithDistance();
//        stopLocationServiceWithDistance();
        progressDialog = new CustomProgressDialog(FareSummaryActivity.this);
        updatedTripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        controller = (Controller) getApplicationContext();
        // handle Intent
        Intent intent = getIntent();
        if (intent != null) {
            String noTiTripSatus = intent.getStringExtra(Config.EXTRA_TRIP_STATUS);
            if (noTiTripSatus != null) {
                TripModel tripModel = new TripModel();
                tripModel.tripId = intent.getStringExtra(Config.EXTRA_TRIP_ID);
                tripModel.tripStatus = intent.getStringExtra(Config.EXTRA_TRIP_STATUS);
                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
            }
        }
        frameRating = findViewById(R.id.frameRating);
        ivRatingIcon = findViewById(R.id.ivRatingIcon);
        comment = findViewById(R.id.comment);
        home = findViewById(R.id.home_button);
        distance = findViewById(R.id.distance);
        duration = findViewById(R.id.duration);
        ratingBarUser = findViewById(R.id.ratingBarUser);
        promo_code_description = findViewById(R.id.promo_code_description);
        reviewRatingView = findViewById(R.id.reviewRatingView);
        reviewRatingView.setVisibility(View.GONE);
        alreadyPaid = findViewById(R.id.alreadyPaid);

        LayerDrawable starsbar = (LayerDrawable) ratingBarUser.getProgressDrawable();
        starsbar.getDrawable(2).setColorFilter(getResources().getColor(R.color.yellow_color), PorterDuff.Mode.SRC_ATOP);


        frameRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reviewRatingView.setVisibility(View.VISIBLE);
            }
        });

        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
        object = SingleObject.getInstance();

        tvFareReview = findViewById(R.id.tv_fare_review);
        getViewId();

        if (getIntent().hasExtra("is_delivery")) {
            isDelivery = true;
            deliveryId = getIntent().getStringExtra("delivery_id");
            deliveryList = AppData.getInstance(FareSummaryActivity.this).getDeliveryList();
            getDeliveryRequests(false, true);
            alreadyPaid.setVisibility(View.VISIBLE);
            for (int i = 0; i < deliveryList.size(); i++) {
                if (deliveryList.get(i).getDeliveryId().equals(deliveryId)) {
                    delivery = deliveryList.get(i);
                    break;
                }
            }
            if (delivery.getIsPrepaid().equals("1")) {
                isPrepaid = true;
//                alreadyPaid.setText(R.string.already_paid);
                alreadyPaid.setText(Localizer.getLocalizerString("k_12_s8_already_paid"));
//                fare_recieved_cash_button.setText(R.string.confirm);
                fare_recieved_cash_button.setText(Localizer.getLocalizerString("k_13_s8_confirm"));
            } else {
                isPrepaid = false;
                alreadyPaid.setTextColor(Color.parseColor("#ff8d5b"));
                alreadyPaid.setText(Localizer.getLocalizerString("k_14_s8_not_pay_yet"));
                fare_recieved_cash_button.setText(Localizer.getLocalizerString("k_8_s8_recieved_cash"));
            }
            String distanceDelivery = delivery.getDeliveryRemarks();

            tvFareReview.setVisibility(View.GONE);
        }
        String trip_pay_amount1 = controller.pref.getTRIP_PAY_AMOUNT();

        if (isDelivery && delivery.getPayAmount() != null && delivery.getPayAmount().length() != 0) {
            trip_pay_amount1 = delivery.getPayAmount();
        }

        float tripPayAmt = 0.0f;
        if (trip_pay_amount1 != null) {
            tripPayAmt = Float.parseFloat(updatedTripModel.trip.getTrip_pay_amount());
        }

        try {
            float promoAmount = 0.0f;
            if (updatedTripModel.trip.getTrip_promo_amt() != null && updatedTripModel.trip.getTrip_promo_amt().trim().length() != 0) {
                promoAmount = Float.parseFloat(updatedTripModel.trip.getTrip_promo_amt());
            }

            if (!isDelivery) {
                tvRiderAmountValue.setText(controller.formatAmountWithCurrencyUnit(updatedTripModel.trip.getTrip_pay_amount()));
                if (updatedTripModel.trip.getTrip_promo_code() != null && updatedTripModel.trip.getTrip_promo_code().trim().length() != 0 && !updatedTripModel.trip.getTrip_promo_code().equals("null")) {
                    promo_code_description.setVisibility(View.VISIBLE);
                    promo_code_description.setText(Localizer.getLocalizerString("k_16_s8_promo_code_applied") + " (" + updatedTripModel.trip.getTrip_promo_code() + ") : " + controller.formatAmountWithCurrencyUnit(promoAmount));
                } else {
                    promo_code_description.setVisibility(View.GONE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        controller.pref.setTRIP_FARE(trip_pay_amount1);

        try {
            boolean layout = object.getFareSummaryLinearLayout();
            if (!layout) {
                paymentSubLayout.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                //reviewRatingView.setVisibility(View.GONE);
                tvFareReview.setVisibility(View.GONE);
            } else {

                linearLayout.setVisibility(View.GONE);
                paymentSubLayout.setVisibility(View.VISIBLE);
                if (controller.pref.getIsSingleMode()) {
                    frameRating.setVisibility(View.GONE);
                    ivRatingIcon.setVisibility(View.GONE);
                    comment.setVisibility(View.GONE);
                } else {
                    frameRating.setVisibility(View.VISIBLE);
                    ivRatingIcon.setVisibility(View.VISIBLE);
                    comment.setVisibility(View.VISIBLE);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


//        if (controller.pref.getIsSingleMode()) {
//            reviewRatingView.setVisibility(View.GONE);
//        }
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    unregisterReceiver(mHandleMessageReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                controller.pref.setTripId("");
                controller.pref.setString("trip_response", null);
                controller.pref.setTripIds("");
                controller.pref.setTripStartTime("");
                controller.pref.setCalculateDistance(true);
                controller.pref.setCalculatedDistance(0);
                PreferencesUtils.setFloat(FareSummaryActivity.this, R.string.recorded_distance, 0);
                controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(0));
                controller.pref.setTRIP_DISTANCE(String.valueOf(0));
                controller.pref.setTRIP_DISTANCE_CHANGEABLE(String.valueOf(0));
                controller.pref.setTRIP_DURATION_CHANGEABLE_INT(0);
                controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(0));
                controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(0));
                TrackRouteSaveData.getInstance(FareSummaryActivity.this).clearData();

                controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
                AppData.getInstance(getApplicationContext()).clearTrip();
                repeatTimerManager.stopRepeatCall();
                driverUpdateProfile(controller.pref.getIsSingleMode());
                controller.pref.setSingleMode(false);
            }
        });
        fare_recieved_cash_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDelivery && isPrepaid) {
                    progressDialog = new CustomProgressDialog(FareSummaryActivity.this);
                    progressDialog.showDialog();
                    updateDeliveryStatus(getIntent().getStringExtra("delivery_id"));
                } else {
                    showdialog(Localizer.getLocalizerString("k_19_s8_hve_u_received_cash"));
                }
            }
        });
        tvFareReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* SingleObject obj = SingleObject.getInstance();
                obj.setFareSummaryLinearLayout(false);
                Intent intent = new Intent(getApplicationContext(), FareReviewActivity.class);
                startActivity(intent);*/
                final Dialog dialog = new Dialog(FareSummaryActivity.this);
                Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fare_review_details);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.90);


                dialog.getWindow().setLayout(width, height);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                window.setGravity(Gravity.CENTER);

                CircleImageView circleImageView = dialog.findViewById(R.id.driver_profile);
                TextView rating = dialog.findViewById(R.id.user_average_rating);
                TextView user_name = dialog.findViewById(R.id.user_name);
                TextView car_name = dialog.findViewById(R.id.car_name);
                RatingBar ratingBar1 = dialog.findViewById(R.id.ratingBar1);
                TextView promo_amount = dialog.findViewById(R.id.promo_amount);
                TextView detaildate = dialog.findViewById(R.id.detaildate);
                TextView pick = dialog.findViewById(R.id.detailpickup1);
                TextView drop = dialog.findViewById(R.id.detaildrop1);
                TextView amount = dialog.findViewById(R.id.total_amount);
                TextView distance = dialog.findViewById(R.id.distance);
                ImageView close = dialog.findViewById(R.id.close);
                TextView tripid = dialog.findViewById(R.id.tripid);
                TextView driverid = dialog.findViewById(R.id.driverid);
                BTextView msa_tv_pickup = dialog.findViewById(R.id.msa_tv_pickup);
                BTextView msa_tv_drop = dialog.findViewById(R.id.msa_tv_drop);

                final TripModel tripHistory = AppData.getInstance(getApplicationContext()).getTripModel();

                TextView tax_amount1 = dialog.findViewById(R.id.tax_amount1);
                View layoutTax = dialog.findViewById(R.id.layoutTax);
                setLocalizeStrings(dialog);

                try {
                    if (Double.parseDouble(tripHistory.trip.getTrip_tax_amt()) > 0) {
                        tax_amount1.setText(controller.formatAmountWithCurrencyUnit(tripHistory.trip.getTrip_tax_amt()));
                    } else {
                        tax_amount1.setText(controller.formatAmountWithCurrencyUnit("0"));
                    }
                } catch (Exception e) {
                    tax_amount1.setText(controller.formatAmountWithCurrencyUnit("0"));
                }

                String categoryId = tripHistory.driver.getCategory_id();
                ArrayList<CategoryActors> categoryResponseList = CategoryActors.parseCarCategoriesResponse(controller.pref.getCategoryResponse());
                String catgoryName = "";
                for (CategoryActors catagories : categoryResponseList) {
                    if (catagories.getCategory_id().equals(categoryId)) {
                        catgoryName = catagories.getCat_name();
                    }
                }

                tripid.setText(tripHistory.trip.getTrip_id());

                driverid.setText(tripHistory.driver.getDriverId());
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });

                float ratings = Float.parseFloat(tripHistory.user.getRating());
                rating.setText(String.valueOf(new DecimalFormat("##.#").format(ratings)));
                ratingBar1.setRating(Float.parseFloat(tripHistory.user.getRating()));
                user_name.setText(tripHistory.user.getU_name());
                car_name.setText(catgoryName);
                TextView waitTime = dialog.findViewById(R.id.tvWaitingTime);
                String waitDuration = tripHistory.trip.getWait_duration();
                if (waitDuration != null && !waitDuration.equals("") && !waitDuration.equalsIgnoreCase("null")) {
                    waitTime.setText(String.format("%s %s", waitDuration, getString(R.string.minute)));
                } else {
                    waitTime.setText(String.format("0 %s", getString(R.string.minute)));
                }
                detaildate.setText(Utils.convertServerDateToAppLocalDateOnly(tripHistory.trip.getTrip_date()));
                Double amountDouble1 = Double.valueOf(tripHistory.trip.getTrip_pay_amount());
                distance.setText(controller.formatDistanceWithUnit(tripHistory.trip.getTrip_distance()));
                amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));

                if (tripHistory.trip.getTrip_promo_code() != null && tripHistory.trip.getTrip_promo_code().trim().length() != 0 && !tripHistory.trip.getTrip_promo_code().equals("null")) {
                    promo_amount.setText(String.format("%s %s", tripHistory.trip.getTrip_promo_code(), controller.formatAmountWithCurrencyUnit(tripHistory.trip.getTrip_promo_amt())));
                } else {
                    promo_amount.setText(controller.formatAmountWithCurrencyUnit(0f));
                }

                if (tripHistory.trip != null && tripHistory.trip.getActual_from_loc() != null && tripHistory.trip.getActual_from_loc().trim().length() != 0) {
                    if (tripHistory.trip.getPickup_notes() != null)
                        pick.setText(String.format("%s, %s", tripHistory.trip.getPickup_notes(), tripHistory.trip.getActual_from_loc()));
                    else
                        pick.setText(tripHistory.trip.getActual_from_loc());
//                    pick.setText(tripHistory.trip.getActual_from_loc());
                } else {


                    if (Objects.requireNonNull(tripHistory.trip).getPickup_notes() != null)
                        pick.setText(String.format("%s, %s", Objects.requireNonNull(tripHistory.trip).getPickup_notes(),
                                Objects.requireNonNull(tripHistory.trip).getTrip_from_loc()));
                    else
                        pick.setText(Objects.requireNonNull(tripHistory.trip).getTrip_from_loc());

//                    pick.setText(Objects.requireNonNull(tripHistory.trip).getTrip_from_loc());
                }

                if (tripHistory.trip != null && tripHistory.trip.getActual_to_loc() != null && tripHistory.trip.getActual_to_loc().trim().length() != 0) {
                    drop.setText(tripHistory.trip.getActual_to_loc());

                } else {
                    drop.setText(Objects.requireNonNull(tripHistory.trip).getTrip_to_loc());

                }
                if (tripHistory.user.isProfileImagePathEmpty()) {
                    AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripHistory.user.getUProfileImagePath(), circleImageView);
                }

                if (convertServerDateToAppLocalTime(tripHistory.trip.getTrip_pickup_time()) != null && !convertServerDateToAppLocalTime(tripHistory.trip.getTrip_pickup_time()).equalsIgnoreCase("nul")) {
                    msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location") + " @ " + convertServerDateToAppLocalTime(tripHistory.trip.getTrip_pickup_time()));
                }
                if (convertServerDateToAppLocalTime(tripHistory.trip.getTrip_drop_time()) != null && !convertServerDateToAppLocalTime(tripHistory.trip.getTrip_drop_time()).equalsIgnoreCase("nul")) {
                    msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location") + " @ " + convertServerDateToAppLocalTime(tripHistory.trip.getTrip_drop_time()));
                }

                dialog.show();

            }
        });

        BButton skipBUtton = findViewById(R.id.fare_rating_skip);
        BButton doneRAting = findViewById(R.id.fare_rating_done);
        doneRAting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDone = true;
                TripModel currentTrip = updatedTripModel;
                if (currentTrip == null) {
                    return;
                }
                progressDialog.showDialog();
                float totalRating = 0;
                int ratingCount = 0;
                fareRatingBar = findViewById(R.id.fare_ratingbar);

                LayerDrawable stars = (LayerDrawable) fareRatingBar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.yellow_color), PorterDuff.Mode.SRC_ATOP);

                float rating = fareRatingBar.getRating();
                try {
                    totalRating = Float.parseFloat(currentTrip.user.getRating());
                    ratingCount = Integer.parseInt(currentTrip.user.getRating_count());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int totalCOunt = ratingCount + 1;
                totalRating = (totalRating * ratingCount) + rating;
                float updatedRating = totalRating / totalCOunt;

                HashMap<String, String> params = new HashMap<>();
                params.put("rating", String.valueOf(updatedRating));
                params.put("rating_count", String.valueOf(totalCOunt));
                params.put("api_key", currentTrip.user.getUser_api_key());
                params.put("user_id", currentTrip.user.getUserId());
                Log.d("url", "" + Constants.Urls.UPDATE_USER_PROFILE);
                ratingBarUser.setRating(rating);
                Log.d("params", "" + params);

                HashMap<String, String> params1 = new HashMap<>();
                params1.put("user_rating", String.valueOf(rating));
                BEditText feedBackView = findViewById(R.id.feedback);
                if (feedBackView != null && feedBackView.getText() != null) {
                    String feedBack = feedBackView.getText().toString();
                    params1.put("user_feefback", feedBack);
                    updateTripRating(params1);
                }
                WebServiceUtil.excuteRequest(FareSummaryActivity.this, params, Constants.Urls.UPDATE_USER_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        progressDialog.dismiss();
                        reviewRatingView.setVisibility(View.GONE);
                        if (isUpdate) {
                            Log.w("dataUpdateds", "" + data);
                            //reviewRatingView.setVisibility(View.GONE);
                        } else {
                            Log.w("error", "" + error.getLocalizedMessage());
                            Log.w("failed", "yes");
                        }
                    }
                });
            }
        });
        skipBUtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //isDone = true;
                //linearLayout.setVisibility(View.VISIBLE);
                //reviewRatingView.setVisibility(View.GONE);
                //hideViewWithAnimation(reviewRatingView);
                //hideViewWithAnimation(reviewRatingView);
                reviewRatingView.setVisibility(View.GONE);
            }
        });
        /*btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHandleMessageReceiver != null) {
                    unregisterReceiver(mHandleMessageReceiver);
                }
                driverUpdateProfile("1", false, "0");
                controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
                AppData.getInstance(getApplicationContext()).clearTrip();
                repeatTimerManager.stopRepeatCall();

                Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                startActivity(intent);
                finishAffinity();
                linearLayout.setVisibility(View.GONE);
                reviewRatingView.setVisibility(View.GONE);
            }
        });
*/
        btnOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    unregisterReceiver(mHandleMessageReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                controller.pref.setTripId("");
                controller.pref.setString("trip_response", null);
                controller.pref.setTripIds("");
                controller.pref.setTripStartTime("");
                controller.pref.setCalculateDistance(true);
                controller.pref.setCalculatedDistance(0);
                PreferencesUtils.setFloat(FareSummaryActivity.this, R.string.recorded_distance, 0);
                controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(0));
                controller.pref.setTRIP_DISTANCE(String.valueOf(0));
                controller.pref.setTRIP_DISTANCE_CHANGEABLE(String.valueOf(0));
                controller.pref.setTRIP_DURATION_CHANGEABLE_INT(0);
                controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(0));
                controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(0));
                TrackRouteSaveData.getInstance(FareSummaryActivity.this).clearData();

                controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
                AppData.getInstance(getApplicationContext()).clearTrip();
                repeatTimerManager.stopRepeatCall();
//                driverUpdateProfile(controller.pref.getIsSingleMode());
                controller.pref.setSingleMode(false);

                logoutApi(controller);
                linearLayout.setVisibility(View.GONE);
            }
        });
        okPaymentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentSubLayout.setVisibility(View.GONE);

                Timer buttonTimer = new Timer();
                buttonTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    paymentSubLayout.setVisibility(View.VISIBLE);
                                    okPaymentbtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            controller.setFareReviewCalled("called");
                                            btnOffline.setEnabled(true);
                                            btnOffline.setEnabled(true);
                                            // sendNotificationToUser();
                                            paymentDone();
                                            paymentSubLayout.setVisibility(View.GONE);
                                            object.setFareSummaryLinearLayout(true);

                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }, 4000);


            }
        });


        registerReceiver(mHandleMessageReceiver, new IntentFilter(Config.DISPLAY_MESSAGE_ACTION));

        setLocalizeData();
    }

    private void setLocalizeData() {
        BTextView tv_title, cost_text, msa_tv_pickup, msa_tv_drop, btvFareSummaryDistance, btvFareSummaryDuration;
        BTextView tv_fare_review, comment;
        BButton btn_offline_id, fare_recieved_cash_button;
        BTextView bTextView;
        BEditText feedback;
        BButton fare_rating_skip, fare_rating_done, home;

        tv_title = findViewById(R.id.tv_title);
        cost_text = findViewById(R.id.cost_text);
        msa_tv_pickup = findViewById(R.id.msa_tv_pickup);
        msa_tv_drop = findViewById(R.id.msa_tv_drop);
        btvFareSummaryDistance = findViewById(R.id.btvFareSummaryDistance);
        btvFareSummaryDuration = findViewById(R.id.btvFareSummaryDuration);
        tv_fare_review = findViewById(R.id.tv_fare_review);
        comment = findViewById(R.id.comment);
        btn_offline_id = findViewById(R.id.btn_offline_id);
        fare_recieved_cash_button = findViewById(R.id.fare_recieved_cash_button);
        bTextView = findViewById(R.id.BTextView);
        feedback = findViewById(R.id.feedback);
        fare_rating_skip = findViewById(R.id.fare_rating_skip);
        fare_rating_done = findViewById(R.id.fare_rating_done);
        home = findViewById(R.id.home_button);


        tv_title.setText(Localizer.getLocalizerString("k_1_s8_fare_summary"));
        cost_text.setText(Localizer.getLocalizerString("k_2_s8_amount_payable"));
        msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
        msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        btvFareSummaryDistance.setText(Localizer.getLocalizerString("k_3_s8_distance"));
        btvFareSummaryDuration.setText(Localizer.getLocalizerString("k_4_s8_duration"));
        tv_fare_review.setText(Localizer.getLocalizerString("k_5_s8_fare_review"));
        comment.setText(Localizer.getLocalizerString("k_7_s8_comment"));
        btn_offline_id.setText(Localizer.getLocalizerString("k_9_s8_go_offline"));
        fare_recieved_cash_button.setText(Localizer.getLocalizerString("k_8_s8_recieved_cash"));
        bTextView.setText(Localizer.getLocalizerString("k_20_s8_plz_rate_rider"));
        feedback.setHint(Localizer.getLocalizerString("k_21_s8_feedback"));
        fare_rating_skip.setText(Localizer.getLocalizerString("k_22_s8_skip"));
        fare_rating_done.setText(Localizer.getLocalizerString("k_23_s8_done"));

        home.setText(Localizer.getLocalizerString("k_r30_s9_home"));

    }


    private void setLocalizeStrings(Dialog dialog) {
        TextView msa_tv_pickup, msa_tv_drop, waiting_label, promo_label, distance_label, tax_layout, cost_label, tripidtext, driveridtext;

        msa_tv_pickup = dialog.findViewById(R.id.msa_tv_pickup);
        tripidtext = dialog.findViewById(R.id.tripidtext);
        driveridtext = dialog.findViewById(R.id.driveridtext);
        msa_tv_drop = dialog.findViewById(R.id.msa_tv_drop);
        waiting_label = dialog.findViewById(R.id.waiting_label);
        promo_label = dialog.findViewById(R.id.promo_label);
        distance_label = dialog.findViewById(R.id.distance_label);
        tax_layout = dialog.findViewById(R.id.tax_layout);
        cost_label = dialog.findViewById(R.id.cost_label);

        msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location"));
        msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        waiting_label.setText(Localizer.getLocalizerString("k_3_s11_wait_time"));
        promo_label.setText(Localizer.getLocalizerString("k_4_s11_promo"));
        distance_label.setText(Localizer.getLocalizerString("k_3_s8_distance"));
        tax_layout.setText(Localizer.getLocalizerString("k_6_s11_taxes"));
        cost_label.setText(Localizer.getLocalizerString("k_7_s11_ride_cost"));
        tripidtext.setText(Localizer.getLocalizerString("k_8_s11_trip_id"));
        driveridtext.setText(Localizer.getLocalizerString("k_9_s11_driver_id"));

    }


    public void updateTripRating(HashMap<String, String> params) {

        params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey());
        params.put(Constants.Keys.TRIP_ID, updatedTripModel.trip.getTrip_id());
        params.put(Constants.Keys.DRIVER_ID, controller.getLoggedDriver().getDriverId());
        //System.out.println("tripRating update : " + params);
        ServerApiCall.callWithApiKey(this, params, Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {

                }
            }
        });
    }

    private void updateDeliveryStatus(final String delivery_id) {
        if (com.grepix.grepixutils.Utils.net_connection_check(FareSummaryActivity.this)) {
            final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel == null) {
                return;
            }
            Map<String, String> params = new HashMap<>();
            params.put("api_key", controller.getLoggedDriver().getApiKey());
            params.put("delivery_id", delivery_id);
            params.put("delivery_status", Constants.DeliveryStatus.PAID);
            params.put("delivered_at", AppUtil.getCurrentDateInGMTZeroInServerFormat());

            ServerApiCall.callWithApiKey(this, params, Constants.Urls.UPDATE_DELIVERY_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    if (isUpdate) {
                        getDeliveryRequests(false, false);
                        alreadyPaid.setText(Localizer.getLocalizerString("k_12_s8_already_paid"));
                        alreadyPaid.setTextColor(Color.parseColor("#4aae6f"));
                        deliveryList = AppData.getInstance(FareSummaryActivity.this).getDeliveryList();
                        for (int i = 0; i < deliveryList.size(); i++) {
                            if (!deliveryList.get(i).getDeliveryStatus().equals("paid") && !deliveryList.get(i).getDeliveryId().equals(delivery_id)) {
                                isFinshed = false;
                                break;
                            }
                        }

                        if (!isFinshed) {
                            getDeliveryRequests(true, false);
                        } else {

                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                            if (delivery.getIsPrepaid().equals("0")) {
                                paymentDone();
                                sendNotificationToUser();
                            } else {
                                paymentDone();
                            }
                        }
                    } else {
                        progressDialog.dismiss();
                    }

                }
            });

        }
    }

    private void driverUpdateProfile(final boolean isSingleMode) {
        try {

            final Controller controller = (Controller) getApplicationContext();
            String refreshedToken = Controller.getSaveDiceToken();
            Map<String, String> params = new HashMap<>();
            params.put(Constants.Keys.D_IS_AVAILABLE, "1");
            if (refreshedToken != null) {
                params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
            }

            params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);
            params.put("d_degree", "176.555");

            String upudateProfileApiTag = "upudateProfileApiTag";


            params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
            params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
            params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

            ServerApiCall.callWithApiKeyAndDriverId(FareSummaryActivity.this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

                    try {
                        progressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (isUpdate) {
                        String response = data.toString();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        if (res.isStatus()) {
                            Driver driver = Driver.parseJsonAfterLogin(response);
                            Objects.requireNonNull(driver).setIsOnline("yes");
                            controller.setDriver(driver, getApplicationContext());
                            String d_is_available = driver.getD_is_available();
                            controller.pref.setD_IS_AVAILABLE(d_is_available);
                            controller.getLoggedDriver().setIsOnline("1");
                            Intent intent;
                            if (isSingleMode) {
                                intent = new Intent(getApplicationContext(), SingleModeActivity.class);
                            } else {
                                intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                            }
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }, upudateProfileApiTag);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDeliveryRequests(final boolean finish1, final boolean sendNotification) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.trip != null) {
            Call<ResponseBody> request = apiInterface.getTripDeliveryResponseList(controller.getLoggedDriver().getApiKey(), tripModel.trip.getTrip_id());
            request.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            String string = response.body().string();
                            handleDeliveryResponse(string, finish1, sendNotification);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    call.isCanceled();
                }
            });
        }
    }

    private void handleDeliveryResponse(String s, Boolean finish, boolean sendNotification) {
        receiverArray = new ArrayList<>();

        try {
            Gson gson = new Gson();
            deliveryList1 = new ArrayList<>();
            JSONObject jsonRootObject = new JSONObject(s);
            JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject1 = response.getJSONObject(i);
                com.ridertechrider.driver.DeliverResponse.Response parseMultiDeliveryResponse = new com.ridertechrider.driver.DeliverResponse.Response();
                parseMultiDeliveryResponse.setDeliveryId(jsonObject1.getString("delivery_id"));
                parseMultiDeliveryResponse.setSenderId(jsonObject1.getString("sender_id"));
                parseMultiDeliveryResponse.setTripId(jsonObject1.getString("trip_id"));
                parseMultiDeliveryResponse.setDeliveryAddress(jsonObject1.getString("delivery_address"));
                parseMultiDeliveryResponse.setDeliveryLatitude(jsonObject1.getString("delivery_latitude"));
                parseMultiDeliveryResponse.setDeliveryLongitude(jsonObject1.getString("delivery_longitude"));
                parseMultiDeliveryResponse.setPayAmount(jsonObject1.getString("pay_amount"));
                parseMultiDeliveryResponse.setDeliveryStatus(jsonObject1.getString("delivery_status"));
                parseMultiDeliveryResponse.setDeliveryNotes(jsonObject1.getString("delivery_notes"));
                parseMultiDeliveryResponse.setDeliveryRemarks(jsonObject1.getString("delivery_remarks"));
                parseMultiDeliveryResponse.setIsPrepaid(jsonObject1.getString("is_prepaid"));
                parseMultiDeliveryResponse.setDeliveryCreated(jsonObject1.getString("delivery_created"));
                parseMultiDeliveryResponse.setDeliveryModified(jsonObject1.getString("delivery_modified"));
                parseMultiDeliveryResponse.setDel_time(jsonObject1.getString("del_time"));
                parseMultiDeliveryResponse.setDeliveryOrder(jsonObject1.getString("delivery_order"));

                deliveryList1.add(parseMultiDeliveryResponse);
                JSONObject jsonObject2 = jsonObject1.optJSONObject("sender");
                Sender sender = gson.fromJson(String.valueOf(jsonObject2), Sender.class);
                JSONObject jsonObject3 = jsonObject1.optJSONObject("receiver");
                Receiver receiver = gson.fromJson(String.valueOf(jsonObject3), Receiver.class);
                receiverArray.add(receiver);
                JSONObject jsonObject4 = jsonObject1.optJSONObject("trip");
                Trip trip = gson.fromJson(String.valueOf(jsonObject4), Trip.class);
//                System.out.print(trip);
            }
            int k = 20;
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (sendNotification) {
                sendDeliveryReceiverNotification(deliveryId);
            }
            deliveryList1.size();
            if (finish) {
                AppData.getInstance(FareSummaryActivity.this).setDeliveryList(deliveryList1).saveDelivery();
                Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                intent.putExtra("moreDelivery", "yes");
                startActivity(intent);
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDeliveryReceiverNotification(String delivery_id) {
        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

        if (tripModel.trip.getIs_delivery() != null &&
                !tripModel.trip.getIs_delivery().equals("null") &&
                tripModel.trip.getIs_delivery().equals("1") &&
                deliveryList.size() > 0) {

            for (int i = 0; i < deliveryList.size(); i++) {

                if (receiverArray != null && receiverArray.size() != 0 && receiverArray.get(i).getUDeviceToken() == null || receiverArray.get(i).getUDeviceToken().equalsIgnoreCase("")) {

                } else if (deliveryList1.get(i).getDeliveryStatus().equals(Constants.DeliveryStatus.Completed) && deliveryList1.get(i).getDeliveryId().equals(delivery_id)) {
                    String notificationMessage = "Delivery Status Updated ";
                    deliveryId = deliveryList1.get(i).getDeliveryId();
                    Map<String, String> params = new HashMap<>();
                    //Log.("deliveryId", "" + deliveryId);

                    if (deliveryId != null) {
                        params.put("delivery_id", deliveryId);
                        params.put("message", notificationMessage);
                        params.put("trip_id", tripModel.trip.getTrip_id());
                        params.put("trip_status", "completed");
                        String u_device_type = receiverArray.get(i).getUDeviceType();
                        if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                            params.put(Constants.Keys.ANDROID, receiverArray.get(i).getUDeviceToken());
                        } else {
                            params.put(Constants.Keys.IOS, receiverArray.get(i).getUDeviceToken());
                        }
                        notificationStatusApiAfterTripBegin(params);

                    }
                    break;
                }
            }
        }
    }


    private void notificationStatusApiAfterTripBegin(final Map<String, String> params) {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> driverNOtificationCall;
        driverNOtificationCall = apiInterface.sendRiderNotrification(params);
        driverNOtificationCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "onResponse: " + response);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t);
            }
        });
    }

    private void showViewWithAnimation(final View view) {
        view.setAlpha(0);
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void hideViewWithAnimation(final View view) {

        view.setAlpha(1);
        view.animate().alpha(0).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void showdialog(String messag) {
        final Dialog dialog = new Dialog(FareSummaryActivity.this);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_layout);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        TextView title2 = dialog.findViewById(R.id.tv_dialog_title);
        Button yes = dialog.findViewById(R.id.yes_btn);
        yes.setText(Localizer.getLocalizerString("k_21_s4_yes"));
        Button no = dialog.findViewById(R.id.no_btn);
        no.setText(Localizer.getLocalizerString("k_22_s4_no"));
        title2.setText(messag);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (isDelivery) {
                    progressDialog.showDialog();
                    updateDeliveryStatus(getIntent().getStringExtra("delivery_id"));
                } else {
                    payWithCashOnHand();
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void payWithCashOnHand() {
        progressDialog.showDialog();
        HashMap<String, String> params = new HashMap<>();
        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

        if (tripModel != null && (tripModel.tripId != null || (tripModel.trip != null && tripModel.trip.getTrip_id() != null))) {
            if (tripModel.tripId != null)
                params.put(Constants.Keys.TRIP_ID, tripModel.tripId);
            else
                params.put(Constants.Keys.TRIP_ID, tripModel.trip.getTrip_id());

            params.put("trip_status", "paid_cancel");
            ServerApiCall.callWithApiKey(this, params, Constants.Urls.URL_USER_UPDATE_TRIP, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    progressDialog.dismiss();
                    if (isUpdate) {
                        paymentDone();
                    } else {

                        Toast.makeText(FareSummaryActivity.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            progressDialog.dismiss();
        }
    }

//    private void savePayment() {
//        HashMap<String, String> params = new HashMap<>();
//        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
//        params.put(Constants.Keys.TRIP_ID, tripModel.tripId);
//        params.put("pay_mode", "Cash");
//        params.put("pay_status", "Paid");
//        params.put("pay_date", AppUtil.getCurrentDateInGMTZeroInServerFormat());
//        float fare = Float.parseFloat(controller.pref.getTRIP_PAY_AMOUNT());
//        try {
//            if (updatedTripModel.trip.getTrip_promo_amt() != null) {
//                if (Float.parseFloat(updatedTripModel.trip.getTrip_promo_amt()) > 0) {
//                    params.put("promo_id", updatedTripModel.trip.getTrip_promo_id());
//                    params.put("pay_promo_code", updatedTripModel.trip.getTrip_promo_code());
//                    params.put("pay_promo_amt", String.format(Locale.ENGLISH, "%.02f", Float.parseFloat(updatedTripModel.trip.getTrip_promo_amt())));
//                }
//                fare = fare - Float.parseFloat(updatedTripModel.trip.getTrip_promo_amt());
//            }
//        } catch (Exception e) {
//            progressDialog.dismiss();
//        }
//        params.put("pay_amount", "" + fare);
//        //System.out.println("PayWithCashOnHand  Params : " + params);
//        ServerApiCall.callWithApiKey(this, params, Constants.Urls.URL_PAYMENT_SAVE, new WebServiceUtil.DeviceTokenServiceListener() {
//            @Override
//            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                progressDialog.dismiss();
//                if (isUpdate) {
//                    paymentDone();
//                   /* if (!isDone) {
//                        reviewRatingView.setVisibility(View.VISIBLE);
//                    }*///
//                } else {
//
//                    Toast.makeText(FareSummaryActivity.this, "" + error, Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//    }

    private void paymentDone() {
        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel == null)
            return;
        progressDialog.dismiss();
        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
        fare_recieved_cash_button.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        if (controller.pref.getIsSingleMode()) {
            frameRating.setVisibility(View.GONE);
            ivRatingIcon.setVisibility(View.GONE);
            comment.setVisibility(View.GONE);
        } else {
            frameRating.setVisibility(View.VISIBLE);
            ivRatingIcon.setVisibility(View.VISIBLE);
            comment.setVisibility(View.VISIBLE);
        }
        repeatTimerManager.stopRepeatCall();
        home.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
        tvFareReview.setVisibility(View.VISIBLE);
    }

//    private void stopLocationServiceWithDistance() {
//        boolean recordingServiceRunning = TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class);
//        if (recordingServiceRunning) {
//            Intent serviceIntent = new Intent(this, LocationService.class);
//            stopService(serviceIntent);
//        }
//    }

   /* public void showdialog() {
        final Dialog dialog = new Dialog(FareSummaryActivity.this);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.payment_layout);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        TextView title2 = dialog.findViewById(R.id.tv_dialog_title);

        Button yes = dialog.findViewById(R.id.yes_btn);

        Button no = dialog.findViewById(R.id.no_btn);
        title2.setText("Do you want to exit now?");
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  sendNotificationToUser();
                updateTripStatusApi();
                dialog.dismiss();
                //reviewRatingView.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
                if (singleMode) {
                    tvFareReview.setVisibility(View.GONE);
                } else {
                    tvFareReview.setVisibility(View.VISIBLE);
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
*/

//    private void stopPickUpLocationServiceWithDistance() {
//        boolean recordingServiceRunning = TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, PickUpLocationService.class);
//        if (recordingServiceRunning) {
//            Intent serviceIntent = new Intent(this, PickUpLocationService.class);
//            stopService(serviceIntent);
//        }
//    }

    public void onBackPressed() {

    }

    private void getViewId() {
        TextView tvFaresummaryTitle = findViewById(R.id.tv_title);

        tvFareReview = findViewById(R.id.tv_fare_review);

        tvFareReview.setVisibility(View.GONE);


        paymentSubLayout = findViewById(R.id.payment_sub_layout);

        fare_recieved_cash_button = findViewById(R.id.fare_recieved_cash_button);

        okPaymentbtn = findViewById(R.id.yes_btn1);
        tvPaymentText = findViewById(R.id.tv_dialog_title1);
        tvPaymentText.setText(Localizer.getLocalizerString("k_17_s8_promo_comp_success"));
        okPaymentbtn.setText(Localizer.getLocalizerString("k_18_s4_Ok"));


        tvRiderAmountValue = findViewById(R.id.tv_rider_amount_value1);

        tvPromCodeApplied = findViewById(R.id.tv_promo_code_applied);


        linearLayout = findViewById(R.id.home_linear_id);

        btnOffline = findViewById(R.id.btn_offline_id);

        pickuploc = findViewById(R.id.pickuploc);
        droploc = findViewById(R.id.droploc);
    }

    private void logoutApi(final Controller controller) {
        progressDialog.showDialog();
        this.controller = controller;
        Map<String, String> params = new HashMap<>();
        params.put("d_is_available", "0");
        ServerApiCall.callWithApiKeyAndDriverId(FareSummaryActivity.this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        Driver driver = Driver.parseJsonAfterLogin(response);
                        controller.setLoggedDriver(driver);
                        controller.setDriver(driver, getApplicationContext());
                        String d_is_available = Objects.requireNonNull(driver).getD_is_available();
                        controller.pref.setD_IS_AVAILABLE(d_is_available);
                        Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                        startActivity(intent);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            finishAffinity();
                        } else {
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_24_s8_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    private void sendNotificationToUser() {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.user != null && tripModel.user.u_device_token != null) {
            String u_device_type = tripModel.user.getU_device_type();
            Map<String, String> params = new HashMap<>();
            String notificationMessage = "";

            if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                    notificationMessage = Constants.Message_ar.PAID_DELIVERY;
                } else {
                    notificationMessage = Constants.Message.PAID_DELIVERY;
                }
            } else {
                if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                    notificationMessage = Constants.Message_ar.PAID;
                } else {
                    notificationMessage = Constants.Message.PAID;
                }
            }

            params.put("message", notificationMessage);
            params.put("trip_id", tripModel.tripId);
            params.put("trip_status", "paid");
            if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                params.put(Constants.Keys.ANDROID, tripModel.user.getU_device_token());
            } else {
                params.put(Constants.Keys.IOS, tripModel.user.getU_device_token());
            }
            notificationStatusApi(params);
        }
    }

    private void notificationStatusApi(final Map<String, String> params) {
        Log.e("payment Params", "" + params);
        WebServiceUtil.excuteRequest(FareSummaryActivity.this, params, Constants.Urls.URL_COMMAN_NOTIFICATION, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    if (data != null) {
                        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
                    } else {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_24_s8_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });


    }

//    private void updateTripStatusApi() {
//        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
//        if (tripModel == null)
//            return;
//        home.setVisibility(View.VISIBLE);
//        linearLayout.setVisibility(View.VISIBLE);
//        tvFareReview.setVisibility(View.VISIBLE);
//        fare_recieved_cash_button.setVisibility(View.GONE);
////        progressDialog.showDialog();
////        ServerApiCall.callWithApiKey(this, tripModel.getParams(1), Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
////            @Override
////            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
////                progressDialog.dismiss();
////                if (isUpdate) {
////                    JSONObject jsonRootObject;
////                    try {
////                        jsonRootObject = new JSONObject(data.toString());
////                        int response = jsonRootObject.getInt(Constants.Keys.RESPONSE);
////                        if (response == 1) {
////                            tripModel.tripStatus = Constants.TripStatus.END;
////                            AppData.getInstance(getApplicationContext()).saveTripTrip();
////                            //     update trip successfully
////
////                        }  // not update
////
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
////
////                }
////            }
////        });
//
//    }

    private void cancelPreviousCallingTripApi() {
        if (callTripApi != null) {
            callTripApi.cancel();
            callTripApi = null;
        }
    }

    private void resetTripApiTimer() {
        if (!isStopMethodCalled) {
            repeatTimerManager.setTimerForRepeat();
        }
    }

    private void getTrip(final boolean isDialog, final boolean isResetTimer) {
        String api;
        if (controller.getLoggedDriver().getApiKey() != null) {
            api = controller.getLoggedDriver().getApiKey();
        } else {
            api = "f6f7d5eeb87aa1762fab30dcd19d1685";
        }


        if (isDialog) {
            progressDialog.showDialog();
        }
        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel == null) {
            progressDialog.dismiss();
            return;
        }
        cancelPreviousCallingTripApi();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        callTripApi = apiInterface.getTripById(api, tripModel.tripId);
        callTripApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        handleTripResponse(string, tripModel);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (isResetTimer) {
                    resetTripApiTimer();
                }
                if (isDialog) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (isDialog) {
                    progressDialog.dismiss();
                }
                if (!call.isCanceled()) {
                    if (isResetTimer) {
                        resetTripApiTimer();
                    }
                }
            }
        });

    }

    private void handleTripResponse(Object data, TripModel tripModel) {
        String response = data.toString();
        ErrorJsonParsing parser = new ErrorJsonParsing();
        CloudResponse res = parser.getCloudResponse("" + response);
        if (res.isStatus()) {
            boolean isParseRe = TripModel.parseJsonWithTripModel(response, tripModel);
            if (isParseRe) {
                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
                boolean isCash = false;
                String status = tripModel.trip.getTrip_pay_status();
                if (status != null) {
                    if (tripModel.trip.getTrip_pay_status().equals("Paid") || status.equalsIgnoreCase("paid_cancel")) {
                        isCash = true;
                        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
                        repeatTimerManager.stopRepeatCall();

                    }
                }

                updatedTripModel = tripModel;

                if (flag == 1) {
                    flag = 2;
                    if (updatedTripModel.trip != null && updatedTripModel.trip.getActual_from_loc() != null && updatedTripModel.trip.getActual_from_loc().trim().length() != 0) {
                        if (updatedTripModel.trip.getPickup_notes() != null)
                            pickuploc.setText(String.format("%s, %s", updatedTripModel.trip.getPickup_notes(), updatedTripModel.trip.getActual_from_loc()));
                        else
                            pickuploc.setText(updatedTripModel.trip.getActual_from_loc());
//                        pickuploc.setText(updatedTripModel.trip.getActual_from_loc());
                    } else {

                        if (Objects.requireNonNull(updatedTripModel.trip).getPickup_notes() != null)
                            pickuploc.setText(String.format("%s, %s", Objects.requireNonNull(updatedTripModel.trip).getPickup_notes(),
                                    Objects.requireNonNull(updatedTripModel.trip).getTrip_from_loc()));
                        else
                            pickuploc.setText(Objects.requireNonNull(updatedTripModel.trip).getTrip_from_loc());

//                        pickuploc.setText(Objects.requireNonNull(updatedTripModel.trip).getTrip_from_loc());
                    }
                    if (isDelivery) {
                        droploc.setText(delivery.getDeliveryAddress());
                    } else if (updatedTripModel != null && updatedTripModel.trip != null && updatedTripModel.trip.getActual_to_loc() != null && updatedTripModel.trip.getActual_to_loc().trim().length() != 0) {
                        droploc.setText(updatedTripModel.trip.getActual_to_loc());
                    } else {
                        droploc.setText(Objects.requireNonNull(Objects.requireNonNull(updatedTripModel).trip).getTrip_to_loc());
                    }
                }
                String trippayamount = tripModel.trip.getTrip_pay_amount();
                String trip_promo_amt = tripModel.trip.getTrip_promo_amt();

                try {
                    float promoAmnt = Float.parseFloat(trip_promo_amt);
                    if (isDelivery) {
                        tvRiderAmountValue.setText(controller.formatAmountWithCurrencyUnit(delivery.getPayAmount()));
                    } else
                        tvRiderAmountValue.setText(controller.formatAmountWithCurrencyUnit(updatedTripModel.trip.getTrip_pay_amount()));
                    if (promoAmnt > 0) {
                        tvPromCodeApplied.setText(Localizer.getLocalizerString("k_16_s8_promo_code_applied") + " (" + controller.formatAmountWithCurrencyUnit(promoAmnt) + ")");
                        tvPromCodeApplied.setVisibility(View.VISIBLE);
                        promo_code_description.setVisibility(View.VISIBLE);
                        promo_code_description.setText(Localizer.getLocalizerString("k_16_s8_promo_code_applied") + " (" + tripModel.trip.getTrip_promo_code() + ") : " + controller.formatAmountWithCurrencyUnit(tripModel.trip.getTrip_promo_amt()));
                    } else {
                        promo_code_description.setVisibility(View.GONE);
                        tvPromCodeApplied.setVisibility(View.GONE);
                    }

//                    distance.setText(controller.formatDistanceWithUnit(updatedTripModel.trip.getTrip_distance()));
                    distance.setText(controller.formatDistanceWithUnit(updatedTripModel.trip.getTrip_distance()));

                    String final_duration = 0 + " " + "min";

                    if (updatedTripModel.trip.getTripTotalTime() == null || updatedTripModel.trip.getTripTotalTime().equals("0")) {
                        Date datePickup = Utils.stringToDate(updatedTripModel.trip.getTrip_pickup_time());
                        Date dateDrop;
                        if (updatedTripModel.trip.getTrip_drop_time() == null || updatedTripModel.trip.getTrip_drop_time().equals("null")) {
                            dateDrop = Utils.stringToDate(AppUtil.getCurrentDateInGMTZeroInServerFormat());
                        } else {
                            dateDrop = Utils.stringToDate(updatedTripModel.trip.getTrip_drop_time());
                        }
                        Log.e("dropDara", "" + dateDrop);
                        Log.e("pickDate", "" + datePickup);
                        if (datePickup == null && dateDrop == null) {
                            final_duration = 0 + " " + "min";
                        } else if (datePickup != null && dateDrop != null) {
                            long diffrent_between_time = dateDrop.getTime() - Objects.requireNonNull(datePickup).getTime();
                            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                            long diffrent_between_time_inSecond = diffrent_between_time / (1000);

                            if (diffrent_between_time_inMinute == 0) {
                                final_duration = diffrent_between_time_inSecond + " " + "sec";
                            } else {
                                final_duration = diffrent_between_time_inMinute + " " + "min";
                            }
                        }
                    } else
                        final_duration = updatedTripModel.trip.getTripTotalTime() + " " + "min";

                    duration.setText(final_duration);

                    if (isCash) {
                        fare_recieved_cash_button.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                        if (controller.pref.getIsSingleMode()) {
                            frameRating.setVisibility(View.GONE);
                            ivRatingIcon.setVisibility(View.GONE);
                            comment.setVisibility(View.GONE);
                        } else {
                            frameRating.setVisibility(View.VISIBLE);
                            ivRatingIcon.setVisibility(View.VISIBLE);
                            comment.setVisibility(View.VISIBLE);
                        }
                        home.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                        tvFareReview.setVisibility(View.VISIBLE);

                        //reviewRatingView.setVisibility(View.VISIBLE);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isStopMethodCalled = false;
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            repeatTimerManager.startRepeatCall();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isStopMethodCalled = true;
        AppData.getInstance(getApplicationContext()).saveData();
        repeatTimerManager.stopRepeatCall();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppData.getInstance(getApplicationContext()).saveData();
        try {
            unregisterReceiver(mHandleMessageReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
