package com.ridertechrider.driver;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.grepix.grepixutils.Validations;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.WebService;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdatePassword extends AppCompatActivity {

    @BindView(R.id.edit_new_password)
    EditText edit_new_password;
    @BindView(R.id.edit_confirm_password)
    EditText edit_confirm_password;
    BTextView edit_bt_save;
    String user_id = "", apiKey = "";
    private Controller controller;
    private CustomProgressDialog progressDialog;
    private TextView textView14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        progressDialog = new CustomProgressDialog(this);
        ButterKnife.bind(this);
        edit_bt_save = findViewById(R.id.edit_bt_save);
        textView14 = findViewById(R.id.textView14);
        controller = (Controller) getApplication();
        if (getIntent().hasExtra("user_id")) {
            user_id = getIntent().getStringExtra("user_id");
        }
        if (getIntent().hasExtra("api_key")) {
            apiKey = getIntent().getStringExtra("api_key");
        }


        edit_new_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    edit_new_password.setError("Space is not allowed");
                    str = str.replaceAll(" ", "");
                    edit_new_password.setText(str);
                }
            }
        });

        edit_confirm_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    edit_confirm_password.setError("Space is not allowed");
                    str = str.replaceAll(" ", "");
                    edit_confirm_password.setText(str);
                }
            }
        });

        textView14.setText(Localizer.getLocalizerString("k_19_s6_update_password"));
        edit_bt_save.setText(Localizer.getLocalizerString("k_34_s6_save"));
        edit_new_password.setHint(Localizer.getLocalizerString("k_21_s6_new_password"));
        edit_confirm_password.setHint(Localizer.getLocalizerString("k_21_s6_conf_new_password"));
    }

    @OnClick(R.id.recancel)
    public void onCancel(View view) {
        onBackPressed();
    }

    @OnClick(R.id.edit_bt_save)
    public void saveProfile(View view) {
        edit_bt_save.setEnabled(false);
        String newPassword = edit_new_password.getText().toString();
        String confrimPasword = edit_confirm_password.getText().toString();
        //   Log.e("newpassword", newPassword);
        // Log.e("confirmPOaswrod", "" + confrimPasword);
        if (Validations.isValidateUpdatePassword(this, edit_new_password, edit_confirm_password)) {
            updateProfile();
        } else {
            edit_bt_save.setEnabled(true);
        }
    }

    private void updateProfile() {
        progressDialog.showDialog();
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.Keys.API_KEY, apiKey);
        params.put(Constants.Keys.DRIVER_ID, user_id);
        params.put(Constants.Keys.PASSWORD, edit_new_password.getText().toString());
        params.put("is_send_email", String.valueOf(0));
        Log.e("updatePASSWORD", "" + params);
        WebService.excuteRequest(this, params, Constants.Urls.UPDATE_PROFILE, new WebService.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                edit_bt_save.setEnabled(true);
                if (isUpdate) {
                    Intent intent = new Intent(UpdatePassword.this, SignInSignUpMainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                    Toast.makeText(getApplication(), R.string.password_reseted, Toast.LENGTH_LONG).show();
                } else {
//                    if (error == null) {
//                        Toast.makeText(UpdatePassword.this, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
//                    }
                    Toast.makeText(UpdatePassword.this, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}


