package com.ridertechrider.driver;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ridertechrider.driver.utils.Utils.convertServerDateToAppLocalDateOnly;
import static com.ridertechrider.driver.utils.Utils.convertServerDateToAppLocalTime;

class WalletAdapter extends BaseAdapter {

    private final List<TransactionList> transactionLists;
    private final Controller controller;
    private final Activity activity;
    Dialog dialog;
    boolean isLoading = false;

    public WalletAdapter(Activity activity, ArrayList<TransactionList> transactionLists) {
        this.transactionLists = transactionLists;
        this.activity = activity;
        controller = (Controller) activity.getApplication();
    }

    @Override
    public int getCount() {
        return transactionLists.size();
    }

    @Override
    public TransactionList getItem(int position) {
        return transactionLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        // String strb = String.format("%.2f", transactionLists.get(position).transactions.getCurrent_bal());
        String strb = transactionLists.get(position).transactions.getCurrent_bal();
//        String currency = controller.currencyUnit();
        mViewHolder.balanceAmount.setText(controller.formatAmountWithCurrencyUnit(strb));
        if (position % 2 == 1) {
            convertView.setBackgroundColor(Color.parseColor("#f6f6f6"));
        } else {
            convertView.setBackgroundColor(Color.parseColor("#D3D3D3"));

        }
        RelativeLayout review = convertView.findViewById(R.id.revi);

        final TransactionList transactionList = getItem(position);
        if (transactionList != null) {
            String date = transactionList.transactions.getCreated();
            //String[] format = date.split(":");
            //  String finalText = format[0] + ":" + format[1];

            mViewHolder.tvTime.setText(Utils.convertServerDateToAppLocalDate(transactionList.transactions.getCreated()));

            //String amt = transactionList.transactions.getAmount();
//        String amtb = String.format(Locale.ENGLISH, "%.2f", Double.valueOf(transactionList.transactions.getAmount()));

            String getTransType = transactionList.transactions.getTrans_type();
            if (getTransType.equalsIgnoreCase("CR")) {
                mViewHolder.tvAmount.setText(controller.formatAmountWithCurrencyUnit(transactionList.transactions.getAmount(), "+"));
                mViewHolder.tvAmount.setTextColor(Color.parseColor("#22a56b"));
            } else {
                mViewHolder.tvAmount.setText(controller.formatAmountWithCurrencyUnit(transactionList.transactions.getAmount(), "-"));
                mViewHolder.tvAmount.setTextColor(Color.parseColor("#ffac30"));
            }
            if (transactionList.driverTransactions != null) {

                String remark = transactionList.transactions.getRemarks();
                if (remark != null && !remark.equals("") && !remark.equals("null")) {
                    if (remark.equals("Trip")) {
                        mViewHolder.tvName.setText(String.format("%s Id : %s", remark, transactionList.driverTransactions.getTrip_id()));
                    } else {
                        mViewHolder.tvName.setText(remark);
                    }
                } else {
                    if (transactionList.driverTransactions.getTrans_type().equals("Trip")) {
                        mViewHolder.tvName.setText(String.format("%s Id : %s", transactionList.driverTransactions.getTrans_type(), transactionList.driverTransactions.getTrip_id()));
                        mViewHolder.tvAmount.setText(String.format("%s (%s)", mViewHolder.tvAmount.getText().toString(), transactionList.driverTransactions.getTrans_pay_mode()));
                    } else {
                        mViewHolder.tvName.setText(transactionList.driverTransactions.getTrans_type());
                    }
                }
                review.setTag(transactionList);
                review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (transactionList.driverTransactions != null) {
                            String tripId = transactionList.driverTransactions.getTrip_id();
                            if (tripId != null && !tripId.equals("null") && !tripId.equals(""))
                                getTripByID(tripId);
                        }
                    }
                });
            }
        }
        return convertView;
    }

    private void getTripByID(String tripId) {
        if (isLoading || controller.getLoggedDriver() == null)
            return;
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        isLoading = true;
        String api;
        api = controller.getLoggedDriver().getApiKey();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> callTripApi = apiInterface.getTripById(api, tripId);
        callTripApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                isLoading = false;
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        TripModel tripHistory = new TripModel();
                        boolean isParseRe = TripModel.parseJsonWithTripModel(string, tripHistory);
                        if (isParseRe) {
                            showTripDetails(tripHistory);
                        }
                    } catch (Exception e) {
                        Log.e(WalletAdapter.class.getSimpleName(), "onResponse: " + e.getMessage(), e);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isLoading = false;
            }
        });

    }

    public void showTripDetails(TripModel tripHistory) {
        if (activity == null)
            return;
        final Dialog dialog = new Dialog(activity);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.trip_details);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int width = (int) (activity.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.90);
        dialog.getWindow().setLayout(width, height);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        setLocalizeStrings(dialog);
        BTextView msa_tv_pickup = dialog.findViewById(R.id.msa_tv_pickup);
        BTextView msa_tv_drop = dialog.findViewById(R.id.msa_tv_drop);

        CircleImageView circleImageView = dialog.findViewById(R.id.driver_profile);
        TextView promo_amount = dialog.findViewById(R.id.promo_amount);
        TextView rating = dialog.findViewById(R.id.user_average_rating);
        TextView user_name = dialog.findViewById(R.id.user_name);
        TextView car_name = dialog.findViewById(R.id.car_name);
        RatingBar ratingBar1 = dialog.findViewById(R.id.ratingBar1);
        TextView detaildate = dialog.findViewById(R.id.detaildate);
        TextView tripStatusView = dialog.findViewById(R.id.tripStatus);
        Log.e("tripStatus", "" + tripHistory.tripStatus);
//            if (tripHistory.trip.getTrip_status() != null && !tripHistory.trip.getTrip_status().equalsIgnoreCase("null") && tripHistory.trip.getTrip_status().equalsIgnoreCase("cancel") || tripHistory.trip.getTrip_status().equalsIgnoreCase("hide_alert")) {
//                tripStatusView.setVisibility(View.VISIBLE);
//            } else {
//                tripStatusView.setVisibility(View.GONE);
//            }
        TextView pick = dialog.findViewById(R.id.detailpickup1);
        TextView drop = dialog.findViewById(R.id.detaildrop1);
        TextView amount = dialog.findViewById(R.id.total_amount);
        TextView distance = dialog.findViewById(R.id.distance);
        ImageView close = dialog.findViewById(R.id.close);
        BTextView passenger_details = dialog.findViewById(R.id.passenger_details);

        TextView tax_amount1 = dialog.findViewById(R.id.tax_amount1);
        View layoutTax = dialog.findViewById(R.id.layoutTax);
        if (tripHistory.trip.getTrip_customer_details() != null) {
            passenger_details.setVisibility(View.VISIBLE);
            passenger_details.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
        } else
            passenger_details.setVisibility(View.GONE);
        passenger_details.setOnClickListener(v -> showPassengerDetails(tripHistory));

        tripStatusView.setText(Utils.getTextForStatus(tripHistory.trip.getTrip_status(), tripHistory.trip.getTrip_pay_status()));

        if (!tripStatusView.getText().toString().equalsIgnoreCase("Cancelled")) {
            tripStatusView.setTextColor(activity.getResources().getColor(R.color.label_color_80));
        }

        try {
            if (Double.parseDouble(tripHistory.trip.getTrip_tax_amt()) > 0) {
                tax_amount1.setText(controller.formatAmountWithCurrencyUnit(tripHistory.trip.getTrip_tax_amt()));
            } else {
                tax_amount1.setText(controller.formatAmountWithCurrencyUnit("0"));
            }
        } catch (Exception e) {
            tax_amount1.setText(controller.formatAmountWithCurrencyUnit("0"));
        }

        TextView waitTime = dialog.findViewById(R.id.tvWaitingTime);
        String waitDuration = tripHistory.trip.getWait_duration();
        if (waitDuration != null && !waitDuration.equals("") && !waitDuration.equalsIgnoreCase("null")) {
            waitTime.setText(String.format("%s %s", waitDuration, activity.getString(R.string.minute)));
        } else {
            waitTime.setText(String.format("0 %s", activity.getString(R.string.minute)));
        }
        String categoryId = tripHistory.driver.getCategory_id();
        ArrayList<CategoryActors> categoryResponseList = CategoryActors.parseCarCategoriesResponse(controller.pref.getCategoryResponse());
        String catgoryName = "";
        for (CategoryActors catagories : categoryResponseList) {
            if (catagories.getCategory_id().equals(categoryId)) {
                catgoryName = catagories.getCat_name();
            }
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        float ratings = Float.parseFloat(tripHistory.user.getRating());
        rating.setText(String.valueOf(new DecimalFormat("##.#").format(ratings)));
        ratingBar1.setRating(Float.parseFloat(tripHistory.user.getRating()));
        user_name.setText(tripHistory.user.getU_name());
        car_name.setText(catgoryName);
        detaildate.setText(convertServerDateToAppLocalDateOnly(tripHistory.trip.getTrip_date()));
        Double amountDouble1 = Double.valueOf(tripHistory.trip.getTrip_pay_amount());
        //amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));
        distance.setText(controller.formatDistanceWithUnit(tripHistory.trip.getTrip_distance()));
        if (tripHistory.trip.getTrip_promo_code() != null && tripHistory.trip.getTrip_promo_code().trim().length() != 0 && !tripHistory.trip.getTrip_promo_code().equals("null")) {
            if (tripHistory.trip.getTrip_status() != null && !tripHistory.trip.getTrip_status().equals("cancel"))
                amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));
            else
                amount.setText(controller.formatAmountWithCurrencyUnit("0"));

            promo_amount.setText(String.format("%s %s", tripHistory.trip.getTrip_promo_code(), controller.formatAmountWithCurrencyUnit(tripHistory.trip.getTrip_promo_amt())));
        } else {
            if (tripHistory.trip.getTrip_status() != null && !tripHistory.trip.getTrip_status().equals("cancel"))
                amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));
            else
                amount.setText(controller.formatAmountWithCurrencyUnit("0"));
            promo_amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(0)));
        }
        if (tripHistory.trip != null && tripHistory.trip.getActual_from_loc() != null && tripHistory.trip.getActual_from_loc().trim().length() != 0) {
            if (tripHistory.trip.getPickup_notes() != null)
                pick.setText(String.format("%s, %s", tripHistory.trip.getPickup_notes(), tripHistory.trip.getActual_from_loc()));
            else
                pick.setText(tripHistory.trip.getActual_from_loc());

//                pick.setText(tripHistory.trip.getActual_from_loc());
        } else {

            if (Objects.requireNonNull(tripHistory.trip).getPickup_notes() != null)
                pick.setText(String.format("%s, %s", Objects.requireNonNull(tripHistory.trip).getPickup_notes(),
                        Objects.requireNonNull(tripHistory.trip).getTrip_from_loc()));
            else
                pick.setText(Objects.requireNonNull(tripHistory.trip).getTrip_from_loc());

//                pick.setText(Objects.requireNonNull(tripHistory.trip).getTrip_from_loc());
        }
        if (tripHistory.trip != null && tripHistory.trip.getActual_to_loc() != null && tripHistory.trip.getActual_to_loc().trim().length() != 0) {
            drop.setText(tripHistory.trip.getActual_to_loc());
        } else {
            drop.setText(Objects.requireNonNull(tripHistory.trip).getTrip_to_loc());
        }

        if (convertServerDateToAppLocalTime(tripHistory.trip.getTrip_pickup_time()) != null && !convertServerDateToAppLocalTime(tripHistory.trip.getTrip_pickup_time()).equalsIgnoreCase("nul")) {
            msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location") + " @ " + convertServerDateToAppLocalTime(tripHistory.trip.getTrip_pickup_time()));
        } else {
            msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location"));
        }
        if (convertServerDateToAppLocalTime(tripHistory.trip.getTrip_drop_time()) != null && !convertServerDateToAppLocalTime(tripHistory.trip.getTrip_drop_time()).equalsIgnoreCase("nul")) {
            msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location") + " @ " + convertServerDateToAppLocalTime(tripHistory.trip.getTrip_drop_time()));
        } else {
            msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        }

        if (tripHistory.user.isProfileImagePathEmpty()) {
            AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripHistory.user.getUProfileImagePath(), circleImageView);
        }
        dialog.show();

    }

    private void showPassengerDetails(TripModel tripModel) {
        final Dialog dialogPassengerDetails = new Dialog(activity);
        dialogPassengerDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPassengerDetails.setCancelable(true);
        dialogPassengerDetails.setContentView(R.layout.someone_else_dialog);
        TextView passengerTitle = dialogPassengerDetails.findViewById(R.id.passengerTitle);
        passengerTitle.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
        BTextView passengerName = dialogPassengerDetails.findViewById(R.id.passengerName);
        BTextView passengerPhone = dialogPassengerDetails.findViewById(R.id.passengerMobile);
        String details = tripModel.trip.getTrip_customer_details();
        try {
            JSONObject jsonObject = new JSONObject(details);
            if (jsonObject.has("p_name"))
                passengerName.setText(jsonObject.getString("p_name"));
            if (jsonObject.has("p_phone"))
                passengerPhone.setText(jsonObject.getString("p_phone"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        BTextView passengerNameTv = dialogPassengerDetails.findViewById(R.id.passengerNameTv);
        passengerNameTv.setText(Localizer.getLocalizerString("k_s3_passenger_name"));
        BTextView passengerMobileTv = dialogPassengerDetails.findViewById(R.id.passengerMobileTv);
        passengerMobileTv.setText(Localizer.getLocalizerString("k_s3_passenger_phone"));
        ImageView closeButtonDialog = dialogPassengerDetails.findViewById(R.id.close_button);
        closeButtonDialog.setOnClickListener(v1 -> {
            dialogPassengerDetails.dismiss();
        });

        dialogPassengerDetails.show();

    }

    private void setLocalizeStrings(Dialog dialog) {
        BTextView msa_tv_pickup = dialog.findViewById(R.id.msa_tv_pickup);
        BTextView msa_tv_drop = dialog.findViewById(R.id.msa_tv_drop);
        BTextView waiting_label = dialog.findViewById(R.id.waiting_label);
        BTextView promo_label = dialog.findViewById(R.id.promo_label);
        BTextView cost_label = dialog.findViewById(R.id.cost_label);
        BTextView distance_label = dialog.findViewById(R.id.distance_label);
        BTextView tax_layout = dialog.findViewById(R.id.tax_layout);
        msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location"));
        msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        waiting_label.setText(Localizer.getLocalizerString("k_3_s11_wait_time"));
        promo_label.setText(Localizer.getLocalizerString("k_4_s11_promo"));
        cost_label.setText(Localizer.getLocalizerString("k_7_s11_ride_cost"));
        distance_label.setText(Localizer.getLocalizerString("k_3_s8_distance"));
        tax_layout.setText(Localizer.getLocalizerString("k_6_s11_taxes"));
    }

    public void clear() {

        this.transactionLists.clear();
        notifyDataSetChanged();
    }

    private class MyViewHolder {

        final TextView tvName;
        final TextView tvTime;
        final TextView tvAmount;
        final TextView balanceAmount;

        MyViewHolder(View view) {
            this.tvName = view.findViewById(R.id.trans_desc);
            this.tvTime = view.findViewById(R.id.trans_time);
            this.tvAmount = view.findViewById(R.id.trans_amount);
            this.balanceAmount = view.findViewById(R.id.rem_amount);

        }
    }
}
