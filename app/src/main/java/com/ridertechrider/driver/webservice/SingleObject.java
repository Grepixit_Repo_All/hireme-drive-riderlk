package com.ridertechrider.driver.webservice;


/**
 * Created by Grepix on 10/21/2016.
 */
public class SingleObject {
    private static SingleObject instance;
    private boolean fareSummaryLinearLayout;

    private SingleObject() {
    }

    public static SingleObject getInstance() {
        if (instance == null) {
            instance = new SingleObject();
        }
        return instance;
    }

    public boolean getFareSummaryLinearLayout() {
        return fareSummaryLinearLayout;
    }

    public void setFareSummaryLinearLayout(boolean fareSummaryLinearLayout) {
        this.fareSummaryLinearLayout = fareSummaryLinearLayout;
    }

}
