package com.ridertechrider.driver;

interface Config {

    // CONSTANTS
    //static final String YOUR_SERVER_URL =  "http://127.0.0.1/gcm_server_files/register.php";
    // YOUR_SERVER_URL : Server url where you have placed your server files
    // Google project id
    // static final String GOOGLE_SENDER_ID = "391226754417";  // Place here your Google project id
    // String GOOGLE_SENDER_ID = "915598348361";  // Place here your Google project id

    /**
     * Tag used on log messages.
     */
    String TOPIC_GLOBAL = "global";

    String DISPLAY_MESSAGE_ACTION = BuildConfig.APPLICATION_ID + ".DISPLAY_MESSAGE";

    String EXTRA_MESSAGE = "message";
    String EXTRA_TRIP_STATUS = "trip_status";
    String EXTRA_TRIP_ID = "trip_id";
    String SHARED_PREF = "ah_firebase";


}
