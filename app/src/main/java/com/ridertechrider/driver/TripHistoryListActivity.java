package com.ridertechrider.driver;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.CustomRiderAdapter;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.custom.XListView.XListView;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TripHistoryListActivity extends BaseActivity implements XListView.IXListViewListener {
    private final int limit = 10;
    //private static final String TAG = TripHistoryListActivity.class.getSimpleName();
    private CustomRiderAdapter adapter;
    private TextView tv;
    private ArrayList<TripModel> tripHistoryList;
    private XListView mListView;
    private boolean isHasMoreData;
    private int offSet = 0;
    private CustomProgressDialog progressDialog;
    //private TextView tv_no_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreview);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        if (getActionBar() != null) {
            getActionBar().hide();
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //    tv_no_item = findViewById(R.id.tv_no_items);

        progressDialog = new CustomProgressDialog(TripHistoryListActivity.this);


        Typeface typeface = Typeface.createFromAsset(getAssets(), Fonts.KARLA);
        Typeface typeface1 = Typeface.createFromAsset(getAssets(), Fonts.KARLA);
        tripHistoryList = new ArrayList<>();
        mListView = findViewById(R.id.xListView);
        mListView.setPullLoadEnable(true);
        tv = findViewById(R.id.listempty);
        tv.setTypeface(typeface);
        TextView tvTripHistory = findViewById(R.id.textView14);
        tvTripHistory.setTypeface(typeface1);
        ImageView recancel = findViewById(R.id.recancel);
        recancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                finish();

            }
        });

        if (net_connection_check()) {
            adapter = new CustomRiderAdapter(TripHistoryListActivity.this, tripHistoryList, typeface);
            mListView.setXListViewListener(TripHistoryListActivity.this);
            mListView.setAdapter(adapter);
            callwebservice(offSet, false);
        } else {
            Toast.makeText(TripHistoryListActivity.this, Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_LONG).show();
        }
        setLocalizerString();
    }

    private void setLocalizerString() {
        TextView textView14;
        textView14 = findViewById(R.id.textView14);
        textView14.setText(Localizer.getLocalizerString("k_6_s4_a1_your_rides"));
    }

    private void callwebservice(final int nextoffset, final boolean isLoadMore) {

        if (!isLoadMore) {
            progressDialog.showDialog();
        }
        Map<String, String> params = new HashMap<>();
        params.put("is_request", "0");
        params.put("offset", String.valueOf(nextoffset));
        params.put("limit", String.valueOf(limit));
        ServerApiCall.callWithApiKeyAndDriverId(TripHistoryListActivity.this, params, Constants.Urls.DRIVER_TRIP_HISTORY, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError
                    error) {
                progressDialog.dismiss();
                mListView.stopLoadMore();
                mListView.stopRefresh();
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        handleHistoryResponse(response);
                    } else {
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private boolean net_connection_check() {
        ConnectionManager cm = new ConnectionManager(this);

        boolean connection = cm.isConnectingToInternet();

        if (!connection) {

            Toast toast = Toast.makeText(getApplicationContext(), R.string.there_is_no_network_please_connect, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 70);
            toast.show();
        }
        return connection;
    }

    private void handleHistoryResponse(String s) {
        try {
            JSONObject jsonRootObject = new JSONObject(s);
            JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
            isHasMoreData = response.length() == limit;
            if (response.length() == 0) {
                tv.setVisibility(View.VISIBLE);
            }
            for (int i = 0; i < response.length(); i++) {
                JSONObject childObject = response.getJSONObject(i);
                TripModel tripModel = TripModel.parseJson(childObject.toString());
                tripHistoryList.add(tripModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
        adapter.notifyDataSetInvalidated();
    }

    @Override
    public void onRefresh() {
        adapter.clear();
        callwebservice(offSet, false);
    }

    @Override
    public void onLoadMore() {
        if (isHasMoreData) {
            offSet += limit;
            callwebservice(offSet, true);
        }
    }

}
