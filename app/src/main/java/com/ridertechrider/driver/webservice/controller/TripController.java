package com.ridertechrider.driver.webservice.controller;


import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;

public class TripController {
    private final TripModel tripModel;
    private TripControllerCallBack tripControllerCallBack;


    public TripController(TripModel tripModel) {
        this.tripModel = tripModel;
    }

    public void setChnageTripStatus(String trip_status) {
        tripModel.tripStatus = trip_status;
        if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
            tripControllerCallBack.onTripRequest(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.ARRIVE)) {
            tripControllerCallBack.onTripArrive(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.ACCEPT)) {
            tripControllerCallBack.onTripAccept(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
            tripControllerCallBack.onTripBegin(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.END) || tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.PAID)) {
            tripControllerCallBack.onTripEnd(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
            tripControllerCallBack.onTripDriverCancelAtDrop(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.CANCEL) || tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.CANCEL_RIDER)) {
            tripControllerCallBack.onTripCancel(tripModel);
        } else if (tripModel.trip.getTrip_status().equalsIgnoreCase(Constants.TripStatus.DRIVER_CANCEL_AT_PICKUP)) {
            tripControllerCallBack.onTripDriverCancelAtPickup(tripModel);
        }
    }

    public void setChnageTripStatusLocal(String trip_status) {
        tripModel.tripStatus = trip_status;
        if (trip_status.equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
            tripControllerCallBack.onTripRequest(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.ARRIVE)) {
            tripControllerCallBack.onTripArrive(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.ACCEPT)) {
            tripControllerCallBack.onTripAccept(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
            tripControllerCallBack.onTripBegin(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.END) || trip_status.equalsIgnoreCase(Constants.TripStatus.PAID)) {
            tripControllerCallBack.onTripEnd(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
            tripControllerCallBack.onTripDriverCancelAtDrop(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.CANCEL) || trip_status.equalsIgnoreCase(Constants.TripStatus.CANCEL_RIDER)) {
            tripControllerCallBack.onTripCancel(tripModel);
        } else if (trip_status.equalsIgnoreCase(Constants.TripStatus.DRIVER_CANCEL_AT_PICKUP)) {
            tripControllerCallBack.onTripDriverCancelAtPickup(tripModel);
        }
    }

    public TripModel getTripModel() {
        return tripModel;
    }

    public void setTripControllerCallBack(TripControllerCallBack tripControllerCallBack) {
        this.tripControllerCallBack = tripControllerCallBack;
    }

    public void handleTripResponse(String tripResponse) {
        ErrorJsonParsing parser = new ErrorJsonParsing();
        CloudResponse res = parser.getCloudResponse("" + tripResponse);
        if (res.isStatus()) {
            boolean isParseSusseccfully = TripModel.parseJsonWithTripModel(tripResponse, this.tripModel);
            if (isParseSusseccfully) {
                setChnageTripStatus(tripModel.tripStatus);
                tripControllerCallBack.onTripResponseValid(tripModel);
            } else {
                tripControllerCallBack.onTripError("Response Parsing Error:");
            }
        } else {
            tripControllerCallBack.onTripError(res.getError());
        }
    }

    public interface TripControllerCallBack {
        void onTripResponseValid(TripModel tripModel);


        void onTripAccept(TripModel tripModel);

        void onTripBegin(TripModel tripModel);


        void onTripCancel(TripModel tripModel);

        void onTripEnd(TripModel tripModel);

        void onTripRequest(TripModel tripModel);

        void onTripError(String message);

        void onTripDriverCancelAtDrop(TripModel tripModel);

        void onTripDriverCancelAtPickup(TripModel tripModel);

        void onTripArrive(TripModel tripModel);
    }
}
