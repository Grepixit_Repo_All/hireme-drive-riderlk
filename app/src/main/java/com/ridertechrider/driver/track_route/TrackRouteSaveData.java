package com.ridertechrider.driver.track_route;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by grepixinfotech on 14/02/18.
 */

public class TrackRouteSaveData implements Serializable {
    transient private final static String filename = "Saved_RouteData";
    transient private static Context context;
    transient private static TrackRouteSaveData trackRouteSaveData;
    // --Commented out by Inspection (24-07-2019 02:45):private Vector<AppLatLng> latLngsRemaingRoute;
    private final Vector<AppLatLng> latLngsD;

    private TrackRouteSaveData() {
        latLngsD = new Vector<>();
    }

    public static TrackRouteSaveData getInstance(Context context) {
        TrackRouteSaveData.context = context;
        if (trackRouteSaveData == null) {
            trackRouteSaveData = loadSavedInstance(context);
            if (trackRouteSaveData == null) {
                trackRouteSaveData = new TrackRouteSaveData();
            }
        }
        return trackRouteSaveData;
    }

    private static TrackRouteSaveData loadSavedInstance(Context context) {
        File filesDir = context.getFilesDir();
        File file = new File(filesDir, TrackRouteSaveData.filename);
        if (!file.exists()) {
            return null;
        }
        try {
            FileInputStream inputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            Object object = objectInputStream.readObject();
            objectInputStream.close();
            return (TrackRouteSaveData) object;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void addLatLongD(LatLng latLng) {
        if (latLng != null) {

            latLngsD.add(new AppLatLng(latLng));
        }
    }

    public ArrayList<LatLng> getAllLatLngsD() {
        ArrayList<LatLng> latLngsNew = new ArrayList<>();
        for (AppLatLng appLatLng :
                latLngsD) {
            latLngsNew.add(new LatLng(appLatLng.lat, appLatLng.lng));
        }
        return latLngsNew;
    }

    public LatLng getLastLatLng() {
        AppLatLng lastLatLng = null;
        if (latLngsD.size() > 0) {
            lastLatLng = latLngsD.get(latLngsD.size() - 1);
        }
        if (lastLatLng != null) {
            return new LatLng(lastLatLng.lat, lastLatLng.lng);
        }
        return null;
    }

    public void clearData() {
        latLngsD.clear();
        saveData();
    }

    public void saveData() {
        File filesDir = context.getFilesDir();
        File file = new File(filesDir, filename);
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
            objectOutputStream.writeObject(this);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


