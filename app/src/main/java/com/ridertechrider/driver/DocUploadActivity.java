package com.ridertechrider.driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.BuildConfig;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.Utils;
import com.grepix.grepixutils.WebServiceUtil;
import com.ridertechrider.driver.GetDriverAsset.GetDriverAsset;
import com.ridertechrider.driver.GetDriverAsset.GetDriverAssetResponse;
import com.ridertechrider.driver.GetStatus.Getstatus;
import com.ridertechrider.driver.GetStatus.Header;
import com.ridertechrider.driver.GetStatus.Input;
import com.ridertechrider.driver.Model.City;
import com.ridertechrider.driver.adaptor.CarCategory;
import com.ridertechrider.driver.adaptor.CityAdapter;
import com.ridertechrider.driver.adaptor.CustomSpinnerAdapter;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BButton;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.custom.NDSpinner;
import com.ridertechrider.driver.dateTimePicker.SingleDateAndTimePicker;
import com.ridertechrider.driver.dateTimePicker.dailog.SingleDateAndTimePickerDialog;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class DocUploadActivity extends BaseCompatActivity implements View.OnClickListener {

    public static final int PERMISSION_REQUEST_CODE = 354;
    // private String imagepath;
    protected static final String TAG = DocUploadActivity.class.getSimpleName();
    // private String reg_no;
    private static final String PREFS_NAME = "MyPrefsFile";
    private static final int RESULT_LOAD_IMAGE = 3;
    private static final int RESULT_LOAD_CAR_IMAGE = 8;
    private static final int CAMERA_REQUEST = 751;
    private static final int ONE_MINUTE_IN_MILLIS = 1000;
    final Calendar myCalendar = Calendar.getInstance();
    //private String getCarResponce;
    boolean isFromRegister = false;
    Driver loggedUser;
    String password_;
    boolean isFinishCalled = false;
    TextView nextb;
    String current;
    String ddmmyyyy = "DDMMYYYY";
    Calendar cal = Calendar.getInstance();
    private NDSpinner categorylistSpinner;
    private String[] category;
    private EditText carlistSpinner, carmodelSpinner, yearSpinner;
    //private int catSize;
    private String seletedcategory = "";
    private String selectedCity = null;
    private ArrayList<CategoryActors> categoryResponseList;
    private Controller controller;
    private ArrayList<CarCategory> catArrayList;
    //private ArrayList<GetCars> getCarArrayList;
    private String checkLicencImg;
    private ImageView lic;
    private ImageView vote;
    //  private ImageView bitmapImgView;
//    private Button licb;
//    private Button voteb;
//    private Button insurance;
    private String picturePath;
    private int count = 0;
    private EditText regNumber;
    private RelativeLayout spinnerLayoutCat;
    //    private ImageView idUpload, licenseUpload, insuranceUpload;
    //  private boolean isInsurance = false;
    //   private boolean isLicence = false;
    // private boolean isIdProof = false;
    private CustomProgressDialog progressDialog;
    private NDSpinner spCity;
    private ArrayList<City> citiesCountryCode = new ArrayList<>();
    private CityAdapter adapterCountryCode;
    //todo where are you changing this to 1
    private String isFront = "0";
    private String string = "[{\"header\":{\"type\":\"TextView\",\"value\":\"\",\"placeholder\":\"Driving license\"},\"inputs\":[{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"LICENSE\",\"placeholder\":\"Front\",\"is_front\":\"1\",\"server_value\":\"Please select license front\",\"is_mandatory\":\"1\"},{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"LICENSE\",\"placeholder\":\"Front\",\"is_front\":\"0\",\"server_value\":\"Please select license back\",\"is_mandatory\":\"1\"},{\"type\":\"EditText\",\"value\":\"\",\"key\":\"expire_date\",\"placeholder\":\"YYYY-DD-MM\",\"server_value\":\"Please select expiry date\",\"is_mandatory\":\"1\"}]},{\"header\":{\"type\":\"TextView\",\"value\":\"\",\"placeholder\":\"Registration Certificate\"},\"inputs\":[{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"RC\",\"placeholder\":\"Front\",\"is_front\":\"1\",\"server_value\":\"Please select rc front\",\"is_mandatory\":\"1\"},{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"RC\",\"placeholder\":\"Front\",\"is_front\":\"0\",\"server_value\":\"Please select rc back\",\"is_mandatory\":\"1\"}]},{\"header\":{\"type\":\"TextView\",\"value\":\"\",\"placeholder\":\"Insurance\"},\"inputs\":[{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"INSURANCE\",\"placeholder\":\"\",\"is_front\":\"1\",\"server_value\":\"Please select insurance front \",\"input\":\"date\",\"is_mandatory\":\"1\"},{\"type\":\"ImageView\",\"server_value\":\"Please select insurance front\",\"value\":\"\",\"input\":\"date\",\"is_front\":\"0\",\"key\":\"INSURANCE\",\"is_mandatory\":\"1\"},{\"type\":\"EditText\",\"value\":\"\",\"key\":\"expire_date\",\"placeholder\":\"YYYY-DD-MM\",\"is_front\":\"0\",\"server_value\":\"Please select expiry date\",\"is_mandatory\":\"1\"}]}]";
    private String string2;
    private String asset_type = "";
    private LinearLayout linear_root_documents, linear_root_vehicle_details;
    private List<String> keySelectList = new ArrayList<>();
    private List<EditText> editTextList = new ArrayList<>();
    private List<String> editTextPlaceHolderList = new ArrayList<>();
    private List<String> editTextservervalue = new ArrayList<>();
    private List<ImageView> imageViewList = new ArrayList<>();
    private List<String> imageViewListID = new ArrayList<>();
    private List<String> imageViewListTags = new ArrayList<>();
    private List<String> editTextListTags = new ArrayList<>();
    private List<Getstatus> getstatusList = new ArrayList<>();
    private List<String> assetTypeList = new ArrayList<>();
    private List<String> placeholdernameslist = new ArrayList<>();
    private List<String> servervaluelist = new ArrayList<>();
    private List<String> frontbacknameslist = new ArrayList<>();
    private List<String> isFrontList = new ArrayList<>();
    private List<Boolean> isValidFormatBoolean = new ArrayList<>();
    private Input publicInput;
    private String driverAssetId;
    private LinearLayout llCarImage;
    private ImageView ivCarImage;
    private BButton bbCarImage;
    private ImageView ivCarImageCheck;
    private String bitmapValueCarImage = null;
    private Button confirmdoc;
    private SingleDateAndTimePickerDialog singleDateTimePicker;
    private boolean isUploading = false;
    private Bitmap pictureBitmap = null;
    private TextView tv_upload_documents;

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
        }

        return Objects.requireNonNull(dir).delete();
    }

    private static Bitmap scaleDown(Bitmap realImage) {
        float ratio = Math.min(
                (float) 500 / realImage.getWidth(),
                (float) 500 / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        return Bitmap.createScaledBitmap(realImage, width,
                height, true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static boolean isValidFormat(String format, String value, Locale locale) {
        LocalDateTime ldt = null;
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);

        try {
            ldt = LocalDateTime.parse(value, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(value, fomatter);
                String result = ld.format(fomatter);
                return result.equals(value);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(value, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(value);
                } catch (DateTimeParseException e2) {
                    // Debugging purposes
                    e2.printStackTrace();
                }
            }
        }

        return false;
    }

    private static Bitmap getResizedBitmap(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio < 1 && width > 300) {

            width = 300;
            height = (int) (width / bitmapRatio);
        } else if (height > 300) {
            height = 300;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    /*  Image rotate.......................*/
    private static Bitmap rotateImageIfRequired(Bitmap img, Context context, Uri selectedImage) throws IOException {

        if (Objects.requireNonNull(selectedImage.getScheme()).equals("content")) {
            String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
            Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
            if (Objects.requireNonNull(c).moveToFirst()) {
                final int rotation = c.getInt(0);
                c.close();
                return rotateImage(img, rotation);
            }
            return img;
        } else {
            ExifInterface ei = new ExifInterface(selectedImage.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//            Log.d("orientation: %s", "->>>>>>>>  " + orientation);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(img, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(img, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(img, 270);
                default:
                    return img;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        return Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("NewApi")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_doc_upload1);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

//        string2 = "[{\"header\":{\"type\":\"TextView\",\"value\":\"\",\"placeholder\":\"Driving licence\"},\"inputs\":[{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"LICENSE\",\"placeholder\":\"Front\",\"is_front\":\"1\",\"server_value\":\"Please enter the name\",\"is_mandatory\":\"1\"},{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"LICENSE\",\"placeholder\":\"Front\",\"is_front\":\"0\",\"server_value\":\"Please enter the name\",\"is_mandatory\":\"1\"},{\"type\":\"EditText\",\"value\":\"\",\"key\":\"expire_date\",\"placeholder\":\"Back\",\"server_value\":\"Please enter the name\",\"is_mandatory\":\"1\"}]},{\"header\":{\"type\":\"TextView\",\"value\":\"\",\"placeholder\":\"Registration Certificate\"},\"inputs\":[{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"RC\",\"placeholder\":\"Front\",\"is_front\":\"1\",\"server_value\":\"Please enter the name\",\"is_mandatory\":\"1\"},{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"RC\",\"placeholder\":\"Front\",\"is_front\":\"0\",\"server_value\":\"Please enter the name\",\"is_mandatory\":\"1\"}]},{\"header\":{\"type\":\"TextView\",\"value\":\"\",\"placeholder\":\"Insurance\"},\"inputs\":[{\"type\":\"ImageView\",\"value\":\"\",\"key\":\"INSURANCE\",\"placeholder\":\"\",\"is_front\":\"1\",\"server_value\":\"Insurance Front\",\"is_mandatory\":\"1\"},{\"type\":\"ImageView\",\"server_value\":\"Insurance Back\",\"value\":\"\",\"input\":\"date\",\"is_front\":\"0\",\"key\":\"INSURANCE\",\"is_mandatory\":\"1\"},{\"type\":\"EditText\",\"value\":\"\",\"key\":\"expire_date\",\"placeholder\":\"Front\",\"is_front\":\"0\",\"server_value\":\"Front\",\"is_mandatory\":\"1\"}]}]";

        if (getIntent() != null) {
            if (getIntent().hasExtra("isFromRegister"))
                isFromRegister = getIntent().getBooleanExtra("isFromRegister", false);

        }

        progressDialog = new CustomProgressDialog(DocUploadActivity.this);
        controller = (Controller) getApplicationContext();
        // bitmapImgView = findViewById(R.id.iv_bitmap_img_id);
        controller.pref.setBITMAP_STRING("");
        controller.pref.setLICENSE_BITMAP_TO_STRING_("");
        controller.pref.setINSURANCE_BITMAP_TO_STRING_("");
        // getCarArrayList = new ArrayList<GetCars>();
        spinnerLayoutCat = findViewById(R.id.spinnerLayoutCat);
        setCheckLicencImg("dummydata");
//        licb = findViewById(R.id.add_license);
//        voteb = findViewById(R.id.add_id_proof);
//        insurance = findViewById(R.id.addinsurance);
        ImageView cancelb = findViewById(R.id.canceldoc);
        nextb = findViewById(R.id.nextdoc);
        confirmdoc = findViewById(R.id.confirmdoc);
//        lic = findViewById(R.id.licimage);
//        vote = findViewById(R.id.voteimage);
//        idUpload = findViewById(R.id.id_upload);
//        licenseUpload = findViewById(R.id.license_upload);
//        insuranceUpload = findViewById(R.id.insurance_upload);
        categorylistSpinner = findViewById(R.id.categorylist);
        carlistSpinner = findViewById(R.id.carlist);
        carmodelSpinner = findViewById(R.id.select_a_model);
        yearSpinner = findViewById(R.id.select_a_year);
        regNumber = findViewById(R.id.ca_reg_no);
        tv_upload_documents = findViewById(R.id.tv_upload_documents);
        LinearLayout addProof = findViewById(R.id.proof);
        LinearLayout addLicense = findViewById(R.id.license);
        LinearLayout addInsurance = findViewById(R.id.insurance);

        llCarImage = findViewById(R.id.llCarImage);
        ivCarImage = findViewById(R.id.ivCarImage);
        bbCarImage = findViewById(R.id.bbCarImage);
        ivCarImageCheck = findViewById(R.id.ivCarImageCheck);

        //Added here
        linear_root_documents = findViewById(R.id.linear_root_documents);
        linear_root_vehicle_details = findViewById(R.id.linear_root_vehicle_details);

        linear_root_documents.setVisibility(View.VISIBLE);
        linear_root_vehicle_details.setVisibility(View.GONE);
        nextb.setVisibility(View.VISIBLE);
        confirmdoc.setVisibility(View.GONE);

        String car_model = controller.getLoggedDriver().getCar_name();
        carlistSpinner.setText(car_model);

        String car_year = controller.getLoggedDriver().getCar_model();
        carmodelSpinner.setText(car_year);

        String car_name = controller.getLoggedDriver().getCar_make();
        yearSpinner.setText(car_name);

        String regNumbers = controller.getLoggedDriver().getCar_reg_no();
        regNumber.setText(regNumbers);

        if (!controller.getLoggedDriver().isDCarImageUploaded()) {
            Picasso.get().load(Constants.IMAGE_BASE_URL + controller.getLoggedDriver().getD_car_image_path()).into(ivCarImage);
//            ivCarImage.setImageURI(Constants.IMAGE_BASE_URL + controller.getLoggedDriver().getD_car_image_path());
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        spCity = findViewById(R.id.spCity);

        carlistSpinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.contains(" ")) {
                    text = text.replaceAll(" ", "");
                    int curSelection = carlistSpinner.getSelectionStart();
                    curSelection = curSelection - 1 >= 0 ? curSelection - 1 : curSelection;
                    carlistSpinner.setText(text);
                    carlistSpinner.setSelection(Math.min(curSelection, text.length()));
                }
            }
        });
        carmodelSpinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.contains(" ")) {
                    text = text.replaceAll(" ", "");
                    int curSelection = carmodelSpinner.getSelectionStart();
                    curSelection = curSelection - 1 >= 0 ? curSelection - 1 : curSelection;
                    carmodelSpinner.setText(text);
                    carmodelSpinner.setSelection(Math.min(curSelection, text.length()));
                }
            }
        });
        regNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.contains(" ")) {
                    text = text.replaceAll(" ", "");
                    int curSelection = regNumber.getSelectionStart();
                    curSelection = curSelection - 1 >= 0 ? curSelection - 1 : curSelection;
                    regNumber.setText(text);
                    regNumber.setSelection(Math.min(curSelection, text.length()));
                }
            }
        });

        citiesCountryCode = Controller.getInstance().getCities();
//        citiesCountryCode.add(0, new City(getString(R.string.select)));
        citiesCountryCode.add(0, new City(Localizer.getLocalizerString("k_26_s2_select")));
        adapterCountryCode = new CityAdapter(this, R.layout.spinner_text, citiesCountryCode);
        spCity.setAdapter(adapterCountryCode);

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    selectedCity = null;
                else
                    selectedCity = citiesCountryCode.get(position).getCity_id();
                getCarCategoryApi();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cancelb.setOnClickListener(v -> onBackPressed());

//        cancelb.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
////                if (linear_root_vehicle_details.getVisibility() == View.VISIBLE) {
////                    linear_root_vehicle_details.setVisibility(View.GONE);
////                    nextb.setVisibility(View.VISIBLE);
////                    confirmdoc.setVisibility(View.GONE);
////                } else {
////                    clearApplicationData();
////                    clearSavedPreferences();
////                    finish();
//////                if (controller.isDocUpdate() && !isFromRegister) {
//////                    finish();
//////                } else {
//////                    controller.logout();
//////                    controller.pref.setIsLogin(false);
//////                    Intent intent = new Intent(DocUploadActivity.this, SignInSignUpMainActivity.class);
//////                    startActivity(intent);
//////                }
////                }
//                onBackPressed();
//            }
//        });

//        licb.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                if (checkPermission()) {
//                    try {
//                        count = 0;
//                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(i, RESULT_LOAD_IMAGE);
//                        setCheckLicencImg("license");
//                    } catch (Exception e) {
//                        Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_15_s7_invalid_image_format"), Toast.LENGTH_LONG).show();
//                    }
//                } else {
//                    requestPermission();
//                }
//            }
//        });

//        voteb.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                if (checkPermission()) {
//                    try {
//                        count = 0;
//                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(i, RESULT_LOAD_IMAGE);
//                        setCheckLicencImg("idproof");
//                    } catch (Exception e) {
//                        Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_15_s7_invalid_image_format"), Toast.LENGTH_LONG).show();
//                    }
//                } else {
//                    requestPermission();
//                }
//            }
//        });

//        insurance.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (checkPermission()) {
//                    try {
//                        count = 0;
//                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(i, RESULT_LOAD_IMAGE);
//                        setCheckLicencImg("insurance");
//                    } catch (Exception e) {
//                        Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_15_s7_invalid_image_format"), Toast.LENGTH_LONG).show();
//                    }
//                } else {
//                    requestPermission();
//                }
//            }
//        });

//        nextb.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (controller == null || controller.getLoggedDriver() == null)
//                    return;
//                //  reg_no = regNumber.getText().toString();
//
//                if (linear_root_vehicle_details.getVisibility() == View.GONE) {
//                    linear_root_vehicle_details.setVisibility(View.VISIBLE);
//                    linear_root_documents.setVisibility(View.GONE);
//                    confirmdoc.setVisibility(View.VISIBLE);
//                    nextb.setVisibility(View.GONE);
//                } else {
//
//                }
//
//
//            }
//        });

        tv_upload_documents.setText(Localizer.getLocalizerString("k_3_s6_upload_doc"));
        nextb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (controller == null || controller.getLoggedDriver() == null)
                    return;
                //  reg_no = regNumber.getText().toString();
                if (isUploading) {
                    progressDialog.showDialog();
                } else {
//                    if (imageViewList.get(0).getDrawable().getConstantState() == null || imageViewList.get(0).getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_outline_add_photo_alternate_24).getConstantState()) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_frnt_pro_crd, Toast.LENGTH_LONG).show();
//                    }  else if (imageViewList.get(1).getDrawable().getConstantState() == null || imageViewList.get(1).getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_outline_add_photo_alternate_24).getConstantState()) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_frnt_pro_crd, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(2).getDrawable().getConstantState() == null || imageViewList.get(2).getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_outline_add_photo_alternate_24).getConstantState()) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_frnt_lic, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(3).getDrawable().getConstantState() == null || imageViewList.get(3).getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_outline_add_photo_alternate_24).getConstantState()) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_id_pass, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(4).getDrawable().getConstantState() == null || imageViewList.get(4).getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_outline_add_photo_alternate_24).getConstantState()) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_ins, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(5).getDrawable().getConstantState() == null || imageViewList.get(5).getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_outline_add_photo_alternate_24).getConstantState()) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_iban, Toast.LENGTH_LONG).show();
//                    } else {
//                        if (layoutCategoryDetails.getVisibility() == View.GONE) {
//                            layoutCategoryDetails.setVisibility(View.VISIBLE);
//                            linear_root_documents.setVisibility(View.GONE);
//                            confirmdoc.setVisibility(View.VISIBLE);
//                            nextb.setVisibility(View.GONE);
//                            tvDocUploadHeader.setText(R.string.vehicle_details);
//                        } else {
//
//                        }
//                    }

                    int integer = 3;
                    for (int i = 0; i < imageViewList.size(); i++) {
                        if (imageViewList.get(i).getId() == -1) {
//                            Toast.makeText(DocUploadActivity.this, placeholdernameslist.get(i), Toast.LENGTH_LONG).show();
//                            Toast.makeText(DocUploadActivity.this, servervaluelist.get(i), Toast.LENGTH_LONG).show();
                            Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_r54_s8_serv_val_msg_" + String.valueOf(i)), Toast.LENGTH_LONG).show();
//                            Log.d(TAG, "adsdasd" + String.valueOf(""))
                            break;
                        } else if (i == imageViewList.size() - 1) {
                            if (linear_root_vehicle_details.getVisibility() == View.GONE) {
                                linear_root_vehicle_details.setVisibility(View.VISIBLE);
                                linear_root_documents.setVisibility(View.GONE);
                                confirmdoc.setVisibility(View.VISIBLE);
                                nextb.setVisibility(View.GONE);
                                tv_upload_documents.setText(Localizer.getLocalizerString("k_11_s7_vehicle_details"));
                            } else {

                            }
                        }
                    }


//                    if (imageViewList.get(0).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_frnt_pro_crd, Toast.LENGTH_LONG).show();
//                    }  else if (imageViewList.get(1).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_frnt_pro_crd, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(2).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_frnt_lic, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(3).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_id_pass, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(4).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_ins, Toast.LENGTH_LONG).show();
//                    } else if (imageViewList.get(5).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, R.string.plz_upld_iban, Toast.LENGTH_LONG).show();
//                    } else {
//
//                    }
                }


//                for (int i=0; i<imageViewList.size(); i++) {
//                    if (imageViewList.get(i).getId() == -1) {
//                        Toast.makeText(DocUploadActivity.this, getString(R.string.plz_upld_doc) + placeHolderList.get(i), Toast.LENGTH_LONG).show();
//                        break;
//                    } else if (i == imageViewList.size()-1) {
//
//                    }
//                }
            }
        });

        confirmdoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (controller.isDocUpdate()) {
                    final Driver driver = controller.getLoggedDriver();

                    if (driver.getCar_name() == null || driver.getCar_make() == null || driver.getCity_id() == null || driver.getCategory_id() == null ||
                            driver.getCar_reg_no() == null || driver.getCar_model() == null || driver.isDCarImageUploaded()) {
                        if (selectedCity == null || selectedCity.length() == 0 || selectedCity.equals("0")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_16_s7_please_select_city"), Toast.LENGTH_SHORT).show();
                        } else if (seletedcategory == null || seletedcategory.length() == 0 || seletedcategory.equals("0")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_17_s7_please_select_car_category"), Toast.LENGTH_SHORT).show();
                        } else if (carlistSpinner.getText().toString().length() == 0 || carlistSpinner.getText().toString().equals(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_18_s7_please_select_your_car_first"), Toast.LENGTH_SHORT).show();
                        } else if (carmodelSpinner.getText().toString().length() == 0 || carmodelSpinner.getText().toString().contains(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_19_s7_please_select_your_car_model_first"), Toast.LENGTH_SHORT).show();
                        } else if (yearSpinner.getText().toString().length() != 4 || yearSpinner.getText().toString().contains(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_20_s7_please_select_year_first"), Toast.LENGTH_SHORT).show();
                        } else if (regNumber.getText().toString().length() == 0 || regNumber.getText().toString().contains(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_21_s7_please_select_your_lisence_no"), Toast.LENGTH_SHORT).show();
                        } else if ((bitmapValueCarImage == null || bitmapValueCarImage.length() == 0) && driver.isDCarImageUploaded()) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_r42_s8_plz_upld_car_img"), Toast.LENGTH_SHORT).show();
                        } else {
                            if (Utils.net_connection_check(DocUploadActivity.this)) {
                                docSubmitApi(carmodelSpinner.getText().toString(), yearSpinner.getText().toString(),
                                        carlistSpinner.getText().toString(), regNumber.getText().toString(),
                                        seletedcategory, selectedCity, null, true);
                            }
                        }
                    } else {
                        if (selectedCity == null || selectedCity.length() == 0 || selectedCity.equals("0")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_16_s7_please_select_city"), Toast.LENGTH_SHORT).show();
                        } else if (seletedcategory == null || seletedcategory.length() == 0 || seletedcategory.equals("0")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_17_s7_please_select_car_category"), Toast.LENGTH_SHORT).show();
                        } else if (carlistSpinner.getText().toString().length() == 0 || carlistSpinner.getText().toString().equals(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_18_s7_please_select_your_car_first"), Toast.LENGTH_SHORT).show();
                        } else if (carmodelSpinner.getText().toString().length() == 0 || carmodelSpinner.getText().toString().contains(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_19_s7_please_select_your_car_model_first"), Toast.LENGTH_SHORT).show();
                        } else if (yearSpinner.getText().toString().length() != 4 || yearSpinner.getText().toString().contains(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_20_s7_please_select_year_first"), Toast.LENGTH_SHORT).show();
                        } else if (regNumber.getText().toString().length() == 0 || regNumber.getText().toString().contains(" ")) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_21_s7_please_select_your_lisence_no"), Toast.LENGTH_SHORT).show();
                        } else if ((bitmapValueCarImage == null || bitmapValueCarImage.length() == 0) && driver.isDCarImageUploaded()) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_r42_s8_plz_upld_car_img"), Toast.LENGTH_SHORT).show();
                        } else {
                            if (!(driver.getCity_id().equalsIgnoreCase(selectedCity)) || !(driver.getCategory_id().equalsIgnoreCase(seletedcategory))) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(DocUploadActivity.this, R.style.Dialog);
                                builder.setTitle(Localizer.getLocalizerString("k_22_s7_alert_on_city_or_category_change"));
                                builder.setMessage(Localizer.getLocalizerString("k_23_s7_alert_message_on_city_or_category_change"));
                                builder.setCancelable(false);
                                builder.setPositiveButton(Localizer.getLocalizerString("k_r8_s8_ok"), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        if (Utils.net_connection_check(DocUploadActivity.this)) {
                                            docSubmitApi(carmodelSpinner.getText().toString(), yearSpinner.getText().toString(),
                                                    carlistSpinner.getText().toString(), regNumber.getText().toString(),
                                                    seletedcategory, selectedCity, driver.getCar_id(), true);
                                        } else {
                                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_25_s7_network_not_found"), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
//                                builder.setNegativeButton(R.string.cancel_j, new DialogInterface.OnClickListener() {
                                builder.setNegativeButton(Localizer.getLocalizerString("k_30_s6_cancel_j"), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                builder.show();
                            } else if (!(driver.getCar_name().equalsIgnoreCase(carlistSpinner.getText().toString())) ||
                                    !(driver.getCar_make().equalsIgnoreCase(yearSpinner.getText().toString())) ||
                                    !(driver.getCar_reg_no().equalsIgnoreCase(regNumber.getText().toString())) ||
                                    !(driver.getCar_model().equalsIgnoreCase(carmodelSpinner.getText().toString())) || bitmapValueCarImage != null) {
                                if (Utils.net_connection_check(DocUploadActivity.this)) {
                                    docSubmitApi(carmodelSpinner.getText().toString(), yearSpinner.getText().toString(),
                                            carlistSpinner.getText().toString(), regNumber.getText().toString(),
                                            seletedcategory, selectedCity, driver.getCar_id(), false);
                                } else {
                                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_25_s7_network_not_found"), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_26_s7_no_change_to_update"), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    }
                } else {
                    // for registration use
                    if (selectedCity == null || selectedCity.length() == 0 || selectedCity.equals("0")) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_16_s7_please_select_city"), Toast.LENGTH_SHORT).show();
                    } else if (seletedcategory == null || seletedcategory.length() == 0 || seletedcategory.equals("0")) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_17_s7_please_select_car_category"), Toast.LENGTH_SHORT).show();
                    } else if (carlistSpinner.getText().toString().length() == 0 || carlistSpinner.getText().toString().equals(" ")) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_18_s7_please_select_your_car_first"), Toast.LENGTH_SHORT).show();
                    } else if (carmodelSpinner.getText().toString().length() == 0 || carmodelSpinner.getText().toString().contains(" ")) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_19_s7_please_select_your_car_model_first"), Toast.LENGTH_SHORT).show();
                    } else if (yearSpinner.getText().toString().length() != 4 || yearSpinner.getText().toString().contains(" ")) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_20_s7_please_select_year_first"), Toast.LENGTH_SHORT).show();
                    } else if (regNumber.getText().toString().length() == 0 || regNumber.getText().toString().contains(" ")) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_21_s7_please_select_your_lisence_no"), Toast.LENGTH_SHORT).show();
                    } else if (bitmapValueCarImage == null || bitmapValueCarImage.length() == 0) {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_r42_s8_plz_upld_car_img"), Toast.LENGTH_SHORT).show();
                    } else {
                        if (Utils.net_connection_check(DocUploadActivity.this)) {
                            docSubmitApi(carmodelSpinner.getText().toString(), yearSpinner.getText().toString(), carlistSpinner.getText().toString(), regNumber.getText().toString(), seletedcategory, selectedCity, null, true);
                        } else {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_25_s7_network_not_found"), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

        // boolean isfirst = false;
        if (controller.isDocUpdate()) {
            //   isfirst = true;
            Driver driver = controller.getLoggedDriver();
            if (driver.isDLicenceUploaded()) {
                String licenceUrl = Constants.IMAGE_BASE_URL + driver.getD_license_image_path();
//                Picasso.get().load(licenceUrl).into(lic);
//                lic.setImageBitmap(getBitmapFromURL(Constants.IMAGE_BASE_URL + driver.getD_license_image_path()));
//                lic.setImageURI(Constants.IMAGE_BASE_URL + driver.getD_license_image_path());
            } else {
//                licenseUpload.setVisibility(View.VISIBLE);
//                licb.setText(Localizer.getLocalizerString("k_4_s7_add_license"));
                // isLicence = true;
            }

            if (driver.isDrcUploaded()) {
                String proofUrl = Constants.IMAGE_BASE_URL + driver.getD_rc_image_path();
//                Picasso.get().load(proofUrl).into(vote);
//                vote.setImageBitmap(getBitmapFromURL(proofUrl));
//                vote.setImageURI(proofUrl);
            } else {
//                idUpload.setVisibility(View.VISIBLE);
//                voteb.setText(Localizer.getLocalizerString("k_3_s7_add_id_proof"));
                // isIdProof = true;
            }

            if (!driver.isDInsuranceUploaded()) {
//                insuranceUpload.setVisibility(View.VISIBLE);
//                insurance.setText(Localizer.getLocalizerString("k_5_s7_add_insurance"));
                //  isInsurance = true;

            }

            if (driver.isDCarImageUploaded()) {
                Picasso.get().load(Constants.IMAGE_BASE_URL + driver.getD_car_image_path()).into(ivCarImage);
//                ivCarImage.setImageURI(Constants.IMAGE_BASE_URL + driver.getD_car_image_path());
            }

            Log.d(TAG, "dadasdasdasd " + driver.getD_car_image_path());


            if (driver.getCar_reg_no() != null) {
                regNumber.setText(driver.getCar_reg_no());
            } else {
                regNumber.setHint(Localizer.getLocalizerString("k_12_s7_enter_licence_number"));
            }

            if (driver.getCity_id() != null && !driver.getCity_id().equals("null") && !driver.getCity_id().equals("0") && !driver.getCity_id().trim().isEmpty()) {
                selectedCity = driver.getCity_id();
            }

        }  // isfirst = false;

        getCities();
        setLocalizerString();


        //Added Here
        for (int i = 0; i < controller.getSettingsList().size(); i++) {
            if (controller.getSettingsList().get(i).getSkey().equals("driver_docs")) {
                string2 = controller.getSettingsList().get(i).getSvalue();
                Log.d(TAG, "dasdas " + string2 + "\n" + controller.getSettingsList().get(i).getSvalue());
                Log.d(TAG, "dynamicUIXML: " + new Gson().toJson(string2));
                break;
            }
        }
        setDynamicLayout();
        callGetDriverAsset();

        confirmdoc.setTextColor(getResources().getColor(R.color.signin_button1));

//        bbCarImage.setText("Car Image Upload");
        bbCarImage.setText(Localizer.getLocalizerString("k_r43_s8_car_img_upld"));

        bbCarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Input input = new Input();
                input.setKey("car_image");
                selectImage(input);
            }
        });

        ivCarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Input input = new Input();
                input.setKey("car_image");
                selectImage(input);
            }
        });
    }

    private void docUpdateForEditText(String date, String key, int id) {
        String driver_asset_id = String.valueOf(id);
        Map<String, String> params = new HashMap<>();
//        params.put(bitmapKey, bitmapValue);

        BitmapDrawable drawable = (BitmapDrawable) imageViewList.get(id).getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);
        params.put("driver_id", controller.getLoggedDriver().getDriverId());
        params.put("asset_type", asset_type);
        params.put("image_type", "jpeg");
        params.put("driver_image", getEncoded64ImageStringFromBitmap(bitmap));
        if (driver_asset_id != null && !driver_asset_id.equals("-1")) {
            params.put("driver_asset_id", driver_asset_id);
        }
        for (int j = 0; j < editTextPlaceHolderList.size(); j++) {
            String header = editTextPlaceHolderList.get(j);
            if (header.toLowerCase().contains(asset_type.toLowerCase())) {
                if (!editTextList.get(j).getText().toString().isEmpty()) {
                    params.put("expire_date", editTextList.get(j).getText().toString());
                }
            }
        }

        params.put("is_front", isFront);
        params.put("image_description", "");

        Log.d(TAG, "dsadas" + params.toString());

//        ServerApiCall.callWithApiKeyAndDriverId(DocUploadActivity.this, params, Constants.Urls.URL_ADD_DRIVER_ASSET, new WebServiceUtil.DeviceTokenServiceListener() {
//            @Override
//            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                progressDialog.dismiss();
//                if (isUpdate) {
//                    String response = data.toString();
//                    Driver driver = Driver.parseJsonAfterLogin(response);
//                    controller.setDriver(driver, getApplicationContext());
//                    ErrorJsonParsing parser = new ErrorJsonParsing();
//                    CloudResponse res = parser.getCloudResponse("" + response);
//                    isFront = "0";
//                }
//            }
//        });
    }

    private void setDynamicLayout() {
        JSONArray jsonArray = null;
        try {

//            jsonArray = new JSONArray(string2);
            jsonArray = new JSONArray(string);
            for (int i = 0; i < jsonArray.length(); i++) {
                Getstatus getstatus = new Getstatus();
                JSONObject jsonObject = jsonArray.getJSONObject(i);


                //Header
                Header header = new Header();
                JSONObject jsonObjectHeader = jsonObject.getJSONObject("header");
//                if (jsonObjectHeader.getString("placeholder").contains("Registration Certificate")) {
//                    continue;
//                }
                header.setPlaceholder(jsonObjectHeader.getString("placeholder"));
                header.setType(jsonObjectHeader.getString("type"));
                header.setValue(jsonObjectHeader.getString("value"));
                getstatus.setHeader(header);

                //Inputs
                List<Input> inputList = new ArrayList<>();
                JSONArray jsonArrayInputs = new JSONArray(jsonObject.getString("inputs"));
                for (int j = 0; j < jsonArrayInputs.length(); j++) {
                    Input input = new Input();
                    JSONObject jsonObjectInput = jsonArrayInputs.getJSONObject(j);
                    input.setType(jsonObjectInput.getString("type"));
                    input.setServerValue(jsonObjectInput.getString("server_value"));
                    input.setValue(jsonObjectInput.getString("value"));
                    input.setKey(jsonObjectInput.getString("key"));
//                    input.setPlaceholder(jsonObjectInput.getString("placeholder"));
//                    if (jsonObjectInput.has("is_front")) {
//                        input.setIsFront(jsonObjectInput.getString("is_front"));
//                    }

                    if (jsonObjectInput.has("input")) {
                        input.setInput(jsonObjectInput.getString("input"));
                    } else {
                        input.setInput("");
                    }

                    if (jsonObjectInput.has("is_front")) {
                        input.setIsFront(jsonObjectInput.getString("is_front"));
                    } else {
                        input.setIsFront("");
                    }
                    if (!jsonObjectInput.has("placeholder") || jsonObjectInput.isNull("placeholder") || jsonObjectInput.getString("placeholder").isEmpty()) {
                        input.setPlaceholder("");
                    } else {
                        input.setPlaceholder(jsonObjectInput.getString("placeholder"));
                    }

                    keySelectList.add(input.getKey() + " " + input.getPlaceholder());
                    inputList.add(input);

                }
                getstatus.setInputs(inputList);
                setHorizontalLinearLayout(getstatus);
                getstatusList.add(getstatus);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        linear_root_documents.setBackgroundColor(getResources().getColor(android.R.color.white));

        View view = new View(this);
        view.setBackgroundColor(getResources().getColor(R.color.white));
        FrameLayout.LayoutParams viewParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 20);
        viewParams.setMargins(0, 10, 0, 0);
        view.setLayoutParams(viewParams);

        linear_root_documents.addView(view);

        linear_root_documents.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void setHorizontalLinearLayout(final Getstatus getstatus) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setBackgroundResource(R.drawable.border);
        LinearLayout.LayoutParams paramsLinearLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLinearLayout.setMargins(40, 20, 40, 0);
        linearLayout.setLayoutParams(paramsLinearLayout);

        TextView tvName = new TextView(this);
        LinearLayout.LayoutParams tvNameParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvNameParams.setMargins(20, 45, 40, 20);
        tvName.setLayoutParams(tvNameParams);
        tvName.setTextSize(14);
        tvName.setTextColor(Color.BLACK);
        tvName.setTypeface(Typeface.createFromAsset(getAssets(), getResources().getString(R.string.KarlaRegular)));
        if (getstatus.getHeader().getPlaceholder().contains("license")) {
            tvName.setText(Localizer.getLocalizerString("k_r51_s8_drving_lic"));
        } else if (getstatus.getHeader().getPlaceholder().contains("Registration Certificate")) {
            tvName.setText(Localizer.getLocalizerString("k_r53_s8_reg_cert"));
        } else if (getstatus.getHeader().getPlaceholder().contains("Insurance")) {
            tvName.setText(Localizer.getLocalizerString("k_r52_s8_ins"));
        }
        linearLayout.addView(tvName);

        //Changed here


        LinearLayout linearLayoutHorizontal = new LinearLayout(this);
        LinearLayout.LayoutParams paramslinearLayouttHorizontal = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearLayoutHorizontal.setWeightSum(4);
        paramslinearLayouttHorizontal.setMargins(40, 0, 20, 40);
        linearLayoutHorizontal.setLayoutParams(paramslinearLayouttHorizontal);
        LinearLayout linearLayoutVertical = new LinearLayout(this);
        LinearLayout.LayoutParams paramslinearLayoutVertical = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        ScrollView scrollView = new ScrollView(this);
//        scrollView.addView(linearLayout);
        linearLayoutVertical.setLayoutParams(paramslinearLayoutVertical);


        for (int a = 0; a < getstatus.getInputs().size(); a++) {
            if (getstatus.getInputs().get(a).getType().equalsIgnoreCase("ImageView")) {
                TextView tvLabel = new TextView(this);
                LinearLayout.LayoutParams tvLabelParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.5f);
                tvNameParams.setMargins(40, 45, 100, 20);
                tvLabelParams.gravity = Gravity.BOTTOM;
                tvLabel.setLayoutParams(tvLabelParams);
                if (getstatus.getInputs().get(a).getIsFront().equalsIgnoreCase("1")) {
                    tvLabel.setText(Localizer.getLocalizerString("k_r46_s8_frnt"));
                    frontbacknameslist.add(Localizer.getLocalizerString("k_r46_s8_frnt"));
                } else {
                    tvLabel.setText(Localizer.getLocalizerString("k_r47_s8_back"));
                    frontbacknameslist.add(Localizer.getLocalizerString("k_r47_s8_back"));
                }
                tvLabel.setTextSize(14);
                tvLabel.setTextColor(Color.BLACK);
                tvLabel.setTypeface(Typeface.createFromAsset(getAssets(), getResources().getString(R.string.KarlaRegular)));
                linearLayoutHorizontal.addView(tvLabel);

                ImageView imageView = new ImageView(this);
                imageView.setTag(getstatus.getInputs().get(a));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                LinearLayout.LayoutParams paramsImageView = new LinearLayout.LayoutParams(120, 120, 0.5f);
                paramsImageView.setMargins(0, 10, 40, 10);
                imageView.setImageResource(R.drawable.ic_outline_add_photo_alternate_24);
                imageView.setLayoutParams(paramsImageView);
                linearLayoutHorizontal.addView(imageView);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        driverAssetId = String.valueOf(imageView.getId());
                        DocUploadActivity.this.onClick(view);
                    }
                });
                imageViewList.add(imageView);
                imageViewListTags.add(getstatus.getHeader().getPlaceholder() + " " + getstatus.getInputs().get(a).getPlaceholder());
                isFrontList.add(getstatus.getInputs().get(a).getIsFront());
                assetTypeList.add(getstatus.getInputs().get(a).getKey());
                if (getstatus.getHeader().getPlaceholder().contains("license")) {
//                    tvName.setText(Localizer.getLocalizerString("k_4_s7_add_license"));
                    if (getstatus.getInputs().get(a).getIsFront().equalsIgnoreCase("1")) {
                        placeholdernameslist.add(Localizer.getLocalizerString("k_r44_s8_plz_upld_img_of") + " " + Localizer.getLocalizerString("k_r51_s8_drving_lic") + " " + Localizer.getLocalizerString("k_r46_s8_frnt"));
                    } else {
                        placeholdernameslist.add(Localizer.getLocalizerString("k_r44_s8_plz_upld_img_of") + " " + Localizer.getLocalizerString("k_r51_s8_drving_lic") + " " + Localizer.getLocalizerString("k_r47_s8_back"));
                    }

                } else if (getstatus.getHeader().getPlaceholder().contains("Registration Certificate")) {
//                    tvName.setText(Localizer.getLocalizerString("k_r48_s8_reg_cert"));
//                    placeholdernameslist.add(Localizer.getLocalizerString("k_r48_s8_reg_cert"));
                    if (getstatus.getInputs().get(a).getIsFront().equalsIgnoreCase("1")) {
                        placeholdernameslist.add(Localizer.getLocalizerString("k_r44_s8_plz_upld_img_of") + " " + Localizer.getLocalizerString("k_r53_s8_reg_cert") + " " + Localizer.getLocalizerString("k_r46_s8_frnt"));
                    } else {
                        placeholdernameslist.add(Localizer.getLocalizerString("k_r44_s8_plz_upld_img_of") + " " + Localizer.getLocalizerString("k_r53_s8_reg_cert") + " " + Localizer.getLocalizerString("k_r47_s8_back"));
                    }
                } else if (getstatus.getHeader().getPlaceholder().contains("Insurance")) {
//                    tvName.setText(Localizer.getLocalizerString("k_5_s7_add_insurance"));
//                    placeholdernameslist.add(Localizer.getLocalizerString("k_5_s7_add_insurance"));
                    if (getstatus.getInputs().get(a).getIsFront().equalsIgnoreCase("1")) {
                        placeholdernameslist.add(Localizer.getLocalizerString("k_r44_s8_plz_upld_img_of") + " " + Localizer.getLocalizerString("k_r52_s8_ins") + " " + Localizer.getLocalizerString("k_r46_s8_frnt"));
                    } else {
                        placeholdernameslist.add(Localizer.getLocalizerString("k_r44_s8_plz_upld_img_of") + " " + Localizer.getLocalizerString("k_r52_s8_ins") + " " + Localizer.getLocalizerString("k_r47_s8_back"));
                    }

                }
//                placeholdernameslist.add(getstatus.getHeader().getPlaceholder());
                servervaluelist.add(getstatus.getInputs().get(a).getServerValue());
            } else if (getstatus.getInputs().get(a).getType().equalsIgnoreCase("EditText")) {
                LinearLayout linearLayoutBottomHorizontal = new LinearLayout(this);
                LinearLayout.LayoutParams paramslinearLayoutBottomHorizontal = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramslinearLayoutBottomHorizontal.setMargins(40, 0, 20, 0);
                linearLayoutBottomHorizontal.setWeightSum(2);
                linearLayoutBottomHorizontal.setLayoutParams(paramslinearLayoutBottomHorizontal);

                TextView tvLabel = new TextView(this);
                LinearLayout.LayoutParams tvLabelParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                tvNameParams.setMargins(40, 45, 140, 20);
                tvLabelParams.gravity = Gravity.CENTER;
                tvLabel.setLayoutParams(tvLabelParams);
                tvLabel.setText(Localizer.getLocalizerString("k_r45_s8_dte_of_exp"));
                tvLabel.setTextSize(14);
                tvLabel.setTextColor(Color.BLACK);
                tvLabel.setTypeface(Typeface.createFromAsset(getAssets(), getResources().getString(R.string.KarlaRegular)));
//                linearLayoutBottomHorizontal.addView(tvLabel);

                EditText editText = new EditText(this);
                editText.setId(imageViewList.get(a - 1).getId());
                editText.setBackgroundColor(Color.WHITE);
                editText.setTag(getstatus.getHeader().getPlaceholder());
                LinearLayout.LayoutParams layoutParamsEditText = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                layoutParamsEditText.setMargins(40, 8, 40, 10);
                editText.setLayoutParams(layoutParamsEditText);
//                editText.setHint(getstatus.getInputs().get(a).getServerValue());
                if (getstatus.getInputs().get(a).getServerValue().contains("expiry")) {
                    editText.setHint(Localizer.getLocalizerString("k_r49_s8_plz_sel_exp_dte"));
                } else {
                    editText.setHint(getstatus.getInputs().get(a).getServerValue());
                }
                editText.setTextSize(15);
                editText.setTypeface(Typeface.createFromAsset(getAssets(), getResources().getString(R.string.KarlaRegular)));
//                linearLayoutBottomHorizontal.addView(editText);
                editText.setFocusable(false);
                editText.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
                editText.setClickable(false);
                editTextList.add(editText);
                editTextListTags.add(getstatus.getInputs().get(a).getServerValue());
                editTextPlaceHolderList.add(getstatus.getHeader().getPlaceholder());
                editTextservervalue.add(getstatus.getInputs().get(a).getServerValue());


                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        editText.setText(sdf.format(myCalendar.getTime()));
                    }
                };

                editText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        new DatePickerDialog(DocUploadActivity.this, date, myCalendar
//                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                        datePicker(editText);
                    }
                });

                linearLayoutVertical.addView(linearLayoutBottomHorizontal);
            }
        }

        linearLayout.addView(linearLayoutHorizontal);
        linearLayout.addView(linearLayoutVertical);

        linear_root_documents.addView(linearLayout);
    }

    private void datePicker(EditText editText) {
        Date minimumDATE = null;
        Calendar date = Calendar.getInstance();
        date.add(Calendar.HOUR, 2);
        long t = date.getTimeInMillis();
        minimumDATE = new Date(t + (Constants.Keys.BOOKING_TIME_RIDE_LATER * ONE_MINUTE_IN_MILLIS));
        singleDateTimePicker = new SingleDateAndTimePickerDialog.Builder(DocUploadActivity.this)
                .displayListener(picker -> Log.d("pickerlis", "" + picker))
                .curved()
                .mustBeOnFuture()
                .title(Localizer.getLocalizerString("k_r49_s8_plz_sel_exp_dte"))
                .mainColor(getResources().getColor(R.color.black_dark))
                .minDateRange(minimumDATE)
                .defaultDate(minimumDATE).setCancelable()
                .displayMinutes(false)
                .displayHours(false)
                .displayDays(false)
                .displayMonth(true)
                .displayYears(true)
                .displayDaysOfMonth(true)
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        // Vibrate for 500 milliseconds
                        v.vibrate(20);
                    }
                })
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String finalDate = simpleDateFormat.format(date);
                        editText.setText(finalDate);

                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        // Vibrate for 500 milliseconds
                        v.vibrate(20);

                    }
                }).minDateRange(minimumDATE).build();
        singleDateTimePicker.display();

    }

    private boolean isValidDate(String dateOfBirth) {
        boolean valid = true;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dob = formatter.parse(dateOfBirth);
        } catch (Exception e) {
            valid = false;
        }
        return valid;
    }

    private boolean checkPermission() {
        int resultCamera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int resultStorage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return resultCamera == PackageManager.PERMISSION_GRANTED && resultStorage == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkPermissionCamera() {
        int resultCamera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        return resultCamera == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkPermissionStorage() {
        int resultStorage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return resultStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            Toast.makeText(controller,
//                    "Camera and Storage permissions needed. Please allow in your application settings.",
//                    Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }


    }

    private void requestPermissionStorage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            Toast.makeText(controller,
//                    "Camera and Storage permissions needed. Please allow in your application settings.",
//                    Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }


    }

    private void requestPermissionCamera() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {

            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CODE);
        }
    }

    private void setLocalizerString() {
        TextView tv_upload_documents, tv_vehical_info, tv_car_name, tv_vehical_model;
        TextView tv_car_year, tv_licence_number, nextdoc;
        Button add_id_proof, add_license, addinsurance;
        BButton confirmdoc;
        EditText carlist, select_a_model, select_a_year, ca_reg_no;

//        tv_upload_documents = findViewById(R.id.tv_upload_documents);
        tv_vehical_info = findViewById(R.id.tv_vehical_info);
        tv_car_name = findViewById(R.id.tv_car_name);
        tv_vehical_model = findViewById(R.id.tv_vehical_model);
        tv_car_year = findViewById(R.id.tv_car_year);
        tv_licence_number = findViewById(R.id.tv_licence_number);
        nextdoc = findViewById(R.id.nextdoc);
        confirmdoc = findViewById(R.id.confirmdoc);

        add_id_proof = findViewById(R.id.add_id_proof);
        add_license = findViewById(R.id.add_license);
        addinsurance = findViewById(R.id.addinsurance);

        carlist = findViewById(R.id.carlist);
        select_a_model = findViewById(R.id.select_a_model);
        select_a_year = findViewById(R.id.select_a_year);
        ca_reg_no = findViewById(R.id.ca_reg_no);

        add_id_proof.setText(Localizer.getLocalizerString("k_3_s7_add_id_proof"));
        add_license.setText(Localizer.getLocalizerString("k_4_s7_add_license"));
        addinsurance.setText(Localizer.getLocalizerString("k_5_s7_add_insurance"));

        tv_car_name.setText(Localizer.getLocalizerString("k_6_s7_car_name"));
        tv_vehical_model.setText(Localizer.getLocalizerString("k_8_s7_vehicle_model"));
        tv_car_year.setText(Localizer.getLocalizerString("k_10_s7_vehicle_year"));
//        tv_upload_documents.setText(Localizer.getLocalizerString("k_1_s7_update_documents"));
//        tv_upload_documents.setText(Localizer.getLocalizerString("k_11_s7_vehicle_details"));
        tv_vehical_info.setText(Localizer.getLocalizerString("k_2_s7_vehicle_info"));
        tv_licence_number.setText(Localizer.getLocalizerString("k_36_s7_enter_licence_number"));
        nextdoc.setText(Localizer.getLocalizerString("k_14_s7_next"));

        carlist.setHint(Localizer.getLocalizerString("k_7_s7_enter_vehicle_name"));
        select_a_model.setHint(Localizer.getLocalizerString("k_9_s7_enter_vehicle_model"));
        select_a_year.setHint(Localizer.getLocalizerString("k_11_s7_enter_vehicle_mfg_year"));
        ca_reg_no.setHint(Localizer.getLocalizerString("k_12_s7_enter_licence_number"));

        confirmdoc.setText(Localizer.getLocalizerString("k_34_s6_save"));

        carlistSpinner.setHint(Localizer.getLocalizerString("k_7_s7_enter_vehicle_name"));
    }

    public void getCities() {
        WebServiceUtil.excuteRequest(this, null, Constants.Urls.URL_GET_CITIES, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    citiesCountryCode = City.parseCities(response);
//                    citiesCountryCode.add(0, new City(getString(R.string.select)));
                    citiesCountryCode.add(0, new City(Localizer.getLocalizerString("k_26_s2_select")));
                    controller.pref.setCitiesResponse(response);
                    adapterCountryCode.setCountryCodes(citiesCountryCode);

                    if (controller.getLoggedDriver() != null && controller.getLoggedDriver().getCity_id() != null) {
                        for (int i = 0; i < citiesCountryCode.size(); i++) {
                            City city = citiesCountryCode.get(i);
                            if (city != null && city.getCity_id() != null && city.getCity_id().equals(controller.getLoggedDriver().getCity_id())) {
                                spCity.setSelection(i, true);
                                break;
                            }
                        }
                    }

                } else {
                    if (error instanceof ServerError) {
                        String s = new String(error.networkResponse.data);
                        try {
                            JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (controller != null)
                            Toast.makeText(controller, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        try {
//            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                Cursor cursor = getContentResolver().query(Objects.requireNonNull(selectedImage), filePathColumn,
//                        null, null, null);
//                Objects.requireNonNull(cursor).moveToFirst();
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                picturePath = cursor.getString(columnIndex);
//                cursor.close();
//                progressDialog.showDialog();
//                if (count == 0) {
//                    lic.setScaleType(ImageView.ScaleType.FIT_XY);
//                    File file = new File(picturePath);
//                    Uri uri = Uri.fromFile(file);
//
////                    Picasso.get().load(uri).into(lic);  //set your images from here to selected imageview
////                    for (int i=0; i<imageViewListTags.size(); i++) {
////                        if (imageViewListTags.get(i).equals(publicInput.getServerValue())) {
////                            Picasso.get().load(uri).into(imageViewList.get(i));
////                            break;
////                        }
////                    }
//
//                    if (publicInput != null) {
//                        for (int j = 0; j < assetTypeList.size(); j++) {
//                            if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
//                                if (publicInput.getIsFront().equalsIgnoreCase("1")) {
//                                    Picasso.get().load(uri).into(imageViewList.get(j));
//                                    break;
//                                }
//                            }
//                        }
//
//                        for (int j = 0; j < assetTypeList.size(); j++) {
//                            if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
//                                if (publicInput.getIsFront().equalsIgnoreCase("0")) {
//                                    Picasso.get().load(uri).into(imageViewList.get(j + 1));
//                                    break;
//                                }
//                            }
//                        }
//                    } else {
//                        Picasso.get().load(uri).into(lic);
//
//                        ivCarImage.setVisibility(View.VISIBLE);
//                        Picasso.get().load(uri).into(ivCarImage);
//                    }
//
//
//                    new ProgressTask().execute();
//
//                } else {
//                    vote.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    File file = new File(picturePath);
//                    Uri uri = Uri.fromFile(file);
//
//                    Picasso.get().load(uri).into(lic);
//                    new ProgressTask().execute();
//
//
//                }
//            } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && null != data) {
//                Bitmap photo = null;
//                photo = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
//
//                picturePath = storeImage(photo);
//
//                Log.d(TAG, "onActivityResult Camera " + Objects.requireNonNull(data.getExtras()).get("data") + " picturePath: " + picturePath);
//
//                File file = new File(picturePath);
//                Uri uri = Uri.fromFile(file);
//
//                if (publicInput != null) {
//                    for (int j = 0; j < assetTypeList.size(); j++) {
//                        if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
//                            if (publicInput.getIsFront().equalsIgnoreCase("1")) {
//                                Picasso.get().load(uri).into(imageViewList.get(j));
//                                break;
//                            }
//                        }
//                    }
//
//                    for (int j = 0; j < assetTypeList.size(); j++) {
//                        if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
//                            if (publicInput.getIsFront().equalsIgnoreCase("0")) {
//                                Picasso.get().load(uri).into(imageViewList.get(j + 1));
//                                break;
//                            }
//                        }
//                    }
//                } else {
//
//                    Picasso.get().load(uri).into(lic);
//
//                    ivCarImage.setVisibility(View.VISIBLE);
//                    Picasso.get().load(uri).into(ivCarImage);
//                }
//
//
//                new ProgressTask().execute();
//
//            } else if (requestCode == RESULT_LOAD_CAR_IMAGE && resultCode == RESULT_OK && null != data) {
//
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                Cursor cursor = getContentResolver().query(Objects.requireNonNull(selectedImage), filePathColumn,
//                        null, null, null);
//                Objects.requireNonNull(cursor).moveToFirst();
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                picturePath = cursor.getString(columnIndex);
//                cursor.close();
//                progressDialog.showDialog();
//                if (count == 0) {
//                    lic.setScaleType(ImageView.ScaleType.FIT_XY);
//                    File file = new File(picturePath);
//                    Uri uri = Uri.fromFile(file);
//                    ivCarImage.setVisibility(View.VISIBLE);
//                    Picasso.get().load(uri).into(ivCarImage);  //set your images from here to selected imageview
//                    new ProgressTask().execute();
//                } else {
//                    vote.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    File file = new File(picturePath);
//                    Uri uri = Uri.fromFile(file);
//
//                    Picasso.get().load(uri).into(lic);
//
//                    ivCarImage.setVisibility(View.VISIBLE);
//                    Picasso.get().load(uri).into(ivCarImage);
//                    new ProgressTask().execute();
//                }
//            }
//
//        } catch (Exception e) {
//            Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_15_s7_invalid_image_format"), Toast.LENGTH_LONG).show();
//        }

        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
                progressDialog.showDialog();

                Uri pictureUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(Objects.requireNonNull(pictureUri));
                pictureBitmap = getResizedBitmap(rotateImageIfRequired(BitmapFactory.decodeStream(imageStream), DocUploadActivity.this, pictureUri));
                if (count == 0) {
                    if (publicInput != null) {
                        for (int j = 0; j < assetTypeList.size(); j++) {
                            if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
                                if (publicInput.getIsFront().equalsIgnoreCase("1")) {
                                    imageViewList.get(j).setImageBitmap(pictureBitmap);
                                    new ProgressTask().execute();
                                    break;
                                }
                            }
                        }

                        for (int j = 0; j < assetTypeList.size(); j++) {
                            if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
                                if (publicInput.getIsFront().equalsIgnoreCase("0")) {
                                    imageViewList.get(j + 1).setImageBitmap(pictureBitmap);
                                    new ProgressTask().execute();
                                    break;
                                }
                            }
                        }
                        publicInput = null;
                    } else {
                        ivCarImage.setVisibility(View.VISIBLE);
                        ivCarImage.setImageBitmap(pictureBitmap);
                        progressDialog.dismiss();
                        new ProgressTask().execute();
                    }
                } else {
                    ivCarImage.setImageBitmap(pictureBitmap);
                    progressDialog.dismiss();
                    new ProgressTask().execute();
                }
            } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && null != data) {
                progressDialog.showDialog();
                Bitmap photo = null;
                photo = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
                pictureBitmap = photo;
                if (publicInput != null) {
                    for (int j = 0; j < assetTypeList.size(); j++) {
                        if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
                            if (publicInput.getIsFront().equalsIgnoreCase("1")) {
                                imageViewList.get(j).setImageBitmap(pictureBitmap);
                                new ProgressTask().execute();
                                break;
                            }
                        }
                    }

                    for (int j = 0; j < assetTypeList.size(); j++) {
                        if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
                            if (publicInput.getIsFront().equalsIgnoreCase("0")) {
                                imageViewList.get(j + 1).setImageBitmap(pictureBitmap);
                                new ProgressTask().execute();
                                break;
                            }
                        }
                    }
                    publicInput = null;
                } else {
                    ivCarImage.setVisibility(View.VISIBLE);
                    ivCarImage.setImageBitmap(pictureBitmap);
                    progressDialog.dismiss();
                    new ProgressTask().execute();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return pictureFile.getAbsolutePath();
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        Random random = new Random();
        timeStamp = timeStamp + random.nextInt(1000);
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private String ImageWrite(Bitmap bitmap1) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream;

        File file = new File(extStorageDirectory, "slectimage.PNG");

        try {

            outStream = new FileOutputStream(file);
            bitmap1.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }

        return getString(R.string.image_path);

    }

    private void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");

                    finish();

                }
            }
        }
    }

    private void clearSavedPreferences() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();

    }

    private void getCarCategoryApi() {
        if (selectedCity == null)
            return;
        progressDialog.showDialog();
        Map<String, String> params = new HashMap<>();
        params.put("city_id", selectedCity);
        ServerApiCall.callWithApiKey(DocUploadActivity.this, params, Constants.Urls.GET_CAR_CATEGORY, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    progressDialog.dismiss();
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        categoryResponseList = CategoryActors.parseCarCategoriesResponse(response);
                        category = new String[categoryResponseList.size() + 1];
                        //catSize = categoryResponseList.size();
                        getCategory(categoryResponseList);

                    } else {
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void getCategory(ArrayList<CategoryActors> categoryActor) {
        Collections.sort(categoryActor, new Comparator<CategoryActors>() {
            @Override
            public int compare(CategoryActors o1, CategoryActors o2) {
                return o1.getSort_order().compareTo(o2.getSort_order());
            }
        });
        catArrayList = new ArrayList<>();
        CarCategory catOb = new CarCategory("dummy", "0");
        catArrayList.add(catOb);
//        category[0] = getString(R.string.select_category);
        category[0] = Localizer.getLocalizerString("k_31_s7_select_category");
        for (int j = 0; j < (categoryActor.size()); j++) {
            CategoryActors categoryActors = categoryActor.get(j);
            category[j + 1] = categoryActors.getCat_name();
            CarCategory catObj = new CarCategory(categoryActors.getCategory_id(), categoryActors.getCategory_id());
            catArrayList.add(catObj);
        }
//        if (catArrayList.size() == 2) {
//            seletedcategory = catArrayList.get(1).getCategory_id();
//            categorylistSpinner.setVisibility(View.GONE);
//            spinnerLayoutCat.setVisibility(View.GONE);
//        } else {
//            categorylistSpinner.setVisibility(View.VISIBLE);
        carCategorySpinner(category);
//            spinnerLayoutCat.setVisibility(View.VISIBLE);
//        }
    }

    private void carCategorySpinner(final String[] carCategory) {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this, R.layout.spinner_text, carCategory);
        adapter.notifyDataSetChanged();
        adapter.setNotifyOnChange(true);
        categorylistSpinner.setAdapter(adapter);
        categorylistSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0 && catArrayList != null && position < catArrayList.size()) {
                    seletedcategory = catArrayList.get(position).getCategory_id();
                } else {
                    seletedcategory = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (controller.isDocUpdate()) {
            try {
                for (int j = 0; j < categoryResponseList.size(); j++) {
                    if (controller.getLoggedDriver().getCategory_id().equals(categoryResponseList.get(j).getCategory_id())) {
                        categorylistSpinner.setSelection(j + 1);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void docSubmitApi(final String year, final String carMakeString,
                              final String carModelString, final String reg_no, final String seletedcategory, final String selectedCity, final String carId, boolean isVerified) {

        progressDialog.showDialog();
        Map<String, String> params = new HashMap<>();
        params.put("category_id", seletedcategory);
        params.put("city_id", selectedCity);
        if (carId != null)
            params.put("car_id", carId);
        params.put("car_model", year);
        params.put("car_reg_no", reg_no);
        params.put("car_make", carMakeString);
        params.put("car_name", carModelString);
        if (controller.getConstantsValueForKey("admin_driver_verif").equalsIgnoreCase("1")) {
            if (isVerified) {
                params.put("d_is_verified", "0");
                params.put("d_is_available", "0");
            }
        } else {
            params.put("d_is_verified", "1");
            params.put("d_is_available", "1");
        }
        if (bitmapValueCarImage != null)
            params.put("car_image", bitmapValueCarImage);

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        //        System.out.print("" + params);
        ServerApiCall.callWithApiKeyAndDriverId(DocUploadActivity.this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        Driver driver = Driver.parseJsonAfterLogin(response);
                        if (driver != null) {
                            controller.setLoggedDriver(driver);
                            if (controller.isDocUpdate()) {
                                Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_32_s7_document_uploaded_successfully_j"), Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();

                            }
//                        else {
//                            controller.logout();
//                            controller.pref.setIsLogin(false);
//                            Intent intent = new Intent(getApplicationContext(), SignInSignUpMainActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
//                        }
                            finish();
                        }
                    } else {
                        progressDialog.dismiss();

                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });

    }

    private void docUpload(final String bitmapValue) {
        Map<String, String> params = new HashMap<>();

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);
        params.put("driver_id", controller.getLoggedDriver().getDriverId());
        params.put("asset_type", asset_type);
        params.put("image_type", "jpeg");
        params.put("driver_image", bitmapValue);
        if (driverAssetId != null && !driverAssetId.equals("-1")) {
            params.put("driver_asset_id", driverAssetId);
        }
        for (int j = 0; j < editTextPlaceHolderList.size(); j++) {
            if (editTextPlaceHolderList.get(j).toLowerCase().contains(asset_type.toLowerCase()) || asset_type.toLowerCase().equalsIgnoreCase("license")) {
                if (!editTextList.get(j).getText().toString().isEmpty()) {
                    params.put("expire_date", editTextList.get(j).getText().toString());
                    break;
                }


            }
        }
        params.put("is_front", isFront);
        params.put("image_description", "");

        ServerApiCall.callWithApiKeyAndDriverId(DocUploadActivity.this, params, Constants.Urls.URL_ADD_DRIVER_ASSET, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    String response = data.toString();
//                    Driver driver = Driver.parseJsonAfterLogin(response);
//                    controller.setDriver(driver, getApplicationContext());
//                    ErrorJsonParsing parser = new ErrorJsonParsing();
//                    CloudResponse res = parser.getCloudResponse("" + response);
                    isFront = "0";
                    callGetDriverAsset();
                    isUploading = false;
                    progressDialog.dismiss();
                } else {
                    Log.e(TAG, "onUpdateDeviceTokenOnServer: onDocUpload" + error.getLocalizedMessage());
                }
            }
        });
    }

    private void callGetDriverAsset() {
        Map<String, String> params = new HashMap<>();
//        params.put(bitmapKey, bitmapValue);

        params.put(Constants.Keys.DRIVER_ID, controller.getLoggedDriver().getDriverId());
        params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApi_key());

        ServerApiCall.callWithApiKey(DocUploadActivity.this, params, Constants.Urls.URL_GET_DRIVER_ASSET, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    JSONObject jsonObject = null;
                    GetDriverAsset getDriverAsset = new GetDriverAsset();
                    try {
                        jsonObject = new JSONObject(data.toString());
                        JSONArray jsonArray = new JSONArray(jsonObject.getString("response"));
                        getDriverAsset.setCode(jsonObject.getInt("code"));
                        getDriverAsset.setNextOffset(jsonObject.getString("next_offset"));
                        getDriverAsset.setLastOffset(jsonObject.getString("last_offset"));
                        getDriverAsset.setMessage(jsonObject.getString("message"));

                        List<GetDriverAssetResponse> driverAssets;
                        Type listType = new TypeToken<List<GetDriverAssetResponse>>() {
                        }.getType();
                        driverAssets = new Gson().fromJson(String.valueOf(jsonArray), listType);
                        getDriverAsset.setGetDriverAssetResponse(driverAssets);
                        controller.setDriverAsset(getDriverAsset);

                        Log.d(TAG, "driverAssets: " + new Gson().toJson(driverAssets));
                        Log.d(TAG, "controller Driver Asset: " + new Gson().toJson(controller.getGetDriverAsset().getGetDriverAssetResponse()));

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectDriverAsset = jsonArray.getJSONObject(i);

                            String driver_asset_id = jsonObjectDriverAsset.getString("driver_asset_id");
                            String asset_type = jsonObjectDriverAsset.getString("asset_type");
                            String is_front = jsonObjectDriverAsset.getString("is_front");
                            String img_path = jsonObjectDriverAsset.getString("img_path");
//                            String expire_date = jsonObjectDriverAsset.getString("expire_date");

                            imageViewListID.add(driver_asset_id);

                            for (int j = 0; j < assetTypeList.size(); j++) {
                                if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
                                    if (is_front.equalsIgnoreCase("1")) {
                                        imageViewList.get(j).setId(Integer.parseInt(driver_asset_id));
                                        Picasso.get().load(Constants.IMAGE_BASE_URL + img_path).into(imageViewList.get(j));
//                                        imageViewList.get(j).setImageBitmap(getBitmapFromURL(Constants.IMAGE_BASE_URL + img_path));
//                                        imageViewList.get(j).setImageURI(Constants.IMAGE_BASE_URL + img_path);
                                        break;
                                    }
                                }
                            }

                            for (int j = 0; j < assetTypeList.size(); j++) {
                                if (assetTypeList.get(j).toLowerCase().contains(asset_type.toLowerCase())) {
                                    if (is_front.equalsIgnoreCase("0")) {
                                        imageViewList.get(j + 1).setId(Integer.parseInt(driver_asset_id));
                                        Picasso.get().load(Constants.IMAGE_BASE_URL + img_path).into(imageViewList.get(j + 1));
//                                        imageViewList.get(j + 1).setImageBitmap(getBitmapFromURL(Constants.IMAGE_BASE_URL + img_path));
//                                        imageViewList.get(j + 1).setImageURI(Constants.IMAGE_BASE_URL + img_path);
                                        break;
                                    }
                                }
                            }

//                            for (int j = 0; j < editTextPlaceHolderList.size(); j++) {
//                                if (editTextPlaceHolderList.get(j).toLowerCase().contains(asset_type.toLowerCase()) || asset_type.toLowerCase().equalsIgnoreCase("license")) {
//                                    if (is_front.equals("1")) {
//                                        editTextList.get(j).setText(expire_date);
//                                        break;
//                                    }
//
//                                }
//                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "onUpdateDeviceTokenOnServer docUpload: " + error.getLocalizedMessage());
                }
            }
        });

        //fetch Imageview driver_asset_id;
        for (int i = 0; i < imageViewListID.size(); i++) {
            Log.d(TAG, "driver_asset_id: " + imageViewListID.get(i) + " position " + i);
        }
    }

    private String getEncoded64ImageStringFromBitmap(Bitmap bmp) {
        Bitmap bitmap = scaleDown(bmp);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    private String getCheckLicencImg() {
        return checkLicencImg;
    }

    private void setCheckLicencImg(String checkLicencImg) {
        this.checkLicencImg = checkLicencImg;
    }

    @Override
    public void onBackPressed() {
        if (linear_root_vehicle_details.getVisibility() == View.VISIBLE) {
            linear_root_vehicle_details.setVisibility(View.GONE);
            linear_root_documents.setVisibility(View.VISIBLE);
            nextb.setVisibility(View.VISIBLE);
            confirmdoc.setVisibility(View.GONE);
            tv_upload_documents.setText(Localizer.getLocalizerString("k_3_s6_upload_doc"));
        } else {
            finish();
        }
    }

    @Override
    public void finish() {
        Intent intent;
        intent = new Intent(getApplicationContext(), SlideMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        isFinishCalled = true;
        super.finish();
    }

    @Override
    public void onClick(View view) {
        Input input = (Input) view.getTag();

        if (isUploading) {
            progressDialog.showDialog();
        } else {
            for (int i = 0; i < keySelectList.size(); i++) {
                if (keySelectList.get(i).equals(input.getKey() + " " + input.getPlaceholder())) {
                    selectImage(input);
                    asset_type = input.getKey();
                    isFront = input.getIsFront();
                    Log.d(TAG, "dsadas" + keySelectList.get(i));
                    publicInput = input;
                    break;
                }
            }

//            for (int j = 0; j < editTextPlaceHolderList.size(); j++) {
//                String header = editTextPlaceHolderList.get(j);
//                if (header.toLowerCase().contains(input.getKey().toLowerCase())) {
//                    if (editTextList.get(j).getText().toString().isEmpty()) {
//                        popUpAlertDialog(input.getKey(), j);
//                        break;
//                    } else if (j == editTextPlaceHolderList.size() - 1) {
//                        for (int i = 0; i < keySelectList.size(); i++) {
//                            if (keySelectList.get(i).equals(input.getKey() + " " + input.getPlaceholder())) {
//                                selectImage(input);
//                                asset_type = input.getKey();
//                                isFront = input.getIsFront();
//                                Log.d(TAG, "dsadas" + keySelectList.get(i));
//                                publicInput = input;
//                                break;
//
//                            }
//                        }
//                    }
//                } else if (!header.toLowerCase().contains(input.getKey().toLowerCase()) && j == editTextPlaceHolderList.size() - 1) {
//
//                }
//            }
        }

    }

    private void popUpAlertDialog(String key, int j) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        if (key.contains("LICENSE")) {
//            builder.setMessage(Localizer.getLocalizerString("k_r51_s8_drving_lic") + Localizer.getLocalizerString("k_r50_s8_field_cnt_empty"));
//        } else if (key.contains("INSURANCE")) {
//            builder.setMessage(Localizer.getLocalizerString("k_r52_s8_ins") + Localizer.getLocalizerString("k_r50_s8_field_cnt_empty"));
//        }

        if (Localizer.getLocalizerString("k_r49_s8_plz_sel_exp_dte").equalsIgnoreCase(editTextservervalue.get(j))) {
            builder.setMessage(Localizer.getLocalizerString("k_r49_s8_plz_sel_exp_dte"));
        } else {
            builder.setMessage(Localizer.getLocalizerString("k_r49_s8_plz_sel_exp_dte"));
        }


        builder.setPositiveButton(Localizer.getLocalizerString("k_r8_s8_ok"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    private void openfileExplorer(String key) {
        if (checkPermissionStorage()) {
            try {
                count = 0;
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                setCheckLicencImg(key);
            } catch (Exception e) {
                Toast.makeText(DocUploadActivity.this, Localizer.getLocalizerString("k_15_s7_invalid_image_format"), Toast.LENGTH_LONG).show();
            }
        } else {
            requestPermissionStorage();
        }
    }

    private void selectImage(Input input) {
        try {
            final CharSequence[] options = {Localizer.getLocalizerString("k_28_s6_camera_j"), Localizer.getLocalizerString("k_29_s6_gallery_j"),
                    Localizer.getLocalizerString("k_30_s6_cancel_j")};

            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(DocUploadActivity.this);
            builder.setTitle(Localizer.getLocalizerString("k_r18_s5_chse_img"));
            builder.setItems(options, (dialog, item) -> {
                if (options[item].equals(Localizer.getLocalizerString("k_28_s6_camera_j"))) {
                    if (checkPermissionCamera()) {
                        dialog.dismiss();
                        setCheckLicencImg(input.getKey());
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    } else {
                        requestPermissionCamera();
                    }
                } else if (options[item].equals(Localizer.getLocalizerString("k_29_s6_gallery_j"))) {
                    dialog.dismiss();
                    openfileExplorer(input.getKey());
                } else if (options[item].equals(Localizer.getLocalizerString("k_30_s6_cancel_j"))) {
                    dialog.dismiss();
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private class ProgressTask extends AsyncTask<String, Void, Boolean> {

        ProgressTask() {
        }

        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        @Override
        protected Boolean doInBackground(final String... args) {
            try {
                Bitmap imgBitmap = BitmapFactory.decodeFile(picturePath);
//                if (imgBitmap != null) {
//                    final String bitmap3 = getEncoded64ImageStringFromBitmap(imgBitmap);
//                    String checkImgBtn = getCheckLicencImg();
//                    if (checkImgBtn.equalsIgnoreCase(Constants.D_ASSET_TYPE_LICENSE)) {
//                        controller.pref.setLICENSE_BITMAP_TO_STRING_(bitmap3);
//                        docUpload(bitmap3);
//                    } else if (checkImgBtn.equalsIgnoreCase(Constants.D_ASSET_TYPE_RC)) {
//                        controller.pref.setBITMAP_STRING(bitmap3);
//                        docUpload(bitmap3);
//                    } else if (checkImgBtn.equalsIgnoreCase(Constants.D_ASSET_TYPE_INSURANCE)) {
//                        controller.pref.setINSURANCE_BITMAP_TO_STRING_(bitmap3);
//                        docUpload(bitmap3);
//                    } else if (checkImgBtn.equalsIgnoreCase(Constants.D_CAR_IMAGE)) {
//                        controller.pref.setCAR_IMAGE_TO_STRING(bitmap3);
//                        bitmapValueCarImage = bitmap3;
//                    }
//                }

                if (pictureBitmap != null) {
                    final String bitmap3 = getEncoded64ImageStringFromBitmap(pictureBitmap);
                    String checkImgBtn = getCheckLicencImg();
                    isUploading = true;
//                    progressDialog.showDialog();
                    if (checkImgBtn.equalsIgnoreCase(Constants.D_ASSET_TYPE_LICENSE)) {
                        controller.pref.setLICENSE_BITMAP_TO_STRING_(bitmap3);
                        docUpload(bitmap3);
                    } else if (checkImgBtn.equalsIgnoreCase(Constants.D_ASSET_TYPE_RC)) {
                        controller.pref.setBITMAP_STRING(bitmap3);
                        docUpload(bitmap3);
                    } else if (checkImgBtn.equalsIgnoreCase(Constants.D_ASSET_TYPE_INSURANCE)) {
                        controller.pref.setINSURANCE_BITMAP_TO_STRING_(bitmap3);
                        docUpload(bitmap3);
                    } else if (checkImgBtn.equalsIgnoreCase(Constants.D_CAR_IMAGE)) {
                        controller.pref.setCAR_IMAGE_TO_STRING(bitmap3);
                        bitmapValueCarImage = bitmap3;
                        Log.e(TAG, "doInBackground: " + bitmapValueCarImage.toString());
                    }
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        }

    }

}