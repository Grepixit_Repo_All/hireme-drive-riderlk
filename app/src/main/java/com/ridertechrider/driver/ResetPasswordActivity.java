package com.ridertechrider.driver;


import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.hbb20.CountryCodePicker;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ridertechrider.driver.utils.Utils.removedFirstZeroMobileNumber;
import static com.ridertechrider.driver.webservice.Constants.Values.USE_EMAIL_AUTH;

public class ResetPasswordActivity extends BaseActivity {
    protected static final String TAG = "ResetPasswordActivity";
    @BindView(R.id.mobile_no)
    EditText etPhone;
    @BindView(R.id.labelField)
    TextView labelField;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    private CustomProgressDialog progressDialog;
    private boolean isCalling;
//    private Spinner spCountryCode;
//    private ArrayList<City> citiesCountryCode = new ArrayList<>();
//    private CountryCodeAdapter adapterCountryCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resetpassword);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        if (getActionBar() != null) {
            getActionBar().hide();
        }
        ButterKnife.bind(this);
        progressDialog = new CustomProgressDialog(ResetPasswordActivity.this);

//        citiesCountryCode = Controller.getInstance().getCities();
//        if (!USE_EMAIL_AUTH)
//            citiesCountryCode.add(0, new City(getString(R.string.select)));
//        spCountryCode = findViewById(R.id.spCountryCode);
//        adapterCountryCode = new CountryCodeAdapter(this, R.layout.spinner_text_cc, citiesCountryCode);
//        spCountryCode.setAdapter(adapterCountryCode);
        setLocalizerString();
        if (USE_EMAIL_AUTH) {
//            spCountryCode.setVisibility(View.GONE);
            ccp.setVisibility(View.GONE);
            etPhone.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            etPhone.setHint(Localizer.getLocalizerString("k_11_s1_plz_enter_valid_email"));
            labelField.setText(Localizer.getLocalizerString("k_13_s1_email"));
        } else {
            etPhone.setInputType(InputType.TYPE_CLASS_PHONE);
            etPhone.setHint(Localizer.getLocalizerString("k_2_s1_mobile_number_hint"));
            labelField.setText(Localizer.getLocalizerString("k_1_s1_mobile"));
        }
//        getCities();
    }

    private void setLocalizerString() {
        TextView tv_forgot_password, labelField;
        EditText mobile_no;
        Button reset;

        tv_forgot_password = findViewById(R.id.tv_forgot_password);
//        labelField = findViewById(R.id.labelField);
//        mobile_no = findViewById(R.id.mobile_no);
        reset = findViewById(R.id.reset);

        tv_forgot_password.setText(Localizer.getLocalizerString("k_1_s3_forgot_password"));

//        labelField.setText(Localizer.getLocalizerString("k_1_s1_mobile"));

//        mobile_no.setHint(Localizer.getLocalizerString("k_1_s1_mobile"));

        reset.setText(Localizer.getLocalizerString("k_4_s3_reset_password"));


    }

//    public void getCities() {
//        WebServiceUtil.excuteRequest(this, null, Constants.Urls.URL_GET_CITIES, new WebServiceUtil.DeviceTokenServiceListener() {
//            @Override
//            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
//                if (isUpdate) {
//                    String response = data.toString();
//                    ErrorJsonParsing parser = new ErrorJsonParsing();
//                    CloudResponse res = parser.getCloudResponse("" + response);
//                    citiesCountryCode = City.parseCities(response);
//                    if (!USE_EMAIL_AUTH)
//                        citiesCountryCode.add(0, new City(getString(R.string.select)));
//                    Controller.getInstance().pref.setCitiesResponse(response);
//                    adapterCountryCode.setCountryCodes(citiesCountryCode);
//                } else {
//                    if (error instanceof ServerError) {
//                        String s = new String(error.networkResponse.data);
//                        try {
//                            JSONObject jso = new JSONObject(s);
////Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
////
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        Toast.makeText(Controller.getInstance(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//    }

    @OnClick(R.id.recancel)
    public void onBackButtonClicked(View v) {
        onBackPressed();
    }

    @OnClick(R.id.reset)
    public void onRestPassordButtonClicked(View v) {
        if (!isCalling) {

            final String phone = etPhone.getText().toString().trim();
            if (USE_EMAIL_AUTH) {
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(phone).matches()) {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_14_s3_plz_enter_valid_email"), Toast.LENGTH_LONG).show();
                    etPhone.requestFocus();
                } else {
                    sendRestPasswordToEmailId(phone);
                }
            } else {
//                if (spCountryCode.getSelectedItemPosition() == 0) {
                Pattern pattern = Pattern.compile("\\d+");
                if (ccp.getSelectedCountryCodeAsInt() == 0) {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_15_s3_select_country_code"), Toast.LENGTH_LONG).show();
                    etPhone.requestFocus();
                } else if (phone.length() <= 5 || phone.length() >= 15 || !pattern.matcher(phone).matches()) {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_21_s2_plz_enter_valid_mobile_number"), Toast.LENGTH_LONG).show();
                    etPhone.requestFocus();
                } else {
                    resetPasswordWithPhoneAuth(phone);
                }
            }
        }
    }

    private void resetPasswordWithPhoneAuth(final String email) {
        final Map<String, String> params = new HashMap<>();
        params.put("username", removedFirstZeroMobileNumber(email));

//        params.put(Constants.Keys.CITY_ID, adapterCountryCode.getCityId(spCountryCode.getSelectedItemPosition()));
//        params.put(Constants.Keys.COUNTRY_CODE, adapterCountryCode.getCountryCode(spCountryCode.getSelectedItemPosition()));
        params.put(Constants.Keys.COUNTRY_CODE, ccp.getSelectedCountryCodeAsInt() + "");
        //System.out.println("Params : " + params);
        isCalling = true;
        progressDialog.showDialog();
        WebServiceUtil.excuteRequest(ResetPasswordActivity.this, params, Constants.Urls.URL_DRIVER_PHONE_VALIDATE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                isCalling = false;
                progressDialog.dismiss();
                Log.d(TAG, "onUpdateDeviceTokenOnServer: data: " + data.toString());
                if (isUpdate) {
                    try {
                        final JSONObject obj = new JSONObject(data.toString());
                        String status = obj.has("status") ? obj.getString("status") : "";
                        if (status != null)
                            status = status.trim();
                        boolean response = (obj.has("response") && !obj.isNull("response") && obj.get("response") instanceof Boolean) && obj.getBoolean("response");
                        String rsp = (obj.has("response") && !obj.isNull("response") && obj.get("response") instanceof JSONObject) ? obj.getJSONObject("response").toString() : "";
                        //comment
                        if (status != null && status.equalsIgnoreCase("OK") && response) {
                            Toast.makeText(ResetPasswordActivity.this, Localizer.getLocalizerString("k_17_s3_phone_number_does_not_exists"), Toast.LENGTH_SHORT).show();

                        } else {
                            final String otp = genrateOtp();

                            progressDialog.showDialog();
                            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                            String onlyDigitsMobileNumber = removedFirstZeroMobileNumber(email).replaceAll("[^0-9]+", "");
                            char value1 = onlyDigitsMobileNumber.charAt(0);
                            if (String.valueOf(value1).equals("0")) {
                                StringBuilder builder = new StringBuilder(onlyDigitsMobileNumber);
                                builder.deleteCharAt(0);
                                onlyDigitsMobileNumber = builder.toString();
                            }
                            Log.e("onlyDigistsN", "" + onlyDigitsMobileNumber);
//                            String phoneNumber = adapterCountryCode.getCountryCode(spCountryCode.getSelectedItemPosition()) + onlyDigitsMobileNumber;
                            String phoneNumber = ccp.getSelectedCountryCodeAsInt() + onlyDigitsMobileNumber;

                            String otpMessage = otpMessage(otp);
                            Call<ResponseBody> request = apiInterface.sendOtp(otpMessage, phoneNumber);
                            request.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    progressDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        Toast.makeText(ResetPasswordActivity.this, Localizer.getLocalizerString("k_9_s3_messege_sent_successfull"), Toast.LENGTH_SHORT).show();
                                        Driver driver = Driver.parseJsonAfterLogin(obj.toString());

                                        if (driver != null && driver.getDriverId() != null) {
                                            Intent intent = new Intent(ResetPasswordActivity.this, OTPActivity.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("otp", otp);
                                            bundle.putString("user_id", driver.getDriverId());
                                            bundle.putString("api_key", driver.getApiKey());
//                                            bundle.putString("u_city_id", adapterCountryCode.getCityId(spCountryCode.getSelectedItemPosition()));
//                                            bundle.putString("u_country_code", adapterCountryCode.getCountryCode(spCountryCode.getSelectedItemPosition()));
                                            bundle.putString("u_country_code", ccp.getSelectedCountryCodeAsInt() + "");
                                            bundle.putString("u_phone", removedFirstZeroMobileNumber(email));
                                            bundle.putBoolean("isFromResetPassword", true);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                        }
                                    } else {
                                        Log.e("responseError", "" + response.errorBody().toString());
                                        Toast.makeText(ResetPasswordActivity.this, Localizer.getLocalizerString("k_10_s3_plz_try_again"), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    t.printStackTrace();
                                    Toast.makeText(ResetPasswordActivity.this, Localizer.getLocalizerString("k_10_s3_plz_try_again"), Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }
                            });

                        }

                    } catch (Exception e) {
                        Log.e(TAG, "onUpdateDeviceTokenOnServer: " + e.getMessage(), e);
                    }
                } else {
                    if (error == null) {
                        Toast.makeText(ResetPasswordActivity.this, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private String otpMessage(String otp) {
        return String.format(Localizer.getLocalizerString("k_27_s2_mav_t_otp_message"), otp);
    }

    private String genrateOtp() {

        Random rnd = new Random();
        int otp = 1000 + rnd.nextInt(9000);
        Log.e("tp genrates", "" + otp);
        return String.valueOf(otp);
    }

    private void sendRestPasswordToEmailId(String email) {
        Map<String, String> params = new HashMap<>();
        params.put("d_email", email);
        progressDialog.showDialog();
        WebServiceUtil.excuteRequest(ResetPasswordActivity.this, params, Constants.Urls.FORGET_PASSWORD, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
                if (isUpdate) {
                    try {
                        JSONObject rootObject = new JSONObject(data.toString());
                        String res = rootObject.optString(Constants.Keys.RESPONSE);
                        if (res != null) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_13_s3_password_will_be_sent_to_email"), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}

