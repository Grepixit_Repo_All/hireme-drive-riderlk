package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.Model.City;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.fonts.Fonts;

import java.util.ArrayList;

public class CityAdapter extends ArrayAdapter<String> {
    private final Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.KARLA);
    // private Controller controller;
    private final LayoutInflater inflater;
    private ArrayList<City> citiesCountryCode = new ArrayList<>();
    // private final Context context;

    public CityAdapter(Context context, int spinner_text, ArrayList<City> citiesCountryCode) {
        super(context, spinner_text);
        //this.context = context;
        inflater = LayoutInflater.from(context);
        this.citiesCountryCode = citiesCountryCode;
    }

    @Override
    public int getCount() {
        return citiesCountryCode.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        //   controller = ( Controller ) getContext().getApplicationContext();
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_text, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItemsName = convertView.findViewById(R.id.spinner_textview);
            mViewHolder.dropItemsName.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.dropItemsName.setText(String.format("%s", citiesCountryCode.get(position).getCity_name()));
        mViewHolder.dropItemsName.setTypeface(font);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_dropdown, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItemsName = convertView.findViewById(R.id.spinner_textview);
            mViewHolder.dropItemsName.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.dropItemsName.setText(String.format("%s", citiesCountryCode.get(position).getCity_name()));
        mViewHolder.dropItemsName.setTypeface(font);
        mViewHolder.dropItemsName.setTextColor(Color.parseColor("#000000"));

        return convertView;
    }

    public void setCountryCodes(ArrayList<City> citiesCountryCode) {
        this.citiesCountryCode = citiesCountryCode;
        notifyDataSetChanged();
    }

    private class MyViewHolder {
        TextView dropItemsName;
    }
}
