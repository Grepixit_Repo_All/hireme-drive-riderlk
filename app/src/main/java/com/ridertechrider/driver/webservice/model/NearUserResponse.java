package com.ridertechrider.driver.webservice.model;

import com.ridertechrider.driver.adaptor.User;
import com.ridertechrider.driver.webservice.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NearUserResponse extends BaseResponse {
    @SerializedName(Constants.Keys.RESPONSE)
    private ArrayList<User> nearUsers;

    public ArrayList<User> getNearUsers() {
        return nearUsers;
    }
}
