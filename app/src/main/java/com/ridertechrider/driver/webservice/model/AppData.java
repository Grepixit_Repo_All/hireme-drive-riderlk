package com.ridertechrider.driver.webservice.model;

import android.content.Context;

import com.ridertechrider.driver.Pref;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.adaptor.TripModel;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("ALL")
public class AppData implements Serializable {
    transient private final static String filename = "Saved_app_data";
    transient private static AppData trackRouteSaveData;
    ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList;
    transient private Context context;
    private Driver driver;
    private TripModel tripModel;

    private AppData() {
    }

    public static AppData getInstance(Context context) {

        if (trackRouteSaveData == null) {
            trackRouteSaveData = loadSavedInstance(context);
        }
        if (trackRouteSaveData == null) {
            trackRouteSaveData = new AppData();
        }
        trackRouteSaveData.context = context;

        return trackRouteSaveData;
    }

    private static AppData loadSavedInstance(Context context) {
        File filesDir = context.getFilesDir();
        File file = new File(filesDir, AppData.filename);
        if (!file.exists()) {
            return null;
        }
        try {
            FileInputStream inputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            Object object = objectInputStream.readObject();

            objectInputStream.close();
            return (AppData) object;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<com.ridertechrider.driver.DeliverResponse.Response> getDeliveryList() {
        return deliveryList;
    }

    public AppData setDeliveryList(ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList) {
        this.deliveryList = deliveryList;
        return this;
    }

    public TripModel getTripModel() {
        return tripModel;
    }

    public AppData setTripModel(TripModel tripModel) {
        Pref pref = new Pref(context);
        pref.setString("trip_response", new Gson().toJson(tripModel));
        this.tripModel = tripModel;
        return this;
    }

    public void clearTrip() {
        Pref pref = new Pref(context);
        pref.setString("trip_response", null);
        this.tripModel = null;
        saveTripTrip();
    }

    public void clearDelivery() {
        this.tripModel = null;
        saveDelivery();
    }

    public void saveDelivery() {
        saveData();
    }

    public void saveTripTrip() {

        saveData();
    }

    public boolean clearData() {
        driver = null;
        tripModel = null;
        return saveData();
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
        saveData();
    }

    public boolean saveData() {
        File filesDir = context.getFilesDir();
        File file = new File(filesDir, filename);
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
            objectOutputStream.writeObject(this);
            objectOutputStream.flush();
            objectOutputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static class Bulder implements Serializable {
        private HashMap<String, String> hashMap;
        private Context context;

        public Bulder(Context context) {
            this.context = context;
            hashMap = new HashMap<>();
        }

        public AppData.Bulder create() {
            return this;
        }

        public AppData.Bulder addApiKey() {
            if (getInstance(context).getDriver() != null)
                hashMap.put("api_key", getInstance(context).getDriver().getApiKey());
            return this;
        }

        public AppData.Bulder addDriverId() {
            if (getInstance(context).getDriver() != null)
                hashMap.put("driver_id", getInstance(context).getDriver().getDriverId());
            return this;
        }

        public HashMap<String, String> build() {
            return hashMap;
        }
    }
}
