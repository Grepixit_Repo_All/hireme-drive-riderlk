package com.ridertechrider.driver.Model;

import com.google.android.gms.maps.model.LatLng;

public class PickDropSelectionModel {
    LatLng pickUpLatLng1;
    String pickUpAddress1;
    boolean isPickUpSelected1;
    boolean isDropSelected1;
    String dropAddress1;
    LatLng dropUpLatLng1;
    private boolean isPickUpSearching;

    public boolean isPickUpSearching() {
        return isPickUpSearching;
    }

    public void setPickUpSearching(boolean pickUpSearching) {
        isPickUpSearching = pickUpSearching;
    }

    public void setPickData(boolean isPickUpSelected, String pickUpAddress, LatLng pickUpLatLng) {
        isPickUpSelected1 = isPickUpSelected;
        pickUpAddress1 = pickUpAddress;
        pickUpLatLng1 = pickUpLatLng;
    }

    public void setDropData(boolean isDropSelected, String dropAddress, LatLng dropUpLatLng) {
        isDropSelected1 = isDropSelected;
        dropAddress1 = dropAddress;
        dropUpLatLng1 = dropUpLatLng;
    }

    public void clearPickAndDropInfo() {
        setPickData(false, null, null);
        setDropData(false, null, null);
    }
}
