package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ridertechrider.driver.R;

import java.util.ArrayList;

public class CustomLangAdapter extends RecyclerView.Adapter<CustomLangAdapter.ViewHolder> {
    public static int selectedItem = -100;
    static int select = -20;
    private ArrayList<LangModel> listData;
    private Context context;
    private LangModel model;

    public CustomLangAdapter(Context aContext, ArrayList<LangModel> listData) {
        this.listData = listData;
        this.context = aContext;
    }

    @NonNull
    @Override
    public CustomLangAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listItem = layoutInflater.inflate(R.layout.language_lists, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomLangAdapter.ViewHolder viewHolder, final int i) {
        model = listData.get(i);
        viewHolder.languagesTv.setText(model.getName());
        if (model.getisChecked()) {
            viewHolder.languagesTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.bg_radio_selected, 0);
        } else
            viewHolder.languagesTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.bg_radio_normal, 0);

        if (model.getisChecked()) {
            select = i;
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView languagesTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            languagesTv = itemView.findViewById(R.id.languagesTv);
            languagesTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedItem = getAdapterPosition();
                }
            });
        }
    }
}
//    @Override
//    public int getCount() {
//        return listData.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return listData.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    public View getView(int position, View v, ViewGroup vg) {
//        final ViewHolder holder;
//        if (v == null) {
//            v = layoutInflater.inflate(R.layout.language_lists, null);
//            holder = new ViewHolder();
//            holder.ln_rbt_english = v.findViewById(R.id.languagesTv);
//            holder.selectedLang = v.findViewById(R.id.selectedLang);
//
//
//            v.setTag(holder);
//        } else {
//            holder = (ViewHolder) v.getTag();
//        }
//        holder.ln_rbt_english.setText(listData.get(position).getName());
//        return v;
//    }
//
//    public static class ViewHolder {
//        public static TextView ln_rbt_english;
//        public static RadioButton selectedLang;
//
//    }

