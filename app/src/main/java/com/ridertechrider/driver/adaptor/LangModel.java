package com.ridertechrider.driver.adaptor;

public class LangModel {
    String name;
    boolean checked;
    String code;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getisChecked() {
        return checked;
    }

    public void setisChecked(boolean checked) {
        this.checked = checked;
    }
}
