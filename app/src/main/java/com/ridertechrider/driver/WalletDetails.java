package com.ridertechrider.driver;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ridertechrider.driver.utils.Utils;

public class WalletDetails extends BaseCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_details);

        Controller controller = (Controller) getApplicationContext();

        ImageButton recancel = findViewById(R.id.recancel);
        TextView paid_text = findViewById(R.id.paid_text);
        TextView amt = findViewById(R.id.amt);
        TextView date = findViewById(R.id.date);
        TextView order_id = findViewById(R.id.order_id);
        TextView txx_id = findViewById(R.id.txn_id);
        TextView add_tx = findViewById(R.id.add_txn_id);
        RelativeLayout recharge_layout = findViewById(R.id.recharge_layout);
        RelativeLayout driver_layout = findViewById(R.id.driver_layout);
        RelativeLayout user_layout = findViewById(R.id.user_layout);
        final Intent intent = getIntent();
        final TransactionList transactionList = (TransactionList) intent.getSerializableExtra("transaction");

        String getStatus = transactionList.driverTransactions.getTrans_type();
        if (getStatus.equalsIgnoreCase("Trip")) {
            paid_text.setText(R.string.money_received_successfully);
        } else {
            paid_text.setText(R.string.money_added_successfully);
            recharge_layout.setVisibility(View.VISIBLE);
            driver_layout.setVisibility(View.GONE);
            user_layout.setVisibility(View.GONE);

        }

        add_tx.setText("Wallet Txn ID: " + transactionList.transactions.getTrans_drv_id());
        amt.setText(controller.formatAmountWithCurrencyUnit(transactionList.transactions.getAmount()));
        date.setText(Utils.convertServerDateToAppLocalDate(transactionList.transactions.getCreated()));
        order_id.setText("Wallet Txn ID: " + transactionList.transactions.getTransaction_id());
        txx_id.setText(transactionList.transactions.getTrans_drv_id());

        recancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
