package com.ridertechrider.driver;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.WebServiceUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RatingActivity extends BaseActivity {

    private RatingBar ratingbar1;
    private TextView ratingCount;
    private CustomProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.star_rating_layout);
        dialog = new CustomProgressDialog(RatingActivity.this);
        Typeface typeface = Typeface.createFromAsset(getAssets(), Fonts.KARLA);
        ratingbar1 = findViewById(R.id.ratingBar1);
        TextView tvRating = findViewById(R.id.tv_rating);
        tvRating.setTypeface(typeface);
        Button button = findViewById(R.id.button1);
        ImageView backBtn = findViewById(R.id.back_btn);
        ratingCount = findViewById(R.id.rating_count);
        ratingbar1.setClickable(false);
        ratingbar1.setFocusable(false
        );

        driverUpdateProfileRating();
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Performing action on Button Click
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String rating = String.valueOf(ratingbar1.getRating());
                Toast.makeText(getApplicationContext(), rating, Toast.LENGTH_LONG).show();
            }

        });
    }

    private void driverUpdateProfileRating() {
        dialog.showDialog();
        final Controller controller = (Controller) getApplicationContext();
        Map<String, String> params = new HashMap<>();
        ServerApiCall.callWithApiKeyAndDriverId(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                dialog.dismiss();
                if (isUpdate) {
                    Driver driver = Driver.parseJsonAfterLogin(data.toString());
                    controller.setLoggedDriver(driver);
                    String driver_rating = Objects.requireNonNull(driver).getD_rating();
                    if (driver_rating.length() != 0) {
                        float rating = Float.parseFloat(driver_rating);
                        ratingbar1.setRating(rating);
                        float s = Float.parseFloat(driver.getD_rating_count());
                        int intRate = (int) s;
                        ratingCount.setText(" " + intRate);
                    }
                }
            }
        });
    }
}
