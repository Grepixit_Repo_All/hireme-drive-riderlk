package com.ridertechrider.driver.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.ridertechrider.driver.R;
import com.ridertechrider.driver.fonts.FontCache;


/**
 * Created by grepixinfotech on 02/01/17.
 */
public class BRadioButton extends androidx.appcompat.widget.AppCompatRadioButton {
    public BRadioButton(Context context) {
        super(context);
    }

    public BRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);


        applyCustomFont(context, attrs);
    }

    public BRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BTextViewStyle,
                0, 0);
        try {
            String name = a.getString(R.styleable.BTextViewStyle_btextfont);
            Typeface customFont = FontCache.getTypeface(name, context);
            setTypeface(customFont);
        } finally {
            a.recycle();
        }
    }
}
