package com.ridertechrider.driver.adaptor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.R;
import com.ridertechrider.driver.fonts.Fonts;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    private final Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.KARLA);
    // private Controller controller;
    private final LayoutInflater inflater;
    private final String[] items;
    // private final Context context;

    public CustomSpinnerAdapter(Context context, int spinner_text, String[] items) {
        super(context, spinner_text, items);
        //this.context = context;
        inflater = LayoutInflater.from(context);
        this.items = items;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        //   controller = ( Controller ) getContext().getApplicationContext();
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_text, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItems = convertView.findViewById(R.id.spinner_textview);
            mViewHolder.dropItems.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        //System.out.println("items size = " + items.length);
        mViewHolder.dropItems.setText(items[position]);
        mViewHolder.dropItems.setTypeface(font);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_dropdown, null);
            mViewHolder = new MyViewHolder();
            mViewHolder.dropItems = convertView.findViewById(R.id.spinner_textview);
            mViewHolder.dropItems.setTypeface(font);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.dropItems.setText(items[position]);
        mViewHolder.dropItems.setTypeface(font);
        mViewHolder.dropItems.setTextColor(Color.parseColor("#000000"));
        return convertView;
    }

    private class MyViewHolder {
        TextView dropItems;
    }
}
