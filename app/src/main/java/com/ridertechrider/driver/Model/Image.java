package com.ridertechrider.driver.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("image_id")
    @Expose
    public String imageId;
    @SerializedName("img_type")
    @Expose
    public String imgType;
    @SerializedName("ref_type")
    @Expose
    public String refType;
    @SerializedName("ref_id")
    @Expose
    public String refId;
    @SerializedName("img_name")
    @Expose
    public String imgName;
    @SerializedName("img_description")
    @Expose
    public String imgDescription;
    @SerializedName("img_path")
    @Expose
    public String imgPath;
    @SerializedName("audio_path")
    @Expose
    public Object audioPath;
    @SerializedName("video_path")
    @Expose
    public Object videoPath;
    @SerializedName("is_delete")
    @Expose
    public String isDelete;
    @SerializedName("img_created")
    @Expose
    public String imgCreated;
    @SerializedName("img_modified")
    @Expose
    public String imgModified;


    public String getImageId() {
        return imageId;
    }

    public String getImgType() {
        return imgType;
    }

    public String getRefType() {
        return refType;
    }

    public String getRefId() {
        return refId;
    }

    public String getImgName() {
        return imgName;
    }

    public String getImgDescription() {
        return imgDescription;
    }

    public String getImgPath() {
        return imgPath;
    }

    public Object getAudioPath() {
        return audioPath;
    }

    public Object getVideoPath() {
        return videoPath;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public String getImgCreated() {
        return imgCreated;
    }

    public String getImgModified() {
        return imgModified;
    }
}