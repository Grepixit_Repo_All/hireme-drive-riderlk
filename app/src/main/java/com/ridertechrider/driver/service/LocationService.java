package com.ridertechrider.driver.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.Pref;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.track_route.TrackRouteSaveData;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.grepix.grepixutils.WebServiceUtil;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Nayanesh Gupte
 */
@SuppressWarnings("JavaDoc")
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ILocationConstants, SensorEventListener {


    private static final String TAG = LocationService.class.getSimpleName();
    public SensorManager manager;
    public Sensor rotation_vector;
    /**
     * Provides the entry point to Google Play services.
     */
    private GoogleApiClient mGoogleApiClient;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;
    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;
    private Location preLocation;
    private int timeForSkipSameLocation = 10;
    private String mLatitudeLabel;
    //    private String mDistance;
    private String mLongitudeLabel;
    private String mLastUpdateTimeLabel;
    /**
     * Time when the location was updated represented as a String.
     */
    private String mLastUpdateTime;
    /**
     * Total distance covered
     */
    private Controller controller;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback locationCallback;
    private float distance;
    private Location oldLocation;
    private double mDeclination = 0;
    private float[] mRotationMatrix = new float[16];

    @Override
    public void onCreate() {
        super.onCreate();
        if (controller == null) {

            controller = (Controller) getApplicationContext();
        }
        controller.isUpdaingLocation = false;

        Log.d(TAG, "onCreate Distance: " + distance);
        oldLocation = new Location("Point A");
        mLastUpdateTime = "";
        Log.d(TAG, "onCreate Distance: " + distance);
        if (PreferencesUtils.getBoolean(this, R.string.is_recording_start, false)) {
            distance = (int) PreferencesUtils.getFloat(this, R.string.recorded_distance);
            Log.e("savedDistance", "" + distance);
        } else {
            PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, 0);
            distance = 0;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildGoogleApiClient();

        mGoogleApiClient.connect();

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String channelId = "location_id";
            Notification.Builder builder = new Notification.Builder(this, channelId)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.trip_is_started))
                    .setAutoCancel(true);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
            mNotificationManager.createNotificationChannel(mChannel);
            Notification notification = builder.build();
            startForeground(364, notification);
        } else {
            String channelId = "location_id";
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.trip_is_started))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);
            Notification notification = builder.build();

            startForeground(364, notification);
        }
        return START_NOT_STICKY;

    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    private synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    private void startLocationUpdates() {

        try {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            manager = (SensorManager) getSystemService(SENSOR_SERVICE);
            rotation_vector = manager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            manager.registerListener(this, rotation_vector, SensorManager.SENSOR_DELAY_NORMAL);
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            /*LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);*/
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    Log.d(TAG, "onLocationResult: distance: " + distance);
                    if (locationResult == null) {
                        return;
                    }
                    for (Location location : locationResult.getLocations()) {
                        if (location != null) {
                            TripModel tripModel = AppData.getInstance(LocationService.this).getTripModel();

//                            if (!Controller.isActivityVisible()) {
                            Pref pref = new Pref(LocationService.this);
                            if (tripModel != null) {
                                if (tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
                                    LatLng lastLatLng = TrackRouteSaveData.getInstance(getApplicationContext()).getLastLatLng();
                                    boolean addLatLng = false;
                                    if (lastLatLng == null) {
                                        addLatLng = true;
                                    } else {
                                        Location location1 = new Location(location.getProvider());
                                        location1.setLatitude(lastLatLng.latitude);
                                        location1.setLongitude(lastLatLng.longitude);
                                        if (lastLatLng.latitude != location.getLatitude() && lastLatLng.longitude != location.getLongitude() && location.distanceTo(location1) > 20) {
                                            addLatLng = true;
                                            Log.d(TAG, "onLocationResult: TrackRouteSaveData: distance: " + location.distanceTo(location1));
                                        }
                                    }

                                    if (addLatLng) {
                                        Log.d(TAG, "onLocationResult: TrackRouteSaveData: " + location.toString());
                                        TrackRouteSaveData.getInstance(getApplicationContext()).addLatLongD(new LatLng(location.getLatitude(), location.getLongitude()));
                                        TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
                                    }
                                }
                            }
//                            }
                            mCurrentLocation = location;
                            GeomagneticField field = new GeomagneticField((float) location.getLatitude(), (float) location.getLongitude(), (float) location.getAltitude(), System.currentTimeMillis());

                            mDeclination = field.getDeclination();
                            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                            updateUI(false);
                            if (tripModel == null || tripModel.tripStatus == null || !tripModel.tripStatus.equalsIgnoreCase("begin")) {
                                PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, 0);
                                distance = 0;
                                oldLocation = mCurrentLocation;
                            }
                        }
                    }
                }
            };
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, null);

        } catch (Exception ignore) {
        }
    }

    /**
     * Updates the latitude, the longitude, and the last location time in the UI.
     */
    private void updateUI(boolean isFromBearing) {

        if (null != mCurrentLocation) {
            if (controller == null) {
                controller = (Controller) getApplicationContext();
            }
            StringBuilder sbLocationData = new StringBuilder();
            if (!isFromBearing) {
//            if (!Controller.isActivityVisible()) {
                if (isCanMakeApiCall(mCurrentLocation)) {
                    updateDriverProfileApi(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), mCurrentLocation.getBearing());
                    preLocation = mCurrentLocation;
                }
//            }
                TripModel tripModel = AppData.getInstance(LocationService.this).getTripModel();
                if (tripModel != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
                    sbLocationData.append(getUpdatedDistance());
                }

            /*.append(mLatitudeLabel)
                    .append(" ")
                    .append(mCurrentLocation.getLatitude())
                    .append("\n")
                    .append(mLongitudeLabel)
                    .append(" ")
                    .append(mCurrentLocation.getLongitude())
                    .append("\n")
                    .append(mLastUpdateTimeLabel)
                    .append(" ")
                    .append(mLastUpdateTime)
                    .append("\n")
                    .append(mDistance)
                    .append(" ")*/
//                    .append(getUpdatedDistance());
                //.append(" meters");


                /*
                 * update preference with latest value of distance
                 */
//            appPreferences.putFloat(PREF_DISTANCE, distance);


//            Log.d(TAG, "Location Data:\n" + sbLocationData.toString());
            }
            sendLocationBroadcast(sbLocationData.toString(), isFromBearing);
        } else {
            Toast.makeText(this, R.string.unable_to_find_location, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Send broadcast using LocalBroadcastManager to update UI in activity
     *
     * @param sbLocationData
     */
    private void sendLocationBroadcast(String sbLocationData, boolean isFromBearing) {

        Intent locationIntent = new Intent();
        locationIntent.setAction(LOACTION_ACTION);
        locationIntent.putExtra(LOCATION_MESSAGE, sbLocationData);

        if (mCurrentLocation != null) {
            locationIntent.putExtra("isFromBearing", isFromBearing);
            locationIntent.putExtra("c_lat", "" + mCurrentLocation.getLatitude());
            locationIntent.putExtra("c_lng", "" + mCurrentLocation.getLongitude());
            locationIntent.putExtra("c_bearing", "" + mCurrentLocation.getBearing());

        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(locationIntent);

    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {

        if (mGoogleApiClient != null && mFusedLocationClient != null && locationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
            //  LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    private void updateDriverProfileApi(final double lat, final double lng, final double bearing) {
        if (controller.isUpdaingLocation || controller == null || controller.getLoggedDriver() == null) {
            return;
        }
        controller.isUpdaingLocation = true;
        Map<String, String> params = new HashMap<>();
        params.put("d_lat", Double.toString(lat));
        params.put("d_lng", Double.toString(lng));
        params.put("d_degree", Double.toString(bearing));
        Log.d(TAG, "updateDriverProfileApi: params: " + params.toString());
        ServerApiCall.callWithApiKeyAndDriverIdWithNoAlert(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdateTeamp, VolleyError error) {
                if (isUpdateTeamp && controller.getLoggedDriver() != null) {
                    controller.getLoggedDriver().setD_lat(Double.toString(lat));
                    controller.getLoggedDriver().setD_lng(Double.toString(lng));
                    controller.getLoggedDriver().setD_degree(Double.toString(bearing));

                    controller.pref.setDriver_Lat(Double.toString(lat));
                    controller.pref.setDriver_Lng(Double.toString(lng));
                    controller.pref.setDriver_Bearing(Double.toString(bearing));
                }
                controller.isUpdaingLocation = false;
            }
        });
    }

    @Override
    public void onDestroy() {

        controller.isUpdaingLocation = false;
        stopLocationUpdates();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) throws SecurityException {
        Log.i(TAG, "Connected to GoogleApiClient");


        if (mCurrentLocation == null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        mCurrentLocation = location;
                        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                        updateUI(false);
                    }
                }
            });
        }

        startLocationUpdates();
    }

    private boolean isCanMakeApiCall(Location myCurrentLocation) {
        if (preLocation == null) {
            return true;
        }
        if (preLocation.distanceTo(myCurrentLocation) <= 5) {
            timeForSkipSameLocation--;
            if (timeForSkipSameLocation < 0) {
                timeForSkipSameLocation = 0;
                timeForSkipSameLocation = 10;
                return true;
            }
            return false;
        } else {
            timeForSkipSameLocation = 10;
            return true;
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onConnectionSuspended(int cause) {

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    private float getUpdatedDistance() {

        TripModel tripModel = AppData.getInstance(LocationService.this).getTripModel();
        if (tripModel != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
            if (mCurrentLocation.getAccuracy() > ACCURACY_THRESHOLD) {
                return Float.parseFloat(String.format(Locale.ENGLISH, "0.2f", distance));
            }

            if (oldLocation.getLatitude() == 0 && oldLocation.getLongitude() == 0) {
                oldLocation.setLatitude(mCurrentLocation.getLatitude());
                oldLocation.setLongitude(mCurrentLocation.getLongitude());
                return Float.parseFloat(String.format(Locale.ENGLISH, "0.2f", distance));
            } else {
                if (mCurrentLocation.getAccuracy() < 60) {
                    float tempDistance = mCurrentLocation.distanceTo(oldLocation);
                    Log.e("tempDistance", "" + tempDistance);
                    if (tempDistance > 20) {
                        distance += tempDistance;
                        Log.d(TAG, "getUpdatedDistance: distance += tempDistance" + distance);
                        PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);
                        oldLocation.setLatitude(mCurrentLocation.getLatitude());
                        oldLocation.setLongitude(mCurrentLocation.getLongitude());
                    }
                }
                return Float.parseFloat(String.format(Locale.ENGLISH, "%.2f", distance));
            }
        } else {
            distance = 0;
            PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);
            return distance;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(
                    mRotationMatrix, sensorEvent.values);
            float[] orientation = new float[3];
            SensorManager.getOrientation(mRotationMatrix, orientation);
            double bearing = Math.toDegrees(orientation[0]) + mDeclination;
            if (mCurrentLocation != null)
                mCurrentLocation.setBearing((float) bearing);
//            Log.d(TAG, "onSensorChanged: bearing: " + (float) bearing);
//            updateUI(true);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
