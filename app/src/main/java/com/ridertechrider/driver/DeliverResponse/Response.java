package com.ridertechrider.driver.DeliverResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

public class Response implements Serializable, Comparator<Response> {

    String deliveryImage;
    @SerializedName("delivery_id")
    @Expose
    private String deliveryId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("del_time")
    @Expose
    private String del_time;
    @SerializedName("delivery_order")
    @Expose

    private String deliveryOrder;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("trip_id")
    @Expose
    private String tripId;
    @SerializedName("delivery_address")
    @Expose
    private String deliveryAddress;
    @SerializedName("delivery_latitude")
    @Expose
    private String deliveryLatitude;
    @SerializedName("delivery_longitude")
    @Expose
    private String deliveryLongitude;
    @SerializedName("pay_amount")
    @Expose
    private String payAmount;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("delivery_notes")
    @Expose
    private String deliveryNotes;
    @SerializedName("delivery_remarks")
    @Expose
    private String deliveryRemarks;
    @SerializedName("is_prepaid")
    @Expose
    private String isPrepaid;
    @SerializedName("delivery_created")
    @Expose
    private String deliveryCreated;
    @SerializedName("delivery_modified")
    @Expose
    private String deliveryModified;
    @SerializedName("city_id")
    @Expose
    private String city_id;
    @SerializedName("sender")
    @Expose
    private Sender sender;
    @SerializedName("receiver")
    @Expose
    private Receiver receiver;
    @SerializedName("trip")
    @Expose
    private String trip;

    public String getDeliveryImage() {
        return deliveryImage;
    }

    public void setDeliveryImage(String deliveryImage) {
        this.deliveryImage = deliveryImage;
    }

    private String getDeliveryOrder() {
        return deliveryOrder;
    }

    public void setDeliveryOrder(String deliveryOrder) {
        this.deliveryOrder = deliveryOrder;
    }

    public String getDel_time() {
        return del_time;
    }

    public void setDel_time(String del_time) {
        this.del_time = del_time;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryLatitude() {
        return deliveryLatitude;
    }

    public void setDeliveryLatitude(String deliveryLatitude) {
        this.deliveryLatitude = deliveryLatitude;
    }

    public String getDeliveryLongitude() {
        return deliveryLongitude;
    }

    public void setDeliveryLongitude(String deliveryLongitude) {
        this.deliveryLongitude = deliveryLongitude;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getDeliveryNotes() {
        return deliveryNotes;
    }

    public void setDeliveryNotes(String deliveryNotes) {
        this.deliveryNotes = deliveryNotes;
    }

    public String getDeliveryRemarks() {
        return deliveryRemarks;
    }

    public void setDeliveryRemarks(String deliveryRemarks) {
        this.deliveryRemarks = deliveryRemarks;
    }

    public String getIsPrepaid() {
        return isPrepaid;
    }

    public void setIsPrepaid(String isPrepaid) {
        this.isPrepaid = isPrepaid;
    }

    public String getDeliveryCreated() {
        return deliveryCreated;
    }

    public void setDeliveryCreated(String deliveryCreated) {
        this.deliveryCreated = deliveryCreated;
    }

    public String getDeliveryModified() {
        return deliveryModified;
    }

    public void setDeliveryModified(String deliveryModified) {
        this.deliveryModified = deliveryModified;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public int compare(Response o1, Response o2) {
        return o1.getDeliveryOrder().compareTo(o2.getDeliveryOrder());
    }

    public String getCity_id() {
        return city_id;
    }
}
