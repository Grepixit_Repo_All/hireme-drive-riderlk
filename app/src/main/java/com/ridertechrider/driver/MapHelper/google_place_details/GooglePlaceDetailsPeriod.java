package com.ridertechrider.driver.MapHelper.google_place_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class GooglePlaceDetailsPeriod {

    @SerializedName("close")
    @Expose
    private GooglePlaceDetailsClose close;
    @SerializedName("open")
    @Expose
    private GooglePlaceDetailsOpen open;

    public GooglePlaceDetailsClose getClose() {
        return close;
    }

    public void setClose(GooglePlaceDetailsClose close) {
        this.close = close;
    }

    public GooglePlaceDetailsOpen getOpen() {
        return open;
    }

    public void setOpen(GooglePlaceDetailsOpen open) {
        this.open = open;
    }

}
