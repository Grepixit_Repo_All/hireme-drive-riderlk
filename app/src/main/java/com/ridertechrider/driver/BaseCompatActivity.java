package com.ridertechrider.driver;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.utils.Utils;

public class BaseCompatActivity extends AppCompatActivity {
    private CustomProgressDialog progressDialog;

    void showProgressBar() {
        hideProgressBar();
        if (progressDialog == null)
            progressDialog = new CustomProgressDialog(BaseCompatActivity.this);
        progressDialog.showDialog();
    }

    void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    void showToast() {
        Toast.makeText(getApplicationContext(), R.string.trip_udpated_successfully, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.applyAppLanguage(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Utils.applyAppLanguage(this);

    }
}
