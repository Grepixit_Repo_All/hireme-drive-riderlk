package com.ridertechrider.driver.GetStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Header {

@SerializedName("type")
@Expose
private String type;
@SerializedName("value")
@Expose
private String value;
@SerializedName("placeholder")
@Expose
private String placeholder;

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

public String getValue() {
return value;
}

public void setValue(String value) {
this.value = value;
}

public String getPlaceholder() {
return placeholder;
}

public void setPlaceholder(String placeholder) {
this.placeholder = placeholder;
}

}