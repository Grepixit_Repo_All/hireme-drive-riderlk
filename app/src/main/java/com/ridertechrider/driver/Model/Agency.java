package com.ridertechrider.driver.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Agency {

    private String user_id;
    private String u_name;

    public Agency() {
    }

    public Agency(String u_name) {
        this.u_name = u_name;
    }

    public static Agency parse(JSONObject obj) throws JSONException {
        Agency city = new Agency();
        city.setUser_id(obj.getString("user_id"));
        city.setU_name(obj.getString("u_name"));
        return city;
    }

    public static ArrayList<Agency> parseAgencies(String response) {
        ArrayList<Agency> agencies = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(response);
            if (obj.has("response")) {
                JSONArray objArray = obj.getJSONArray("response");

                if (objArray != null && objArray.length() > 0) {
                    for (int i = 0; i < objArray.length(); i++) {
                        agencies.add(parse(objArray.getJSONObject(i)));
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(Agency.class.getSimpleName(), "parseAgencies: " + e.getMessage(), e);
        }
        return agencies;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }
}
