package com.ridertechrider.driver;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;


public class MainActivityFragment extends BaseCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);

        Intent intent = getIntent();
        String latitude = intent.getStringExtra("latitude");
        String longitude = intent.getStringExtra("longitude");
        String type = intent.getStringExtra("type");
        //Log.("value lat",latitude+longitude);
        FragmentManager fragmentManager = getFragmentManager();
        Bundle b = new Bundle();
        b.putString("latitude", latitude);
        b.putString("longitude", longitude);
        b.putString("type", type);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        PickupDetailsFragment myfragment = new PickupDetailsFragment();  //your fragment

        // work here to add, remove, etc
        fragmentTransaction.add(R.id.frame_fragment, myfragment);
        myfragment.setArguments(b);
        fragmentTransaction.commit();
    }
}
