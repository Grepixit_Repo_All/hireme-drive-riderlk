package com.ridertechrider.driver.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.ridertechrider.driver.ITrackRecordingService;


/**
 * Created by grepixinfotech on 09/11/17.
 */

class TrackRecordingServiceConnection {
    private static final String TAG = TrackRecordingServiceConnection.class.getSimpleName();
    private final Context context;
    private final Runnable callback;
    private ITrackRecordingService trackRecordingService;
    private final IBinder.DeathRecipient deathRecipient = new IBinder.DeathRecipient() {
        @Override
        public void binderDied() {
            Log.d(TAG, "Service died.");
            setTrackRecordingService(null);
        }
    };
    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.i(TAG, "Connected to the service.");
            try {
                service.linkToDeath(deathRecipient, 0);
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to bind a death recipient.", e);
            }
            setTrackRecordingService(ITrackRecordingService.Stub.asInterface(service));
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.i(TAG, "Disconnected from the service.");
            setTrackRecordingService(null);
        }
    };


    /**
     * Constructor.
     *
     * @param context  the context
     * @param callback the callback to invoke when the service binding changes
     */
    private TrackRecordingServiceConnection(Context context, Runnable callback) {
        this.context = context;
        this.callback = callback;
    }


    /**
     * Unbinds the service (but leave it running).
     */
    public void unbind() {
        try {
            context.unbindService(serviceConnection);
        } catch (IllegalArgumentException e) {
            // Means not bound to the service. OK to ignore.
        }
        setTrackRecordingService(null);
    }

    /**
     * Gets the track recording service if bound. Returns null otherwise
     */
    public ITrackRecordingService getServiceIfBound() {
        if (trackRecordingService != null && !trackRecordingService.asBinder().isBinderAlive()) {
            setTrackRecordingService(null);
            return null;
        }
        return trackRecordingService;
    }


    /**
     * Sets the trackRecordingService.
     *
     * @param value the value
     */
    private void setTrackRecordingService(ITrackRecordingService value) {
        trackRecordingService = value;
        if (callback != null) {
            callback.run();
        }
    }


}

