package com.ridertechrider.driver.app;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.SingleModeActivity;
import com.ridertechrider.driver.SlideMainActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by grepixinfotech on 13/11/17.
 */

public class GoogleSDRoute {


    private final Context context;
    private final LatLng srourceLoc;
    private final LatLng destinationLoc;

    public GoogleSDRoute(Context context, LatLng srourceLoc, LatLng destinationLoc) {
        this.context = context;
        this.srourceLoc = srourceLoc;
        this.destinationLoc = destinationLoc;

    }

    public GoogleSDRoute(Context context, LatLng srourceLoc, LatLng destinationLoc, String waypoints) {
        this.context = context;
        this.srourceLoc = srourceLoc;
        this.destinationLoc = destinationLoc;

    }

    private void getRoute(final GoogleSDRouteCallBack call, String urlDirection) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlDirection,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleDirectionResponse(response, call);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //System.out.println("VolleyError : " + error);
                        Toast.makeText(context, "" + error, Toast.LENGTH_SHORT).show();
                    }
                }) {

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            Log.e(GoogleSDRoute.class.getSimpleName(), "getRoute: " + e.getMessage(), e);
        }
    }

    public void getRoute(final GoogleSDRouteCallBack call) {
        final String directionsUrl = getDirectionsUrl(srourceLoc, destinationLoc, call);
        getRoute(call, directionsUrl);
    }

    private void handleDirectionResponse(String response, GoogleSDRouteCallBack call) {
        try {
            ParseRoute parseRoute = new ParseRoute(new JSONObject(response));
            RouteResponse routes = parseRoute.parse();
            call.onCompleteSDRoute(routes);
        } catch (Exception e) {
            e.printStackTrace();
            call.onCompleteSDRoute(null);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest, GoogleSDRouteCallBack call) {
        String googleapikey = null;
        if (SlideMainActivity.controller != null) {
            googleapikey = SlideMainActivity.controller.getConstantsValueForKey("gkey");
        } else {
            googleapikey = SingleModeActivity.controller.getConstantsValueForKey("gkey");
        }
        String parameters = "";
        String output = "json";
        try {
            // Origin of route
            String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
            // Destination of route
            String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
            String key = "key=" + googleapikey;
            // Sensor enabled
            String sensor = "sensor=true";
            parameters = str_origin + "&" + str_dest + "&" + key + "&" + sensor;
        } catch (Exception e) {
            call.onCompleteSDRoute(null);
        }
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

    private DataTest nearestLatLngTest(ArrayList<LatLng> latLngs, Location location) {
        LatLng p = latLngs.get(0);

        DataTest dataTest = new DataTest();
        ArrayList<LatLng> latLngsRouteData = new ArrayList<>();

        for (int i = 0; i < latLngs.size(); i++) {
            LatLng latLng = latLngs.get(i);
            Location location1 = convertLatLngToLocation(latLng);
            Location pLoca = convertLatLngToLocation(p);
            if (location1.distanceTo(location) < pLoca.distanceTo(location)) {
                p = latLngs.get(i);
            }
        }
        for (int i = latLngs.size() - 1; i >= 0; i--) {
            LatLng latLng = latLngs.get(i);
            if (latLng == p) {
//                    latLngsRouteData.add(new LatLng(location.getLatitude(), location.getLongitude()));
                latLngsRouteData.add(p);
                break;
            }
            latLngsRouteData.add(latLng);
        }
        dataTest.latLng = p;
        dataTest.latLngsRouteData = latLngsRouteData;
        return dataTest;
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;

    }


    public interface GoogleSDRouteCallBack {
        void onCompleteSDRoute(RouteResponse routeResponse);


    }

    public class RouteResponse {
        public final ArrayList<String> maneuver = new ArrayList<>();
        public final ArrayList<String> dis = new ArrayList<>();
        public final ArrayList<Double> ending_lat = new ArrayList<>();
        public final ArrayList<Double> ending_long = new ArrayList<>();
        public LatLng srcLoc;
        LatLng destLoc;
        String distance;
        String duration;
        float distanceFloat;
        int durationInt;
        ArrayList<Route> routesObject;

        public ArrayList<Polyline> addRouteOnMap(GoogleMap googleMap) {
            return addRouteOnMap(googleMap, true, 8);
        }

        public Marker addStartLocation(GoogleMap googleMap, LatLng srcLocTemp) {
            if (srcLocTemp != null) {

                return googleMap.addMarker(new MarkerOptions().position(srcLocTemp).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon)));

                // googleMap.addMarker(new MarkerOptions().position(srcLoc).icon(BitmapDescriptorFactory.fromResource(R.drawable.carimage)));
            }
            return null;
        }


        public ArrayList<Polyline> addRouteOnMap(GoogleMap googleMap, boolean isZoomMap, int width) {

            ArrayList<PolylineOptions> polylineOptionses = makePloylineOnMap();
            ArrayList<Polyline> polylines = new ArrayList<>();
            for (PolylineOptions lineOptions :
                    polylineOptionses) {
                lineOptions.width(width);
                lineOptions.color(Color.BLACK);
                lineOptions.geodesic(true);
                lineOptions.zIndex(4);
                polylines.add(googleMap.addPolyline(lineOptions));
            }

            if (destLoc != null) {
                googleMap.addMarker(new MarkerOptions().position(destLoc).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_25)));
            }
            if (isZoomMap) {
                if (routesObject != null && routesObject.size() > 0) {
                    Route route = routesObject.get(0);
                    LatLngBounds.Builder bounds = new LatLngBounds.Builder();
                    bounds.include(route.bounds.northeast);
                    bounds.include(route.bounds.southwest);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 100));
                }
            }
            return polylines;
        }

        ArrayList<PolylineOptions> makePloylineOnMap() {
            ArrayList<PolylineOptions> polylineOptionses = new ArrayList<>();
            if (routesObject != null) {
                // Traversing through all the routes
                try {
                    for (int i = 0; i < routesObject.size(); i++) {
                        PolylineOptions lineOptions = new PolylineOptions();
                        // Fetching i-th route
                        Route route = routesObject.get(i);
                        lineOptions.addAll(route.points);
                        polylineOptionses.add(lineOptions);
                        srcLoc = route.points.get(0);
                        destLoc = route.points.get(route.points.size() - 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return polylineOptionses;
        }

        public DataTest findNearestLocationTest(Location location) {
            if (routesObject != null) {
                int i = 0;
                while (i < routesObject.size()) {
                    ArrayList<Leg> legs = routesObject.get(i).legs;
                    ArrayList<LatLng> pointsAll = new ArrayList<>();
                    for (int j = 0; j < legs.size(); j++) {
                        Leg leg = legs.get(j);
                        for (int k = 0; k < leg.steps.size(); k++) {
                            Step step = leg.steps.get(k);
                            pointsAll.addAll(step.points);
                        }
                    }
                    i++;

                    return nearestLatLngTest(pointsAll, location);
                }
            }
            return null;
        }

        private Location convertLatLngToLocation(LatLng latLng) {

            Location location = new Location("");
            location.setLatitude(latLng.latitude);
            location.setLongitude(latLng.longitude);
            return location
                    ;
        }

        public boolean isOnOffRoute(Location location) {

            if (routesObject != null) for (int i = 0; i < routesObject.size(); i++) {
                ArrayList<Leg> legs = routesObject.get(i).legs;
                for (int j = 0; j < legs.size(); j++) {
                    Leg leg = legs.get(j);
                    for (int k = 0; k < leg.steps.size(); k++) {
                        Step step = leg.steps.get(k);
                        ArrayList<LatLng> points = step.points;
                        for (int jk = 0; jk < points.size(); jk++) {
                            Location location1 = convertLatLngToLocation(points.get(jk));
                            float dis = location1.distanceTo(location);
                            if (dis < 40) {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }

    class ParseRoute {


        private final JSONObject jObject;

        ParseRoute(JSONObject jsonObject) {
            jObject = jsonObject;
        }

        RouteResponse parse() throws JSONException {

            RouteResponse routeResponse = new RouteResponse();
            List<List<HashMap<String, String>>> routes = new ArrayList<>();
            ArrayList<Route> routesObject = new ArrayList<>();
            JSONArray jRoutes;
            jRoutes = jObject.getJSONArray("routes");

            for (int i = 0; i < jRoutes.length(); i++) {
                Route route = new Route(jRoutes.getJSONObject(i));
                routeResponse.distanceFloat = route.getTotalDistance();
                routeResponse.durationInt = route.getTotalDuration();
                routeResponse.distance = route.getTotalDurationInKm();
                routeResponse.duration = route.getTotalDurationInMin();
                routesObject.add(route);
            }
            routeResponse.routesObject = routesObject;
            return routeResponse;
        }

    }

    public class DataTest {
        public LatLng latLng;
        public ArrayList<LatLng> latLngsRouteData;
    }
}
