package com.ridertechrider.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.drawerlayout.widget.DrawerLayout;

import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.ridertechrider.driver.SlideMainActivity.controller;

class SideDrawerManager {


    private final Context context;
    private final DrawerLayout mDrawerLayout;
    private final RelativeLayout navrl;
    NavDrawerListAdapter navDrawerListAdapter;

    public SideDrawerManager(Context context, DrawerLayout mDrawerLayout, ListView mDrawerList, RelativeLayout navrl) {
        this.context = context;
        this.mDrawerLayout = mDrawerLayout;
        this.navrl = navrl;
        mDrawerLayout.openDrawer(navrl);
        mDrawerLayout.closeDrawer(navrl);
        ArrayList<NavDrawerItem> navDrawerItemList = getNavDrawerItemList(context);
        Typeface drawerListTypeface = Typeface.createFromAsset(context.getAssets(), Fonts.KARLA);
        navDrawerListAdapter = new NavDrawerListAdapter(context, navDrawerItemList);
        mDrawerList.setAdapter(navDrawerListAdapter);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
    }

    @SuppressLint("ResourceType")
    private ArrayList<NavDrawerItem> getNavDrawerItemList(Context context) {
        String[] navMenuTitles = context.getResources().getStringArray(R.array.profile_list);
        int[] navMenuIds = context.getResources().getIntArray(R.array.profile_list_id);
        TypedArray navMenuIcons = context.getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
        ///navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1), navMenuIds[1]));
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1), navMenuIds[4]));
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), navMenuIds[6]));
//        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1), navMenuIds[7]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons.getResourceId(9, -1), navMenuIds[10]));
        if (controller.checkWallet().equalsIgnoreCase("1")) {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1), navMenuIds[8]));
        }
        if (controller.checkSingleMode().equalsIgnoreCase("1")) {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons.getResourceId(8, -1), navMenuIds[9]));
        }


        navMenuIcons.recycle();
        return navDrawerItems;
    }

    private void displayView(int position) {
        if (navDrawerListAdapter != null) {
            switch (navDrawerListAdapter.getNavId(position)) {
                case 2: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent history = new Intent(context, TripHistoryListActivity.class);
                    history.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(history);
                    break;
                }

                case 5: {
                    if (Utils.net_connection_check(context)) {
                        Share();
                    }
                    break;
                }

                case 7: {
                    if (Utils.net_connection_check(context)) {
                        String[] email = {Constants.Urls.EMAIL_FOR_SUPPORT};
                        shareToGMail(email);

                    }

                    break;
                }
                case 8: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent brain = new Intent(context, LanguageSelectionActivity.class);
                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(brain);

                    break;

                }
                case 9: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent brain = new Intent(context, WalletActivity.class);
                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(brain);

                    break;

                }
                case 10: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent brain = new Intent(context, SingleModeActivity.class);
                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(brain);
                    break;
                }
                case 11: {
                    mDrawerLayout.closeDrawer(navrl);

                    Intent intent = new Intent(context, NotificationActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    break;
                }
            }
        }
    }

    private void Share() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, Constants.Urls.URL_TO_SHARE);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void shareToGMail(String[] email) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Rider Driver App Support");
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(emailIntent);
    }

//    private void displayView(int position) {
//        switch (position) {
//            case 0: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent history = new Intent(context, TripHistoryListActivity.class);
//                history.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(history);
//                break;
//            }
//
//            case 1: {
//                if (Utils.net_connection_check(context)) {
//                    Share();
//                }
//                break;
//            }
//
//            case 2: {
//                if (Utils.net_connection_check(context)) {
//                    String[] email = {Constants.Urls.EMAIL_FOR_SUPPORT};
//                    shareToGMail(email);
//
//                }
//
//                break;
//            }
//            case 3: {
//
//                if (controller.checkWallet().equalsIgnoreCase("1")) {
//                    mDrawerLayout.closeDrawer(navrl);
//                    Intent brain = new Intent(context, WalletActivity.class);
//                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(brain);
//                } else if (controller.checkSingleMode().equalsIgnoreCase("1")) {
//                    mDrawerLayout.closeDrawer(navrl);
//                    Intent brain = new Intent(context, SingleModeActivity.class);
//                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(brain);
//                }
//                break;
//            }
//            case 4: {
//                if (controller.getConstantsValueForKey("single_ride").equalsIgnoreCase("1")) {
//                    mDrawerLayout.closeDrawer(navrl);
//                    Intent brain = new Intent(context, SingleModeActivity.class);
//                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(brain);
//                }
//                break;
//            }
//            case 5: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent brain = new Intent(context, LanguageSelectionActivity.class);
//                brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(brain);
//                break;
//            }
//        }
//    }

    public void setSideDrawerManagerCallback(SideDrawerManagerCallback sideDrawerManagerCallback) {
    }

    public void setDrawerLockModeClosed() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void setDrawerLockModeUnDefined() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(navrl);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout.isDrawerOpen(navrl);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(navrl);
    }

    public interface SideDrawerManagerCallback {
        void onSideShowdialog(boolean isLogout, String message);
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            displayView(position);
        }
    }

}
