package com.ridertechrider.driver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ridertechrider.driver.adaptor.AssignedTripListAdaptor;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.custom.XListView.XListView;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.DriverConstants;
import com.ridertechrider.driver.webservice.RepeatTimerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsignedRequestFragment extends Fragment implements XListView.IXListViewListener {

    private static AsignedRequestFragment tripRequestFragment;
    private final int limit = 10;
    private final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    int firstItemVisibleL = 0;
    int lastItemVisible = 0;
    private ListView listView;
    private TextView listEmptyTxt;
    private Activity activity;
    private AssignedTripListAdaptor adaptor;
    private int offSet = 0;
    private ArrayList<TripModel> tripHistoryList;
    private Controller controller;
    private RequestFragment.ClickListenerCallback mCallback;
    private SwipeRefreshLayout pullToRefresh;
    private Parcelable state;
    public final RepeatTimerManager repeatTimerManager = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
        @Override
        public void onRepeatPerfrom() {
            System.out.print("onRepeatPerfrom : " + repeatTimerManager + "  tripRequestFragment:" + tripRequestFragment);
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        requestOngoingTrips(false);
                    }
                });
            }

        }

        @Override
        public void onStopRepeatPerfrom() {
            System.out.print("onStopRepeatPerfrom : " + repeatTimerManager + "  tripRequestFragment:" + tripRequestFragment);
            Controller controller = (Controller) Objects.requireNonNull(getActivity()).getApplication();
            if (controller.getRequestTripResuestAssigned() != null) {
                controller.getRequestTripResuestAssigned().cancel();
                controller.setRequestTripResuestAssigend(null);
            }
        }
    }, 30000);
    private BroadcastReceiver availabilityChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            requestOngoingTrips(false);
        }
    };
    private BroadcastReceiver requestPageChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int page = intent.getIntExtra("page", -1);
            if (page == 1) {
                requestOngoingTrips(false);
            }
        }
    };

    public static AsignedRequestFragment getInstance() {

        if (tripRequestFragment == null) {
            tripRequestFragment = new AsignedRequestFragment();
        }
        return tripRequestFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        repeatTimerManager.startRepeatCall();
    }

    @Override
    public void onPause() {

        state = listView.onSaveInstanceState();
        super.onPause();
        repeatTimerManager.stopRepeatCall();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.asigned_fragment_layout, container, false);
        pullToRefresh = view.findViewById(R.id.pullToRefresh);
        listEmptyTxt = view.findViewById(R.id.list_empty_txt);
        listView = view.findViewById(R.id.asigned_req_listView);

        // listView.setPullLoadEnable(true);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), Fonts.KARLA);
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        tripHistoryList = new ArrayList<>();
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        CategoryActors driverCat = null;
        for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
            if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                driverCat = categoryActors;
            }
        }
        adaptor = new AssignedTripListAdaptor(activity, tripHistoryList, typeface, mCallback, driverCat, this);
        // listView.setXListViewListener(this);
        // listView.setPullLoadEnable(false);
        //setting an setOnRefreshListener on the SwipeDownLayout
        if (state != null) {

            listView.onRestoreInstanceState(state);
        }
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                state = listView.onSaveInstanceState();

                View c = listView.getChildAt(0);
                if (c != null) {
                    int scrolly = -c.getTop() + listView.getFirstVisiblePosition() * c.getHeight();
                    firstItemVisibleL = scrolly;
                }
            }
        });
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            int Refreshcounter = 1; //Counting how many times user have refreshed the layout

            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                requestOngoingTrips(false);
            }
        });
        listView.setAdapter(adaptor);
        controller = (Controller) getActivity().getApplicationContext();

    }

    public void setCallback(RequestFragment.ClickListenerCallback mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (RequestFragment.ClickListenerCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ClickListenerCallback");
        }

        Controller.getInstance().registerReceiver(availabilityChangedReceiver, new IntentFilter("AVAILABILITY_CHANGED_RECEIVER"));
        Controller.getInstance().registerReceiver(requestPageChangedReceiver, new IntentFilter("refresh_request_list"));
    }

    private void requestOngoingTrips(final boolean isLoadMore) {
        if (!isLoadMore) {
            offSet = 0;
            if (adaptor != null)
                adaptor.clear();
        }
        if (Controller.isActivityVisible() && controller != null) {
            if (controller.getLoggedDriver() != null && controller.getLoggedDriver().getD_is_verified()) {
                getTripRequestList(offSet, isLoadMore);
            } else {
                Controller.getInstance().stopNotificationSound();

                if (adaptor != null) {
                    CategoryActors driverCat = null;
                    for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                        if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                            driverCat = categoryActors;
                        }
                    }
                    adaptor.setDriverCat(driverCat);
                    adaptor.notifyDataSetChanged();
                }
                controller.stopNotificationSound();
                listEmptyTxt.setVisibility(View.GONE);
            }
        }
        pullToRefresh.setRefreshing(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Controller.getInstance().unregisterReceiver(availabilityChangedReceiver);
        Controller.getInstance().unregisterReceiver(requestPageChangedReceiver);
    }

    public void getTripRequestList(final int nextoffset, final boolean isLoadMore) {

        Controller controller = (Controller) Objects.requireNonNull(getActivity()).getApplication();
        String categoryId = controller.getLoggedDriver().getCategory_id();
        String d_lat = controller.getLoggedDriver().getD_lat();
        String d_lng = controller.getLoggedDriver().getD_lng();

        if (!((SlideMainActivity) getActivity()).hasLocationPermission)
            return;

        if (controller.getRequestTripResuestAssigned() != null) {
            repeatTimerManager.stopRepeatCall();
            repeatTimerManager.startRepeatCall();
            return;
        }

        String radius = "";
        for (DriverConstants driverConstants : controller.getConstantsList()) {
            if (driverConstants.getConstant_key().equalsIgnoreCase("driver_radius")) {
                radius = driverConstants.getConstant_value();
            }
        }
        String t_idss = controller.pref.geTripIds();
        Log.d("driver", "" + controller.getLoggedDriver().getCategory_id());

        Call<ResponseBody> request = apiInterface.getAsignedTripRequestList(controller.getLoggedDriver().getApiKey(), controller.getLoggedDriver().getDriverId(), Constants.TripStatus.ASSIGN);
        controller.setRequestTripResuestAssigend(request);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("TAG", "onResponse: response.isSuccessful(): " + response.isSuccessful());
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        //listView.stopLoadMore();
                        //listView.stopRefresh();
                        handleHistoryResponse(string);

                    } catch (IOException e) {
                        e.printStackTrace();
                        if (repeatTimerManager.isRunning()) {
                            repeatTimerManager.setTimerForRepeat();
                        }
                    }
                }
                if (repeatTimerManager.isRunning()) {
                    repeatTimerManager.setTimerForRepeat();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (call.isCanceled()) {
                    listView.setVisibility(View.GONE);
                    listEmptyTxt.setVisibility(View.VISIBLE);
                } else {
                    if (repeatTimerManager.isRunning()) {
                        repeatTimerManager.setTimerForRepeat();
                    }
                }
            }
        });
    }

    private void handleHistoryResponse(String s) {
        try {
            JSONObject jsonRootObject = new JSONObject(s);
            if (jsonRootObject.get(Constants.Keys.RESPONSE) instanceof JSONArray) {
                JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
                boolean isHasMoreData = response.length() == limit;
                tripHistoryList.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject childObject = response.getJSONObject(i);
                    Log.d("childOBject", "" + childObject.getJSONObject("User"));
                    TripModel tripModel = TripModel.parseJson(childObject.toString());
                    tripHistoryList.add(tripModel);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Collections.sort(tripHistoryList, TripModel.comparator);
        if (tripHistoryList.size() == 0) {
            listView.setVisibility(View.GONE);
            listEmptyTxt.setVisibility(View.VISIBLE);
            listEmptyTxt.setText(Localizer.getLocalizerString("k_52_s4_waiting_new_ride_req"));
        } else {
            listView.setVisibility(View.VISIBLE);
            listEmptyTxt.setVisibility(View.GONE);
        }
        try {

            if (adaptor != null) {
                CategoryActors driverCat = null;
                for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                    if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                        driverCat = categoryActors;
                    }
                }
                adaptor.setDriverCat(driverCat);
                adaptor.notifyDataSetChanged();
            }
            Log.e("scrollu", "" + firstItemVisibleL);
            if (state != null) {

                listView.onRestoreInstanceState(state);
            }
            // listView.setSelection(firstItemVisibleL);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRefresh() {
        requestOngoingTrips(false);
    }

    @Override
    public void onLoadMore() {
        adaptor.clear();
        offSet += limit;
        requestOngoingTrips(true);
    }

}
