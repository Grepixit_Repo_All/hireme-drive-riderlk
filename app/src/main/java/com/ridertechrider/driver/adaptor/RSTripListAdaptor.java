package com.ridertechrider.driver.adaptor;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.Localizer;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.RequestFragment.ClickListenerCallback;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RSTripListAdaptor extends RecyclerView.Adapter<RSTripListAdaptor.MyViewHolder> {
    private final ArrayList<TripModel> tripHistories;
    private final Typeface typeface;
    private final Controller controller;
    private final ClickListenerCallback mCallback;
    private final Fragment requestFragment;
    private CategoryActors driverCat;


    public RSTripListAdaptor(Activity activity, ArrayList<TripModel> tripHistoryList, Typeface typeface, ClickListenerCallback callback, CategoryActors driverCat, Fragment requestFragment) {
        this.tripHistories = tripHistoryList;
        this.typeface = typeface;
        controller = (Controller) activity.getApplication();
        this.mCallback = callback;
        this.requestFragment = requestFragment;
        this.driverCat = driverCat;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder mViewHolder, int position) {
        final TripModel tripModel = getItem(position);

        if (tripModel != null) {

            if (tripModel.user.isProfileImagePathEmpty()) {
                AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripModel.user.getUProfileImagePath(), mViewHolder.thumbNail);
            } else {
                mViewHolder.thumbNail.setImageResource(R.drawable.user);
            }

            mViewHolder.tvName.setText(tripModel.user.getU_name());

            if (tripModel.trip.getPickup_notes() != null) {
                mViewHolder.tvPickAddress.setText(String.format("%s\n\n%s %s", tripModel.trip.getTrip_from_loc(), Localizer.getLocalizerString("k_1_s8_special_notes"), tripModel.trip.getPickup_notes()));
            } else {
                mViewHolder.tvPickAddress.setText(tripModel.trip.getTrip_from_loc());
            }

            if (tripModel.trip.getTrip_pay_mode() != null && !tripModel.trip.getTrip_pay_mode().trim().isEmpty() && driverCat != null && driverCat.getShow_paymode().equalsIgnoreCase("1")) {
                mViewHolder.trip_pay_mode.setVisibility(View.VISIBLE);
                mViewHolder.trip_pay_mode.setText(String.format("%s %s", Localizer.getLocalizerString("k_1_s8_pay_via"), tripModel.trip.getTrip_pay_mode()));
            } else {
                mViewHolder.trip_pay_mode.setVisibility(View.GONE);
            }

            if (tripModel.trip.getTrip_pay_amount() != null && !tripModel.trip.getTrip_pay_amount().trim().isEmpty() &&
                    !tripModel.trip.getTrip_pay_amount().trim().equalsIgnoreCase("null") &&
                    !tripModel.trip.getTrip_pay_amount().trim().equalsIgnoreCase("0") && driverCat != null && driverCat.getShow_fare().equalsIgnoreCase("1")) {

                mViewHolder.trip_pay_amount.setVisibility(View.VISIBLE);
                mViewHolder.trip_pay_amount.setText(String.format("%s %s", Localizer.getLocalizerString("k_1_s8_est"), controller.formatAmountWithCurrencyUnit(tripModel.trip.getTrip_pay_amount())));
            } else {
                mViewHolder.trip_pay_amount.setVisibility(View.GONE);
            }

            mViewHolder.requestAcceptButton.setText(Utils.getTextOfStatus(tripModel.trip.getTrip_status()));

            mViewHolder.dateTrip.setText("@" + Utils.convertServerDateToAppLocalDate(tripModel.trip.getTrip_date()));

//        mViewHolder.tvPickAddress.setText(tripModel.trip.getTrip_from_loc());
            mViewHolder.tvDropAddress.setText(tripModel.trip.getTrip_to_loc());
            mViewHolder.request_layout.setTag(tripModel);

            mViewHolder.destinationLayoutT.setVisibility(View.VISIBLE);
            mViewHolder.requestAcceptButton.setVisibility(View.VISIBLE);

            mViewHolder.requestAcceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position != -1 && position < tripHistories.size()) {
//                        mCallback.showProgress();
                        if (Utils.isOngoingTrip(tripModel.trip.getTrip_status()))
                            mCallback.onCurrentTripChange(tripModel);
                    }
                }
            });
            mViewHolder.pickupAddressMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.viewOnMap(controller.getResources().getString(R.string.trip_detail),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lng())),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lng())));
                }
            });
            mViewHolder.dropAddressView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.viewOnMap(controller.getResources().getString(R.string.trip_detail),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lng())),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lng())));
                }
            });
        }
        setLocalizeData(mViewHolder);
    }

    private TripModel getItem(int position) {
        if (position != -1 && tripHistories.size() > 0 && position <= tripHistories.size() - 1)
            return tripHistories.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return tripHistories.size();
    }

    private void setLocalizeData(MyViewHolder mViewHolder) {
        mViewHolder.msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
        mViewHolder.msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        mViewHolder.callMe.setText(Localizer.getLocalizerString("s_2_s10_call_me"));
    }

    public void clear() {
        this.tripHistories.clear();
        notifyDataSetChanged();
    }

    public void setDriverCat(CategoryActors driverCat) {
        this.driverCat = driverCat;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        final TextView msa_tv_drop, msa_tv_pickup, callMe;
        TextView tvName, tvPickAddress, tvDropAddress, isDeliveryView;
        TextView requestAcceptButton, dateTrip;
        TextView trip_pay_mode, trip_pay_amount;
        RelativeLayout request_layout;
        LinearLayout destinationLayoutT;
        CircleImageView thumbNail;
        ImageView pickupAddressMap, dropAddressView, request_reject;

        MyViewHolder(View view) {
            super(view);
            this.callMe = view.findViewById(R.id.callMe);
            this.tvName = view.findViewById(R.id.name);
            this.isDeliveryView = view.findViewById(R.id.isDelivery);
            this.tvName.setTypeface(typeface);
            this.msa_tv_drop = view.findViewById(R.id.msa_tv_drop);
            this.msa_tv_pickup = view.findViewById(R.id.msa_tv_pickup);
            this.dateTrip = view.findViewById(R.id.date_trip);
            this.tvPickAddress = view.findViewById(R.id.pick_address);
            this.tvPickAddress.setTypeface(typeface);
            this.tvDropAddress = view.findViewById(R.id.drop_address);
            this.tvDropAddress.setTypeface(typeface);
            this.requestAcceptButton = view.findViewById(R.id.request_accept);
            this.request_layout = view.findViewById(R.id.req_layou);
            this.destinationLayoutT = view.findViewById(R.id.destinationLayoutT);
            this.thumbNail = view.findViewById(R.id.icon);
            this.pickupAddressMap = view.findViewById(R.id.pickupAddressMap);
            this.dropAddressView = view.findViewById(R.id.dropAddressMap);
            this.trip_pay_mode = view.findViewById(R.id.trip_pay_mode);
            this.trip_pay_amount = view.findViewById(R.id.trip_pay_amount);
            this.request_reject = view.findViewById(R.id.request_reject);

            callMe.setVisibility(View.GONE);
        }
    }

}
