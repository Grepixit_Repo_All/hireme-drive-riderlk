package com.ridertechrider.driver.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.ridertechrider.driver.Localizer;
import com.ridertechrider.driver.R;

import java.util.Objects;


/**
 * Created by BEveryware on 4/4/14.
 */

public class CustomProgressDialog {
    //private final TextView textview;
    private final Dialog dialog;
    private final Context context;

    public CustomProgressDialog(Context context) {
        this.context = context;
        dialog = new Dialog(context, R.style.loader_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_progress_dialog);
        Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        BTextView wait = dialog.findViewById(R.id.text);
        wait.setText(Localizer.getLocalizerString("k_r16_s3_plz_wait"));

        ProgressBar progressBar = dialog.findViewById(R.id.progressBar1);
        // textview = dialog.findViewById(R.id.text);
        progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.picker_background), android.graphics.PorterDuff.Mode.SRC_ATOP);

    }

    public void showDialog() {
        //dialog.setCancelable(true);
        try {
            if (!((Activity) context).isFinishing() && dialog != null)
                dialog.show();
        } catch (Exception e) {
            Log.e(CustomProgressDialog.class.getSimpleName(), "showDialog: " + e.getMessage(), e);
        }
    }

    public void dismiss() {
        try {
            if (dialog.isShowing()) dialog.dismiss();
        } catch (Exception e) {
            dialog.dismiss();
        }

    }

    public boolean isShowing() {
        return dialog.isShowing();
    }
}