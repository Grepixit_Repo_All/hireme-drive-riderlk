package com.ridertechrider.driver.adaptor;

import android.util.Log;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.SingleModeActivity;
import com.ridertechrider.driver.SlideMainActivity;
import com.ridertechrider.driver.webservice.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.ridertechrider.driver.webservice.Constants.Keys.TRIP_PAY_AMOUNT;

/**
 * Created by grepix on 2/1/2017.
 */
public class TripModel implements Serializable {

    public static Comparator<TripModel> comparator = new Comparator<TripModel>() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        @Override
        public int compare(TripModel o1, TripModel o2) {
            return compare(o1.trip.getTrip_date(), o2.trip.getTrip_date());
        }

        public int compare(String lhs, String rhs) {
            try {
                return dateFormat.parse(lhs).compareTo(dateFormat.parse(rhs));
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;

            }
        }
    };
    private final String TAG = "TripModel";
    public String tripId;
    public String trip_reason = "";
    public String tripStatus;
    public String tripDistance;
    public String tripDistanceUnit;
    public String taxAmount;
    public String tripAmount;
    public String tripBaseFare;
    public String tripCurrency;
    public User user;
    public Driver driver;
    public String pick_time;
    public String drop_time;
    public String wait_duration;
    public double minutes;
    public Trip trip;
    public String city_id;
    public String otp;
    public String pickup_notes;
    public String actual_pickup_lat, actual_pickup_lng, actual_drop_lat, actual_drop_lng;

    public static TripModel parseJson(String tripJson) throws JSONException {
        TripModel tripModel = new TripModel();
        tripModel.trip = Trip.parseJson(tripJson);
        JSONObject tripJsonObject = new JSONObject(tripJson);
        tripModel.user = User.parseJson(tripJsonObject.getJSONObject("User").toString());
//        if (tripJsonObject.get("Driver") instanceof JSONObject)
        if (tripJsonObject.has("Driver") && !tripJsonObject.isNull("Driver") && tripJsonObject.get("Driver") instanceof JSONObject)
            tripModel.driver = Driver.parseJson(tripJsonObject.getJSONObject("Driver").toString());
        return tripModel;
    }

    public static TripModel parseJson(Trip trip) {
        TripModel tripModel = new TripModel();
        tripModel.trip = trip;
        tripModel.tripId = trip.getTrip_id();
        tripModel.tripStatus = trip.getTrip_status();
        tripModel.otp = trip.getOtp();
        tripModel.city_id = trip.getCity_id();
        tripModel.actual_drop_lat = trip.getTrip_actual_drop_lat();
        tripModel.actual_drop_lng = trip.getTrip_actual_drop_lng();
        tripModel.actual_pickup_lat = trip.getTrip_actual_pick_lat();
        tripModel.actual_pickup_lng = trip.getTrip_actual_pick_lng();
        tripModel.pick_time = trip.getTrip_pickup_time();
        tripModel.tripCurrency = trip.getTrip_currency();
        tripModel.tripBaseFare = trip.getTrip_base_fare();
        tripModel.tripAmount = trip.getTrip_pay_amount();
        tripModel.taxAmount = trip.getTax_amt();
        tripModel.user = trip.getUser();
        tripModel.driver = trip.getDriver();
        return tripModel;
    }

    public static boolean parseJsonWithTripModel(String tripJson, TripModel tripModel) {
        try {
            JSONObject jsonRootObject = new JSONObject(tripJson);
            Object response = jsonRootObject.get("response");
            if (response == null) {
                return false;
            }
            JSONArray jsonArray = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
            JSONObject childObject = jsonArray.getJSONObject(0);
            tripModel.trip = Trip.parseJson(childObject.toString());
            tripModel.tripStatus = tripModel.trip.getTrip_status();
            tripModel.user = User.parseJson(childObject.getJSONObject("User").toString());
            try {
                tripModel.driver = Driver.parseJson(childObject.getJSONObject("Driver").toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getCity_id() {
        return city_id;
    }

    public String getWait_duration() {
        return wait_duration;
    }

    public void setWait_duration(String wait_duration) {
        this.wait_duration = wait_duration;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getActual_pickup_lat() {
        return actual_pickup_lat;
    }

    public void setActual_pickup_lat(String actual_pickup_lat) {
        this.actual_pickup_lat = actual_pickup_lat;
    }

    public String getPickup_notes() {
        return pickup_notes;
    }

    public void setPickup_notes(String pickup_notes) {
        this.pickup_notes = pickup_notes;
    }

    public String getActual_pickup_lng() {
        return actual_pickup_lng;
    }

    public void setActual_pickup_lng(String actual_pickup_lng) {
        this.actual_pickup_lng = actual_pickup_lng;
    }

    public Map<String, String> getParams(int i) {

        Map<String, String> params = new HashMap<>();
        params.put("trip_status", tripStatus);
        params.put("trip_id", tripId);

        if (trip_reason != null && trip_reason.length() > 0) {
            params.put("trip_reason", trip_reason);
        }
        if (tripStatus.equals(Constants.TripStatus.END) || tripStatus.equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {

            if (!trip.getIs_share().equalsIgnoreCase("1")) {
                params.put(Constants.Keys.TRIP_DISTANCE, tripDistance);
                params.put(Constants.Keys.TRIP_DISTANCE_UNIT, tripDistanceUnit);

//            params.put("trip_distance", tripDistance);
                params.put("tax_amt", taxAmount);
                params.put(TRIP_PAY_AMOUNT, tripAmount);
                params.put("wait_duration", "" + wait_duration);
                params.put("trip_total_time", "" + minutes);
            }
            params.put("trip_drop_time", drop_time);
            params.put(Constants.Keys.TRIP_ACTUAL_DROP_LAT, actual_drop_lat);
            params.put(Constants.Keys.TRIP_ACTUAL_DROP_LNG, actual_drop_lng);

            if (i == 2) {
                params.put(Constants.Keys.TRIP_ACTULA_TO_LOC, SingleModeActivity.dropAddress);
            } else {
                params.put(Constants.Keys.TRIP_ACTULA_TO_LOC, SlideMainActivity.dropAddress);

            }
        }
        if (tripStatus.equals(Constants.TripStatus.END) && trip.getTrip_to_loc() != null && trip.getTrip_to_loc().trim().length() == 0) {
            if (i == 2) {
                params.put(Constants.Keys.TRIP_DEST_LOC, SingleModeActivity.dropAddress);
            } else {
                params.put(Constants.Keys.TRIP_DEST_LOC, SlideMainActivity.dropAddress);

            }
        }
        if (tripStatus.equals(Constants.TripStatus.BEGIN)) {
            params.put("trip_pickup_time", pick_time);
            params.put(Constants.Keys.TRIP_ACTUAL_PICK_LAT, actual_pickup_lat);
            params.put(Constants.Keys.TRIP_ACTUAL_PICK_LNG, actual_pickup_lng);
            if (otp != null)
                params.put(Constants.Keys.OTP, otp);
            if (i == 2) {
                params.put(Constants.Keys.TRIP_ACTUAL_FROM_LOC, SingleModeActivity.pickupAddress);

            } else {
                params.put(Constants.Keys.TRIP_ACTUAL_FROM_LOC, SlideMainActivity.pickupAddress);

            }
        }


        Log.d(TAG, " Update Trip Params : " + params);
        return params;
    }

    public Map<String, String> getUpdateParams() {
        Map<String, String> params = new HashMap<>();
        params.put("trip_status", tripStatus);
        params.put("trip_id", tripId);
        return params;
    }

    public Map<String, String> getParamsforGetTrip() {

        Map<String, String> params = new HashMap<>();
        params.put("trip_id", tripId);
        return params;
    }


    @NonNull
    @Override
    public String toString() {
        String object = "  " + tripStatus + " " + tripId;
        Log.d(TAG, object);
        return object;
    }
}
