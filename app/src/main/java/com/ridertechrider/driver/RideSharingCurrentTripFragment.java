package com.ridertechrider.driver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.Model.TripMaster;
import com.ridertechrider.driver.adaptor.RSTripListAdaptor;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.fonts.Fonts;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.model.AppData;
import com.grepix.grepixutils.WebServiceUtil;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static com.ridertechrider.driver.webservice.Constants.Urls.URL_GET_MTRIPS_TRIP_REQUEST;

public class RideSharingCurrentTripFragment extends Fragment {

    private static final String TAG = "RSCTFragment";
    private static RideSharingCurrentTripFragment requestFragment;
    public TextView listEmptyTxt;
    private RecyclerView listView;
    private Activity activity;
    private RSTripListAdaptor adaptor;
    private ArrayList<TripModel> tripHistoryList;
    private Controller controller;
    private RequestFragment.ClickListenerCallback mCallback;
    private TripMaster tripMaster;
    private BroadcastReceiver requestPageChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int page = intent.getIntExtra("page", -1);
            if (page == 0) {
                refresh(false);
            }
        }
    };

    public static RideSharingCurrentTripFragment getInstance() {

        if (requestFragment == null) {
            requestFragment = new RideSharingCurrentTripFragment();
        }
        return requestFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.rsr_fragment_layout, container, false);
        listEmptyTxt = view.findViewById(R.id.list_empty_txt);
        listView = view.findViewById(R.id.req_listView);
        listView.setLayoutManager(new LinearLayoutManager(activity));
        listView.setItemAnimator(new DefaultItemAnimator());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), Fonts.KARLA);
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        tripHistoryList = new ArrayList<>();
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        CategoryActors driverCat = null;
        for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
            if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                driverCat = categoryActors;
            }
        }
        adaptor = new RSTripListAdaptor(activity, tripHistoryList, typeface, mCallback, driverCat, this);
        listView.setAdapter(adaptor);
        controller = (Controller) getActivity().getApplicationContext();

        refresh(false);
    }

    public void setCallback(RequestFragment.ClickListenerCallback mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (RequestFragment.ClickListenerCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ClickListenerCallback");
        }

        Controller.getInstance().registerReceiver(requestPageChangedReceiver, new IntentFilter("refresh_request_list1"));
    }

    public void refresh(final boolean isLoadMore) {
        if (adaptor != null)
            adaptor.clear();
        if (Controller.isActivityVisible() && controller != null) {
            if (controller.getLoggedDriver() != null) {
                tripHistoryList.clear();
                if (getActivity() != null) {
                    getMTrips();
                }
            } else {
                tripHistoryList.clear();
                if (adaptor != null) {
                    CategoryActors driverCat = null;
                    for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                        if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                            driverCat = categoryActors;
                        }
                    }
                    adaptor.setDriverCat(driverCat);
                    adaptor.notifyDataSetChanged();
                }
                listEmptyTxt.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Controller.getInstance().unregisterReceiver(requestPageChangedReceiver);
    }

    private void getMTrips() {
        TripModel tripModel = AppData.getInstance(getContext()).getTripModel();
        if (tripModel == null || tripModel.trip == null)
            return;
        HashMap<String, String> params = new HashMap<>();
        if (tripModel.trip.getM_trip_id() != null)
            params.put("m_trip_id", tripModel.trip.getM_trip_id());
        else
            params.put("trip_id", tripModel.trip.getTrip_id());

        ServerApiCall.callWithApiKeyAndDriverId(getContext(), params, URL_GET_MTRIPS_TRIP_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    try {
                        JSONObject objResponse = new JSONObject(data.toString());
                        if (objResponse.has("response") && !objResponse.isNull("response")) {
                            if (objResponse.get("response") instanceof JSONObject) {
                                tripMaster = TripMaster.parseTripMaster(objResponse.getJSONObject("response").toString());
                            } else if (objResponse.get("response") instanceof JSONArray && objResponse.getJSONArray("response").length() > 0) {
                                tripMaster = TripMaster.parseTripMaster(objResponse.getJSONArray("response").getJSONObject(0).toString());
                            } else {
                                tripMaster = null;
                            }
                            handleHistoryResponse();

                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "getMTrips: " + e.getMessage(), e);
                    }
                }
            }
        });
    }

    private void handleHistoryResponse() {
        tripHistoryList.clear();
        if (tripMaster != null && tripMaster.getTrips() != null) {
            for (int i = 0; i < tripMaster.getTrips().size(); i++) {
                TripModel tripModel = TripModel.parseJson(tripMaster.getTrips().get(i));
                if (tripModel.trip != null && Utils.isOngoingTrip(tripModel.trip.getTrip_status())) {
                    tripHistoryList.add(tripModel);
                }
            }
        }
        if (tripHistoryList.size() == 0) {
            listView.setVisibility(View.GONE);
            listEmptyTxt.setVisibility(View.VISIBLE);
            listEmptyTxt.setText(Localizer.getLocalizerString("k_52_s4_waiting_new_ride_req"));

        } else {
            listView.setVisibility(View.VISIBLE);
            listEmptyTxt.setVisibility(View.GONE);
        }
        try {
            if (adaptor != null) {
                CategoryActors driverCat = null;
                for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                    if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                        driverCat = categoryActors;
                    }
                }
                adaptor.setDriverCat(driverCat);
                adaptor.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

