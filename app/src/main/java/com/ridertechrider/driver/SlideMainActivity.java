package com.ridertechrider.driver;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.kayvannj.permission_utils.Func2;
import com.github.kayvannj.permission_utils.PermissionUtil;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.maps.android.SphericalUtil;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.Utils;
import com.grepix.grepixutils.WebServiceUtil;
import com.ridertechrider.driver.DeliverResponse.Receiver;
import com.ridertechrider.driver.DeliverResponse.Sender;
import com.ridertechrider.driver.Model.TripMaster;
import com.ridertechrider.driver.adaptor.DeliveryListAdapter;
import com.ridertechrider.driver.adaptor.DistanceAndTimeViewModel;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.adaptor.Trip;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.adaptor.User;
import com.ridertechrider.driver.app.GoogleSDRoute;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BButton;
import com.ridertechrider.driver.custom.BEditText;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.distance_martix.DistanceMatrixResponse;
import com.ridertechrider.driver.distance_martix.Element;
import com.ridertechrider.driver.service.LocationService;
import com.ridertechrider.driver.service.PreferencesUtils;
import com.ridertechrider.driver.service.TrackRecordingServiceConnectionUtils;
import com.ridertechrider.driver.track_route.TrackRouteSaveData;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.TripFareCalculator;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.DriverConstants;
import com.ridertechrider.driver.webservice.RepeatTimerManager;
import com.ridertechrider.driver.webservice.SingleObject;
import com.ridertechrider.driver.webservice.controller.TripController;
import com.ridertechrider.driver.webservice.model.AppData;
import com.ridertechrider.driver.webservice.model.LoginResponse;
import com.ridertechrider.driver.webservice.model.NearUserResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ridertechrider.driver.service.ILocationConstants.LOACTION_ACTION;
import static com.ridertechrider.driver.service.ILocationConstants.LOCATION_MESSAGE;
import static com.ridertechrider.driver.service.ILocationConstants.PERMISSION_ACCESS_LOCATION_CODE;
import static com.ridertechrider.driver.utils.Utils.dpToPx;
import static com.ridertechrider.driver.webservice.Constants.TripStatus.ACCEPT;
import static com.ridertechrider.driver.webservice.Constants.TripStatus.ARRIVE;
import static com.ridertechrider.driver.webservice.Constants.TripStatus.CANCEL;
import static com.ridertechrider.driver.webservice.Constants.TripStatus.CANCEL_RIDER;
import static com.ridertechrider.driver.webservice.Constants.TripStatus.REQUEST;
import static com.ridertechrider.driver.webservice.Constants.Urls.DRIVER_PENDING_TRIPS;
import static com.ridertechrider.driver.webservice.Constants.Urls.URL_DRIVER_LOGIN;
import static com.ridertechrider.driver.webservice.Constants.Urls.URL_DRIVER_PHONE_SIGN_IN;
import static com.ridertechrider.driver.webservice.Constants.Urls.URL_GET_MTRIPS_TRIP_REQUEST;
import static com.ridertechrider.driver.webservice.Constants.Values.USE_EMAIL_AUTH;

public class SlideMainActivity extends BaseCompatActivity implements
        ConnectionCallbacks, OnConnectionFailedListener, TripController.TripControllerCallBack, RequestFragment.ClickListenerCallback {
    private static final int REQUEST_PERMISSIONS = 100;
    private static final String TAG = "SlideMainActivity";
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 3; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 5000; // in Milliseconds
    public static String pickupAddress = "";
    public static Controller controller;
    public static String dropAddress = "";
    private static String is_required_drawRoute = "No";
    private final ArrayList<LatLng> coverLatLongsD = new ArrayList<>();
    private final Handler customHandler = new Handler();
    private final Handler handler1 = new Handler();
    private final String upudateProfileApiTag = "upudateProfileApiTag";
    private final Handler waitingHandler = new Handler();
    public boolean hasLocationPermission = false;
    boolean isFareSummary = false;
    RelativeLayout otpLayout;
    BEditText otpView;
    BButton applyOtp;
    LinearLayout riderInfoLayout;
    CircleImageView riderProfile;
    ImageView callRider;
    BTextView riderName;
    BTextView passengerDetails;
    boolean isTripFinish = false;
    double previousCalulatedAmount = 0.0;
    NavDrawerListAdapter navDrawerListAdapter;
    BTextView waitinTimeView;
    private final Runnable waitingTimerThread = new Runnable() {

        public void run() {
            if (!controller.pref.getIsWaiting()) {
                return;
            }
            int waitingTime = controller.pref.getWaitingTime();
            int waitingTimeTemp = 0;
            if (controller.pref.getWaitingStartTime() > 0) {
                waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
            }
            waitingTime = waitingTime + waitingTimeTemp;
//            waitingTime = waitingTime + 1;
//            controller.pref.setWaitingTime(waitingTime);
            Log.e("waitnig", "" + waitingTime);
            if (waitinTimeView != null)
                waitinTimeView.setText(Localizer.getLocalizerString("k_16_s4_waiting_time") + waitingTime + Localizer.getLocalizerString("k_17_s4_min"));
            waitingHandler.postDelayed(this, 30 * 1000);
        }

    };
    BTextView waitingView;
    boolean isWaiting = false;
    private LinearLayout layoutAccountStatus;
    private TextView refreshAccountStatus;
    private TextView nodocumentalerttext;
    private Typeface drawerListTypeface;
    private boolean isDelivery = false;
    private boolean isBegin = false;
    private int i = 0;
    private ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList;
    private int numDel = 0;
    private String finalTime = "1 min";
    private boolean destinationPopUpVisible = false;
    private boolean pickupPopupVisible = false;
    private boolean commentBoxVisible = false;
    private boolean isTripBegin = false;
    private Marker markerDriverCarD;
    private Dialog listDialog;
    private DistanceAndTime distanceAndTimeView;
    private DestinationView destinationActivity;
    private LinearLayout linearBackNext;
    private CountDownTimer count_downt_timer;
    private ActionBarDrawerToggle mDrawerToggle;
    private ImageView drawerbut;
    private GoogleMap mGoogleMap;
    private String acc1;
    private GoogleApiClient googleApiClient = null;
    private ImageView imgLocation;
    private Marker trackStartMarker;
    private TripModel tripModel;
    private TripMaster tripMaster;
    private String waypoints;
    private LinearLayout full;
    private Button fullbutton;
    private CustomProgressDialog progressDialog;
    private Switch switchAvailable1;
    private ClientPickedUpView clientPickedUpView;
    private Button goButton;
    private BackToOrderView backToOrderView;
    //    private Location preLocation;
    private TextView tvSwitchText;
    private AddressLocationView addressLocationView;
    private UserInfoView userInfoView;
    private Location myCurrentLocation;
    private Location myLastCurrentLocation;
    private CircleImageView imageV;
    private boolean driver_rejected;
    private boolean isRequest = true;
    private GoogleApiClient client;
    private SideDrawerManager sideDrawerManager;
    private boolean isBeginRouteDrow;
    private ArrayList<Marker> markersNearUsers;
    private GoogleSDRoute.RouteResponse mRouteResponse;
    //    private ArrayList<Polyline> polylines;
    private GoogleSDRoute googleSDRoute;
    //    private Marker markerDriverCar;
    private boolean isPickRouteDrow = false;
    private boolean isCallingForGetRoute = false;
    private Call<LoginResponse> updateDriverLocation;
    private LocationReceiver locationReceiver;
    //    private PickUpLocationReceiver pickUpLocationReceiver;
//    private ArrayList<LatLng> coverLatLongs = new ArrayList<>();
    private ArrayList<Polyline> coverRoutePolyLineD = new ArrayList<>();
    //    private ArrayList<Polyline> coverRoutePolyLine = new ArrayList<>();
    private Button btn_change;
    private FloatingActionButton btn_toggle_rs;
    private View view;
    private int width, height;
    private boolean status = false;
    private RelativeLayout topRelativeLayout;
    //    private LinearLayout riderSharingLayout;
//    private BottomSheetBehavior sheetBehavior;
    private RideSharingFragment rideSharingFragment;

    private RelativeLayout rvRequest;
    private final View.OnClickListener listener = new View.OnClickListener() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View v) {
            if (!status) {
                status = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    btn_change.setBackground(getResources().getDrawable(R.drawable.down_arrow));
                }
                resizeFragment(rvRequest, width, height);
            } else {
                status = false;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    btn_change.setBackground(getResources().getDrawable(R.drawable.up_arrow));
                }
                resizeFragment(rvRequest, width, height / 2);
            }
        }
    };
    private TripRequestFragment myFrag;
    private RequestFragment myReqFrag;
    private double pikLat, pikLng;
    private String url;
    private boolean isZoomEnabledFirst = true;
    private boolean isZoomEnabled = false;
    private final Runnable r = new Runnable() {
        @Override
        public void run() {
            isZoomEnabled = true;
            handler1.removeCallbacks(r);
        }
    };
    private String tripTime = "0 min";
    private ArrayList<Receiver> receiverArray;
    private boolean isAlready = false;
    private AlertDialog.Builder builder;
    private Toolbar toolbar;
    private BTextView about;
    private BTextView privacy;
    private boolean isEndable = false;
    private boolean isEndableTwo = false;
    private RelativeLayout navrl;
    // ==============   TEST   ===================
    private DrawerLayout mDrawerLayout;
    //    private boolean isOnPauseMethodCalled = false;
    private int timeForSkipSameLocation = 20;
    //    private final RepeatTimerManager repeatTimerManagerUpdateLocation = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
//        @Override
//        public void onRepeatPerfrom() {
//            workerForTimer();
//        }
//
//        @Override
//        public void onStopRepeatPerfrom() {
//            cancelUserUpdateApiCall();
//        }
//    }, 6000);
    private Call<NearUserResponse> nearUserResponseCall;
    private long prevTime = 0;
    private Call<ResponseBody> callTripApi;
    private boolean isStopMethodCalled = false;
    private final RepeatTimerManager repeatTimerManager = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
        private long timeTest = -1;

        @Override
        public void onRepeatPerfrom() {
            long timeDiff = 0;
            if (timeTest > 0) {
                timeDiff = (System.currentTimeMillis() - timeTest);
            }
            timeTest = System.currentTimeMillis();
            //System.out.println("Nearby User : onRepeatPerfrom  : " + timeDiff);
            SlideMainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (myCurrentLocation != null) {
                        if (!Utils.isLatLngZero(myCurrentLocation)) {
                            getNearByUsers(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                        } else {
                            resetNearByUserSearch();
                        }
                    } else {
                        resetNearByUserSearch();
                    }
                }
            });
        }

        @Override
        public void onStopRepeatPerfrom() {
            timeTest = -1;
            //System.out.println("Nearby User : onStopRepeatPerfrom");
            cancelNearByUserApiCall();
        }
    }, 5000);
    private final RepeatTimerManager repeatTimerManagerTripStatus = new RepeatTimerManager(new RepeatTimerManager.RepeatTimerManagerCallBack() {
        @Override
        public void onRepeatPerfrom() {
            long diff = System.currentTimeMillis() - prevTime;
            prevTime = System.currentTimeMillis();
            Log.d(TAG, "onRepeatPerfrom " + diff);
            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel != null) {
                getTrip(tripModel);
            } else {
                Log.d(TAG, "TripModel Object is Null.");
            }
        }

        @Override
        public void onStopRepeatPerfrom() {
            Log.d(TAG, "onRepeatPerfrom");
            cancelPreviousCallingTripApi();
        }
    }, 10000);
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            tripRequestsRequest();


            String tripStatus = intent.getStringExtra(Config.EXTRA_TRIP_STATUS);
            if (tripStatus != null && !Objects.requireNonNull(tripStatus).equals("null")) {
                if (tripStatus.equalsIgnoreCase(Constants.TripStatus.Hide_Alert)) {
                    //hide the request time view and stop the notification sound
                    repeatTimerManagerTripStatus.stopRepeatCall();
                    controller.isRequestRejected = true;
//                    updateUIForRejectRequest();
                    controller.stopNotificationSound();
                    getTrip(tripModel, false);

                }
                playVoice(tripStatus);
            }
        }
    };
    private LocationListener locationListenerCurrent;
    private boolean isTripStatusUpdating = false;
    private boolean isCalling = false;
    /* @NonNull
     private DrawerLayout setupDrawerLayout() {
         drawerbut = findViewById(R.id.icdrawer);
         drawerbut.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View v) {
                 sideDrawerManager.openDrawer();
             }
         });
         DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
         RelativeLayout navrl = findViewById(R.id.rv);
         ListView mDrawerList = findViewById(R.id.list_slidermenu);
         sideDrawerManager = new SideDrawerManager(getApplicationContext(), mDrawerLayout, mDrawerList, navrl);
         sideDrawerManager.setSideDrawerManagerCallback(new SideDrawerManager.SideDrawerManagerCallback() {
             @Override
             public void onSideShowdialog(boolean isLogout, String message) {
                 showdialog(isLogout, message);
             }
         });
         return mDrawerLayout;
     }*/
    private RequestQueue requestQueue;
    /*private void addDraweToggleListener(final DrawerLayout mDrawerLayout) {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }*/
    private Timer timerForTripStatus;
    private Double distanceCoverInKm = 0.0;
    private final Runnable updateTimerThread = new Runnable() {

        public void run() {
            calculateTimeDis();
            customHandler.postDelayed(this, 1000);
        }

    };
    /**
     * handle state clicked trip button
     */

    private final OnClickListener fullButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (net_connection_check()) {
                TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                if (tripModel != null) {
                    if (tripModel.tripStatus.equalsIgnoreCase(ACCEPT)) {
                        controller.pref.setLocalTripState("Go");
                        fullbutton.setText(Localizer.getLocalizerString("k_20_s4_pick"));
                        updateTripStatusApi(ARRIVE, "");
                    } else if (tripModel.tripStatus.equalsIgnoreCase(ARRIVE)) {
                        if (controller.pref.getLocalTripState().equalsIgnoreCase("Go")) {
                            /*if (controller.isShowYesNowDialog()) {
                                isPickCaption();
                                clientPickedUpView.show();
                                pickupPopupVisible = true;
                            } else {
                                changeAfterClientPick();
                            }*/
                            isPickCaption();
                            clientPickedUpView.show();
                            pickupPopupVisible = true;
                        }
                    }
                }
            }
        }
    };
    private boolean isdialogShowing = false;
    private boolean isCancel = false;
    private final BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                repeatTimerManagerTripStatus.stopRepeatCall();
                showRiderDialog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    // **************************  Google Map  Function End **********************
    private boolean ishowingUpdateDailog = false;
    private boolean isMovedCameraToCurrentLocation = false;
    private AlertDialog gpsDialog;
    private float v;
    private ArrayList<Location> latLngPublishRelay = new ArrayList<>();
    /**
     * handle state clicked trip button
     */
    private final OnClickListener goButtonClickListener = new OnClickListener() {
        public void onClick(View v) {
            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel != null) {
                String status = tripModel.tripStatus;
                if (status.equalsIgnoreCase(ARRIVE)) {
                    if (mGoogleMap != null) {
                        if (ActivityCompat.checkSelfPermission(SlideMainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SlideMainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mGoogleMap.setMyLocationEnabled(false);
                    }

                    if (controller.checkOtpStart().equalsIgnoreCase("1") && tripModel.trip != null && !tripModel.trip.getIs_delivery().equalsIgnoreCase("1") && !tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
                        otpView.setText("");

                        otpLayout.setVisibility(View.VISIBLE);
                        otpView.requestFocus();
                    } else {
                        updateTripStatusApi(Constants.TripStatus.BEGIN, "");
                    }
                } else if (status.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                    if (controller.isShowYesNowDialog()) {
                        controller.pref.setIS_FARE_SUMMARY_OPEN("open_destination_view");
                        showDistanceAndTimeView();
                        //goButton.setVisibility(View.GONE);
                        //goButton.setText("");
                        //mGoogleMap.clear();

                        if (controller.checkOtpEnd().equalsIgnoreCase("1") && tripModel.trip != null &&
                                !tripModel.trip.getIs_delivery().equalsIgnoreCase("1") &&
                                !tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
                            otpLayout.setVisibility(View.VISIBLE);
                            otpView.setText("");
                            isEndable = true;
                        } else {

                            if (distanceAndTimeView != null)
                                distanceAndTimeView.hide();
                            if (destinationActivity != null)
                                destinationActivity.show();
                            destinationPopUpVisible = true;

                        }
                    } else {
                        if (destinationActivity != null)
                            destinationActivity.hide();
                        destinationPopUpVisible = false;

                        if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
                            showDeliveryListView();
                        } else {
                            if (controller.checkOtpEnd().equalsIgnoreCase("1") && !tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
                                otpLayout.setVisibility(View.VISIBLE);
                                otpView.setText("");

                                isEndableTwo = true;
                            } else {
                                isEndableTwo = true;
                                updateTripStatusApi(Constants.TripStatus.END, "");
                            }
                        }
                    }
                }
            }
        }

    };
    private boolean isChangingTrip = false;
    private String mTripId = "0";
    private boolean isAcceptingRSRTrip = false;

    public static void playVoice(String tripStatus) {
        Log.d(TAG, "playVoice: " + tripStatus);
        if (tripStatus != null)
//            if (tripStatus.equalsIgnoreCase(BEGIN)){
//                Controller.getInstance().playNotificationSound(R.raw.begin);
//
//            } else if (tripStatus.equalsIgnoreCase(END)){
//                Controller.getInstance().playNotificationSound(R.raw.ended_succ);
//
//            }else if (tripStatus.equalsIgnoreCase(ACCEPT)){
//                Controller.getInstance().playNotificationSound(R.raw.accepted);
//
//            }else if (tripStatus.equalsIgnoreCase(ARRIVE)){
//                Controller.getInstance().playNotificationSound(R.raw.arrive);
//
//            }else
            if (tripStatus.equalsIgnoreCase(CANCEL) || tripStatus.equals(CANCEL_RIDER)) {
                Controller.getInstance().stopNotificationSound();
                Controller.getInstance().playNotificationSound(R.raw.trip_cancelled);
//
//            }else if (tripStatus.equalsIgnoreCase("Cash")){
//                Controller.getInstance().playNotificationSound(R.raw.collect_cash);
//
//            }else if (tripStatus.equalsIgnoreCase("Paid")){
//                Controller.getInstance().playNotificationSound(R.raw.collect_cash);
//
//            }else if (tripStatus.equalsIgnoreCase("accept_payment_promo")){
//                Controller.getInstance().playNotificationSound(R.raw.promo_applied);

            }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide_activity_main);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        controller = (Controller) getApplicationContext();
        handleIntentForPushNotification();

        nodocumentalerttext = findViewById(R.id.tv_document_alert);
        layoutAccountStatus = findViewById(R.id.layoutAccountStatus);
        refreshAccountStatus = findViewById(R.id.refreshAccountStatus);

        getConstantApi(1);
        getCarCategoryApi();
        imgLocation = findViewById(R.id.imgMyLocation);
        deliveryList = new ArrayList<>();

        if (savedInstanceState != null) {
            v = savedInstanceState.getFloat("v", 0);
            latLngPublishRelay = savedInstanceState.getParcelableArrayList("latLngPublishRelay");
            if (latLngPublishRelay == null)
                latLngPublishRelay = new ArrayList<>();

            if (latLngPublishRelay.size() > 0) {
//                myLastCurrentLocation = setLocationData(latLngPublishRelay.get(0).latitude, latLngPublishRelay.get(0).longitude, 0);
                myLastCurrentLocation = latLngPublishRelay.get(0);
                if (latLngPublishRelay.size() > 1) {
//                    myCurrentLocation = setLocationData(latLngPublishRelay.get(1).latitude, latLngPublishRelay.get(1).longitude, 0);
                    myCurrentLocation = latLngPublishRelay.get(1);
                } else {
//                    myCurrentLocation = setLocationData(latLngPublishRelay.get(0).latitude, latLngPublishRelay.get(0).longitude, 0);
                    myCurrentLocation = latLngPublishRelay.get(0);
                }
            } else {
                myLastCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
                myCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
            }
        } else {
            myLastCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
            myCurrentLocation = setLocationData(Double.parseDouble(controller.pref.getDriver_Lat()), Double.parseDouble(controller.pref.getDriver_Lng()), Float.parseFloat(controller.pref.getDriver_Bearing()));
        }
        otpLayout = findViewById(R.id.otp_layout);
        applyOtp = findViewById(R.id.otp_apply);
        otpView = findViewById(R.id.otp_et_promocode);
        toolbar = null;//findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        navrl = findViewById(R.id.rv);

        riderInfoLayout = findViewById(R.id.riderInfoLayout);
        callRider = findViewById(R.id.callRider);
        riderName = findViewById(R.id.riderName);
        passengerDetails = findViewById(R.id.passenger_details);
        passengerDetails.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
        riderProfile = findViewById(R.id.riderProfile);
        // ======TEST===========
        rvRequest = findViewById(R.id.rv_request);
//        riderSharingLayout = findViewById(R.id.riderSharingLayout);
        rvRequest.setVisibility(View.VISIBLE);
//        sheetBehavior = BottomSheetBehavior.from(riderSharingLayout);

        rideSharingFragment = RideSharingFragment.newInstance();
        //        riderSharingLayout.setVisibility(View.GONE);
        btn_change = findViewById(R.id.btn_change);
        btn_toggle_rs = findViewById(R.id.btn_toggle_rs);
        topRelativeLayout = findViewById(R.id.top_relative_layout);
        about = findViewById(R.id.about);
        privacy = findViewById(R.id.privacy);

        btn_toggle_rs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!rideSharingFragment.isVisible())
                    rideSharingFragment.show(getSupportFragmentManager(), "RideSharing");
                else
                    rideSharingFragment.dismiss();
            }
        });
        TextView logout = findViewById(R.id.logout);
        ImageView edit_profiles = findViewById(R.id.edit_profiles);

        logout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialog(true, Localizer.getLocalizerString("k_54_s4_do_you_want_to_exit_now"));
            }
        });

        edit_profiles.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SlideMainActivity.this, DriverProfileActivity.class));
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        view = findViewById(R.id.fragment);
        tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

        if (tripModel != null && tripModel.trip != null)
            mTripId = tripModel.trip.getM_trip_id();

        resizeFragment(rvRequest, width, height / 2);

        FragmentManager fragMan = getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragMan.beginTransaction();

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SlideMainActivity.this, AboutUsActivity.class));

            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SlideMainActivity.this, PrivacyPolicy.class));

            }
        });

        if (myFrag == null) {
            //myFrag = TripRequestFragment.getInstance();
            myFrag = new TripRequestFragment();

          /*  ViewPager viewPager = view.findViewById(R.id.viewpager);
            tab = view.findViewById(R.id.tabs);
            setupViewPager(viewPager);*/
            myReqFrag = new RequestFragment();
            fragTransaction.add(R.id.fragment, myFrag);
            fragTransaction.commit();
 /*   fragTransaction1.add(R.id.fragment, myReqFrag);
    fragTransaction1.commit();*/

        }

        btn_change.setOnClickListener(listener);

        //=======END================

        progressDialog = new CustomProgressDialog(SlideMainActivity.this);
        controller.setFareReviewCalled("notcalled");
        goButton = findViewById(R.id.fullbutton2);
        fullbutton = findViewById(R.id.fullbutton);
        tvSwitchText = findViewById(R.id.tv_availiabiliyt_text_id);
        linearBackNext = findViewById(R.id.linear_back_next_layout_id);
        imageV = findViewById(R.id.iii);
        waitinTimeView = findViewById(R.id.waitingTimeView);
        waitingView = findViewById(R.id.waiting);
        addressLocationView = new AddressLocationView(findViewById(R.id.location_address_layout_id), drawerListTypeface);
        addressLocationView.hide();
        imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LatLng latLng = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                    mGoogleMap.animateCamera(cameraUpdate);
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    LatLngBounds bounds;
                    bounds = builder.build();

                    // define value for padding
                    int padding = 20;
                    //This cameraupdate will zoom the map to a level where both location visible on map and also set the padding on four side.
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    mGoogleMap.moveCamera(cu);
                } catch (Exception ignore) {

                }

            }
        });
        ProfileDetails();
        checkAndroidVersionAndAskPermission();
        linearBackNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                destinationActivity.hide();
                linearBackNext.setVisibility(View.GONE);
            }
        });
        destinationActivity = new DestinationView(this, findViewById(R.id.endtrip_request));
        destinationActivity.hide();
        destinationActivity.setDestinationActivityCallBack(getDestinationCallBack());
        LocalBroadcastManager.getInstance(SlideMainActivity.this).registerReceiver(mNotificationReceiver, new IntentFilter("some_custom_id"));
        clientPickedUpView = new ClientPickedUpView(SlideMainActivity.this, findViewById(R.id.client_pickup_view), isDelivery);
        clientPickedUpView.hide();
        clientPickedUpView.setClientPickedUpCallback(getClientPickedUpCallBack());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        full = findViewById(R.id.full);
        ImageView marker = findViewById(R.id.marker);

//        checkDocumentUploaded();

        marker.setEnabled(false);
        marker.setVisibility(View.INVISIBLE);
        marker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.clearAnimation();
            }
        });
        fullbutton.setOnClickListener(fullButtonClickListener);
        goButton.setOnClickListener(goButtonClickListener);
        setupGoogleClient();
        fn_permission();
        registerReceiver(mHandleMessageReceiver, new IntentFilter(Config.DISPLAY_MESSAGE_ACTION));
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        locationReceiver = new LocationReceiver();
//        pickUpLocationReceiver = new PickUpLocationReceiver();
        startDistanceServiceWithDistance();
        Log.d("refreshedDevice", "" + Controller.getSaveDiceToken());
        callRider.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tripModel != null && tripModel.user != null) {
                    String phone_no = tripModel.user.getU_phone();
                    if (tripModel.user.getC_code() != null && !tripModel.user.getC_code().equalsIgnoreCase("null") && !tripModel.user.getC_code().trim().isEmpty()) {
                        phone_no = "+" + tripModel.user.getC_code() + phone_no;
                    }

                    if (phone_no == null) {
                        Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_43_s4_no_number"), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + phone_no));
                        if (callIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(callIntent);
                        }
                    }
                }
            }
        });

        passengerDetails.setOnClickListener(v -> {
            final Dialog dialogPassengerDetails = new Dialog(this);
            dialogPassengerDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogPassengerDetails.setCancelable(true);
            dialogPassengerDetails.setContentView(R.layout.someone_else_dialog);
            TextView passengerTitle = dialogPassengerDetails.findViewById(R.id.passengerTitle);
            passengerTitle.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
            BTextView passengerName = dialogPassengerDetails.findViewById(R.id.passengerName);
            BTextView passengerPhone = dialogPassengerDetails.findViewById(R.id.passengerMobile);
            String details = tripModel.trip.getTrip_customer_details();
            try {
                JSONObject jsonObject = new JSONObject(details);
                if (jsonObject.has("p_name"))
                    passengerName.setText(jsonObject.getString("p_name"));
                if (jsonObject.has("p_phone"))
                    passengerPhone.setText(jsonObject.getString("p_phone"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            BTextView passengerNameTv = dialogPassengerDetails.findViewById(R.id.passengerNameTv);
            passengerNameTv.setText(Localizer.getLocalizerString("k_s3_passenger_name"));
            BTextView passengerMobileTv = dialogPassengerDetails.findViewById(R.id.passengerMobileTv);
            passengerMobileTv.setText(Localizer.getLocalizerString("k_s3_passenger_phone"));
            ImageView closeButtonDialog = dialogPassengerDetails.findViewById(R.id.close_button);
            closeButtonDialog.setOnClickListener(v1 -> {
                dialogPassengerDetails.dismiss();
            });

            dialogPassengerDetails.show();

        });

        applyOtp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpView.getText().toString().length() != 0) {
                    Log.e("tripModelotrp", "" + tripModel.getOtp());
                    if (otpView.getText().toString().equalsIgnoreCase(tripModel.getOtp())) {
                        otpLayout.setVisibility(View.GONE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        if (isEndable) {
                            distanceAndTimeView.hide();
                            destinationActivity.show();
                            destinationPopUpVisible = true;

                        } else if (isEndableTwo) {
                            updateTripStatusApi(Constants.TripStatus.END, "");
                        } else {
                            updateTripStatusApi(Constants.TripStatus.BEGIN, "");
                        }
                    } else {
                        Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_44_s4_plz_enter_valid_otp"), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_45_s4_plz_enter_otp_ask_frm_driver"), Toast.LENGTH_SHORT).show();
                }

            }
        });
        setUpSideDrawer();

        isWaiting = controller.pref.getIsWaiting();
        waitingView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int waitingTime = controller.pref.getWaitingTime();
                waitinTimeView.setText(Localizer.getLocalizerString("k_16_s4_waiting_time") + waitingTime + Localizer.getLocalizerString("k_17_s4_min"));
                waitinTimeView.setVisibility(View.VISIBLE);
                if (controller.pref.getIsWaiting()) {
                    waitingView.setText(Localizer.getLocalizerString("k_35_s4_start_waiting"));
                    controller.pref.setIsWaiting(false);

                    int waitingTimeTemp = 0;
                    if (controller.pref.getWaitingStartTime() > 0) {
                        waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
                    }
                    waitingTime = waitingTime + waitingTimeTemp;
                    controller.pref.setWaitingTime(waitingTime);
                    Log.e("waitnig", "" + waitingTime);
                    controller.pref.setWaitingStartTime(0);
                    waitinTimeView.setText(Localizer.getLocalizerString("k_16_s4_waiting_time") + waitingTime + Localizer.getLocalizerString("k_17_s4_min"));
                    waitinTimeView.setVisibility(View.GONE);

                    if (waitingHandler != null) {
                        try {
                            waitingHandler.removeCallbacks(waitingTimerThread);

                        } catch (Exception ignore) {

                        }
                    }
                } else {
                    controller.pref.setWaitingStartTime(Calendar.getInstance().getTimeInMillis());
                    controller.pref.setIsWaiting(true);
                    waitingView.setText(Localizer.getLocalizerString("k_36_s4_stop_waiting"));

                    if (waitingHandler != null) {
                        try {
                            waitingHandler.postDelayed(waitingTimerThread, 30 * 1000);
                        } catch (Exception ignore) {
                        }
                    }
                }
            }
        });
        try {
            tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel != null) {
                if (tripModel.trip != null)
                    mTripId = tripModel.trip.getM_trip_id();
                if (tripModel.tripStatus.equalsIgnoreCase(REQUEST)) {
                    onTripCancel(tripModel);
                } else if (!tripModel.tripStatus.equals(Constants.TripStatus.BEGIN)) {
                    clearBeginTripDistanceAndTime();
                } else {
                    isAlready = true;

                    if (tripModel.trip != null && tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
                        this.coverLatLongsD.addAll(TrackRouteSaveData.getInstance(getApplicationContext()).getAllLatLngsD());
                        drawCoverRoutePolyLineOnMapD();
                    } else {
                        this.coverLatLongsD.addAll(TrackRouteSaveData.getInstance(getApplicationContext()).getAllLatLngsD());
                        Log.d(TAG, "onCreate: " + coverLatLongsD.size());
                        for (LatLng ltln : coverLatLongsD) {
                            Log.d(TAG, "onCreate: first: " + ltln.toString());
                        }
                        if (coverLatLongsD.size() > 0) {
                            Log.d(TAG, "onCreate: first: " + coverLatLongsD.get(0).toString());
                            Log.d(TAG, "onCreate: last: " + coverLatLongsD.get(coverLatLongsD.size() - 1).toString());
                        }
                        drawCoverRoutePolyLineOnMapD();
                    }
                }
            } else {
                getPendingTrip();
            }
        } catch (Exception e) {
            AppData.getInstance(getApplicationContext()).clearTrip();
            clearBeginTripDistanceAndTime();
            this.tripModel = null;
        }
        setLocalizeData();
        reLogin();

//        Toast.makeText(this, controller.getLoggedDriver().getText_password(), Toast.LENGTH_LONG).show();

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        Log.d(TAG, "fcmToken: " + token);
                    }
                });

        getMTrips();
    }

    private void getPendingTrip() {

        Map<String, String> params = new HashMap<>();
        params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey());
        params.put(Constants.Keys.DRIVER_ID, controller.getLoggedDriver().getDriverId());
        params.put(Constants.Keys.LIMIT, "1");

        WebServiceUtil.excuteRequest(this, params, DRIVER_PENDING_TRIPS, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    try {
                        JSONObject jsonRootObject = new JSONObject(data.toString());
                        if (jsonRootObject.get(Constants.Keys.RESPONSE) instanceof JSONArray) {
                            JSONArray tripsArray = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
                            ArrayList<TripModel> tripModels = new ArrayList<>();
                            for (int i = 0; i < tripsArray.length(); i++) {
                                JSONObject childObject = tripsArray.getJSONObject(i);
                                TripModel tripModel = TripModel.parseJson(childObject.toString());
                                Log.e("tripId", "" + tripModel.trip.getTrip_id());
                                tripModels.add(tripModel);
                            }

                            if (tripModels.size() > 0) {
                                TripModel tripModel1 = tripModels.get(0);
                                if (tripModel1 != null && tripModel1.trip != null) {

                                    rvRequest.setVisibility(View.GONE);
                                    tripModel1.tripId = tripModel1.trip.getTrip_id();
                                    tripModel1.tripStatus = tripModel1.trip.getTrip_status();
                                    tripModel1.otp = tripModel1.trip.getOtp();
                                    tripModel = tripModel1;
                                    if (tripModel.trip != null)
                                        mTripId = tripModel.trip.getM_trip_id();
                                    AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
                                    controller.pref.setTripIds(tripModel.tripId);
                                    controller.pref.setTripId(tripModel.tripId);
                                    controller.pref.setLocalTripState(tripModel.tripStatus);
                                    controller.pref.setTripStartTime(AppUtil.getCurrentDateInGMTZeroInServerFormat());

                                    Intent intent = new Intent(SlideMainActivity.this, SlideMainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void setLocalizeData() {
        //For Slide Menu
        BTextView about, privacy, logout, tv_availiabiliyt_text_id, tv_document_alert, tvSlideMainDrawerTripRequest;
        BEditText otp_et_promocode;
        BButton otp_apply;
        TextView tv_back_to_orders;

        about = findViewById(R.id.about);
        privacy = findViewById(R.id.privacy);
        logout = findViewById(R.id.logout);
        tv_availiabiliyt_text_id = findViewById(R.id.tv_availiabiliyt_text_id);
        otp_et_promocode = findViewById(R.id.otp_et_promocode);
        otp_apply = findViewById(R.id.otp_apply);
        tv_back_to_orders = findViewById(R.id.tv_back_to_orders);
        tv_document_alert = findViewById(R.id.tv_document_alert);
        tvSlideMainDrawerTripRequest = findViewById(R.id.textView14);
        //offlineNotiText = findViewById(R.id.offlineNotiText);

        //Setting texts in TextView
        about.setText(Localizer.getLocalizerString("k_1_s4_about"));
        privacy.setText(Localizer.getLocalizerString("k_2_s4_privacy"));
        logout.setText(Localizer.getLocalizerString("k_3_s4_logout"));
        tv_availiabiliyt_text_id.setText(Localizer.getLocalizerString("k_4_s4_availability_on"));
        tv_back_to_orders.setText(Localizer.getLocalizerString("k_27_s4_back_to_orders"));
        tv_document_alert.setText(Localizer.getLocalizerString("k_28_s4_doc_nt_uploaded"));
        tvSlideMainDrawerTripRequest.setText(Localizer.getLocalizerString("k_29_s4_trip_requests"));
        //offlineNotiText.setText(Localizer.getLocalizerString("k_1_s14_you_are_offline") + "\n" + Localizer.getLocalizerString("k_2_s14_go_online_accept_ride"));
        //Setting hints in EditText
        otp_et_promocode.setHint(Localizer.getLocalizerString("k_18_s4_plz_enter_otp"));

        //Setting text in Button
        otp_apply.setText(Localizer.getLocalizerString("k_18_s4_otp_apply"));


        //For account hold alert (Driver Verification)
        BTextView accountOnHoldAlertText, refreshAccountStatus;
        accountOnHoldAlertText = findViewById(R.id.accountOnHoldAlertText);
        refreshAccountStatus = findViewById(R.id.refreshAccountStatus);
        accountOnHoldAlertText.setText(Localizer.getLocalizerString("k_1_s5_driver_verification"));
        refreshAccountStatus.setText(Localizer.getLocalizerString("k_1_s5_refresh_status"));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putFloat("v", v);
        outState.putParcelableArrayList("latLngPublishRelay", latLngPublishRelay);

    }

    @SuppressLint("ResourceType")
    private void setUpSideDrawer() {

        mDrawerLayout.openDrawer(navrl);
        mDrawerLayout.closeDrawer(navrl);
        String[] navMenuTitles = getResources().getStringArray(R.array.profile_list);
        int[] navMenuIds = getResources().getIntArray(R.array.profile_list_id);
        TypedArray navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();
///navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_6_s4_a1_your_rides"), navMenuIcons.getResourceId(1, -1), navMenuIds[1]));
//navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_9_s4_a1_share"), navMenuIcons.getResourceId(4, -1), navMenuIds[4]));
//navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_11_s4_a1_contact_us"), navMenuIcons.getResourceId(6, -1), navMenuIds[6]));
//        navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_12_s4_a1_language"), navMenuIcons.getResourceId(7, -1), navMenuIds[7]));
        navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_15_s4_a1_notifications"), navMenuIcons.getResourceId(9, -1), navMenuIds[10]));
        //navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_1_s13_my_earnings"), navMenuIcons.getResourceId(10, -1), navMenuIds[11]));
        /*if (controller.checkWallet().equalsIgnoreCase("1")) {
            navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_1_s10_wallet"), navMenuIcons.getResourceId(8, -1), navMenuIds[8]));
        }*/


//TODO uncomment
        /*if (controller.checkSingleMode().equalsIgnoreCase("1")) {
            navDrawerItems.add(new NavDrawerItem(Localizer.getLocalizerString("k_14_s4_a1_single_mode"), -1, navMenuIds[9]));
        }
*/
        navMenuIcons.recycle();
        // setting the nav drawer list adapter
        ListView mDrawerList = findViewById(R.id.list_slidermenu);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        navDrawerListAdapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(navDrawerListAdapter);
    }

    private void displayView(int position) {

        if (navDrawerListAdapter != null) {
            switch (navDrawerListAdapter.getNavId(position)) {
                /*case 0: {
                mDrawerLayout.closeDrawer(navrl);
                Intent profile = new Intent(context, DriverProfileActivity.class);
                profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(profile);
                break;
            }
*/
                case 2: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent history = new Intent(SlideMainActivity.this, TripHistoryListActivity.class);
                    history.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(history);
                    break;
                }

           /* case 1: {
                if (Utils.net_connection_check(context)) {
                    sideDrawerManagerCallback.onSideShowdialog(true, context.getResources().getString(R.string.do_you_want_to_exit_now));
                }
                break;
            }*/

                case 5: {
                    if (Utils.net_connection_check(SlideMainActivity.this)) {
                        Share();
                    }
                    break;
                }
            /*case 4: {
                if (Utils.net_connection_check(context)) {
                    sideDrawerManagerCallback.onSideShowdialog(false, context.getResources().getString(R.string.do_you_want_to_deactivate_now));
                    break;
                }
            }*/
                case 7: {
                    if (Utils.net_connection_check(SlideMainActivity.this)) {
//                String[] email = {Constants.Urls.EMAIL_FOR_SUPPORT};
                        shareToGMail();

                    }

                    break;
                }
                case 8: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent brain = new Intent(SlideMainActivity.this, LanguageSelectionActivity.class);
                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(brain);
                    break;

                }
                case 9: {
                    mDrawerLayout.closeDrawer(navrl);
                    Intent brain = new Intent(SlideMainActivity.this, WalletActivity.class);
                    brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(brain);
                    break;
                }
                case 10: {
                    mDrawerLayout.closeDrawer(navrl);
                    if (tripModel != null && !controller.pref.getIsSingleMode()) {
                        Toast.makeText(controller, Localizer.getLocalizerString("k_46_s4_already_in_trip"), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent brain = new Intent(SlideMainActivity.this, SingleModeActivity.class);
                        brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(brain);
                    }
                    break;
                }
                case 11: {
                    mDrawerLayout.closeDrawer(navrl);

                    Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;
                }
            }
        }
    }

    //    private void displayView(int position) {
//        switch (position) {
//            /*case 0: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent profile = new Intent(context, DriverProfileActivity.class);
//                profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(profile);
//                break;
//            }
//*/
//            case 0: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent history = new Intent(SlideMainActivity.this, TripHistoryListActivity.class);
//                history.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(history);
//                break;
//            }
//
//           /* case 1: {
//                if (Utils.net_connection_check(context)) {
//                    sideDrawerManagerCallback.onSideShowdialog(true, context.getResources().getString(R.string.do_you_want_to_exit_now));
//                }
//                break;
//            }*/
//
//            case 1: {
//                if (Utils.net_connection_check(SlideMainActivity.this)) {
//                    Share();
//                }
//                break;
//            }
//            /*case 4: {
//                if (Utils.net_connection_check(context)) {
//                    sideDrawerManagerCallback.onSideShowdialog(false, context.getResources().getString(R.string.do_you_want_to_deactivate_now));
//                    break;
//                }
//            }*/
//            case 2: {
//                if (Utils.net_connection_check(SlideMainActivity.this)) {
//                    shareToGMail();
//                }
//                break;
//            }
//            case 3: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent brain = new Intent(SlideMainActivity.this, WalletActivity.class);
//                brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(brain);
//                break;
//            }
//            case 4: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent brain = new Intent(SlideMainActivity.this, SingleModeActivity.class);
//                brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(brain);
//                break;
//            }
//            case 5: {
//                mDrawerLayout.closeDrawer(navrl);
//                Intent brain = new Intent(SlideMainActivity.this, LanguageSelectionActivity.class);
//                brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(brain);
//                break;
//            }
//        }
//    }
    private void Share() {
        String shareText = controller.getShareText();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void shareToGMail() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{controller.getSupportEmailValue()});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, controller.getSupportEmail());
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        final PackageManager pm = getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(emailIntent);
    }

    private void addDraweToggleListener(final DrawerLayout mDrawerLayout) {
        mDrawerToggle = new androidx.appcompat.app.ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @NonNull
    private DrawerLayout setupDrawerLayout() {
        final DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        final RelativeLayout navrl = findViewById(R.id.rv);

        drawerbut = findViewById(R.id.icdrawer);
        drawerbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(navrl);
            }
        });
        ListView mDrawerList = findViewById(R.id.list_slidermenu);
        /*sideDrawerManager = new SideDrawerManager(getApplicationContext(), mDrawerLayout, mDrawerList, navrl);
        sideDrawerManager.setSideDrawerManagerCallback(new SideDrawerManager.SideDrawerManagerCallback() {
            @Override
            public void onSideShowdialog(boolean isLogout, String message) {
                showdialog(isLogout, message);
            }
        });*/
        return mDrawerLayout;
    }

    private void resizeFragment(View f, int newWidth, int newHeight) {
        if (f != null) {

            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(newWidth, newHeight);
            p.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            rvRequest.setLayoutParams(p);
            rvRequest.requestLayout();
        }
    }

    // ==============   END   ===================
//    private void reLogin() {
//        if (controller.getLoggedDriver() != null && controller.getCities() != null && controller.getCities().size() > 0 && net_connection_check()) {
//            HashMap<String, String> params = new HashMap<>();
//            String loginUrl = "";
//            if (USE_EMAIL_AUTH) {
//
//                com.ridertechrider.driver.app.Request.Login login = new com.ridertechrider.driver.app.Request.Login();
//                params = login.addEmail(controller.getLoggedDriver().getD_email())
//                        .addPassword(controller.getLoggedDriver().getText_password())
//                        .addIsAvailable(String.valueOf(1))
//                        .build();
//                loginUrl = URL_DRIVER_LOGIN;
//            } else {
//                params.put("username", controller.getLoggedDriver().getD_phone());
//                params.put("d_password", controller.getLoggedDriver().getText_password());
////                params.put("city_id", controller.getLoggedDriver().getCity_id());
////                params.put("c_code", controller.getCityCountryCode(controller.getLoggedDriver().getCity_id()));
//                params.put("c_code", controller.getLoggedDriver().getC_code());
//                loginUrl = URL_DRIVER_PHONE_SIGN_IN;
//            }
//            progressDialog.showDialog();
//            Log.e("loginPara,s", "" + params);
//            WebServiceUtil.excuteRequest(getApplication(), params, loginUrl, new WebServiceUtil.DeviceTokenServiceListener() {
//                @Override
//                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                    if (isUpdate) {
//                        String response = data.toString();
//                        ErrorJsonParsing parser = new ErrorJsonParsing();
//                        CloudResponse res = parser.getCloudResponse("" + response);
//                        if (res.isStatus()) {
//                            final Driver driver = Driver.parseJsonAfterLogin(response);
//                            controller.setLoggedDriver(driver);
//                            controller.pref.setPASSWORD(controller.getLoggedDriver().getText_password());
//                            if (Objects.requireNonNull(driver).getD_is_verified()) {
//                                driverTokenUpateProfile(driver.getD_is_available(), true);
//                            } else {
//                                builder = new AlertDialog.Builder(SlideMainActivity.this, R.style.Dialog);
//                                builder.setMessage(Localizer.getLocalizerString("k_55_s4_loginMessage"));
//                                builder.setCancelable(false);
//                                builder.setPositiveButton(Localizer.getLocalizerString("k_r8_s8_ok"), new DialogInterface.OnClickListener() {
//
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        progressDialog.dismiss();
//                                        dialog.cancel();
//                                        driverTokenUpateProfile("0", true);
//                                    }
//                                });
//                                if (!isFinishing()) {
//                                    builder.show();
//                                }
//                            }
//
//
//                        } else {
//                            progressDialog.dismiss();
//                            Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
//                        }
//                    } else {
//                        progressDialog.dismiss();
//                        logoutApi(controller);
//                    }
//                }
//            });
//        }
//    }

    private void reLogin() {
        showProgressBar();
        controller.reSignIn((data, isUpdate, error) -> {
            hideProgressBar();
            if (isUpdate) {
                String response = data.toString();
                ErrorJsonParsing parser = new ErrorJsonParsing();
                CloudResponse res = parser.getCloudResponse("" + response);
                if (res.isStatus()) {
                    final Driver driver = Driver.parseJsonAfterLogin(response);
                    if (driver != null) {
                        controller.setLoggedDriver(driver);
                        controller.pref.setPASSWORD(controller.pref.getPASSWORD());
                        if (driver.getD_is_verified()) {
                            driverTokenUpateProfile(driver.getD_is_available(), true);
                        } else {
                            driverTokenUpateProfile("0", true);
                        }
                    }

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                }
            } else {
                progressDialog.dismiss();
//                        logoutApi(controller);
            }
        });

    }

    private void driverTokenUpateProfile() {
        com.ridertechrider.driver.app.Request.UpdateProfile profile = new com.ridertechrider.driver.app.Request.UpdateProfile();
        String token = Controller.getSaveDiceToken();
        profile.addDviceType();
        if (token != null) {
            profile.addDviceToken(token);
        }
        Map<String, String> params = profile.build();
        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, profile.build(), Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

            }
        });
    }

    private void driverTokenUpateProfile(String available, boolean updateAvailability) {
        com.ridertechrider.driver.app.Request.UpdateProfile profile = new com.ridertechrider.driver.app.Request.UpdateProfile();
        String token = Controller.getSaveDiceToken();
        //profile.addIsAvailable(String.valueOf(1)).addDviceType();
        String trip_response = Controller.getInstance().pref.getString("trip_response");
        if (updateAvailability)
            profile.addIsAvailable(available);
        profile.addDviceType();
        if (token != null) {
            profile.addDviceToken(token);
        }


        Map<String, String> params = profile.build();
        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, profile.build(), Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    progressDialog.dismiss();
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        controller.pref.setIsLogin(true);
                        controller.setLoggedDriver(Driver.parseJsonAfterLogin(response));
                        checkIsVerified();
                        checkOfflineStatus();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void drawCoverRoutePolyLineOnMapD() {
        clearCoveredRouteD();
//        for (int i = 0; i < coverLatLongsD.size(); i++) {
//            if (coverLatLongsD.get(i).longitude == 0.0) {
//                coverLatLongsD.remove(i);
//            }
//            Log.e("lkatlang", "" + coverLatLongsD.get(i).latitude + "," + coverLatLongsD.get(i).longitude);
//        }
        PolylineOptions polylineOptionses = new PolylineOptions();
        polylineOptionses.addAll(coverLatLongsD);
        polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
        polylineOptionses.color(getResources().getColor(R.color.black_dark));
        polylineOptionses.geodesic(true);
        polylineOptionses.zIndex(4);
        if (mGoogleMap != null) {
            coverRoutePolyLineD.add(mGoogleMap.addPolyline(polylineOptionses));
//            animateCarOnMap(myLastCurrentLocation, myCurrentLocation);

            if (coverLatLongsD.size() > 0) {
                LatLng latLngFirst = coverLatLongsD.get(0);
                Location location = new Location("");
                location.setLatitude(latLngFirst.latitude);
                location.setLongitude(latLngFirst.longitude);
                //  addMarkerOnPickupLocation(location);


                LatLng latlng = coverLatLongsD.get(coverLatLongsD.size() - 1);

                Log.d(TAG, "drawCoverRoutePolyLineOnMapD: animateCarOnMap");
                if (trackStartMarker != null) {
                    trackStartMarker.setPosition(latLngFirst);
                } else {
                    trackStartMarker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(latLngFirst)
                            .title("")
                            .snippet("")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_20)));

                }
            }
        }
    }

    private void animateMarkerNew(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());
            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(500); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());

            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        int zoomValue = 16;
                        // zoomValue = 19;
                        if (isZoomEnabled) {
                            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                    .target(newPosition)
                                    .zoom(zoomValue)
                                    .build()));
                        }
                        marker.setAnchor((float) 0.5, (float) 0.5);
                        //marker.setRotation(angle);
                        //marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            valueAnimator.start();
        }
    }

//    private void drawCoverRoutePolyLineOnMap() {
//        PolylineOptions polylineOptionses = new PolylineOptions();
//        polylineOptionses.addAll(coverLatLongs);
//        polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
//        polylineOptionses.color(Color.GRAY);
//        polylineOptionses.geodesic(true);
//        polylineOptionses.zIndex(4);
//        if (mGoogleMap != null) {
//            coverRoutePolyLine.add(mGoogleMap.addPolyline(polylineOptionses));
//            if (coverLatLongs.size() > 0) {
//                LatLng latLngFirst = coverLatLongs.get(0);
//            }
//        }
//    }

//    private void clearCoveredRoute() {
//        if (coverRoutePolyLine == null) {
//            coverRoutePolyLine = new ArrayList<>();
//        }
//        for (Polyline polyline : coverRoutePolyLine) {
//            polyline.remove();
//        }
//        coverRoutePolyLine.clear();
//    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void setupGoogleClient() {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are
            int requestCode = 10;
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, status, requestCode);
            dialog.show();
        } else { // Google Play Services are available
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(GData.UPDATE_INTERVAL_IN_MILLISECONDS);
            // Use high accuracy
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // Set the interval ceiling to one minute
            mLocationRequest.setFastestInterval(GData.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
            // Note that location updates are off until the user turns them on
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            googleApiClient.connect();
        }
    }

    private void getDelNum() {
        if (deliveryList != null && deliveryList.size() != 0) {
            // Collections.sort(deliveryList, new DeliveryComparator());

            for (int i = 0; i < deliveryList.size(); i++) {
                if (deliveryList.get(i).getDeliveryStatus().equals("Created")) {
                    numDel = i;
                    //Log.("delNum1", "" + numDel);
                    break;
                }
            }
        }
    }

//    private void workerForTimer() {
//        if (ActivityCompat.checkSelfPermission(SlideMainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SlideMainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        try {
//            if (myCurrentLocation == null) {
//                FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//                mFusedLocationClient.getLastLocation()
//                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
//                            @Override
//                            public void onSuccess(Location location) {
//                                // Got last known location. In some rare situations, this can be null.
//                                if (location != null) {
//                                    myCurrentLocation = location;
//                                }
//                            }
//                        });
//
//            }
//
//            if (myCurrentLocation == null) {
//                myCurrentLocation = new Location("Driver");
//                if (lastKnownLng != 0.0 && lastKnownLat != 0.0) {
//                    myCurrentLocation.setLatitude(lastKnownLat);
//                    myCurrentLocation.setLongitude(lastKnownLng);
//                }
//            } else {
//                lastKnownLat = myCurrentLocation.getLatitude();
//                lastKnownLng = myCurrentLocation.getLongitude();
//                controller.getLoggedDriver().setD_lat(String.valueOf(myCurrentLocation.getLatitude()));
//                controller.getLoggedDriver().setD_lng(String.valueOf(myCurrentLocation.getLongitude()));
//            }
//            if (Utils.isLatLngZero(myCurrentLocation)) {
//                if (!isOnPauseMethodCalled) {
//                    repeatTimerManagerUpdateLocation.setTimerForRepeat();
//                }
//            } else {
//                if (isCanMakeApiCall(myCurrentLocation)) {
//                    Log.e("yes can make api call", "yes it can make api call");
//                    if (myCurrentLocation.getLongitude() != 0.0 && myCurrentLocation.getLatitude() != 0.0) {
//                        updateDriverProfileApi(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
//                    }
//                    preLocation = myCurrentLocation;
//                } else {
//                    if (!isOnPauseMethodCalled) {
//                        repeatTimerManagerUpdateLocation.setTimerForRepeat();
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            if (!isOnPauseMethodCalled) {
//                repeatTimerManagerUpdateLocation.setTimerForRepeat();
//            }
//        }
//    }

//    private boolean isCanMakeApiCall(Location myCurrentLocation) {
//        if (preLocation == null) {
//            return true;
//        }
//        if (preLocation.distanceTo(myCurrentLocation) <= 5) {
//            timeForSkipSameLocation--;
//            if (timeForSkipSameLocation < 0) {
//                timeForSkipSameLocation = 0;
//                timeForSkipSameLocation = 10;
//                return true;
//            }
//            return false;
//        } else {
//            timeForSkipSameLocation = 10;
//            return true;
//        }
//    }

    private void makeRouteAndRedrwaRouteD(Location locationNear) {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            if (tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
                clearCoveredRouteD();
                LatLng latLngNear = new LatLng(locationNear.getLatitude(), locationNear.getLongitude());
//                coverLatLongs.add(latLngNear);
//                TrackRouteSaveData.getInstance(this).addLatLongD(latLngNear);
//                TrackRouteSaveData.getInstance(getApplicationContext()).addLatLongD(latLngNear);
//                TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
                //drawCoverRoutePolyLineOnMap();uncomment
            } else {
//                coverLatLongs.clear();
            }
        } else {
//            coverLatLongs.clear();
        }
    }

//    private void updateDriverProfileApi(final double lat, final double lng) {
//        Log.e("updatelatlong", "" + lat + "," + lng);
//        Log.e("controllerdriver", "" + controller.getLoggedDriver().getDriverId());
//        cancelUserUpdateApiCall();
//        controller.pref.setDriver_Lat(String.valueOf(lat));
//        controller.pref.setDriver_Lng(String.valueOf(lng));
//        controller.getLoggedDriver().setD_lat(Double.toString(lat));
//        controller.getLoggedDriver().setD_lng(Double.toString(lng));
//        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
//        updateDriverLocation = apiInterface.updateDriverLocation(controller.getLoggedDriver().getApiKey(), controller.getLoggedDriver().getDriverId(), Double.toString(lat), Double.toString(lng));
//        updateDriverLocation.enqueue(new Callback<LoginResponse>() {
//            @Override
//            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
//
//                if (!isOnPauseMethodCalled) {
//                    repeatTimerManagerUpdateLocation.setTimerForRepeat();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LoginResponse> call, Throwable t) {
//                if (!call.isCanceled()) {
//                    if (!isOnPauseMethodCalled) {
//                        repeatTimerManagerUpdateLocation.setTimerForRepeat();
//                    }
//                }
//            }
//        });
//    }

    private void handleIntentForPushNotification() {
        Intent intent = getIntent();
        if (intent != null) {
            String noTiTripSatus = intent.getStringExtra(Config.EXTRA_TRIP_STATUS);
            if (noTiTripSatus != null) {
                TripModel tripModel = new TripModel();
                tripModel.tripId = intent.getStringExtra(Config.EXTRA_TRIP_ID);
                tripModel.tripStatus = intent.getStringExtra(Config.EXTRA_TRIP_STATUS);
                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
            }
        }
    }

    private void checkAndroidVersionAndAskPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
            List<String> permissions = new ArrayList<>();
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.SEND_SMS);
            }
            if (!permissions.isEmpty()) {
                int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 1;
                requestPermissions(permissions.toArray(new String[0]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
            }
        }
    }

    private void addMarker(ArrayList<LatLng> userLatLngs) {
        if (mGoogleMap != null) {
            removeAllNearByUserOfMap();
            for (LatLng latlng : userLatLngs) {
                MarkerOptions marker = new MarkerOptions();
                marker.position(latlng).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_20));
                markersNearUsers.add(mGoogleMap.addMarker(marker));
            }
        }
    }

    private void removeAllNearByUserOfMap() {
        if (markersNearUsers == null) {
            markersNearUsers = new ArrayList<>();
        }
        for (Marker marker : markersNearUsers) {
            marker.remove();
        }
        markersNearUsers.clear();
    }

    private void cancelUserUpdateApiCall() {
        if (updateDriverLocation != null) {
            updateDriverLocation.cancel();
            updateDriverLocation = null;
        }
    }

    @NonNull
    private DestinationView.DestinationViewCallBack getDestinationCallBack() {
        return new DestinationView.DestinationViewCallBack() {
            @Override
            public void onYESButtonCliked() {
                calculateDistances();
            }

            @Override
            public void onNOButtonClicked() {
                backToOrderView = new BackToOrderView(findViewById(R.id.no_client_reached));
                backToOrderView.show();
                commentBoxVisible = true;
                showDistanceAndTimeView();
                backToOrderView.setBackToOrderCallback(new BackToOrderView.BackToOrderViewCallback() {
                    @Override
                    public void onOkButtonClicked() {
                        String reasonMsg = BackToOrderView.editText.getText().toString().trim();
                        if (reasonMsg.length() == 0) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_47_s4_plz_give_reason") + reasonMsg, Toast.LENGTH_LONG).show();
                        } else {
                            //driverUpdateProfile("1", false, "0");
                            updateTripStatusApi(Constants.TripStatus.DRIVER_CANCEL_AT_DROP, reasonMsg);
                        }
                    }
                });
            }
        };
    }

    @NonNull
    private ClientPickedUpView.ClientPickedUpCallback getClientPickedUpCallBack() {
        return new ClientPickedUpView.ClientPickedUpCallback() {
            public void onYesButtonClicked() {
                changeAfterClientPick();

            }

            public void onNoButtonClicked() {
                clientPickedUpView.hide();
                pickupPopupVisible = false;
                backToOrderView = new BackToOrderView(findViewById(R.id.no_client_reached));
                backToOrderView.show();
                commentBoxVisible = true;
                BackToOrderView.relativeLayout.setVisibility(View.VISIBLE);
                setDriver_rejected();
                showDistanceAndTimeView();

                backToOrderView.setBackToOrderCallback(new BackToOrderView.BackToOrderViewCallback() {
                    // for pickedup view or after pickedup button clicked
                    @Override
                    public void onOkButtonClicked() {
                        String reasonMsg = BackToOrderView.editText.getText().toString().trim();
                        if (reasonMsg.length() == 0) {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_47_s4_plz_give_reason") + reasonMsg, Toast.LENGTH_LONG).show();
                        } else {
                            updateTripStatusApi(Constants.TripStatus.DRIVER_CANCEL_AT_PICKUP, reasonMsg);
                            sendNotificationToUser();
                            driverUpdateProfile("1", false, false);
                        }
                    }
                });

            }
        };
    }

    private void checkDocumentUploaded() {
        if (controller.getLoggedDriver().isDocUploaded()) {
            nodocumentalerttext.setVisibility(View.VISIBLE);
            nodocumentalerttext.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SlideMainActivity.this, DriverProfileActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            nodocumentalerttext.setVisibility(View.GONE);
        }

        if (controller.getLoggedDriver().isCarDetailsAdded()) {
            Intent intent = new Intent(SlideMainActivity.this, DocUploadActivity.class);
            startActivity(intent);
        }
    }

    private void checkIsVerified() {
        if (!controller.getLoggedDriver().isDocVerified()) {
            layoutAccountStatus.setVisibility(View.VISIBLE);
            refreshAccountStatus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (refreshAccountStatus.getVisibility() == View.VISIBLE)
                        driverTokenUpateProfile(controller.getLoggedDriver().getD_is_available(), false);
                }
            });
        } else {
            layoutAccountStatus.setVisibility(View.INVISIBLE);
        }
    }

    private void showDeliveryListView() {
        destinationActivity.hide();
        destinationPopUpVisible = false;
        listDialog = new Dialog(SlideMainActivity.this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        // listDialog.setTitle("Select Item");
        LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.select_delivery_layout, null, false);
        listDialog.setContentView(v);
        listDialog.setCancelable(true);
        //there are a lot of settings, for dialog, check them all out!
        ListView list1 = listDialog.findViewById(R.id.delivery_list_view);
        ImageView cancelDailog = listDialog.findViewById(R.id.cancelDailog);
        cancelDailog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listDialog.dismiss();
                progressDialog.dismiss();
                onResume();
            }
        });
        //list1.setOnItemClickListener(this);
        DeliveryListAdapter deliveryListAdapter = new DeliveryListAdapter(deliveryList);
        list1.setAdapter(deliveryListAdapter);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.("address", "" + deliveryList.get(position).getDeliveryAddress());
                //Log.("id", "" + deliveryList.get(position).getDeliveryId());

                listDialog.dismiss();
                updateDeliveryStatus(deliveryList.get(position).getDeliveryId(), Constants.DeliveryStatus.Completed);
            }
        });
        //now that the dialog is set up, it's time to show it
        listDialog.show();

    }

    /**
     * Trip fragment   interface method
     *
     * @param tripModel
     */
    @Override
    public void onTripDetail(TripModel tripModel) {
        ArrayList<DriverConstants> constantList = controller.getConstantsList();
        String driver_wallet_cons = "0";
        String wallet_amt = controller.getLoggedDriver().getD_wallet();

        for (DriverConstants constants : constantList) {
            if (constants.getConstant_key().equals("driver_wallet_cons")) {
                Log.e(TAG, "constants.getConstant_value()" + constants.getConstant_value());
                driver_wallet_cons = constants.getConstant_value();
                break;
            }
        }
        if (driver_wallet_cons.equals("1") && (wallet_amt != null && wallet_amt.trim().length() != 0) && Integer.parseInt(wallet_amt) <= 0) {
            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SlideMainActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setCancelable(false);

            builder.setMessage(getString(R.string.please_add_money_in_wallet));
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        } else {
            Intent can = new Intent(this, RequestActivity.class);
            can.putExtra("trip_history", tripModel);
            if (tripModel.trip != null)
                can.putExtra("destination", tripModel.trip.getTrip_to_loc());
            startActivityForResult(can, 1);
        }
    }

    @Override
    public void onAcceptAssignedTrip(TripModel tripModel, final Fragment requestFragment) {
        if (!(requestFragment instanceof AsignedRequestFragment) || controller.getLoggedDriver() == null) {
            return;
        }
        AsignedRequestFragment fragment = (AsignedRequestFragment) requestFragment;

        ArrayList<DriverConstants> constantList = controller.getConstantsList();
        String driver_wallet_cons = "0";
        String wallet_amt = controller.getLoggedDriver().getD_wallet();
        if (constantList != null && constantList.size() != 0) {
            for (DriverConstants constants : constantList) {
                if (constants.getConstant_key().equalsIgnoreCase("driver_wallet_cons")) {
                    driver_wallet_cons = constants.getConstant_value();
                    break;
                }
            }
        }

        if (driver_wallet_cons.equals("1") && (wallet_amt != null && wallet_amt.trim().length() != 0) && Integer.parseInt(wallet_amt) <= 0) {
            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SlideMainActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setCancelable(false);
            builder.setMessage(getString(R.string.please_add_money_in_wallet));
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        } else {
            rvRequest.setVisibility(View.GONE);
            this.tripModel = tripModel;
            if (tripModel != null && tripModel.trip != null)
                mTripId = tripModel.trip.getM_trip_id();
            tripModel.tripId = tripModel.trip.getTrip_id();
            tripModel.tripStatus = tripModel.trip.getTrip_status();
            AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
            isTripBegin = true;
            controller.pref.setDriverStartLatitude(String.valueOf(myCurrentLocation.getLatitude()));
            controller.pref.setDriverStartLongitude(String.valueOf(myCurrentLocation.getLongitude()));
            topRelativeLayout.setVisibility(View.GONE);
            view.setVisibility(View.GONE);

            if (!isTripStatusUpdating) {
                controller.pref.setTripIds(tripModel.tripId);

                isTripStatusUpdating = true;
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.Keys.TRIP_ID, tripModel.tripId);
                params.put(Constants.Keys.TRIP_STATUS, Constants.TripStatus.ACCEPT);
                params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey());

                if (tripModel.trip != null && tripModel.trip.getM_trip_id() != null && !tripModel.trip.getM_trip_id().equalsIgnoreCase("null"))
                    params.put("m_trip_id", tripModel.trip.getM_trip_id());

                final String otp = genrateOtp();
                params.put(Constants.Keys.OTP, otp);
                System.out.println("Accept Params : " + params);
                ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, params, Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        isTripStatusUpdating = false;
                        if (isUpdate) {

                            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                            tripModel.tripStatus = Constants.TripStatus.ACCEPT;
                            tripModel.otp = otp;
                            AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();

                            if (requestFragment != null) {
                                if (fragment.repeatTimerManager != null)
                                    fragment.repeatTimerManager.stopRepeatCall();

                                fragment.onRefresh();
                            }

                            full.setVisibility(View.VISIBLE);
                            fullbutton.setVisibility(View.VISIBLE);
                            //sendNotificationToUser(Constants.TripStatus.ACCEPT);
                            sendNotification(Constants.TripStatus.ACCEPT);
                            repeatTimerManager.stopRepeatCall();
                            repeatTimerManagerTripStatus.startRepeatCall();
                            updateUIForAcceptRequest();
                        } else {
                            updateUIForRejectRequest();
                        }
                    }
                });

            }
        }
    }

    @Override
    public void onAssignTrip(final TripModel tripModel, final Fragment requestFragment) {
        if (!(requestFragment instanceof RequestFragment) || controller.getLoggedDriver() == null) {
            return;
        }
        RequestFragment fragment = (RequestFragment) requestFragment;


//        this.tripModel = tripModel;
//        tripModel.tripId = tripModel.trip.getTrip_id();
//        tripModel.tripStatus = tripModel.trip.getTrip_status();

        if (!isTripStatusUpdating) {
            isTripStatusUpdating = true;
            HashMap<String, String> params = new HashMap<>();
            params.put(Constants.Keys.TRIP_ID, tripModel.trip.getTrip_id());
            params.put(Constants.Keys.TRIP_STATUS, Constants.TripStatus.ASSIGN);
            params.put(Constants.Keys.DRIVER_ID, controller.getLoggedDriver().getDriverId());
            params.put(Constants.Keys.API_KEY, controller.getLoggedDriver().getApiKey());
            Log.e("Assign Params : ", "" + params);
            ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, params, Constants.Urls.URL_ASSIGN_TRIP_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

                    isTripStatusUpdating = false;

                    if (isUpdate) {
                        if (requestFragment != null) {
                            if (fragment.repeatTimerManager != null)
                                fragment.repeatTimerManager.stopRepeatCall();

                            fragment.onRefresh();
                        }

//                        tripRequestsRequest();
                        if (tripModel.user != null && tripModel.user.getU_device_type() != null) {
                            String u_device_type = tripModel.user.getU_device_type();
                            Map<String, String> params = new HashMap<>();
                            String notificationMessage = "change message:";
                            if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                                notificationMessage = Constants.Message_ar.AASSIGNED;
                            } else {
                                notificationMessage = Constants.Message.AASSIGNED;
                            }
//                            notificationMessage = Constants.Message.AASSIGNED;
                            params.put("message", notificationMessage);
                            params.put("trip_id", tripModel.trip.getTrip_id());
                            params.put("trip_status", "assigned");
                            if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                                params.put(Constants.Keys.ANDROID, tripModel.user.getU_device_token());
                            } else {
                                params.put(Constants.Keys.IOS, tripModel.user.getU_device_token());
                            }
                            Log.e("notificaitom", "" + params);

                            notificationStatusApiAfterTripBegin(params);
                        }

                    } else {
                        if (error instanceof ServerError) {
                            String s = new String(error.networkResponse.data);
                            try {
                                JSONObject jso = new JSONObject(s);
//Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
//
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                        }
                    }


                }
            });

        }

    }

    private void getTrip(final boolean isDialog, final boolean isResetTimer) {
        if (controller.getLoggedDriver() == null)
            return;
        if (isDialog) {
            progressDialog.showDialog();
        }
        final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel == null) {
            progressDialog.dismiss();
            return;
        }
        cancelPreviousCallingTripApi();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        callTripApi = apiInterface.getTripById(controller.getLoggedDriver().getApiKey(), tripModel.tripId);
        callTripApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + string);
                        if (res.isStatus()) {
                            boolean isParseRe = TripModel.parseJsonWithTripModel(string, tripModel);
                            if (isParseRe) {
                                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();

                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onRejectTrip(TripModel tripModel, final Fragment fragment) {
        if (!(fragment instanceof RequestFragment) || controller == null || controller.getLoggedDriver() == null) {
            return;
        }
        RequestFragment requestFragment = (RequestFragment) fragment;

        if (controller == null || controller.getLoggedDriver() == null)
            return;
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", controller.getLoggedDriver().getDriverId());
        params.put("api_key", controller.getLoggedDriver().getApiKey());
        params.put("trip_id", tripModel.trip.getTrip_id());
        params.put("status", "reject");
        showProgressBar();
        WebServiceUtil.excuteRequest(this, params, Constants.Urls.URL_REJECT_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                hideProgressBar();
                if (isUpdate) {
                    if (requestFragment != null) {
                        requestFragment.onRefresh();
                    }
                } else {
                    Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void tripRequestsRequest() {
        if (myReqFrag != null) {
            myReqFrag.refresh(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            Log.e("onActivityResult", "YES");

            if (mGoogleMap != null) {
                mGoogleMap.clear();
                markerDriverCarD = null;
                Log.d(TAG, "onActivityResult: animateCarOnMap");
                animateCarOnMap(latLngPublishRelay);
            }
            if (data.hasExtra("moreDelivery")) {
                Log.e("hasMoredELIVERY", "YES");
                TrackRouteSaveData.getInstance(SlideMainActivity.this).clearData();
//                coverLatLongs.clear();
//                coverLatLongs = new ArrayList<>();
                clearCoveredRouteD();
                clearBeginTripDistanceAndTime();
                onResume();
            } else {
//                isTripBegin = true;
                tripModel = (TripModel) data.getSerializableExtra("trip_history");
                //System.out.print(tripModel.tripId);
                controller.pref.setDriverStartLongitude(String.valueOf(myCurrentLocation.getLongitude()));
                controller.pref.setDriverStartLatitude(String.valueOf(myCurrentLocation.getLatitude()));
//                if (tripModel.trip.getIs_delivery().equals("1")) {
//
//                    top_text.setText(R.string.del);
//                    ride_text.setText(R.string.del);
//                } else {
//                    top_text.setText(R.string.on_ride);
//                    ride_text.setText(R.string.on_ride);
//                }
//                acceptView.hide();
                if (tripModel != null)
                    onAcceptTrip(tripModel, myReqFrag);
            }
        }
    }

    private void updateDeliveryStatus(final String deliveryId, String deliveryStatus) {
        if (Utils.net_connection_check(SlideMainActivity.this)) {
            final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
            if (tripModel == null) {
                return;
            }
            Map<String, String> params = new HashMap<String, String>();
            params.put("api_key", controller.getLoggedDriver().getApiKey());
            params.put("delivery_id", deliveryId);
            params.put("delivery_status", deliveryStatus);
            params.put("del_time", AppUtil.getCurrentDateInGMTZeroInServerFormat());
            //Log.("paramsStatus", "" + params);
            //Log.("urlStatus", "" + Constants.Urls.UPDATE_DELIVERY_URL);
            ServerApiCall.callWithApiKey(this, params, Constants.Urls.UPDATE_DELIVERY_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    if (isUpdate) {
                        //Log.("dataDeliveryResponse", "" + data);
                        progressDialog.dismiss();
                        Date datePickup = Utils.stringToDate(tripModel.trip.getTrip_pickup_time());
                        Date dateDrop = Utils.stringToDate(AppUtil.getCurrentDateInGMTZeroInServerFormat());
                        ////Log.("pickupdate", "" + tripModel.trip.getTrip_pickup_time());
                        ////Log.("dateDrop", "" + tripModel.trip.getTrip_drop_time());
                        if (datePickup != null && dateDrop != null) {
                            long diffrent_between_time = dateDrop.getTime() - datePickup.getTime();
                            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                            long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                            if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                                finalTime = diffrent_between_time_inMinute + " " + "min";
                            } else if (diffrent_between_time_inMinute < 1 || diffrent_between_time_inMinute == 0) {
                                finalTime = diffrent_between_time_inSeconds + " " + "sec";
                            }
                        }
                        if (datePickup != null && dateDrop == null) {
                            String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                            Date dateDrop1 = Utils.stringToDate(dropDate);

                            long diffrent_between_time = dateDrop1.getTime() - datePickup.getTime();
                            long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                            long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                            if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                                finalTime = diffrent_between_time_inMinute + " " + "min";
                            } else {
                                finalTime = diffrent_between_time_inSeconds + " " + "sec";
                            }
                        }

                        Intent intent = new Intent(getApplicationContext(), FareSummaryActivity.class);
                        intent.putExtra("time", tripTime);
                        intent.putExtra("is_delivery", "true");
                        intent.putExtra("delivery_id", deliveryId);

//                        if (distanceCoverInKm != 0) {
//                            intent.putExtra("distance", distanceCoverInKm);
//                        }
                        startActivityForResult(intent, 1);
                    }
                }
            });

        }
    }

    private void changeAfterClientPick() {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        LinearLayout linear = findViewById(R.id.full);
        linear.setVisibility(View.GONE);
        fullbutton.setText("");
        fullbutton.setVisibility(View.GONE);
        goButton.setVisibility(View.VISIBLE);
        if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
            goButton.setText(Localizer.getLocalizerString("k_30_s4_begin_delivery"));
        } else {
            goButton.setText(Localizer.getLocalizerString("k_31_s4_begin_ride"));
        }
        controller.pref.setLocalTripState("Pick");
        controller.pref.setTIME_DISTANCE_VIEW(true);
        clientPickedUpView.hide();
        showDistanceAndTimeView();
        addressLocationView.show();
        /*addressLocationView.setLocationTitle(getText(R.string.drop_location));
        addressLocationView.setLocationAddress(tripModel.trip.getTrip_to_loc());*/

        if (tripModel.trip.getTrip_status().equalsIgnoreCase("begin")) {
            addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
            addressLocationView.setLocationAddress(tripModel.trip.getTrip_to_loc());
        } else {
            addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_11_s8_search_drop_location"));

            if (tripModel.trip != null && tripModel.trip.getPickup_notes() != null)
                addressLocationView.setLocationAddress(tripModel.trip.getPickup_notes() + ", " + tripModel.trip.getTrip_from_loc());
            else
                addressLocationView.setLocationAddress(tripModel.trip.getTrip_from_loc());
        }
        //driverUpdateProfile("1", false, "0");
        pickupAddress = getCompleteAddressString(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                // Log.w("My Current loction address", strReturnedAddress.toString());
            }  //Log.w("My Current loction address", "No Address returned!");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void checkOfflineStatus() {
        try {
            boolean isAvailable = controller.getLoggedDriver().getD_is_available().equals("1");
            switchAvailable1 = findViewById(R.id.available_switch);
            switchAvailable1.setChecked(isAvailable);
            tvSwitchText.setText(isAvailable ? Localizer.getLocalizerString("k_4_s4_availability_on") : Localizer.getLocalizerString("k_51_s4_availability_off"));

            checkIsVerified();
            sendBroadcast(new Intent("AVAILABILITY_CHANGED_RECEIVER"));

            switchAvailable1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        switchAvailable1.setChecked(true);
                        tvSwitchText.setText(Localizer.getLocalizerString("k_4_s4_availability_on"));
                        driverUpdateProfile("1", false, false);
                    } else {
                        switchAvailable1.setChecked(false);
                        tvSwitchText.setText(Localizer.getLocalizerString("k_51_s4_availability_off"));
                        driverUpdateProfile("0", false, false);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void updateNotAvailability() {
        if (controller.getLoggedDriver() == null)
            return;
        String availability = "";
        if (tripMaster != null) {
            int maxSeats = Integer.parseInt(tripMaster.getMaxSeat());
            int nSeats = Integer.parseInt(tripMaster.getnSeat());
            if (nSeats < maxSeats && tripMaster.getIsShare().equalsIgnoreCase("1")) {
                availability = "2";
            } else {
                availability = "0";
            }
        }
        if (tripModel != null) {
            if (!availability.isEmpty()) {
                driverUpdateProfile(availability, false, false);
            } else if (tripModel.trip != null && tripModel.trip.getIs_share().equalsIgnoreCase("1"))
                driverUpdateProfile("2", false, false);
            else
                driverUpdateProfile("0", false, false);
        } else {
            driverUpdateProfile(controller.getLoggedDriver().getD_is_available(), false, false);
        }
    }

    private void ProfileDetails() {
        Driver driver = controller.getLoggedDriver();
        if (driver != null) {
            TextView tvDrawerProfileName = findViewById(R.id.tv_drawerProfileName);
            TextView tvmobilenumber = findViewById(R.id.tv_mobile);
            tvDrawerProfileName.setText(String.format("%s %s", driver.getD_fname(), driver.getD_lname()));
            if (USE_EMAIL_AUTH) {
                tvmobilenumber.setText(driver.getD_email());
            } else {
                tvmobilenumber.setText(String.format("%s%s", driver.getC_code(), driver.getD_phone()));
            }
            if (driver.isProfileImagePathEmpty() && driver.getD_profile_image_path().length() != 0) {
                String fullpath = Constants.IMAGE_BASE_URL + driver.getD_profile_image_path();
                Picasso.get().load(fullpath).into(imageV);
            } else {
                imageV.setImageResource(R.drawable.ic_user);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        isOnPauseMethodCalled = true;
        repeatTimerManager.stopRepeatCall();
//        repeatTimerManagerUpdateLocation.stopRepeatCall();
        AppData.getInstance(getApplicationContext()).saveData();
        clearCoveredRouteD();
        if (mGoogleMap != null)
            mGoogleMap.clear();
    }

    public void onResume() {
        super.onResume();
        i = 0;
        if (isGetNearByUser()) {
            repeatTimerManager.startRepeatCall();
        }
        timeForSkipSameLocation = 10;
//        isOnPauseMethodCalled = false;
//        repeatTimerManagerUpdateLocation.startRepeatCall();
        isBegin = false;
        if (!isDelivery) {

            if (mGoogleMap != null) {
                mGoogleMap.clear();

                if (markerDriverCarD != null) {
                    markerDriverCarD.remove();
                    markerDriverCarD = null;
                }
                Log.d(TAG, "onResume: isDelivery: animateCarOnMap");
                animateCarOnMap(latLngPublishRelay);
            }
            setupMap();
        } else
            getDeliveryRequests();
        if (!ishowingUpdateDailog) {
            checkVersionUpdateRequired();
        }
        ProfileDetails();
        checkOfflineStatus();
        checkGPS();
        Log.d(TAG, "onResume: animateCarOnMap");
        animateCarOnMap(latLngPublishRelay);

        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.tripStatus.equals(Constants.TripStatus.BEGIN)) {
            coverLatLongsD.clear();
            coverLatLongsD.addAll(TrackRouteSaveData.getInstance(this).getAllLatLngsD());
        }

//        checkWalletBalance();
//        getMTrips();

    }


//    private void checkWalletBalance() {
//        String wallet = controller.getLoggedDriver().getD_wallet();
//        double walletBalance = 0;
//        if (wallet != null && !wallet.equals("") && !wallet.equals("null")) {
//            walletBalance = Double.parseDouble(wallet);
//        }
//        if (walletBalance < WALLET_BALANCE_THRESHOLD) {
//            driverUpdateProfile("0", false, true);
//        }
//    }

    private void checkGPS() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            hasLocationPermission = false;
            buildAlertMessageNoGps();
        } else {
            if (checkLocationPermission()) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MINIMUM_TIME_BETWEEN_UPDATES,
                        MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new MyLocationListener()
                );
                hasLocationPermission = true;
            } else {
                hasLocationPermission = false;
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    @SuppressLint("NewApi")
    private void setupMap() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                mGoogleMap.getUiSettings().setCompassEnabled(false);
                mGoogleMap.clear();
                markerDriverCarD = null;
                Log.d(TAG, "setupMap: animateCarOnMap");
                animateCarOnMap(latLngPublishRelay);

                try {
                    // Customise map styling via JSON file
                    boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.uberstyle));

                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }
                TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

                if (tripModel != null) {
                    if (tripModel.trip.getTrip_to_loc().trim().length() == 0) {
                        drawCoverRoutePolyLineOnMapD();
                    } else {
                        drawCoverRoutePolyLineOnMapD();
                    }
                }

                if (tripModel != null && tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().length() != 0 && tripModel.trip.getIs_delivery().equals("1")) {
                    isDelivery = true;
                    deliveryList = AppData.getInstance(SlideMainActivity.this).getDeliveryList();
                }

                if (tripModel != null && tripModel.trip != null && tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
                    btn_toggle_rs.setVisibility(View.VISIBLE);
                } else {
                    btn_toggle_rs.setVisibility(View.GONE);
                }
                destinationActivity = new DestinationView(getApplicationContext(), findViewById(R.id.endtrip_request), isDelivery);
                destinationActivity.hide();
                destinationActivity.setDestinationActivityCallBack(getDestinationCallBack());
                destinationPopUpVisible = false;
                clientPickedUpView = new ClientPickedUpView(SlideMainActivity.this, findViewById(R.id.client_pickup_view), isDelivery);
                clientPickedUpView.hide();
                pickupPopupVisible = false;
                clientPickedUpView.setClientPickedUpCallback(getClientPickedUpCallBack());
                try {
                    customHandler.postDelayed(updateTimerThread, 0);
                    //Log.("isRequiredRouteDraw234", "" + is_required_drawRoute);
                    if (is_required_drawRoute.equalsIgnoreCase("Yes1")) {
                        Log.e("setupMap", "truess");
                        Log.e("tripStatus", "" + Objects.requireNonNull(tripModel).trip.getTrip_status());
                        String destLat = tripModel.trip.getTrip_scheduled_pick_lat();
                        double dsLat = Double.parseDouble(destLat);
                        String destLng = tripModel.trip.getTrip_scheduled_pick_lng();
                        double dsLng = Double.parseDouble(destLng);
                        /*double orLat = myCurrentLocation.getLatitude();
                        double orLng = myCurrentLocation.getLongitude();*/
                        double orLat = Double.parseDouble(controller.pref.getDriverStartLatitude());
                        double orLng = Double.parseDouble(controller.pref.getDriverStartLongitude());
                        drawDriverRoute(dsLat, dsLng, orLat, orLng, 1);
                    } else if (is_required_drawRoute.equalsIgnoreCase("Yes2")) {

                        if (!isBeginRouteDrow) {
//                            if (pickUpLocationReceiver != null) {
//                                LocalBroadcastManager.getInstance(SlideMainActivity.this).unregisterReceiver(pickUpLocationReceiver);
//                            }
                            pikLat = myCurrentLocation.getLatitude();
                            pikLng = myCurrentLocation.getLongitude();
                            if (isDelivery) {
                                drawDriverRouteWithWayPoints();
                            } else if (tripModel != null && tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
                                String dropLat = tripModel.trip.getTrip_scheduled_drop_lat();
                                double drop_Lat = Double.parseDouble(dropLat.trim());
                                String dropLng = tripModel.trip.getTrip_scheduled_drop_lng();
                                double drop_Lng = Double.parseDouble(dropLng);

                                // drawDriverRoute(drop_Lat, drop_Lng, myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude(), 2);
                                if (tripModel.actual_pickup_lat != null && tripModel.actual_pickup_lng != null) {
                                    drawDriverRoute(drop_Lat, drop_Lng, Double.parseDouble(tripModel.actual_pickup_lat), Double.parseDouble(tripModel.actual_pickup_lng), 2);
                                } else {
                                    drawDriverRoute(drop_Lat, drop_Lng, myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude(), 2);
                                }
                            } else {
                                handler1.removeCallbacks(r);
//                                coverLatLongs.clear();
                                clearCoveredRouteD();
                                mGoogleMap.clear();
                                markerDriverCarD = null;
                                Log.d(TAG, "setupMap: isBeginRouteDrow: animateCarOnMap");
                                animateCarOnMap(latLngPublishRelay);
                                drawRouteWithoutDestination(null);
//                                if (myCurrentLocation != null) {
//                                    drawRouteWithoutDestination(myCurrentLocation);
//                                } else {
//                                    drawRouteWithoutDestination(myLastCurrentLocation);
//                                }
                            }
                            // }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
//                    @Override
//                    public void onCameraIdle() {
//                        LatLng latLng = mGoogleMap.getCameraPosition().target;
//                        Log.d(TAG, "onCameraIdle: " + latLng.toString());
//                    }
//                });
                googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {
                        if (i == REASON_GESTURE) {
                            isZoomEnabled = false;

                            try {
                                handler1.removeCallbacks(r);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            handler1.postDelayed(r, 10000);
                        }
                    }
                });

                animateCameraAndLocation();
            }
        });
        layoutZoomControlOnMap();
        layoutUserLocationButtonOnMap();
        animateCameraAndLocation();
    }

    private void drawRouteWithoutDestination(Location location) {
//        coverLatLongs.clear();
        clearCoveredRouteD();
        //Log.e("updatredLocationTO Draw", "" + location.getLatitude() + "," + location.getLongitude());
        setAndUpdateDriverLocation(location);

//        updateDriverProfileApi(location.getLatitude(), location.getLongitude());
        makeRouteAndRedrwaRouteWithoutDestination(location);
        //addDriverCurrentLocationMarker(location);
    }

    private void makeRouteAndRedrwaRouteWithoutDestination(Location location) {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();

//        addDriverCurrentLocationMarker(location);
        if (tripModel != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
            clearCoveredRouteD();
            if (location != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                if (latLng.longitude != 0.0 && latLng.latitude != 0.0) {
                    coverLatLongsD.add(latLng);
                }
            }
            //Log.e("sizecovers", "" + coverLatLongsD.size());
            //Log.e("latLng", "" + latLng);
//            if (TrackRouteSaveData.getInstance(SlideMainActivity.this) != null) {
//                TrackRouteSaveData.getInstance(SlideMainActivity.this).addLatLongD(latLng);
//                TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
//
//            }
            drawCoverRoutePolyLineOnMapD();
        } else {
            coverLatLongsD.clear();
        }
    }

    private void clearCoveredRouteD() {
        if (coverRoutePolyLineD == null) {
            coverRoutePolyLineD = new ArrayList<>();
        }
        for (Polyline polyline : coverRoutePolyLineD) {
            polyline.remove();
        }
        coverRoutePolyLineD.clear();

//        removeDriverMarker();
        if (trackStartMarker != null) {
            trackStartMarker.remove();
            trackStartMarker = null;
        }
    }

    private void setAndUpdateDriverLocation(Location location) {
        if (location != null) {
            double driverlat = location.getLatitude();
            double driverlong = location.getLongitude();
            controller.pref.setDriver_Lat(String.valueOf(driverlat));
            controller.pref.setDriver_Lng(String.valueOf(driverlong));
            controller.setD_lat(driverlat);
            controller.setD_lng(driverlong);
            controller.setDriverLocation(location);
        }
    }

    private void drawDriverRouteWithWayPoints() {
        getDelNum();
        Log.e("deliveryList", "" + deliveryList.size());
        double drop_Lat1 = Double.parseDouble(deliveryList.get(numDel).getDeliveryLatitude());
        double drop_Lng1 = Double.parseDouble(deliveryList.get(numDel).getDeliveryLongitude());
        String deliveryPrice = deliveryList.get(numDel).getPayAmount();
        if (isCallingForGetRoute) {
            return;
        }
        isCallingForGetRoute = true;
        LatLng dest;
        dest = new LatLng(drop_Lat1, drop_Lng1);
        pikLat = Double.parseDouble(tripModel.actual_pickup_lat);
        pikLng = Double.parseDouble(tripModel.actual_pickup_lng);
        LatLng origin = new LatLng(pikLat, pikLng);
//        preLocation = null;
        Log.e("origin Delivery", "" + origin);
        googleSDRoute = new GoogleSDRoute(this, origin, dest, waypoints);
        googleSDRoute.getRoute(getGoogeSdRoute(2));
    }

    private void animateCameraAndLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (mGoogleMap != null && myCurrentLocation != null) {
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            if (isTripBegin()) {
                imgLocation.setVisibility(View.VISIBLE);
                mGoogleMap.setMyLocationEnabled(false);
            } else {
                TripModel tripModel = AppData.getInstance(getApplication()).getTripModel();
                if (tripModel == null) {
                    imgLocation.setVisibility(View.GONE);
                    mGoogleMap.setMyLocationEnabled(true);
                    btn_toggle_rs.setVisibility(View.GONE);
                } else {
                    if (tripModel.trip != null && tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
                        btn_toggle_rs.setVisibility(View.VISIBLE);
                    } else {
                        btn_toggle_rs.setVisibility(View.GONE);
                    }
                    imgLocation.setVisibility(View.VISIBLE);
                    mGoogleMap.setMyLocationEnabled(false);
                }
            }
            if (!isMovedCameraToCurrentLocation && myCurrentLocation != null) {
                isMovedCameraToCurrentLocation = true;
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude())).zoom(16f).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }

    private void layoutZoomControlOnMap() {
        // Zoom Control Position
        @SuppressLint("ResourceType") View zoomControls = Objects.requireNonNull(getFragmentManager().findFragmentById(
                R.id.map).getView()).findViewById(0x1);
        if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            // ZoomControl is inside of RelativeLayout
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();

            // Align it to - parent top|left
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            // Update margins, set to 10dp
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
                    getResources().getDisplayMetrics());
            params.setMargins(margin, margin, margin, margin + 200);
        }
    }

    private void layoutUserLocationButtonOnMap() {
        @SuppressLint("ResourceType") View locationButton = Objects.requireNonNull(getFragmentManager().findFragmentById(R.id.map).getView()).findViewById(2);
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rlp.setMargins(0, 230, 30, 0);
    }

    @Override
    public void viewOnMap(String type, LatLng latLng) {
        Intent intent = new Intent(getApplicationContext(), MainActivityFragment.class);
        intent.putExtra("latitude", String.valueOf(latLng.latitude));
        intent.putExtra("longitude", String.valueOf(latLng.longitude));
        intent.putExtra("type", type);
        startActivity(intent);

    }

    @Override
    public void viewOnMap(String type, LatLng pickLatLng, LatLng dropLatLng) {
        Intent intent = new Intent(getApplicationContext(), MainActivityFragment.class);
        intent.putExtra("latitude", String.valueOf(pickLatLng.latitude));
        intent.putExtra("longitude", String.valueOf(pickLatLng.longitude));
        intent.putExtra("type", type);
        startActivity(intent);

    }

    @Override
    public void onRejectTripLater(TripModel tripModel, Fragment fragment) {

        if (!(fragment instanceof AsignedRequestFragment) || controller == null || controller.getLoggedDriver() == null) {
            return;
        }
        AsignedRequestFragment requestFragment = (AsignedRequestFragment) fragment;

        Map<String, String> params = new HashMap<>();
        params.put("driver_id", controller.getLoggedDriver().getDriverId());
        params.put("api_key", controller.getLoggedDriver().getApiKey());
        params.put("trip_id", tripModel.trip.getTrip_id());
        params.put("status", "reject");
        showProgressBar();
        WebServiceUtil.excuteRequest(this, params, Constants.Urls.URL_REJECT_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                hideProgressBar();
                if (isUpdate) {
                    tripRequestsRequest();
                } else {
                    Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult arg0) {

    }

    @Override
    public void onConnected(Bundle arg0) {
        setupMap();
    }

    @Override
    public void onStart() {
        super.onStart();
        client.connect();
        isStopMethodCalled = false;
//        repeatTimerManagerUpdateLocation.startRepeatCall();
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            if (controller.pref.getIsSingleMode()) {
                Intent brain = new Intent(SlideMainActivity.this, SingleModeActivity.class);
                brain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(brain);
            } else {
                String tripStatus = tripModel.tripStatus;
                try {
                    if (Integer.parseInt(tripModel.tripId) > 0) {
                        TripController tripController = new TripController(tripModel);
                        tripController.setTripControllerCallBack(this);

                        if (tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.REQUEST)) {
                            getTrip(tripModel, false);
                        } else {
                            if (tripModel.tripStatus.equalsIgnoreCase(ARRIVE)) {
                                String localTripState = controller.pref.getLocalTripState();
                                if (localTripState.equalsIgnoreCase("Pick")) {
                                }
                            }
                            tripController.setChnageTripStatusLocal(tripStatus);
                            repeatTimerManagerTripStatus.startRepeatCall();
                        }
                    }
                } catch (Exception ignore) {

                }
                if (tripStatus != null && tripStatus.equals(Constants.TripStatus.BEGIN)) {
            /*
              Runtime permissions are required on Android M and above to access User's location
             */
                    if (AppUtil.hasM() && !(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                        askPermissions();
                    }
                }
            }
        } else {
            full.setVisibility(View.GONE);
        }
        if (AppUtil.hasM()) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startLocationService();
            } else {
                askPermissions();
            }
        } else {
            startLocationService();
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver, new IntentFilter(LOACTION_ACTION));
//        LocalBroadcastManager.getInstance(this).registerReceiver(pickUpLocationReceiver, new IntentFilter(PICKUP_LOCATION_ACTION));
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    private void askPermissions() {
        PermissionUtil.with(this).request(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION).onResult(
                new Func2() {
                    @Override
                    protected void call(int requestCode, String[] permissions, int[] grantResults) {

                        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                            startLocationService();

                        } else {
                            Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_37_s4_permission_denied"), Toast.LENGTH_LONG).show();
                        }
                    }

                }).ask(PERMISSION_ACCESS_LOCATION_CODE);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        isStopMethodCalled = true;
        repeatTimerManagerTripStatus.stopRepeatCall();
        AppData.getInstance(getApplicationContext()).saveData();
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(pickUpLocationReceiver);
        googleApiClient.disconnect();
        client.disconnect();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("SlideMain Page") //Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    public void onBackPressed() {
        if (pickupPopupVisible) {
            clientPickedUpView.hide();
            pickupPopupVisible = false;
        } else if (destinationPopUpVisible) {
            destinationActivity.hide();
            destinationPopUpVisible = false;
        } else if (commentBoxVisible) {
            backToOrderView.hide();
            commentBoxVisible = false;
        } else if (isTripBegin) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);

        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //finish();
        }
    }

    private String getcurrenttime() {
        DateFormat df;
        df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        Calendar calobj = Calendar.getInstance();
        return df.format(calobj.getTime());
    }

    private void buildAlertMessageNoGps() {
        if (gpsDialog != null) {
            if (gpsDialog.isShowing())
                gpsDialog.hide();
            gpsDialog = null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Dialog);
        builder.setMessage(Localizer.getLocalizerString("k_57_s4_gps_disabled"))
                .setCancelable(false)
                .setPositiveButton(Localizer.getLocalizerString("k_21_s4_yes"), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(Localizer.getLocalizerString("k_22_s4_no"), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkGPS();
                        dialog.cancel();
                    }
                });
        gpsDialog = builder.create();
        gpsDialog.show();
    }

    @Override
    public void onConnectionSuspended(int arg0) {


    }

    @Override
    protected void onDestroy() {
        AppData.getInstance(getApplicationContext()).saveData();
        TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
        try {
            // Unregister Broadcast Receiver
            unregisterReceiver(mHandleMessageReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    private void logoutApi(final Controller controller) {
        SlideMainActivity.controller = controller;
        progressDialog.showDialog();
        Map<String, String> params = new HashMap<>();
        params.put("d_is_available", "0");
        if (requestQueue != null) {
            requestQueue.cancelAll(upudateProfileApiTag);
        }
        cancelUserUpdateApiCall();
        requestQueue = ServerApiCall.callWithApiKeyAndDriverId(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                progressDialog.dismiss();
//                if (!isOnPauseMethodCalled) {
//                    repeatTimerManagerUpdateLocation.setTimerForRepeat();
//                }
//                if (isUpdate) {
//                } else {
//                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_LONG).show();
//                }

                controller.logout();
                stopLocationRecordService();
                Intent intent = new Intent(getApplicationContext(), SignInSignUpMainActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        }, upudateProfileApiTag);
    }

    @Override
    public void setTitle(CharSequence title) {

    }

    private boolean net_connection_check() {
        ConnectionManager cm = new ConnectionManager(this);
        boolean connection = cm.isConnectingToInternet();
        if (!connection) {
            Toast toast = Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_39_s4_no_mob_net"), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 70);
            toast.show();
        }
        return connection;
    }

    private void getNearByUsers(double latitude, double longitude) {
        if (controller.getLoggedDriver() != null) {
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            cancelNearByUserApiCall();
            String radius = "";
            for (DriverConstants driverConstants : controller.getConstantsList()) {
                if (driverConstants.getConstant_key().equalsIgnoreCase("driver_radius")) {
                    radius = driverConstants.getConstant_value();
                }

            }
            nearUserResponseCall = apiInterface.getNearUsers(controller.getLoggedDriver().getApiKey(), String.valueOf(latitude), String.valueOf(longitude), radius);
            nearUserResponseCall.enqueue(new Callback<NearUserResponse>() {
                @Override
                public void onResponse(Call<NearUserResponse> call, retrofit2.Response<NearUserResponse> response) {
                    if (response.isSuccessful()) {
                        NearUserResponse body = response.body();
                        ArrayList<User> nearUsers = body.getNearUsers();
                        ArrayList<LatLng> userLatLngs = new ArrayList<>();
                        for (User user : nearUsers) {
                            LatLng t = new LatLng(Double.parseDouble(user.getU_lat()), Double.parseDouble(user.getU_lng()));
                            userLatLngs.add(t);
                        }
                        addMarker(userLatLngs);
                    }
                    resetNearByUserSearch();
                }

                @Override
                public void onFailure(Call<NearUserResponse> call, Throwable t) {
                    if (!call.isCanceled()) {
                        resetNearByUserSearch();
                    }
                }
            });
        }
    }

    private String genrateOtp() {
        Random rnd = new Random();
        int otp = 10000 + rnd.nextInt(90000);
        Log.e("genrated otp", "" + otp);
        return String.valueOf(otp);
    }

    private void resetNearByUserSearch() {
        if (isGetNearByUser()) {
            if (!isStopMethodCalled) {
                repeatTimerManager.setTimerForRepeat();
            }
        } else {
            removeAllNearByUserOfMap();
        }
    }

    private void cancelNearByUserApiCall() {
        if (nearUserResponseCall != null) {
            nearUserResponseCall.cancel();
            nearUserResponseCall = null;
        }
    }

    private boolean isGetNearByUser() {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null) {
            return !tripModel.tripStatus.equalsIgnoreCase(ACCEPT) && !tripModel.tripStatus.equalsIgnoreCase(ARRIVE) && !tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN);
        }
        return true;
    }

    private void showdialog(final boolean islogout, String messag) {
        final Dialog dialog = new Dialog(SlideMainActivity.this);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_layout);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        TextView title2 = dialog.findViewById(R.id.tv_dialog_title);
        Button yes = dialog.findViewById(R.id.yes_btn);
        yes.setText(Localizer.getLocalizerString("k_21_s4_yes"));
        Button no = dialog.findViewById(R.id.no_btn);
        no.setText(Localizer.getLocalizerString("k_22_s4_no"));
        title2.setText(messag);
        yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (islogout) {
                    controller.setDocUpdate(false);
                    logoutApi(controller);
                } else {
                    driverUpdateProfile("0", true, false);
                }

            }
        });
        no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void updateUIForRejectRequest() {
        controller.setIsNotificationCome(false);
        cancelCountDownTimer();
        repeatTimerManager.stopRepeatCall();
        fullbutton.setText("");
        goButton.setText("");

        controller.setIsNotificationCome(false);
        cancelCountDownTimer();
        fullbutton.setText("");
        goButton.setText("");
        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
        AppData.getInstance(getApplicationContext()).clearTrip();
        this.tripModel = null;
        repeatTimerManagerTripStatus.stopRepeatCall();
        clearBeginTripDistanceAndTime();
        goButton.setText("");
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);

        goButton.setVisibility(View.GONE);
        SingleObject obj = SingleObject.getInstance();
        obj.setFareSummaryLinearLayout(false);
        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
        if (destinationActivity != null) {
            destinationActivity.view.setVisibility(View.GONE);
        }
        TrackRouteSaveData.getInstance(this).clearData();
//        if (markerDriverCar != null) {
//            markerDriverCar.remove();
//        }

        controller.pref.setTripIds("");
        Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
        startActivity(intent);
        finish();
    }

    private void updateUIForAcceptRequest() {
        cancelCountDownTimer();
        controller.setIsNotificationCome(false);
        fullbutton.setText(Localizer.getLocalizerString("k_19_s4_arrived"));
        //userInfoView.show();
        goButton.setText(Localizer.getLocalizerString("k_32_s4_dummy"));
        mGoogleMap.clear();
        markerDriverCarD = null;
        Log.d(TAG, "updateUIForAcceptRequest: animateCarOnMap");
        animateCarOnMap(latLngPublishRelay);
        is_required_drawRoute = "Yes1";
        setupMap();
        addressLocationView.show();
        addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
        String acc = "yes";

    }

    private void cancelPreviousCallingTripApi() {
        if (callTripApi != null) {
            callTripApi.cancel();
            callTripApi = null;
        }
    }

    private void resetTripApiTimer() {
        if (!isStopMethodCalled) {
            repeatTimerManagerTripStatus.setTimerForRepeat();
        }
    }

    private void getTrip(final TripModel tripModel) {
        getTrip(tripModel, true);
    }

    private void getTrip(final TripModel tripModel, final boolean isResetTimer) {
        if (tripModel == null)
            return;
        cancelPreviousCallingTripApi();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        callTripApi = apiInterface.getTripById(controller.getLoggedDriver().getApiKey(), tripModel.tripId);
        callTripApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        TripController tripController = new TripController(tripModel);
                        tripController.setTripControllerCallBack(SlideMainActivity.this);
                        tripController.handleTripResponse(string);
                        getMTrips();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (isRequest) {
                    isRequest = false;
                    progressDialog.dismiss();
                }
                if (isResetTimer) {
                    resetTripApiTimer();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (isRequest) {
                    isRequest = false;
                    progressDialog.dismiss();
                }
                if (!call.isCanceled()) {
                    if (isResetTimer) {
                        resetTripApiTimer();
                    }
                }
            }
        });

    }

//    private void clearPickRoute() {
//        if (polylines != null) {
//            for (Polyline polyline : polylines) {
//                polyline.remove();
//            }
//            polylines.clear();
//        }
//    }

    private void getDeliveryRequests() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> request = apiInterface.getTripDeliveryResponseList(controller.getLoggedDriver().getApiKey(), tripModel.trip.getTrip_id());
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string();
                        handleDeliveryResponse(string);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


            }
        });
    }

    private void handleDeliveryResponse(String s) {
        receiverArray = new ArrayList<>();
        try {
            Gson gson = new Gson();
            deliveryList = new ArrayList<>();
            JSONObject jsonRootObject = new JSONObject(s);
            JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject1 = response.getJSONObject(i);
                com.ridertechrider.driver.DeliverResponse.Response parseMultiDeliveryResponse = new com.ridertechrider.driver.DeliverResponse.Response();
                parseMultiDeliveryResponse.setDeliveryId(jsonObject1.getString("delivery_id"));
                parseMultiDeliveryResponse.setSenderId(jsonObject1.getString("sender_id"));
                parseMultiDeliveryResponse.setTripId(jsonObject1.getString("trip_id"));
                parseMultiDeliveryResponse.setDeliveryAddress(jsonObject1.getString("delivery_address"));
                parseMultiDeliveryResponse.setDeliveryLatitude(jsonObject1.getString("delivery_latitude"));
                parseMultiDeliveryResponse.setDeliveryLongitude(jsonObject1.getString("delivery_longitude"));
                parseMultiDeliveryResponse.setPayAmount(jsonObject1.getString("pay_amount"));
                parseMultiDeliveryResponse.setDeliveryStatus(jsonObject1.getString("delivery_status"));
                parseMultiDeliveryResponse.setDeliveryNotes(jsonObject1.getString("delivery_notes"));
                parseMultiDeliveryResponse.setDeliveryRemarks(jsonObject1.getString("delivery_remarks"));
                parseMultiDeliveryResponse.setIsPrepaid(jsonObject1.getString("is_prepaid"));
                parseMultiDeliveryResponse.setDeliveryCreated(jsonObject1.getString("delivery_created"));
                parseMultiDeliveryResponse.setDeliveryModified(jsonObject1.getString("delivery_modified"));
                parseMultiDeliveryResponse.setDel_time(jsonObject1.getString("del_time"));
                parseMultiDeliveryResponse.setDeliveryOrder(jsonObject1.getString("delivery_order"));

                if (jsonObject1.has("Image")) {
                    JSONArray imageArray = jsonObject1.getJSONArray("Image");
                    if (imageArray != null && imageArray.length() != 0) {
                        parseMultiDeliveryResponse.setDeliveryImage(imageArray.get(0).toString());
                    }
                }
                JSONObject jsonObject2 = jsonObject1.optJSONObject("sender");
                Sender sender = gson.fromJson(String.valueOf(jsonObject2), Sender.class);
                JSONObject jsonObject3 = jsonObject1.optJSONObject("receiver");
                Receiver receiver = gson.fromJson(String.valueOf(jsonObject3), Receiver.class);
                receiverArray.add(receiver);
                parseMultiDeliveryResponse.setReceiver(receiver);
                parseMultiDeliveryResponse.setSender(sender);
                deliveryList.add(parseMultiDeliveryResponse);
                JSONObject jsonObject4 = jsonObject1.optJSONObject("trip");
                Trip trip = gson.fromJson(String.valueOf(jsonObject4), Trip.class);
                isBegin = false;
                setupMap();
//                System.out.print(trip);
            }
            int k = 20;
            if (deliveryList != null || deliveryList.size() > 0) {
                AppData.getInstance(SlideMainActivity.this).setDeliveryList(deliveryList).saveDelivery();
            }
            for (int i = 0; i < deliveryList.size(); i++) {
                if (deliveryList.get(i).getDeliveryStatus().equals(Constants.DeliveryStatus.Completed)) {
                    Date datePickup = Utils.stringToDate(tripModel.trip.getTrip_pickup_time());
                    Date dateDrop = Utils.stringToDate(deliveryList.get(i).getDel_time());
                    ////Log.("pickupdate", "" + tripModel.trip.getTrip_pickup_time());
                    ////Log.("dateDrop", "" + tripModel.trip.getTrip_drop_time());
                    if (datePickup != null && dateDrop != null) {
                        long diffrent_between_time = dateDrop.getTime() - datePickup.getTime();
                        long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                        ////Log.("diffrent_between_tim", "" + diffrent_between_time_inMinute);
                        //if(diffrent_between_time_inMinute==0){diffrent_between_time_inMinute =1;}
                        long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                        if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                            finalTime = diffrent_between_time_inMinute + " " + "min";
                        } else if (diffrent_between_time_inMinute < 1 || diffrent_between_time_inMinute == 0) {
                            finalTime = diffrent_between_time_inSeconds + " " + "sec";

                        }
                    }
                    if (datePickup != null && dateDrop == null) {
                        String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                        Date dateDrop1 = Utils.stringToDate(dropDate);
                        long diffrent_between_time = dateDrop1.getTime() - datePickup.getTime();
                        long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                        ////Log.("diffrent_between_tim", "" + diffrent_between_time_inMinute);
                        //if(diffrent_between_time_inMinute==0){diffrent_between_time_inMinute =1;}
                        long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                        if (diffrent_between_time_inMinute != 0 && diffrent_between_time_inMinute > 0) {
                            finalTime = diffrent_between_time_inMinute + " " + "min";
                        } else if (diffrent_between_time_inMinute < 1 || diffrent_between_time_inMinute == 0) {
                            finalTime = diffrent_between_time_inSeconds + " " + "sec";

                        }
                    }


                    Intent intentFare = new Intent(getApplicationContext(), FareSummaryActivity.class);
                    intentFare.putExtra("time", tripTime);
                    intentFare.putExtra("is_delivery", "true");
//                    if (distanceCoverInKm != 0) {
//                        intentFare.putExtra("distance", distanceCoverInKm);
//                    }
                    intentFare.putExtra("delivery_id", deliveryList.get(i).getDeliveryId());
                    startActivityForResult(intentFare, 1);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTripResponseValid(TripModel tripModel) {
        AppData.getInstance(getApplicationContext()).setTripModel(tripModel);
        String ss1 = tripModel.trip.getTrip_distance();
        controller.pref.setTRIP_DISTANCE(ss1);
        String ss = controller.pref.getTRIP_DISTANCE();
        String ss2 = tripModel.trip.getTrip_pickup_time();
        controller.pref.setTRIP_PICKUP_TIME(ss2);
        String droptime = tripModel.trip.getTrip_drop_time();
        String modified_time = tripModel.trip.getTrip_modified();
        try {
            if (ss != null && ss.length() != 0 && droptime != null && droptime.length() != 0 && modified_time != null && modified_time.length() != 0) {
                controller.pref.setTIME_DISTANCE_VIEW(true);
                DistanceAndTimeViewModel modelObj = new DistanceAndTimeViewModel(ss, droptime, modified_time);
            } else {
                controller.pref.setTIME_DISTANCE_VIEW(false);
                DistanceAndTimeViewModel modelObj = new DistanceAndTimeViewModel("30", "26", "10:13");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String user_lat = tripModel.trip.getTrip_scheduled_pick_lat();
            String user_lng = tripModel.trip.getTrip_scheduled_pick_lng();
            //String TripId = controller.pref.setTripId();
            Double userlat = Double.valueOf(user_lat);
            Double userlng = Double.valueOf(user_lng);
            double driver_lat = Double.parseDouble(controller.pref.getDriver_Lat());
            double driver_lng = Double.parseDouble(controller.pref.getDriver_Lng());
            //Calculating the distance in meters
            LatLng from = new LatLng(userlat, userlng);
            LatLng to = new LatLng(driver_lat, driver_lng);
            double distanceInMeters = SphericalUtil.computeDistanceBetween(from, to);
            double pickedupDistance = controller.convertDistanceKmToUnit(distanceInMeters * 1000);
            //	For example spead is 10 meters per minute.
            float speedIs10MetersPerMinute = 20.f;
            int estimatedDriveTimeInMinutes = (int) (pickedupDistance / speedIs10MetersPerMinute);
            if (!(estimatedDriveTimeInMinutes < 1.0)) {
                String esttime = String.valueOf(estimatedDriveTimeInMinutes);

            }
            if (tripModel.tripStatus.equalsIgnoreCase(ARRIVE)) {
                if (controller.pref.getLocalTripState().equalsIgnoreCase("Go")) {
                    if (tripModel.trip.getPickup_notes() != null)
                        addressLocationView.setLocationAddress(tripModel.trip.getPickup_notes() + ", " + tripModel.trip.getTrip_from_loc());
                    else
                        addressLocationView.setLocationAddress(tripModel.trip.getTrip_from_loc());
                }
            } else if (tripModel.trip.getTrip_status().equalsIgnoreCase("begin")) {
                addressLocationView.setLocationAddress(tripModel.trip.getTrip_to_loc());

            } else {
                Log.e("tripResponseValid", "" + "" + tripModel.trip.getTrip_status());
                if (tripModel.trip.getPickup_notes() != null)
                    addressLocationView.setLocationAddress(tripModel.trip.getPickup_notes() + ", " + tripModel.trip.getTrip_from_loc());
                else
                    addressLocationView.setLocationAddress(tripModel.trip.getTrip_from_loc());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAcceptTrip(TripModel tripModel, final Fragment fragment) {
        if (controller == null || controller.getLoggedDriver() == null)
            return;
        if ((fragment instanceof RequestFragment) || (fragment instanceof AsignedRequestFragment) || (fragment instanceof RideSharingRequestFragment)) {
            if (fragment instanceof RideSharingRequestFragment && rideSharingFragment != null) {
                rideSharingFragment.dismiss();
//                if (this.tripModel != null && this.tripModel.trip != null)
//                    mTripId = this.tripModel.trip.getM_trip_id();
            }
            ArrayList<DriverConstants> constantList = controller.getConstantsList();
            String driver_wallet_cons = "0";
            String wallet_amt = controller.getLoggedDriver().getD_wallet();

            for (DriverConstants constants : constantList) {
                if (constants.getConstant_key().equalsIgnoreCase("driver_wallet_cons")) {
                    driver_wallet_cons = constants.getConstant_value();
                    break;
                }
            }

            if (driver_wallet_cons.equals("1") && (wallet_amt != null && wallet_amt.trim().length() != 0) && Integer.parseInt(wallet_amt) <= 0) {
                final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SlideMainActivity.this, R.style.AppCompatAlertDialogStyle);
                builder.setCancelable(false);
                builder.setMessage(R.string.please_add_money_in_wallet);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
            } else {
                rvRequest.setVisibility(View.GONE);
                this.tripModel = tripModel;
                if (tripModel.trip != null)
                    mTripId = tripModel.trip.getM_trip_id();
                tripModel.tripId = tripModel.trip.getTrip_id();
                tripModel.tripStatus = tripModel.trip.getTrip_status();
                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
                isTripBegin = true;
                controller.pref.setDriverStartLatitude(String.valueOf(myCurrentLocation.getLatitude()));
                controller.pref.setDriverStartLongitude(String.valueOf(myCurrentLocation.getLongitude()));
                topRelativeLayout.setVisibility(View.GONE);
                view.setVisibility(View.GONE);

                if (!isTripStatusUpdating) {
                    controller.pref.setTripIds(tripModel.tripId);

                    isTripStatusUpdating = true;
                    final String otp = genrateOtp();

                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.Keys.TRIP_ID, tripModel.tripId);
                    params.put(Constants.Keys.TRIP_STATUS, Constants.TripStatus.ACCEPT);
                    params.put(Constants.Keys.OTP, otp);
                    if (fragment instanceof RideSharingRequestFragment && mTripId != null && !mTripId.equalsIgnoreCase("") && !mTripId.equalsIgnoreCase("0") && !mTripId.equalsIgnoreCase("null"))
                        params.put("m_trip_id", mTripId);
                    else if (tripModel.trip != null && tripModel.trip.getM_trip_id() != null && !tripModel.trip.getM_trip_id().equalsIgnoreCase("0") && !tripModel.trip.getM_trip_id().equalsIgnoreCase("null"))
                        params.put("m_trip_id", tripModel.trip.getM_trip_id());

                    //System.out.println("Accept Params : " + params);
                    ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, params, Constants.Urls.URL_ACCEPT_TRIP_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
                        @Override
                        public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                            isTripStatusUpdating = false;
                            if (isUpdate) {

                                TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                                tripModel.tripStatus = Constants.TripStatus.ACCEPT;
                                tripModel.otp = otp;
                                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();

                                if (fragment instanceof RequestFragment) {
                                    RequestFragment requestFragment = (RequestFragment) fragment;
                                    if (requestFragment != null) {
                                        if (requestFragment.repeatTimerManager != null)
                                            requestFragment.repeatTimerManager.stopRepeatCall();

                                        requestFragment.onRefresh();
                                    }
                                }

                                full.setVisibility(View.VISIBLE);
                                fullbutton.setVisibility(View.VISIBLE);
                                //sendNotificationToUser(Constants.TripStatus.ACCEPT);
                                sendNotification(Constants.TripStatus.ACCEPT);
                                repeatTimerManager.stopRepeatCall();
                                repeatTimerManagerTripStatus.startRepeatCall();
                                updateUIForAcceptRequest();
                                getTrip(tripModel, false);
                            } else {
                                updateUIForRejectRequest();
                            }
                        }
                    });

                }
            }
        }
    }

    private void getMTrips() {
        if (tripModel == null || tripModel.trip == null) {
            updateNotAvailability();
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        if (tripModel.trip.getM_trip_id() != null && !tripModel.trip.getM_trip_id().equalsIgnoreCase("0") && !tripModel.trip.getM_trip_id().equalsIgnoreCase("null"))
            params.put("m_trip_id", tripModel.trip.getM_trip_id());
        else
            params.put("trip_id", tripModel.trip.getTrip_id());
        params.put("is_detail", "0");

        ServerApiCall.callWithApiKeyAndDriverId(this, params, URL_GET_MTRIPS_TRIP_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    try {
                        JSONObject objResponse = new JSONObject(data.toString());
                        if (objResponse.has("response") && !objResponse.isNull("response")) {
                            if (objResponse.get("response") instanceof JSONObject) {
                                tripMaster = TripMaster.parseTripMaster(objResponse.getJSONObject("response").toString());
                            } else if (objResponse.get("response") instanceof JSONArray && objResponse.getJSONArray("response").length() > 0) {
                                tripMaster = TripMaster.parseTripMaster(objResponse.getJSONArray("response").getJSONObject(0).toString());
                            } else {
                                tripMaster = null;
                            }
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "getMTrips: " + e.getMessage(), e);
                    }
                }
                updateNotAvailability();
            }
        });
    }

    @Override
    public void onTripBegin(TripModel tripModel) {
        this.tripModel = tripModel;

        passengerDetails.setVisibility(View.GONE);

        if (tripModel.trip != null)
            mTripId = tripModel.trip.getM_trip_id();
        AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
        riderInfoLayout.setVisibility(View.GONE);
        i = i + 1;
        repeatTimerManager.stopRepeatCall();
        repeatTimerManagerTripStatus.stopRepeatCall();
        rvRequest.setVisibility(View.GONE);
        topRelativeLayout.setVisibility(View.GONE);
        rvRequest.setVisibility(View.GONE);
        topRelativeLayout.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
//            goButton.setText(R.string.deliver);
            goButton.setText(Localizer.getLocalizerString("k_33_s4_deliver"));
        } else {
//            goButton.setText(getText(R.string.end_trip));
            goButton.setText(Localizer.getLocalizerString("k_34_s4_end_ride"));
        }

        isWaiting = controller.pref.getIsWaiting();
        if (!tripModel.trip.getIs_share().equalsIgnoreCase("1"))
            waitingView.setVisibility(View.VISIBLE);
        if (isWaiting) {

            int waitingTime = controller.pref.getWaitingTime();
            int waitingTimeTemp = 0;
            if (controller.pref.getWaitingStartTime() > 0) {
                waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
            }
            waitingTime = waitingTime + waitingTimeTemp;

//            waitinTimeView.setText("Waiting Time : " + waitingTime + " min");
            waitinTimeView.setText(Localizer.getLocalizerString("k_16_s4_waiting_time") + " " + waitingTime + " " + Localizer.getLocalizerString("k_17_s4_min"));
            waitinTimeView.setVisibility(View.VISIBLE);
//            waitingView.setText("Stop Waiting");
            waitingView.setText(Localizer.getLocalizerString("k_36_s4_stop_waiting"));
        } else {
//            waitingView.setText("Start Waiting");
            waitingView.setText(Localizer.getLocalizerString("k_35_s4_start_waiting"));
            waitinTimeView.setVisibility(View.GONE);
        }

        goButton.setVisibility(View.VISIBLE);
        fullbutton.setVisibility(View.INVISIBLE);
        full.setVisibility(View.GONE);
        addressLocationView.show();
        addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        addressLocationView.setLocationAddress(tripModel.trip.getTrip_to_loc());
        view.setVisibility(View.GONE);
        if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
//            goButton.setText(R.string.deliver);
            goButton.setText(Localizer.getLocalizerString("k_33_s4_deliver"));
        } else {
//            goButton.setText(getText(R.string.end_trip));
            goButton.setText(Localizer.getLocalizerString("k_34_s4_end_ride"));
        }
        goButton.setVisibility(View.VISIBLE);
        fullbutton.setVisibility(View.INVISIBLE);
        full.setVisibility(View.GONE);
        // if (!isBegin) {
        isPickRouteDrow = true;
        isBeginRouteDrow = false;
        is_required_drawRoute = "Yes2";
        if (!isBegin) {
            isBegin = true;
            if (mGoogleMap != null) {
                mGoogleMap.clear();
                markerDriverCarD = null;
                Log.d(TAG, "onTripBegin: animateCarOnMap");
                animateCarOnMap(latLngPublishRelay);
                // todo check why it's clearing routes
//                TrackRouteSaveData.getInstance(SlideMainActivity.this).clearData();
                clearCoveredRouteD();
                AppData.getInstance(getApplicationContext()).setTripModel(tripModel).saveTripTrip();
                setupMap();
                //  clearGoogleRoutePolyLine();
            }
        }

//        if (pickUpLocationReceiver != null) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(pickUpLocationReceiver);
//        }
        showDistanceAndTimeView();
        startDistanceServiceWithDistance();
    }

    @Override
    public void onTripAccept(TripModel tripModel) {
        if (!repeatTimerManagerTripStatus.isRunning()) {
            repeatTimerManagerTripStatus.startRepeatCall();
        }
        if (repeatTimerManager.isRunning()) {
            repeatTimerManager.stopRepeatCall();
        }

        if (tripModel != null && tripModel.trip != null)
            mTripId = tripModel.trip.getM_trip_id();

        controller.pref.setLocalTripState("Go");
        if (tripModel.tripStatus.equalsIgnoreCase(ACCEPT)) {
            if (tripModel.trip.getTrip_customer_details() != null && !tripModel.trip.getTrip_customer_details().equalsIgnoreCase("null"))
                passengerDetails.setVisibility(View.VISIBLE);
            else
                passengerDetails.setVisibility(View.GONE);
            riderName.setText(tripModel.user.getU_fname() + " " + tripModel.user.getU_lname());
            callRider.setContentDescription(tripModel.user.getU_phone());
            String fullpath = Constants.IMAGE_BASE_URL + tripModel.user.getUProfileImagePath();
            Picasso.get().load(fullpath).error(R.drawable.user).into(riderProfile);
            riderInfoLayout.setVisibility(View.VISIBLE);
            rvRequest.setVisibility(View.GONE);
            topRelativeLayout.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("null") && tripModel.trip.getIs_delivery().equals("1")) {
                getDeliveryRequests();
            }
            if (controller.pref.getLocalTripState().equalsIgnoreCase("Pick")) {
//                fullbutton.setText(R.string.pick);
                fullbutton.setText(Localizer.getLocalizerString("k_20_s4_pick"));

            } else {
                fullbutton.setText(R.string.go);
                fullbutton.setText(Localizer.getLocalizerString("k_19_s4_arrived"));
            }
            is_required_drawRoute = "Yes1";
            fullbutton.setVisibility(View.VISIBLE);
            goButton.setVisibility(View.GONE);
            getMTrips();
        }
        addressLocationView.show();
        addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
        if (tripModel.trip.getPickup_notes() != null)
            addressLocationView.setLocationAddress(tripModel.trip.getPickup_notes() + ", " + tripModel.trip.getTrip_from_loc());
        else
            addressLocationView.setLocationAddress(tripModel.trip.getTrip_from_loc());
    }

    @Override
    public void onTripCancel(TripModel tripModel) {
        repeatTimerManagerTripStatus.stopRepeatCall();

        controller.pref.setTripId("");
        controller.pref.setString("trip_response", null);
        controller.pref.setTripIds("");
        controller.pref.setTripStartTime("");
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
        PreferencesUtils.setFloat(SlideMainActivity.this, R.string.recorded_distance, 0);
        controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(0));
        controller.pref.setTRIP_DISTANCE(String.valueOf(0));
        controller.pref.setTRIP_DISTANCE_CHANGEABLE(String.valueOf(0));
        controller.pref.setTRIP_DURATION_CHANGEABLE_INT(0);
        controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(0));
        controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(0));

        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
        AppData.getInstance(getApplicationContext()).clearTrip();
        this.tripModel = null;
        controller.setIsNotificationCome(false);
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();

        fullbutton.setText("");
        goButton.setText("");

        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }

        showRiderDialog();

    }

    @Override
    public void onTripEnd(TripModel tripModel) {
        repeatTimerManager.stopRepeatCall();
        repeatTimerManagerTripStatus.stopRepeatCall();
        clearBeginTripDistanceAndTime();
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
//        stopLocationRecordService();
        //driverUpdateProfile("1", false, "0");
        goButton.setText("");
        goButton.setVisibility(View.GONE);
        waitinTimeView.setVisibility(View.GONE);
        waitingView.setVisibility(View.GONE);
        SingleObject obj = SingleObject.getInstance();
        obj.setFareSummaryLinearLayout(false);
        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
        destinationActivity.view.setVisibility(View.GONE);
        TrackRouteSaveData.getInstance(this).clearData();
//        if (markerDriverCar != null) {
//            markerDriverCar.remove();
//        }

        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }

        if (!isFareSummary) {
            isFareSummary = true;
            Intent intent = new Intent(getApplicationContext(), FareSummaryActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onTripRequest(TripModel tripModel) {
        isPickRouteDrow = false;
        isBeginRouteDrow = false;
        controller.pref.setLocalTripState("NO");

        if (controller.isFromBackground) {
            controller.stopNotificationSound();
            controller.playNotificationSound(R.raw.trip_request);
        }

        if (sideDrawerManager != null)
            sideDrawerManager.setDrawerLockModeClosed();
        if (drawerbut != null)
            drawerbut.setVisibility(View.GONE);

        onTripCancel(tripModel);
    }

    @Override
    public void onTripError(String message) {
        controller.pref.setTripId("");
        controller.pref.setString("trip_response", null);
        controller.pref.setTripIds("");
        controller.pref.setTripStartTime("");
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
        PreferencesUtils.setFloat(SlideMainActivity.this, R.string.recorded_distance, 0);
        controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(0));
        controller.pref.setTRIP_DISTANCE(String.valueOf(0));
        controller.pref.setTRIP_DISTANCE_CHANGEABLE(String.valueOf(0));
        controller.pref.setTRIP_DURATION_CHANGEABLE_INT(0);
        controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(0));
        controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(0));
        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();

        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
        AppData.getInstance(getApplicationContext()).clearTrip();
        this.tripModel = null;
        controller.setIsNotificationCome(false);
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
        controller.setIsNotificationCome(false);
        //TODO handle trip on expired trip data
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();


        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }

    }

    @Override
    public void onTripDriverCancelAtDrop(TripModel tripModel) {
        if (repeatTimerManager != null)
            repeatTimerManager.stopRepeatCall();
        if (repeatTimerManagerTripStatus != null)
            repeatTimerManagerTripStatus.stopRepeatCall();
        clearBeginTripDistanceAndTime();
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
//        stopLocationRecordService();
        //driverUpdateProfile("1", false, "0");

        waitingView.setVisibility(View.GONE);

        goButton.setText("");
        goButton.setVisibility(View.GONE);
        waitingView.setVisibility(View.GONE);
        waitinTimeView.setVisibility(View.GONE);

        SingleObject obj = SingleObject.getInstance();
        obj.setFareSummaryLinearLayout(false);
        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
        destinationActivity.view.setVisibility(View.GONE);
        TrackRouteSaveData.getInstance(this).clearData();
//        if (markerDriverCar != null) {
//            markerDriverCar.remove();
//        }
        try {
            customHandler.removeCallbacks(updateTimerThread);
        } catch (Exception e) {
            e.printStackTrace();
        }


        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }


        if (!isFareSummary) {
            isFareSummary = true;
            Intent intent = new Intent(getApplicationContext(), FareSummaryActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            finish();
        }


//        controller.pref.setTIME_DISTANCE_VIEW(false);
//        repeatTimerManagerTripStatus.stopRepeatCall();
//        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
//        goButton.setText("");
//        PreferencesUtils.setBoolean(this, R.string.is_recording_start, true);
//
////        driverUpdateProfile("1", false, false);
////        switchAvailable1.setChecked(true);
//        if (distanceAndTimeView != null)
//            distanceAndTimeView.hide();
//        if (addressLocationView != null)
//            addressLocationView.hide();
//        if (mGoogleMap != null)
//            mGoogleMap.clear();
//        if (backToOrderView != null)
//            backToOrderView.hide();
//        clearBeginTripDistanceAndTime();
//        try {
//            unregisterReceiver(mHandleMessageReceiver);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        goButton.setText("");
//        SingleObject obj = SingleObject.getInstance();
//        obj.setFareSummaryLinearLayout(false);
//        controller.pref.setIS_FARE_SUMMARY_OPEN("yes_open");
//        destinationActivity.view.setVisibility(View.GONE);
//        try {
//            customHandler.removeCallbacks(updateTimerThread);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////        stopLocationRecordService();
//        Intent intent = new Intent(getApplicationContext(), FareSummaryActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//
//        startActivity(intent);
//        finish();
    }

    @Override
    public void onTripDriverCancelAtPickup(TripModel tripModel) {
        repeatTimerManagerTripStatus.stopRepeatCall();

        controller.pref.setTripId("");
        controller.pref.setString("trip_response", null);
        controller.pref.setTripIds("");
        controller.pref.setTripStartTime("");
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
        PreferencesUtils.setFloat(SlideMainActivity.this, R.string.recorded_distance, 0);
        controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(0));
        controller.pref.setTRIP_DISTANCE(String.valueOf(0));
        controller.pref.setTRIP_DISTANCE_CHANGEABLE(String.valueOf(0));
        controller.pref.setTRIP_DURATION_CHANGEABLE_INT(0);
        controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(0));
        controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(0));
        controller.pref.setTIME_DISTANCE_VIEW(false);
        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();

        controller.pref.setIS_FARE_SUMMARY_OPEN("not_open");
        AppData.getInstance(getApplicationContext()).clearTrip();
        this.tripModel = null;
        controller.setIsNotificationCome(false);
        PreferencesUtils.setBoolean(this, R.string.is_recording_start, false);
        controller.pref.setIsWaiting(false);

        if (count_downt_timer != null) {
            count_downt_timer.cancel();
        }

        driverUpdateProfile("1", false, false);

        fullbutton.setText("");
        goButton.setText("");
        waitingView.setVisibility(View.GONE);

        controller.pref.setWaitingTime(0);
        controller.pref.setIsWaiting(false);
        isWaiting = controller.pref.getIsWaiting();

        if (waitingHandler != null && waitingTimerThread != null) {
            try {
                waitingHandler.removeCallbacks(waitingTimerThread);
            } catch (Exception ignored) {
            }
        }

        //for lock the screen before click accept or decline button

//        stopLocationRecordService();
        Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onTripArrive(TripModel tripModel) {
        if (repeatTimerManager.isRunning()) {
            repeatTimerManager.stopRepeatCall();
        }

        if (tripModel.trip.getTrip_customer_details() != null && !tripModel.trip.getTrip_customer_details().equalsIgnoreCase("null"))
            passengerDetails.setVisibility(View.VISIBLE);
        else
            passengerDetails.setVisibility(View.GONE);

        if (tripModel != null && tripModel.trip != null)
            mTripId = tripModel.trip.getM_trip_id();

        riderName.setText(tripModel.user.getU_fname() + " " + tripModel.user.getU_lname());
        callRider.setContentDescription(tripModel.user.getU_phone());
        String fullpath = Constants.IMAGE_BASE_URL + tripModel.user.getUProfileImagePath();

        Picasso.get().load(fullpath).error(R.drawable.user).into(riderProfile);
        riderInfoLayout.setVisibility(View.VISIBLE);
        if (controller.pref.getLocalTripState().equalsIgnoreCase("Pick")) {
            fullbutton.setText("");
            fullbutton.setVisibility(View.INVISIBLE);

            if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
//                goButton.setText(R.string.begin_delivery);
                goButton.setText(Localizer.getLocalizerString("k_30_s4_begin_delivery"));
            } else {
//                goButton.setText(R.string.begin_trip);
                goButton.setText(Localizer.getLocalizerString("k_31_s4_begin_ride"));
            }
            is_required_drawRoute = "Yes1";
            goButton.setVisibility(View.VISIBLE);
            full.setVisibility(View.GONE);
            rvRequest.setVisibility(View.GONE);
            topRelativeLayout.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
            addressLocationView.setLocationAddress(tripModel.trip.getTrip_to_loc());

        } else if (controller.pref.getLocalTripState().equalsIgnoreCase("Go")) {
            is_required_drawRoute = "Yes1";
            // show
            fullbutton.setText(Localizer.getLocalizerString("k_20_s4_pick"));
            full.setVisibility(View.VISIBLE);
            fullbutton.setVisibility(View.VISIBLE);
            addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
            if (tripModel.trip.getPickup_notes() != null)
                addressLocationView.setLocationAddress(tripModel.trip.getPickup_notes() + ", " + tripModel.trip.getTrip_from_loc());
            else
                addressLocationView.setLocationAddress(tripModel.trip.getTrip_from_loc());            // hide
            goButton.setText("");
            goButton.setVisibility(View.GONE);
            rvRequest.setVisibility(View.GONE);
            topRelativeLayout.setVisibility(View.GONE);
            view.setVisibility(View.GONE);

        } else {
            // show
            fullbutton.setText(Localizer.getLocalizerString("k_19_s4_arrived"));
            full.setVisibility(View.VISIBLE);
            fullbutton.setVisibility(View.VISIBLE);
            addressLocationView.setLocationTitle(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
            if (tripModel.trip.getPickup_notes() != null)
                addressLocationView.setLocationAddress(tripModel.trip.getPickup_notes() + ", " + tripModel.trip.getTrip_from_loc());
            else
                addressLocationView.setLocationAddress(tripModel.trip.getTrip_from_loc());
            // hide
            goButton.setText("");
            goButton.setVisibility(View.GONE);
            rvRequest.setVisibility(View.GONE);
            topRelativeLayout.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        }
        addressLocationView.show();

        //showDistanceAndTimeView();
        clearBeginTripDistanceAndTime();
        if (!isPickRouteDrow) {
            setupMap();
        }
    }

    @Override
    public void onCallRider(TripModel tripModel) {
        if (tripModel != null && tripModel.user != null) {
            String phone_no = tripModel.user.getU_phone();
            if (tripModel.user.getC_code() != null && !tripModel.user.getC_code().equalsIgnoreCase("null") && !tripModel.user.getC_code().trim().isEmpty()) {
                phone_no = "+" + tripModel.user.getC_code() + phone_no;
            }

            if (phone_no == null) {
                Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_43_s4_no_number"), Toast.LENGTH_SHORT).show();
            } else {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone_no));
                if (callIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(callIntent);
                }
            }
        }
    }

    @Override
    public void onCurrentTripChange(TripModel tripModel) {
        if (isChangingTrip)
            return;
        isChangingTrip = true;
        showProgressBar();
        tripModel.tripId = tripModel.trip.getTrip_id();
        tripModel.tripStatus = tripModel.trip.getTrip_status();
        tripModel.otp = tripModel.trip.getOtp();
        AppData.getInstance(controller).setTripModel(tripModel).saveTripTrip();
        controller.pref.setTripIds(tripModel.tripId);

        Intent intent = new Intent(this, SlideMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

//        hideProgressBar();
        startActivity(intent);
    }

    @Override
    public void showProgress() {
        showProgressBar();
    }

    @Override
    public void onAcceptRSTrip(TripModel tripModel) {
        if (controller == null || controller.getLoggedDriver() == null || isAcceptingRSRTrip)
            return;
        if (rideSharingFragment != null)
            rideSharingFragment.dismiss();
        isAcceptingRSRTrip = true;
        final String otp = genrateOtp();
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.Keys.TRIP_ID, tripModel.trip.getTrip_id());
        params.put(Constants.Keys.TRIP_STATUS, Constants.TripStatus.ACCEPT);
        params.put(Constants.Keys.OTP, otp);
        if (mTripId != null && !mTripId.equalsIgnoreCase("") && !mTripId.equalsIgnoreCase("0") && !mTripId.equalsIgnoreCase("null"))
            params.put("m_trip_id", mTripId);

        //System.out.println("Accept Params : " + params);
        ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, params, Constants.Urls.URL_ACCEPT_TRIP_REQUEST, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                isAcceptingRSRTrip = false;
                if (isUpdate) {

                    tripModel.tripStatus = Constants.TripStatus.ACCEPT;
                    tripModel.trip.setTrip_status(Constants.TripStatus.ACCEPT);
                    tripModel.otp = otp;

                    sendNotification(Constants.TripStatus.ACCEPT);

                    continueCurrentTripDialog(tripModel);
                }
            }
        });

    }

    private void startDistanceServiceWithDistance() {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.tripStatus.equals(Constants.TripStatus.BEGIN)) {
            if (!PreferencesUtils.getBoolean(this, R.string.is_recording_start, false)) {
                PreferencesUtils.setBoolean(this, R.string.is_recording_start, true);
                PreferencesUtils.getBoolean(this, R.string.is_recording_start, true);
                if (!isAlready) {
                    PreferencesUtils.setFloat(this, R.string.recorded_distance, 0);
                }

            }
            controller.pref.setCalculatedDistance(PreferencesUtils.getFloat(this, R.string.recorded_distance) / 1000);

        }
    }

    private void startLocationService() {
        Intent serviceIntent = new Intent(this, LocationService.class);
        boolean recordingServiceRunning = TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class);
        if (!recordingServiceRunning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(serviceIntent);
            } else {
                startService(serviceIntent);
            }
        }
//        else {
//            stopLocationRecordService();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForegroundService(serviceIntent);
//            } else {
//                startService(serviceIntent);
//            }
//        }
    }

    private void calculateDistances() {
        //progressDialog = new CustomProgressDialog(getApplicationContext());

        //  progressDialog.showDialog();
        double finalCurrentLat = myCurrentLocation.getLatitude();
        double finalCurrentLng = myCurrentLocation.getLongitude();
        String origin = String.format(Locale.ENGLISH, "%f,%f", pikLat, pikLng);
        String destination = String.format(Locale.ENGLISH, "%f,%f", finalCurrentLat, finalCurrentLng);
        url = calculateDistance(origin, destination);
        parseDistanceMatrixResponse();
    }

    private void clearBeginTripDistanceAndTime() {
        controller.pref.setTripStartTime("");
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
//        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();
    }

    private void setBeginTripDistanceAndTime() {
        DateFormat df = AppUtil.getDateFormat();
        controller.pref.setTripStartTime(df.format(new Date()));
        controller.pref.setCalculateDistance(true);
        controller.pref.setCalculatedDistance(0);
//        TrackRouteSaveData.getInstance(getApplicationContext()).clearData();
    }

    private String calculateDistance(String origins, String destinations) {
        String gKey = controller.getConstantsValueForKey("gkey");
        /*if (gKey == null || gKey.equalsIgnoreCase("null") || gKey.trim().length() == 0) {
            gKey = getString(R.string.googleapikey);
        }*/

        return "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" +
                origins +
                "&destinations=" +
                destinations +
                "&key=" + gKey;
    }

    private void parseDistanceMatrixResponse() {

        progressDialog.showDialog();
        //Log.("url", "" + url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    private Integer getLastDestToOriginDistance = 0;

                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject result = null;
                        try {
                            result = new JSONObject(response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Gson gson = new Gson();
                        DistanceMatrixResponse distanceMatrixResponse = gson.fromJson(response.toString(), DistanceMatrixResponse.class);
                        if (distanceMatrixResponse.getStatus().equals("INVALID_REQUEST")) {
                            progressDialog.dismiss();
                            Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_40_s4_plz_sel_source_or_dest"), Toast.LENGTH_SHORT).show();
                        } else {
                            if (distanceMatrixResponse.getStatus().equalsIgnoreCase("OK")) {
                                try {
                                    dropAddress = getCompleteAddressString(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());

                                    List<Element> elements;
                                    elements = distanceMatrixResponse.getRows().get(0).getElements();
                                    for (Element e : elements) {
                                        if (e.getStatus().equals("NOT_FOUND")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(SlideMainActivity.this, Localizer.getLocalizerString("k_40_s4_plz_sel_source_or_dest"), Toast.LENGTH_SHORT).show();
                                        } else {
                                            if (e.getDistance() != null && e.getDistance().getValue() != null) {
                                                getLastDestToOriginDistance = e.getDistance().getValue();
                                                getLastDestToOriginDistance = (int) (getLastDestToOriginDistance * 0.001f);
                                            }
                                            if (getLastDestToOriginDistance == 0) {
                                                try {
                                                    getLastDestToOriginDistance = Integer.valueOf(String.valueOf(distanceCoverInKm));
                                                } catch (Exception e1) {
                                                    e1.printStackTrace();
                                                }
                                            }
                                            if (tripModel != null && tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().length() != 0 && tripModel.trip.getIs_delivery().equals("1")) {
                                                showDeliveryListView();
                                            } else {
                                                updateTripStatusApi(Constants.TripStatus.END, "");
                                            }

                                        }

                                    }

                                } catch (ArrayIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                    //Log.d("Error", "Size is zero");
                                }
                            } else {

                                if (distanceMatrixResponse.getStatus().equalsIgnoreCase("REQUEST_DENIED") || distanceMatrixResponse.getStatus().equalsIgnoreCase("OVER_QUERY_LIMIT")) {
                                    try {
                                        progressDialog.dismiss();
                                        Toast.makeText(SlideMainActivity.this, Objects.requireNonNull(result).getString("error_message"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(SlideMainActivity.this);
        requestQueue.add(jsonObjReq);
    }

    private void stopLocationRecordService() {
        if (TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class)) {
            Intent serviceIntent = new Intent(this, LocationService.class);
            stopService(serviceIntent);
        }
    }

    private void updateTripStatusApi(final String trip_status, final String trip_cancel_reason) {
        if (Utils.net_connection_check(getApplicationContext())) {
            if (!isTripStatusUpdating) {
                isTripStatusUpdating = true;
                final TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                if (tripModel == null) {
                    return;
                }
                tripModel.tripStatus = trip_status;
                tripModel.trip_reason = trip_cancel_reason;
                if (trip_status.equals(Constants.TripStatus.BEGIN)) {
                    tripModel.actual_pickup_lat = String.valueOf(myCurrentLocation.getLatitude());
                    tripModel.actual_pickup_lng = String.valueOf(myCurrentLocation.getLongitude());
                }
                if (trip_status.equals(Constants.TripStatus.END) || trip_status.equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
                    int waitingTime = controller.pref.getWaitingTime();

                    if (controller.pref.getIsWaiting()) {
                        int waitingTimeTemp = 0;
                        if (controller.pref.getWaitingStartTime() > 0) {
                            waitingTimeTemp = (int) ((Calendar.getInstance().getTimeInMillis() - controller.pref.getWaitingStartTime()) / (60 * 1000));
                        }
                        waitingTime = waitingTime + waitingTimeTemp;
                        controller.pref.setWaitingTime(waitingTime);
                        Log.e("waitnig", "" + waitingTime);
                        controller.pref.setWaitingStartTime(0);
                        waitinTimeView.setText(getString(R.string.waiting_time) + waitingTime + getString(R.string.minute));
                        waitinTimeView.setVisibility(View.GONE);

                        if (waitingHandler != null) {
                            try {
                                waitingHandler.removeCallbacks(waitingTimerThread);

                            } catch (Exception ignore) {

                            }
                        }
                    }
                    CategoryActors driverCat = null;
                    for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                        if (categoryActors.getCategory_id().equalsIgnoreCase(tripModel.driver.getCategory_id())) {
                            driverCat = categoryActors;
                        }
                    }
                    if (driverCat != null) {
                        int freeWaitTime = 0;
                        float fareWaitTime = 0;
                        freeWaitTime = driverCat.getWait_free_min();
                        fareWaitTime = driverCat.getWait_per_min();
                        if (waitingTime <= freeWaitTime) {
                            waitingTime = 0;
                        } else {
                            waitingTime = waitingTime - freeWaitTime;
                        }
                        tripModel.wait_duration = String.valueOf(waitingTime);

                        TripFareCalculator tripFareCalculator = new TripFareCalculator(controller, distanceCoverInKm, Objects.requireNonNull(driverCat).getCat_special_fare());
                        TripFareCalculator.TripFare tripFare = tripFareCalculator.calculateFare(waitingTime);
                        tripModel.tripAmount = String.format(Locale.ENGLISH, "%.02f", (tripFare.getTotalPrice()));
                        tripModel.taxAmount = String.format(Locale.ENGLISH, "%.02f", tripFare.getTripTax());
                        tripModel.minutes = tripFareCalculator.getTotalMinutes();
                    }

                    tripModel.tripDistance = String.format(Locale.ENGLISH, "%.02f", (float) controller.convertDistanceKmToUnit(distanceCoverInKm));
                    tripModel.tripDistanceUnit = controller.checkCityDistanceUnit();
                    tripModel.drop_time = AppUtil.getCurrentDateInGMTZeroInServerFormat();

                    tripModel.actual_drop_lat = String.valueOf(myCurrentLocation.getLatitude());
                    tripModel.actual_drop_lng = String.valueOf(myCurrentLocation.getLongitude());

                }
                if (trip_status.equals(Constants.TripStatus.BEGIN)) {
                    tripModel.pick_time = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                    tripModel.otp = genrateOtp();
                }
                cancelPreviousCallingTripApi();
                ////Log.("tripmodelParams", "" + tripModel.getParams());
                ServerApiCall.callWithApiKey(this, tripModel.getParams(1), Constants.Urls.UPDATE_TRIP_URL, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        isTripStatusUpdating = false;
                        if (isUpdate) {
                            handleTripUpdateResponse(data, tripModel, trip_status);
                        } else {
                            repeatTimerManagerTripStatus.setTimerForRepeat();
                        }
                    }
                });
            }
        }
    }

    private boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void handleTripUpdateResponse(Object data, TripModel tripModel, String trip_status) {
        tripModel.tripStatus = trip_status;
        AppData.getInstance(getApplicationContext()).saveTripTrip();
        JSONObject jsonRootObject;
        try {
            jsonRootObject = new JSONObject(data.toString());
            int response = jsonRootObject.getInt(Constants.Keys.RESPONSE);
            if (response == 1) {
                TripController tripController = new TripController(tripModel);
                tripController.setTripControllerCallBack(SlideMainActivity.this);
                if (trip_status.equals(Constants.TripStatus.END) || trip_status.equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
                    controller.pref.setWaitingTime(0);
                    controller.pref.setTRIP_PAY_AMOUNT("" + tripModel.trip.getTrip_pay_amount());
                    isRequest = true;
                    progressDialog.showDialog();
                    isStopMethodCalled = true;
                    tripController.setChnageTripStatusLocal(trip_status);
                } else {
                    repeatTimerManagerTripStatus.setTimerForRepeat();
                    tripController.setChnageTripStatus(trip_status);
                    tripController.setChnageTripStatusLocal(trip_status);
                }
                if (trip_status.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                    setBeginTripDistanceAndTime();

                }
                sendNotification(trip_status);
                // sendNotificationToUser(trip_status);
                //     update trip
            } else {

                repeatTimerManagerTripStatus.setTimerForRepeat();
                // not update
            }
        } catch (JSONException e) {
            e.printStackTrace();
            repeatTimerManagerTripStatus.setTimerForRepeat();
        }
    }

    private void driverUpdateProfile(String is_available, final boolean isdeactivate, final boolean isFromWallet) {
        if (!isCalling) {
            try {
                progressDialog.showDialog();
                isCalling = true;
                final Controller controller = (Controller) getApplicationContext();
                String refreshedToken = Controller.getSaveDiceToken();
                Map<String, String> params = new HashMap<>();
                String trip_response = Controller.getInstance().pref.getString("trip_response");

                params.put(Constants.Keys.D_IS_AVAILABLE, is_available);
                if (refreshedToken != null) {
                    params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
                }
                if (isdeactivate) {
                    params.put("active", "0");
                }
                params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);

                params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
                params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
                params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

                Log.e("driverUpdateProfile", "" + params);
                if (requestQueue != null) {
                    requestQueue.cancelAll(upudateProfileApiTag);
                }
                cancelUserUpdateApiCall();
                requestQueue = ServerApiCall.callWithApiKeyAndDriverId(SlideMainActivity.this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
                    @Override
                    public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                        isCalling = false;
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        if (!isOnPauseMethodCalled) {
//                            repeatTimerManagerUpdateLocation.setTimerForRepeat();
//                        }
                        if (isUpdate) {
                            String response = data.toString();
                            ErrorJsonParsing parser = new ErrorJsonParsing();
                            CloudResponse res = parser.getCloudResponse("" + response);
                            if (res.isStatus()) {
                                Driver driver = Driver.parseJsonAfterLogin(response);
                                controller.setDriver(driver, getApplicationContext());
                                String d_is_available = Objects.requireNonNull(driver).getD_is_available();
                                controller.pref.setD_IS_AVAILABLE(d_is_available);
                                checkOfflineStatus();
                                if (isFromWallet) {
                                    Toast.makeText(controller, Localizer.getLocalizerString("k_41_s4_insuf_bal_wallet"), Toast.LENGTH_SHORT).show();
                                }

                                tripRequestsRequest();

                                if (isdeactivate) {
                                    logoutApi(controller);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                            }
                        }  //  Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();

                        if (isCancel) {
                            AppData.getInstance(getApplicationContext()).clearTrip();
                            SlideMainActivity.this.tripModel = null;
                            controller.pref.setTripIds("");
                            Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, upudateProfileApiTag);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNotificationToUser() {
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.user != null) {
            String u_device_type = tripModel.user.getU_device_type();
            Map<String, String> params = new HashMap<>();
            String notificationMessage;
            if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar"))
                notificationMessage = Constants.Message_ar.DRIVER_CANCEL;
            else notificationMessage = Constants.Message.DRIVER_CANCEL;

//            notificationMessage = Constants.Message.DRIVER_CANCEL;

            params.put("message", notificationMessage);
            params.put("trip_id", tripModel.trip.getTrip_id());
            params.put("trip_status", "p_cancel_pickup");
            if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                params.put(Constants.Keys.ANDROID, tripModel.user.getU_device_token());
            } else {
                params.put(Constants.Keys.IOS, tripModel.user.getU_device_token());
            }
            notificationStatusApi(params);
        }
    }

    private void notificationStatusApi(final Map<String, String> params) {
        WebServiceUtil.excuteRequest(getApplicationContext(), params, Constants.Urls.URL_COMMAN_NOTIFICATION, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
            }
        });
    }

    private void sendNotification(String tripStatus) {

        //Log.d("tripStatus", "" + tripStatus);
        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        if (tripModel != null && tripModel.user != null) {
            String u_device_type = tripModel.user.getU_device_type();
            Map<String, String> params = new HashMap<>();
            String notificationMessage = "change message:";
            String deliveryId;
            if (tripStatus.equalsIgnoreCase(ACCEPT)) {
                if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.ACCEPTED_DELIVERY;
                    } else {
                        notificationMessage = Constants.Message.ACCEPTED_DELIVERY;
                    }
                } else {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.ACCEPTED;
                    } else {
                        notificationMessage = Constants.Message.ACCEPTED;
                    }
                }

            } else if (tripStatus.equalsIgnoreCase(Constants.TripStatus.DRIVER_CANCEL_AT_PICKUP)) {
                if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.DRIVER_CANCEL_DELIVERY;
                    } else {
                        notificationMessage = Constants.Message.DRIVER_CANCEL_DELIVERY;
                    }
                } else {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.DRIVER_CANCEL;
                    } else {
                        notificationMessage = Constants.Message.DRIVER_CANCEL;
                    }
                }
            } else if (tripStatus.equalsIgnoreCase("go")) {
                if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.DRIVER_REQUEST_ACCEPT_DELIVERY;
                    } else {
                        notificationMessage = Constants.Message.DRIVER_REQUEST_ACCEPT_DELIVERY;
                    }
                } else {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.DRIVER_REQUEST_ACCEPT;
                    } else {
                        notificationMessage = Constants.Message.DRIVER_REQUEST_ACCEPT;
                    }
                }
            } else if (tripStatus.equalsIgnoreCase(Constants.TripStatus.END) || tripStatus.equalsIgnoreCase(Constants.TripStatus.DRIVER_CANCEL_AT_DROP)) {
                if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.END_DELIVERY;
                    } else {
                        notificationMessage = Constants.Message.END_DELIVERY;
                    }
                } else {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.END;
                    } else {
                        notificationMessage = Constants.Message.END;
                    }
                }
                if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("null") && tripModel.trip.getIs_delivery().equals("1") && deliveryList.size() > 0) {
                    for (int i = 0; i < deliveryList.size(); i++) {
                        if (receiverArray.get(i).getUDeviceToken() != null && !receiverArray.get(i).getUDeviceToken().equalsIgnoreCase("")) {

                            if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                                if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                                    notificationMessage = Constants.Message_ar.DRIVER_ARIVED_DELIVERY;
                                } else {
                                    notificationMessage = Constants.Message.DRIVER_ARIVED_DELIVERY;
                                }
                            } else {
                                if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                                    notificationMessage = Constants.Message_ar.DRIVER_ARIVED;
                                } else {
                                    notificationMessage = Constants.Message.DRIVER_ARIVED;
                                }
                            }

                            deliveryId = deliveryList.get(i).getDeliveryId();
                            Log.e("deliveryId", "" + deliveryId);
                            if (deliveryId != null) {
                                params.put("delivery_id", deliveryId);
                                params.put("message", notificationMessage);
                                params.put("trip_id", tripModel.trip.getTrip_id());
                                params.put("trip_status", tripStatus);
                                if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                                    params.put(Constants.Keys.ANDROID, receiverArray.get(i).getUDeviceToken());
                                } else {
                                    params.put(Constants.Keys.IOS, receiverArray.get(i).getUDeviceToken());
                                }
                                notificationStatusApiAfterTripBegin(params);
                            }
                        }
                    }
                }
            } else if (tripStatus.equalsIgnoreCase(ARRIVE)) {

                if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.ARRIVE_DELIVERY;
                    } else {
                        notificationMessage = Constants.Message.ARRIVE_DELIVERY;
                    }
                } else {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.ARRIVE;
                    } else {
                        notificationMessage = Constants.Message.ARRIVE;
                    }
                }

            } else if (tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {

                if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.BEGIN_DELIVERY;
                    } else {
                        notificationMessage = Constants.Message.BEGIN_DELIVERY;
                    }
                } else {
                    if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                        notificationMessage = Constants.Message_ar.BEGIN;
                    } else {
                        notificationMessage = Constants.Message.BEGIN;
                    }
                }

                if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("null") && tripModel.trip.getIs_delivery().equals("1") && deliveryList.size() > 0) {
                    for (int i = 0; i < deliveryList.size(); i++) {
                        if (receiverArray != null && (receiverArray.get(i).getUDeviceToken() != null && !receiverArray.get(i).getUDeviceToken().equalsIgnoreCase(""))) {
                            String notificationMessage1 = "";

                            if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                                if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                                    notificationMessage1 = Constants.Message_ar.SOME_ONE_SOMETHING_DELIVERY;
                                } else {
                                    notificationMessage1 = Constants.Message.SOME_ONE_SOMETHING_DELIVERY;
                                }
                            } else {
                                if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                                    notificationMessage1 = Constants.Message_ar.SOME_ONE_SOMETHING;
                                } else {
                                    notificationMessage1 = Constants.Message.SOME_ONE_SOMETHING;
                                }
                            }

                            deliveryId = deliveryList.get(i).getDeliveryId();
                            Log.e("deliveryId", "" + deliveryId);
                            if (deliveryId != null) {
                                params.put("delivery_id", deliveryId);
                                params.put("message", notificationMessage1);
                                params.put("trip_id", tripModel.trip.getTrip_id());
                                params.put("trip_status", tripStatus);
                                if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                                    params.put(Constants.Keys.ANDROID, receiverArray.get(i).getUDeviceToken());
                                } else {
                                    params.put(Constants.Keys.IOS, receiverArray.get(i).getUDeviceToken());
                                }
                                notificationStatusApiAfterTripBegin(params);
                            }
                        }
                    }
                } else {

                    if (tripModel.trip != null && tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equalsIgnoreCase("1")) {
                        if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                            notificationMessage = Constants.Message_ar.BEGIN_DELIVERY;
                        } else {
                            notificationMessage = Constants.Message.BEGIN_DELIVERY;
                        }
                    } else {
                        if (tripModel.user != null && tripModel.user.getU_lang() != null && tripModel.user.getU_lang().equalsIgnoreCase("ar")) {
                            notificationMessage = Constants.Message_ar.BEGIN;
                        } else {
                            notificationMessage = Constants.Message.BEGIN;
                        }
                    }

                }
            }
            params.put("message", notificationMessage);
            params.put("trip_id", tripModel.trip.getTrip_id());
            params.put("trip_status", tripStatus);
            if (tripStatus.equals(ARRIVE))
                params.put("sound", "cab_arrive.caf");
            if (u_device_type != null) {
                if (u_device_type.equalsIgnoreCase(Constants.Keys.ANDROID)) {
                    params.put(Constants.Keys.ANDROID, tripModel.user.getU_device_token());
                } else {
                    params.put(Constants.Keys.IOS, tripModel.user.getU_device_token());
                }
            }
            ////Log.("accept Trip params", "" + params);
            notificationStatusApi(params);
        }
    }

    private void notificationStatusApiAfterTripBegin(final Map<String, String> params) {
        Log.d("notificationParams", "" + params);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> driverNOtificationCall;
        driverNOtificationCall = apiInterface.sendRiderNotrification(params);
        driverNOtificationCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    private void drawDriverRoute(double destLat1, double destLng1, double originLat, double originLng, final int routeType) {
        if (isCallingForGetRoute) {
            return;
        }

        isCallingForGetRoute = true;
        LatLng dest1 = new LatLng(destLat1, destLng1);
        if (originLat == 0.0 || originLng == 0.0) {
            originLat = Double.parseDouble(controller.getLoggedDriver().getD_lat());
            originLng = Double.parseDouble(controller.getLoggedDriver().getD_lng());
        }
        LatLng origin = new LatLng(originLat, originLng);
//        driverFollowedWayPoints = null;
//        preLocation = null;
        String waypoint = "";

        Log.e("origin Ride", "" + origin);
        googleSDRoute = new GoogleSDRoute(this, origin, dest1, waypoint);
        googleSDRoute.getRoute(getGoogeSdRoute(routeType));
    }

    @NonNull
    private GoogleSDRoute.GoogleSDRouteCallBack getGoogeSdRoute(final int routeType) {
        return new GoogleSDRoute.GoogleSDRouteCallBack() {
            @Override
            public void onCompleteSDRoute(GoogleSDRoute.RouteResponse routeResponse) {
                isCallingForGetRoute = false;
                mRouteResponse = routeResponse;
                if (routeResponse != null) {
                    switch (routeType) {
                        case 1: {
                            isPickRouteDrow = true;
                            break;
                        }
                        case 2: {
                            isBeginRouteDrow = true;
                            break;
                        }
                        case 3: {
                            break;
                        }
                    }

                    if (routeType == 2) {
                        clearCoveredRouteD();
                        coverRoutePolyLineD = routeResponse.addRouteOnMap(mGoogleMap, true, Constants.Values.BeginTripRouteWidth);
                    } else {
                        coverRoutePolyLineD = routeResponse.addRouteOnMap(mGoogleMap);
                    }
//                    markerDriverCar = routeResponse.addStartLocation(mGoogleMap, new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude()));
                    handleRouteDataAfterPolyLine(routeResponse);
                    TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                    if (tripModel != null && tripModel.tripStatus != null && (tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN) || tripModel.tripStatus.equalsIgnoreCase(ARRIVE))) {
                        showDistanceAndTimeView();
                    }
                }

            }


        };
    }

    private void handleRouteDataAfterPolyLine(GoogleSDRoute.RouteResponse routeResponse) {
        if (routeResponse.maneuver.size() > 1) {
            if (myCurrentLocation == null) {
                return;
            }
            Location locationA = new Location("point A");
            locationA.setLatitude(myCurrentLocation.getLatitude());
            locationA.setLongitude(myCurrentLocation.getLongitude());
            Location locationB = new Location("point B");
            locationB.setLatitude(routeResponse.ending_lat.get(1));
            locationB.setLongitude(routeResponse.ending_long.get(1));
            //float distancedirection = locationA.distanceTo(locationB);
            float distancedirection = locationA.distanceTo(locationB) / 1000;
            String s = String.format(Locale.ENGLISH, "%.1f", distancedirection);
            float p = Float.parseFloat(s);
            if (!routeResponse.maneuver.get(1).equals("No side")) {
                String split = String.valueOf(routeResponse.dis.get(0));
                String[] splitarray = split.split(" ");
                String getval = splitarray[0];
                String getmkm = splitarray[1];
                float textfloat = Float.parseFloat(getval);
                if (getmkm.matches("km")) {
                    //System.out.println("inside if km loop");
                    double val = 0.1;
                    if (!(p <= val)) {
                        //System.out.println("inside else in km loop");
                    }
                } else {
                    //System.out.println("inside else ");
                    float k = (textfloat) / 1000;
                }
            }
        }
    }

    private void checkOffRouteAndRedrwaRoute(final Location location) {

        if (mRouteResponse != null) {
            if (coverRoutePolyLineD == null) {
                coverRoutePolyLineD = new ArrayList<>();
            }
            GoogleSDRoute.DataTest nearestLocationTest = mRouteResponse.findNearestLocationTest(location);
            TrackRouteSaveData.getInstance(getApplicationContext()).addLatLongD(nearestLocationTest.latLng);
            TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
            final Location locationNear = new Location("");
            locationNear.setLatitude(nearestLocationTest.latLng.latitude);
            locationNear.setLongitude(nearestLocationTest.latLng.longitude);
            ArrayList<LatLng> lastNearstLoc = nearestLocationTest.latLngsRouteData;
            PolylineOptions polylineOptionses = new PolylineOptions();
            if (lastNearstLoc == null)
                return;
            polylineOptionses.addAll(lastNearstLoc);
            polylineOptionses.width(Constants.Values.BeginTripRouteWidth);
            polylineOptionses.color(getResources().getColor(R.color.black_dark));
            polylineOptionses.geodesic(true);
            polylineOptionses.zIndex(2);
            clearCoveredRouteD();
            makeRouteAndRedrwaRouteD(locationNear);
            coverRoutePolyLineD.add(mGoogleMap.addPolyline(polylineOptionses));

            boolean onOffRoute1 = mRouteResponse.isOnOffRoute(location);
            Log.e(TAG, "onOffRoute1: " + onOffRoute1);
            if (onOffRoute1) {
                Log.e(TAG, "ofRote: " + "yes");
                Log.e(TAG, "is_required_drawRoute: " + is_required_drawRoute);
                try {
                    double lat = myCurrentLocation.getLatitude();
                    double lng = myCurrentLocation.getLongitude();

                    if (lat == 0.0 || lng == 0.0) {
                        lat = Double.parseDouble(controller.getLoggedDriver().getD_lat());
                        lng = Double.parseDouble(controller.getLoggedDriver().getD_lng());
                    }
                    Log.e("origin update", "" + lat + lng);
                    TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                    double destLat, destLng;
                    if (is_required_drawRoute.equalsIgnoreCase("Yes2") && isDelivery) {
                        destLat = Double.parseDouble(deliveryList.get(numDel).getDeliveryLatitude());
                        destLng = Double.parseDouble(deliveryList.get(numDel).getDeliveryLongitude());
                    } else if (is_required_drawRoute.equalsIgnoreCase("Yes1")) {
                        destLat = Double.parseDouble(tripModel.trip.getTrip_scheduled_pick_lat());
                        destLng = Double.parseDouble(tripModel.trip.getTrip_scheduled_pick_lng());
                    } else {
                        destLat = Double.parseDouble(tripModel.trip.getTrip_scheduled_drop_lat());
                        destLng = Double.parseDouble(tripModel.trip.getTrip_scheduled_drop_lng());
                    }
                    waypoints = myCurrentLocation.getLatitude() + "," + myCurrentLocation.getLongitude();
                    googleSDRoute = new GoogleSDRoute(getApplicationContext(), new LatLng(lat, lng), new LatLng(destLat, destLng), waypoints);
                    googleSDRoute.getRoute(new GoogleSDRoute.GoogleSDRouteCallBack() {
                        @Override
                        public void onCompleteSDRoute(GoogleSDRoute.RouteResponse routeResponse) {
                            if (routeResponse != null) {
                                mRouteResponse = routeResponse;
                                clearCoveredRouteD();
                                coverRoutePolyLineD = routeResponse.addRouteOnMap(mGoogleMap, false, Constants.Values.BeginTripRouteWidth);
                                LatLng latlng = routeResponse.srcLoc;
                                //mGoogleMap.clear();
//                                if (markerDriverCar != null) {
//                                    if (latlng != null) {
//                                        Location location1 = new Location("");
//                                        location1.setLatitude(latlng.latitude);
//                                        location1.setLongitude(latlng.longitude);
//                                        animateMarkerNew(myCurrentLocation, markerDriverCar);
//                                    }
//                                    //MarkerAnimation.animateMarkerToICS(markerDriverCar, nearestLocationTest.latLng, new LatLngInterpolator.Spherical());
//                                } else {
//                                    ////Log.e("tuk tuk kahmer", "ytes");
//                                    MarkerOptions markerOptions;
//                                    markerOptions = new MarkerOptions().position(new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
//                                    markerDriverCar.setAnchor((float) 0.5, (float) 0.5);
//                                    //markerDriverCar.setRotation(angle);
//                                    markerDriverCar = mGoogleMap.addMarker(markerOptions);
//
//
//                                }
//                                if (markerDriverCar == null) {
//                                    //Log.e("redrawRoute", "fale");
//                                    markerDriverCar = routeResponse.addStartLocation(mGoogleMap, new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude()));
//                                }
                                if (isZoomEnabled) {
                                    moveTOCenter();
                                }
                            }
                        }


                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void moveTOCenter() {
        try {
            LatLng latLng = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
            mGoogleMap.animateCamera(cameraUpdate);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            LatLngBounds bounds;
            bounds = builder.build();

            // define value for padding
            int padding = 20;
            //This cameraupdate will zoom the map to a level where both location visible on map and also set the padding on four side.
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mGoogleMap.moveCamera(cu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void startDistanceServiceWithDistance() {
//        TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
//        if (tripModel != null && tripModel.tripStatus.equals(Constants.TripStatus.BEGIN)) {
//            Intent serviceIntent = new Intent(this, LocationService.class);
//            boolean recordingServiceRunning = TrackRecordingServiceConnectionUtils.isRecordingServiceRunning(this, LocationService.class);
//            if (!PreferencesUtils.getBoolean(this, R.string.is_recording_start, false)) {
//                PreferencesUtils.setBoolean(this, R.string.is_recording_start, true);
//                PreferencesUtils.getBoolean(this, R.string.is_recording_start, true);
//                if (!isAlready) {
//                    PreferencesUtils.setFloat(this, R.string.recorded_distance, 0);
//                }
//
//            }
//            controller.pref.setCalculatedDistance(PreferencesUtils.getFloat(this, R.string.recorded_distance));
//            if (!recordingServiceRunning) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    startForegroundService(serviceIntent);
//                } else {
//                    startService(serviceIntent);
//                }
//            }
//        }
//    }

//    private void clearGoogleRoutePolyLine() {
//        if (coverRoutePolyLineD != null) {
//            for (Polyline polyline : coverRoutePolyLineD) {
//                polyline.remove();
//            }
//            coverRoutePolyLineD.clear();
//        }
//    }

    private void calculateTimeDis() {
        if (tripModel != null) {
            Date datePickup = null;
            if (tripModel.trip != null) {
                datePickup = Utils.stringToDate(tripModel.trip.getTrip_pickup_time());
            }
            Date dateDrop = Utils.stringToDate(AppUtil.getCurrentDateInGMTZeroInServerFormat());
            if (datePickup != null && dateDrop != null) {
                long diffrent_between_time = dateDrop.getTime() - datePickup.getTime();
                long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                if (diffrent_between_time_inMinute > 0) {
                    tripTime = diffrent_between_time_inMinute + " " + "min";
                } else {
                    if (diffrent_between_time_inSeconds < 0) {
                        tripTime = "0" + " " + "min";
                    } else {
                        tripTime = diffrent_between_time_inSeconds + " " + "sec";
                    }
                }
            }
            if (datePickup != null && dateDrop == null) {
                String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                Date dateDrop1 = Utils.stringToDate(dropDate);

                long diffrent_between_time = dateDrop1.getTime() - datePickup.getTime();
                long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                if (diffrent_between_time_inMinute > 0) {
                    tripTime = diffrent_between_time_inMinute + " " + "min";
                } else {
                    if (diffrent_between_time_inSeconds < 0) {
                        tripTime = "0" + " " + "min";
                    } else {
                        tripTime = diffrent_between_time_inSeconds + " " + "sec";
                    }
                }
            }

            if (datePickup == null && dateDrop != null) {
                try {
                    Date tripStartDate = AppUtil.convertStringDateToAppTripDate(controller.pref.getTripStartTime());

                    if (tripStartDate != null) {
                        String dropDate = AppUtil.getCurrentDateInGMTZeroInServerFormat();
                        Date dateDrop1 = Utils.stringToDate(dropDate);
                        Date tripEndDate = new Date();

                        long diffrent_between_time = AppUtil.getTimeBetweenTwoDateDifferenceInMi(Objects.requireNonNull(tripStartDate), tripEndDate);
                        long diffrent_between_time_inMinute = diffrent_between_time / (1000 * 60);
                        long diffrent_between_time_inSeconds = diffrent_between_time / (1000);
                        if (diffrent_between_time_inMinute > 0) {
                            tripTime = diffrent_between_time_inMinute + " " + "min";
                        } else {
                            if (diffrent_between_time_inSeconds < 0) {
                                tripTime = "0" + " " + "min";
                            } else {
                                tripTime = diffrent_between_time_inSeconds + " " + "sec";
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            controller.pref.setTRIP_DISTANCE_CHANGEABLE_FLOAT(String.valueOf(distanceCoverInKm));
            controller.pref.setTRIP_DURATION_CHANGEABLE(String.valueOf(tripTime));
            controller.pref.setTRIP_DURATION_VALUE_CHANGEABLE(String.valueOf(tripTime));
            //Log.("drawRoutre", "fcndnv");

            showDistanceAndTimeView();
        }
    }

    private boolean isTripBegin() {
        TripModel tripModel = AppData.getInstance(getApplication()).getTripModel();
        return tripModel != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN);
    }

    private void cancelCountDownTimer() {
        if (count_downt_timer != null) {
            count_downt_timer.cancel();
            count_downt_timer = null;
        }
    }

    private void isPickCaption() {
        getMTrips();
        if (distanceAndTimeView != null && tripModel.trip.getTrip_status().equals(Constants.TripStatus.BEGIN) && !tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
            distanceAndTimeView.show();
        }
    }

    private void showDistanceAndTimeView() {
        String currentTime;// = getcurrenttime();
        DateFormat df;
        df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        float trip_distance_changable_float = Float.parseFloat(controller.pref.getTRIP_DISTANCE_CHANGEABLE_FLOAT());
        int trip_duration_changable_int = controller.pref.getTRIP_DURATION_CHANGEABLE_INT();
        String trip_duration_changableInMinute = controller.pref.getTRIP_DURATION_CHANGEABLE();
        String approxTime = controller.pref.getTRIP_DURATION_CHANGEABLE();
        double payAmt = 0.0;
        if (tripModel.trip != null) {
            if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
                payAmt = Double.parseDouble(deliveryList.get(numDel).getPayAmount());
            } else if (tripModel.trip.getTrip_pay_amount() != null && !tripModel.trip.getTrip_pay_amount().equals("null") && tripModel.trip.getTrip_pay_amount().trim().length() != 0) {
                // payAmt = Double.parseDouble(tripModel.trip.getTrip_pay_amount());
                payAmt = Double.parseDouble(calculateFare());
            }
        }
        String distancebtw = controller.formatMeterToDistanceWithUnit(controller.convertDistanceKmToUnit(distanceCoverInKm));

        Date cDate = new Date();
        long totalTime = cDate.getTime() + trip_duration_changable_int * 1000;
        Date mDate = new Date(totalTime);
        //    currentTime = df.format(mDate);
        currentTime = trip_duration_changableInMinute;
        if (currentTime == null || Objects.requireNonNull(currentTime).equalsIgnoreCase("null")) {
            currentTime = "0";
        }
        try {
            View view = findViewById(R.id.distance_time_set);
            distanceAndTimeView = new DistanceAndTime(SlideMainActivity.this, view, distancebtw, controller.formatAmountWithCurrencyUnitInt((int) Math.round(payAmt)), tripTime);
//            Log.d(TAG, "showDistanceAndTimeView: is working fine");
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.e("tripModerStatus", "" + tripModel.tripStatus);
        if (distanceAndTimeView != null && tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN) && !tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
            distanceAndTimeView.show();
        }
    }

    private String calculateFare() {
        if (tripModel != null && tripModel.trip != null && tripModel.driver != null) {
            if (tripModel.trip.getIs_share().equalsIgnoreCase("1")) {
                tripModel.tripAmount = String.format(Locale.ENGLISH, "%.02f", Float.parseFloat(tripModel.trip.getTrip_pay_amount()));
                tripModel.minutes = Double.parseDouble(tripModel.trip.getTrip_total_time());
                return tripModel.tripAmount;
            } else {
                double distanceCoverInKm1 = controller.pref.getCalculatedDistance();
                CategoryActors driverCat = null;
                for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                    if (categoryActors.getCategory_id().equalsIgnoreCase(tripModel.driver.getCategory_id())) {
                        driverCat = categoryActors;
//                Log.d("insideLoop", "" + driverCat.getCat_special_fare());
                        break;
                    }
                }
                if (driverCat != null && driverCat.getCat_special_fare() != null) {
                    TripFareCalculator tripFareCalculator = new TripFareCalculator(controller, distanceCoverInKm1, driverCat.getCat_special_fare());
                    TripFareCalculator.TripFare tripFare = tripFareCalculator.calculateFare();
                    tripModel.tripAmount = String.format(Locale.ENGLISH, "%.02f", tripFare.getTotalPrice());
                    tripModel.minutes = tripFareCalculator.getTotalMinutes();
                    return tripModel.tripAmount;
                } else {
                    return String.valueOf(0);
                }
            }
        } else {
            return String.valueOf(0);
        }
    }

    private boolean getDriver_rejected() {
        return driver_rejected;
    }

    private void setDriver_rejected() {
        this.driver_rejected = true;
    }

    private void getCarCategoryApi() {
        if (controller == null || controller.getLoggedDriver() == null)
            return;
        Map<String, String> params = new HashMap<>();
        params.put("city_id", controller.getLoggedDriver().getCity_id());

        ServerApiCall.callWithApiKey(SlideMainActivity.this, params, Constants.Urls.GET_CAR_CATEGORY, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        controller.pref.setCategoryResponse(response);
                        controller.setCategoryResponseList(CategoryActors.parseCarCategoriesResponse(response));
                    }  //  getCarCategoryApi();

                    if (markerDriverCarD != null) {
                        markerDriverCarD.remove();
                        markerDriverCarD = null;
                    }
                    Log.d(TAG, "onResume: isDelivery: animateCarOnMap");
                    animateCarOnMap(latLngPublishRelay);
                }  //   getCarCategoryApi();

            }
        });


    }

    private void checkVersionUpdateRequired() {
        try {
            String serverVersionCode = controller.getConstantsValueForKey("and_driver_app_ver");
            int currentAppVersionCode = BuildConfig.VERSION_CODE;

            Log.d("serverVersion", "" + serverVersionCode);
            Log.d("currentAppVersion", "" + currentAppVersionCode);
            if (serverVersionCode != null && !serverVersionCode.trim().equalsIgnoreCase("") && !serverVersionCode.equals("0")) {
                int servAppVersion = Integer.valueOf(serverVersionCode);

                Log.d("servAppVersion", "" + servAppVersion);
                Log.d("currAppVersion", "" + currentAppVersionCode);

                if (currentAppVersionCode < servAppVersion) {
                    showUpdateDialogForceFully();
                }
            }
        } catch (Exception ignored) {
        }
    }

    public void showUpdateDialogForceFully() {
        ishowingUpdateDailog = true;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SlideMainActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(SlideMainActivity.this.getString(R.string.app_name));
        alertDialogBuilder.setMessage(Localizer.getLocalizerString("k_60_s4_update_message"));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(Localizer.getLocalizerString("k_61_s4_update_now"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SlideMainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.dismiss();
                ishowingUpdateDailog = false;
            }
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();
    }

    public void showUpdateDialog() {
        ishowingUpdateDailog = true;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SlideMainActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(SlideMainActivity.this.getString(R.string.app_name));
        alertDialogBuilder.setMessage(SlideMainActivity.this.getString(R.string.update_message));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.update_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SlideMainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.dismiss();
                ishowingUpdateDailog = false;
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ishowingUpdateDailog = false;
            }
        });
        alertDialogBuilder.show();
    }

    public void continueCurrentTripDialog(TripModel tripModelNew) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SlideMainActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle("Trip");
        alertDialogBuilder.setMessage("Do you want to continue your current trip?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onCurrentTripChange(tripModelNew);
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }

    private void getConstantApi(final int retries) {
        Log.d(TAG, "getConstantApi: url: " + Constants.Urls.GET_CONSTANTS_API);
        ServerApiCall.callWithApiKey(SlideMainActivity.this, null, Constants.Urls.GET_CONSTANTS_API, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {

                if (isUpdate) {
                    String response = data.toString();
                    controller.setConstantsList(response);
                    // TODO uncomment
/*                    if (controller.checkPrivacyPolicy().equalsIgnoreCase("0")) {
                        privacy.setVisibility(View.GONE);
                    } else {
                        privacy.setVisibility(View.VISIBLE);
                    }

                    if (controller.checkAboutUs().equalsIgnoreCase("0")) {
                        about.setVisibility(View.GONE);
                    } else {
                        about.setVisibility(View.VISIBLE);
                    }*/
                    setUpSideDrawer();
                    addDraweToggleListener(setupDrawerLayout());
                    if (!ishowingUpdateDailog) {
                        checkVersionUpdateRequired();
                    }

                } else {
                    if (retries <= 3) {
                        int retry = retries + 1;
                        getConstantApi(retry);
                    }
                    //  Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if ((!ActivityCompat.shouldShowRequestPermissionRationale(SlideMainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION))) {
                ActivityCompat.requestPermissions(SlideMainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION

                        },
                        REQUEST_PERMISSIONS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_42_s4_plz_allow_perms"), Toast.LENGTH_LONG).show();
            } else {
                startLocationService();

                hasLocationPermission = true;
            }
        }
    }

    //Rider canclled trip popup
    private void showRiderDialog() {
        if (isdialogShowing) {
            return;
        }
        isdialogShowing = true;
        try {
            controller.stopNotificationSound();
            cancelCountDownTimer();
            final Dialog dialog = new Dialog(SlideMainActivity.this);
            Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.trip_cancelled_layout);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            TextView title2 = dialog.findViewById(R.id.tv_dialog_title);
            Button yes = dialog.findViewById(R.id.yes_btn);
            yes.setText(Localizer.getLocalizerString("k_21_s4_yes"));
            yes.setTypeface(drawerListTypeface);
            title2.setText(Localizer.getLocalizerString("k_53_s4_rider_has_cancelled_trip"));
            yes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    isdialogShowing = false;
                    isCancel = true;
                    AppData.getInstance(getApplicationContext()).clearTrip();
                    SlideMainActivity.this.tripModel = null;
                    controller.pref.setTripIds("");
                    driverUpdateProfile("1", false, false);


                }
            });
            dialog.show();
        } catch (Exception e) {
            isdialogShowing = false;
        }
    }

    private Location setLocationData(double c_lat, double c_lng, float c_bearing) {
        Location location = new Location("");
        location.setLatitude(c_lat);
        location.setLongitude(c_lng);
        location.setBearing(c_bearing);
        return location;
    }

    private void animateCarOnMap(final List<Location> latLngs) {
        if (mGoogleMap != null && latLngs.size() > 0 && controller.getLoggedDriver() != null) {
            Log.d(TAG, "animateCarOnMap: latLngsSize: " + latLngs.size());
            if (markerDriverCarD == null) {
                markerDriverCarD = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latLngs.get(0).getLatitude(), latLngs.get(0).getLongitude()))
                        .flat(true)
                        .rotation(latLngs.get(0).getBearing())
                );
                setDriverMarkerIcon();
                markerDriverCarD.setAnchor(0.5f, 0.5f);
            }
            markerDriverCarD.setPosition(new LatLng(latLngs.get(0).getLatitude(), latLngs.get(0).getLongitude()));
            markerDriverCarD.setRotation(latLngs.get(0).getBearing());
            if (latLngs.size() == 2) {
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(500);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        v = valueAnimator.getAnimatedFraction();
                        double lng = v * latLngs.get(1).getLongitude() + (1 - v)
                                * latLngs.get(0).getLongitude();
                        double lat = v * latLngs.get(1).getLatitude() + (1 - v)
                                * latLngs.get(0).getLatitude();
                        LatLng newPos = new LatLng(lat, lng);

                        if (markerDriverCarD != null) {
                            markerDriverCarD.setPosition(newPos);
                            markerDriverCarD.setAnchor(0.5f, 0.5f);
                            float mBearing = getBearing(new LatLng(latLngs.get(0).getLatitude(), latLngs.get(0).getLongitude()), newPos);
//                            markerDriverCarD.setRotation(mBearing);
                            markerDriverCarD.setRotation(latLngs.get(1).getBearing());
                            if (isZoomEnabled || isZoomEnabledFirst) {
                                isZoomEnabledFirst = false;
                                mGoogleMap.animateCamera(
                                        CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                                .target(newPos)
                                                .bearing(latLngs.get(1).getBearing())
                                                .zoom(16).build()));
                            }
                        }

                    }
                });
                valueAnimator.start();
            }
        }
    }

    private void setDriverMarkerIcon() {
        if (markerDriverCarD == null)
            return;
        if (controller.getLoggedDriver().getCategory_id() != null) {

            CategoryActors driverCat = null;
            for (CategoryActors categoryActors : controller.getCategoryResponseList()) {
                if (categoryActors.getCategory_id().equalsIgnoreCase(controller.getLoggedDriver().getCategory_id())) {
                    driverCat = categoryActors;
                    break;
                }
            }
            if (driverCat != null) {
                loadUserImage(driverCat.getCat_map_icon_path());
            } else
                markerDriverCarD.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_top_view_1));
        } else
            markerDriverCarD.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_top_view_1));
    }

    public void loadUserImage(String url) {
        Log.d(TAG, "loadUserImage: " + url);
        VolleySingleton.getInstance(this).getImageLoader().get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null) {
                    Bitmap bitmap = response.getBitmap();
                    setCurrentLocationMarkerImage(SlideMainActivity.this, bitmap);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error while loading image.");
                setCurrentLocationMarkerImage(SlideMainActivity.this, null);
            }
        });
    }

    private void setCurrentLocationMarkerImage(Context context, Bitmap bitmap) {
        if (markerDriverCarD != null) {
            if (bitmap == null) {
                bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.car_top_view_1);
            }
            Bitmap bitmap1 = createDrawableFromView(context, bitmap);
            markerDriverCarD.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap1));
        }
    }

    @SuppressLint("InflateParams")
    public Bitmap createDrawableFromView(Context context, Bitmap bitmap1) {
        View view;
        view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.car_location_view, null);
        ImageView marker = view.findViewById(R.id.ivCar);
        marker.setImageBitmap(bitmap1);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        view.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        float ratio = Math.min(
                (float) dpToPx(56, context) / bitmap.getWidth(),
                (float) dpToPx(56, context) / bitmap.getHeight());
        int width = Math.round(ratio * bitmap.getWidth());
        int height = Math.round(ratio * bitmap.getHeight());
        if (width <= 0 || height <= 0)
            return Bitmap.createScaledBitmap(bitmap, 56, 56, true);

        return Bitmap.createScaledBitmap(bitmap, width, height, true);

//        return bitmap;
    }

    private boolean isCanMakeApiCall(Location preLocation, Location myCurrentLocation) {
        if (preLocation == null) {
            return true;
        }
        if (myCurrentLocation == null) {
            return true;
        }
//        Log.d(TAG, "isCanMakeApiCall: " + preLocation.toString());
//        Log.d(TAG, "isCanMakeApiCall: " + myCurrentLocation.toString());
//        Log.d(TAG, "isCanMakeApiCall: distance: " + preLocation.distanceTo(myCurrentLocation));
        return preLocation.distanceTo(myCurrentLocation) > 10;
//        return true;
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            displayView(position);
        }
    }

    private class MyLocationListener implements LocationListener {

        public void onLocationChanged(Location location) {
//            myCurrentLocation = location;
//            try {
//                if (myCurrentLocation != null) {
//                    lastKnownLat = myCurrentLocation.getLatitude();
//                    lastKnownLng = myCurrentLocation.getLongitude();
//                    controller.getLoggedDriver().setD_lat(Double.toString(lastKnownLat));
//                    controller.getLoggedDriver().setD_lng(Double.toString(lastKnownLng));
//                } else {
//                    if (lastKnownLng != 0.0 && lastKnownLat != 0.0) {
//                        myCurrentLocation = new Location("");
//                        myCurrentLocation.setLatitude(lastKnownLat);
//                        myCurrentLocation.setLongitude(lastKnownLng);
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            if (net_connection_check()) {
//                LatLng target = new LatLng(location.getLatitude(), location.getLongitude());
//                if (location.hasBearing() && mGoogleMap != null) {
//                    //
//                    if (!isTripBegin() && isZoomEnabled) {
//                        CameraPosition cameraPosition = new CameraPosition.Builder()
//                                .target(target)             // Sets the center of the map to current location
//                                .zoom(16)
//                                // .bearing(location.getBearing()) // Sets the orientation of the camera to east
////                            .tilt(0)                   // Sets the tilt of the camera to 0 degrees
//                                .build();                   // Creates a CameraPosition from the builder
//                        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                    }
//                }
//            }
        }

        public void onStatusChanged(String s, int i, Bundle b) {

        }

        public void onProviderDisabled(String s) {

        }

        public void onProviderEnabled(String s) {

        }

    }

    private class LocationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null)
//                Log.d(TAG, "onReceive: " + intent.getAction());
                if (Objects.requireNonNull(intent.getAction()).equals(LOACTION_ACTION)) {
                    try {
                        String locationData = intent.getStringExtra(LOCATION_MESSAGE);
//                    Log.d(TAG, "onReceive: locationData" + locationData);
                        if (locationData != null && !locationData.trim().equalsIgnoreCase("")) {
                            distanceCoverInKm = Double.parseDouble(locationData) / 1000;
//                        Log.d(TAG, "onReceive: distanceInKm" + distanceCoverInKm);
                            controller.pref.setCalculatedDistance(distanceCoverInKm);
                        }

                        float c_lat = Float.parseFloat(intent.getStringExtra("c_lat"));
                        float c_lng = Float.parseFloat(intent.getStringExtra("c_lng"));
                        final float c_bearing = Float.parseFloat(intent.getStringExtra("c_bearing"));
                        boolean isFromBearing = intent.getBooleanExtra("isFromBearing", false);

                        if (!isFromBearing) {
                            if (latLngPublishRelay.size() <= 1) {
                                if (latLngPublishRelay.size() == 0) {
                                    myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                    myLastCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                    latLngPublishRelay.add(myLastCurrentLocation);
                                    Log.d(TAG, "onReceive: animateCarOnMap");
                                    animateCarOnMap(latLngPublishRelay);
                                } else {
                                    myLastCurrentLocation = setLocationData(latLngPublishRelay.get(0).getLatitude(), latLngPublishRelay.get(0).getLongitude(), latLngPublishRelay.get(0).getBearing());
                                    myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                    if (isCanMakeApiCall(myLastCurrentLocation, myCurrentLocation)) {
                                        latLngPublishRelay.add(myCurrentLocation);
                                        Log.d(TAG, "onReceive: animateCarOnMap");
                                        animateCarOnMap(latLngPublishRelay);
                                    }
                                }
                            } else {
                                myLastCurrentLocation = setLocationData(latLngPublishRelay.get(1).getLatitude(), latLngPublishRelay.get(1).getLongitude(), latLngPublishRelay.get(1).getBearing());
                                myCurrentLocation = setLocationData(c_lat, c_lng, c_bearing);
                                if (isCanMakeApiCall(myLastCurrentLocation, myCurrentLocation)) {
                                    latLngPublishRelay.set(0, latLngPublishRelay.get(1));
                                    latLngPublishRelay.set(1, myCurrentLocation);
                                    Log.d(TAG, "onReceive: animateCarOnMap");
                                    animateCarOnMap(latLngPublishRelay);
                                }
                            }
                        } else {

                        }

                        if (mGoogleMap != null && !isFromBearing) {
                            TripModel tripModel = AppData.getInstance(getApplicationContext()).getTripModel();
                            int zoomValue = 16;
                            if (tripModel != null) {
                                if (tripModel.tripStatus.equalsIgnoreCase(Constants.TripStatus.BEGIN)) {
                                    calculateTimeDis();
                                    zoomValue = 16;
                                    //  zoomValue = 19;
                                }
                            }

                            if (tripModel != null && tripModel.trip != null && tripModel.tripStatus != null && !tripModel.tripStatus.equalsIgnoreCase("begin")) {
                                checkOffRouteAndRedrwaRoute(myCurrentLocation);
                            } else if (tripModel != null && tripModel.trip != null && tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin") && tripModel.trip.getTrip_to_loc() != null && tripModel.trip.getTrip_to_loc().trim().length() != 0) {
                                checkOffRouteAndRedrwaRoute(myCurrentLocation);
                            } else {
                                drawRouteWithoutDestination(myCurrentLocation);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }
    }
}
