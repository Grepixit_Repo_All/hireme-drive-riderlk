package com.ridertechrider.driver.adaptor;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.Localizer;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.RequestFragment.ClickListenerCallback;
import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TripListAdaptor extends BaseAdapter {
    private final ArrayList<TripModel> tripHistories;
    private final Typeface typeface;
    private final Controller controller;
    private final ClickListenerCallback mCallback;
    private final Fragment requestFragment;
    private CategoryActors driverCat;
    private Activity activity;

    public TripListAdaptor(Activity activity, ArrayList<TripModel> tripHistoryList, Typeface typeface, ClickListenerCallback callback, CategoryActors driverCat, Fragment requestFragment) {
        this.tripHistories = tripHistoryList;
        this.typeface = typeface;
        this.activity = activity;
        controller = (Controller) activity.getApplication();
        this.mCallback = callback;
        this.requestFragment = requestFragment;
        this.driverCat = driverCat;
    }

    @Override
    public int getCount() {
        return tripHistories.size();
    }

    @Override
    public Object getItem(int i) {
        return tripHistories.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_list_items, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        if (position != -1 && tripHistories.size() > 0 && position <= tripHistories.size() - 1) {
            final TripModel tripModel = (TripModel) getItem(position);
            if(tripModel.trip.getTrip_customer_details() != null)
                mViewHolder.passengerDetails.setVisibility(View.VISIBLE);
            else
                mViewHolder.passengerDetails.setVisibility(View.GONE);

            if (tripModel.user.isProfileImagePathEmpty()) {
                AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripModel.user.getUProfileImagePath(), mViewHolder.thumbNail);
            } else {
                mViewHolder.thumbNail.setImageResource(R.drawable.user);
            }

            mViewHolder.tvName.setText(tripModel.user.getU_name());

            if (tripModel.trip.getPickup_notes() != null) {
                mViewHolder.tvPickAddress.setText(String.format("%s\n\n%s %s", tripModel.trip.getTrip_from_loc(), Localizer.getLocalizerString("k_1_s8_special_notes"), tripModel.trip.getPickup_notes()));
            } else {
                mViewHolder.tvPickAddress.setText(tripModel.trip.getTrip_from_loc());
            }

            if (tripModel.trip.getTrip_pay_mode() != null && !tripModel.trip.getTrip_pay_mode().trim().isEmpty() && driverCat != null && driverCat.getShow_paymode().equalsIgnoreCase("1")) {
                mViewHolder.trip_pay_mode.setVisibility(View.VISIBLE);
                mViewHolder.trip_pay_mode.setText(String.format("%s %s", Localizer.getLocalizerString("k_1_s8_pay_via"), tripModel.trip.getTrip_pay_mode()));
            } else {
                mViewHolder.trip_pay_mode.setVisibility(View.GONE);
            }

            if (tripModel.trip.getTrip_pay_amount() != null && !tripModel.trip.getTrip_pay_amount().trim().isEmpty() &&
                    !tripModel.trip.getTrip_pay_amount().trim().equalsIgnoreCase("null") &&
                    !tripModel.trip.getTrip_pay_amount().trim().equalsIgnoreCase("0") && driverCat != null && driverCat.getShow_fare().equalsIgnoreCase("1")) {

                mViewHolder.trip_pay_amount.setVisibility(View.VISIBLE);
                mViewHolder.trip_pay_amount.setText(String.format("%s %s", Localizer.getLocalizerString("k_1_s8_est"), controller.formatAmountWithCurrencyUnit(tripModel.trip.getTrip_pay_amount())));
            } else {
                mViewHolder.trip_pay_amount.setVisibility(View.GONE);
            }

            if (tripHistories.get(position).trip.getIs_ride_later().equals("1") && !tripHistories.get(position).trip.getIs_share().equals("1")) {
                mViewHolder.requestAcceptButton.setText(Localizer.getLocalizerString("k_4_s9_assign"));
            } else {
                mViewHolder.requestAcceptButton.setText(Localizer.getLocalizerString("k_1_s9_accept"));
            }
            mViewHolder.dateTrip.setText("@" + Utils.convertServerDateToAppLocalDate(tripModel.trip.getTrip_date()));

//        mViewHolder.tvPickAddress.setText(tripModel.trip.getTrip_from_loc());
            mViewHolder.tvDropAddress.setText(tripModel.trip.getTrip_to_loc());
            mViewHolder.request_layout.setTag(tripModel);

            if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("1")) {
                mViewHolder.destinationLayoutT.setVisibility(View.VISIBLE);
                mViewHolder.requestAcceptButton.setVisibility(View.VISIBLE);
                mViewHolder.isDeliveryView.setVisibility(View.GONE);
            } else {
                mViewHolder.destinationLayoutT.setVisibility(View.GONE);
                mViewHolder.requestAcceptButton.setVisibility(View.GONE);
                mViewHolder.isDeliveryView.setVisibility(View.VISIBLE);
            }

            mViewHolder.passengerDetails.setOnClickListener(v -> {
                final Dialog dialogPassengerDetails = new Dialog(activity);
                dialogPassengerDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogPassengerDetails.setCancelable(true);
                dialogPassengerDetails.setContentView(R.layout.someone_else_dialog);
                TextView passengerTitle = dialogPassengerDetails.findViewById(R.id.passengerTitle);
                passengerTitle.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
                BTextView passengerName = dialogPassengerDetails.findViewById(R.id.passengerName);
                BTextView passengerPhone = dialogPassengerDetails.findViewById(R.id.passengerMobile);
                String details = tripModel.trip.getTrip_customer_details();
                try {
                    JSONObject jsonObject = new JSONObject(details);
                    if(jsonObject.has("p_name"))
                        passengerName.setText(jsonObject.getString("p_name"));
                    if(jsonObject.has("p_phone"))
                        passengerPhone.setText(jsonObject.getString("p_phone"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BTextView passengerNameTv = dialogPassengerDetails.findViewById(R.id.passengerNameTv);
                passengerNameTv.setText(Localizer.getLocalizerString("k_s3_passenger_name"));
                BTextView passengerMobileTv = dialogPassengerDetails.findViewById(R.id.passengerMobileTv);
                passengerMobileTv.setText(Localizer.getLocalizerString("k_s3_passenger_phone"));
                ImageView closeButtonDialog = dialogPassengerDetails.findViewById(R.id.close_button);
                closeButtonDialog.setOnClickListener(v1 -> {
                    dialogPassengerDetails.dismiss();
                });

                dialogPassengerDetails.show();

            });

            mViewHolder.request_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tripModel.trip.getIs_delivery() != null && tripModel.trip.getIs_delivery().equals("1")) {
                        TripModel request = (TripModel) view.getTag();
                        mCallback.onTripDetail(tripModel);
                    }
                }
            });

            mViewHolder.requestAcceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position != -1 && position < tripHistories.size()) {
                        if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("1")) {
                            controller.stopNotificationSound();

                            if (tripHistories.get(position).trip.getIs_ride_later().equals("1") && !tripHistories.get(position).trip.getIs_share().equals("1")) {
                                mCallback.onAssignTrip(tripHistories.get(position), requestFragment);
//                                tripHistories.remove(position);
//                                notifyDataSetChanged();
                            } else {
                                mCallback.onAcceptTrip(tripHistories.get(position), requestFragment);
                            }
                        }
                    }
                }
            });
            mViewHolder.request_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position != -1 && position < tripHistories.size()) {
                        if (tripModel.trip.getIs_delivery() != null && !tripModel.trip.getIs_delivery().equals("1")) {
                            controller.stopNotificationSound();
                            mCallback.onRejectTrip(tripHistories.get(position), requestFragment);
                        }
                    }
                }
            });
            mViewHolder.pickupAddressMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.viewOnMap(controller.getResources().getString(R.string.trip_detail),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lng())),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lng())));
                }
            });
            mViewHolder.dropAddressView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.viewOnMap(controller.getResources().getString(R.string.trip_detail),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_pick_lng())),
                            new LatLng(Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lat()),
                                    Double.parseDouble(tripHistories.get(position).trip.getTrip_scheduled_drop_lng())));
                }
            });
        }
        setLocalizeData(mViewHolder);
        return convertView;
    }

    private void setLocalizeData(MyViewHolder mViewHolder) {
        mViewHolder.msa_tv_pickup.setText(Localizer.getLocalizerString("k_10_s8_pickup_nd_location "));
        mViewHolder.msa_tv_drop.setText(Localizer.getLocalizerString("k_11_s8_search_drop_location"));
        mViewHolder.callMe.setText(Localizer.getLocalizerString("s_2_s10_call_me"));
        mViewHolder.passengerDetails.setText(Localizer.getLocalizerString("k_s3_passenger_details"));
    }

    public void clear() {
        this.tripHistories.clear();
        notifyDataSetChanged();
    }

    public void setDriverCat(CategoryActors driverCat) {
        this.driverCat = driverCat;
    }

    private class MyViewHolder {

        final TextView msa_tv_drop, msa_tv_pickup, callMe;
        TextView tvName, tvPickAddress, tvDropAddress, isDeliveryView;
        TextView requestAcceptButton, dateTrip;
        TextView trip_pay_mode, trip_pay_amount, passengerDetails;
        RelativeLayout request_layout;
        LinearLayout destinationLayoutT;
        CircleImageView thumbNail;
        ImageView pickupAddressMap, dropAddressView, request_reject;

        MyViewHolder(View view) {
            this.callMe = view.findViewById(R.id.callMe);
            this.tvName = view.findViewById(R.id.name);
            this.isDeliveryView = view.findViewById(R.id.isDelivery);
            this.tvName.setTypeface(typeface);
            this.msa_tv_drop = view.findViewById(R.id.msa_tv_drop);
            this.msa_tv_pickup = view.findViewById(R.id.msa_tv_pickup);
            this.dateTrip = view.findViewById(R.id.date_trip);
            this.tvPickAddress = view.findViewById(R.id.pick_address);
            this.tvPickAddress.setTypeface(typeface);
            this.tvDropAddress = view.findViewById(R.id.drop_address);
            this.tvDropAddress.setTypeface(typeface);
            this.requestAcceptButton = view.findViewById(R.id.request_accept);
            this.request_layout = view.findViewById(R.id.req_layou);
            this.destinationLayoutT = view.findViewById(R.id.destinationLayoutT);
            this.thumbNail = view.findViewById(R.id.icon);
            this.pickupAddressMap = view.findViewById(R.id.pickupAddressMap);
            this.dropAddressView = view.findViewById(R.id.dropAddressMap);
            this.trip_pay_mode = view.findViewById(R.id.trip_pay_mode);
            this.trip_pay_amount = view.findViewById(R.id.trip_pay_amount);
            this.request_reject = view.findViewById(R.id.request_reject);
            this.passengerDetails = view.findViewById(R.id.passenger_details);

            callMe.setVisibility(View.GONE);
        }
    }

}
