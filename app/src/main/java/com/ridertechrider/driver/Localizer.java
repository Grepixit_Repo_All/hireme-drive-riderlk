package com.ridertechrider.driver;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ridertechrider.driver.webservice.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ridertechrider.driver.webservice.Constants.Language.ENGLISH;

public class Localizer {
    public static JSONObject localizeJson;

    public static String getLocalizerString(String key) {
        Controller controller = Controller.getInstance();
        if (controller != null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(controller);
            String lang = prefs.getString(Constants.Keys.APP_LANGUAGE, ENGLISH);
            if (localizeJson == null) {
                controller.setLocalizeData();
            }
            if (localizeJson != null && localizeJson.has(key)) {
                try {
                    JSONObject keyValueObj = localizeJson.getJSONObject(key);
                    if (keyValueObj != null && keyValueObj.has(lang)) {
                        return keyValueObj.getString(lang);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return key;
    }

}
