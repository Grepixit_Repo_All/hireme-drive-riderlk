package com.ridertechrider.driver;

import com.google.gson.Gson;

import java.io.Serializable;

public class UserTransactions implements Serializable {

    private String trans_user_id;
    private String transaction_id;
    private String trans_type;
    private String user_id;
    private String amount;
    private String remarks;
    private String created;
    private String modified;

    public static UserTransactions parseJson(String userJson) {

        return new Gson().fromJson(userJson, UserTransactions.class);
    }


    public String getTrans_user_id() {
        return trans_user_id;
    }

    public void setTrans_user_id(String trans_user_id) {
        this.trans_user_id = trans_user_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
