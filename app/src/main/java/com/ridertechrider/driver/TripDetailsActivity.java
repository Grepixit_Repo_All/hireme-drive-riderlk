package com.ridertechrider.driver;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.utils.AppUtil;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.APIClient;
import com.ridertechrider.driver.webservice.APIInterface;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TripDetailsActivity extends BaseCompatActivity {
    private static final String TAG = TripDetailsActivity.class.getSimpleName();
    TripModel tripHistory;
    Controller controller;
    SupportMapFragment mapFragment;
    private boolean isFromRiderCancel;
    private String tripId;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);
        MapsInitializer.initialize(this);
        controller = (Controller) getApplication();

        isFromRiderCancel = getIntent().getBooleanExtra("isFromRiderCancel", false);
        tripId = getIntent().getStringExtra("tripId");
        tripHistory = (TripModel) getIntent().getSerializableExtra("tripHistory");
        progressDialog = new CustomProgressDialog(TripDetailsActivity.this);

        if (isFromRiderCancel && tripId != null && controller.getLoggedDriver() != null) {
            getTripByID(tripId);
        }
        showTripDetails();

        ImageView close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getTripByID(String tripId) {
        String api = controller.getLoggedDriver().getApiKey();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        try {
            progressDialog.showDialog();
        } catch (Exception ignored) {
        }

        Call<ResponseBody> callTripApi = apiInterface.getTripById(api, tripId);
        callTripApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        String string = response.body().string().toString();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + string);
                        TripModel tripModel = new TripModel();
                        if (res.isStatus()) {
                            boolean isParseRe = TripModel.parseJsonWithTripModel(string, tripModel);
                            if (isParseRe) {
                                tripHistory = tripModel;
                                showTripDetails();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void showTripDetails() {
        if (tripHistory != null) {

//        if (getWindow() != null) {
//            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.tripdetail);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
//            int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.90);
//            dialog.getWindow().setLayout(width, height);
//            Window window = dialog.getWindow();
//            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//            window.setGravity(Gravity.CENTER);
//        }
            final FrameLayout layoutMapView = findViewById(R.id.layoutMapView);
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
            if (mapFragment != null)
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(final GoogleMap googleMap) {
                        googleMap.getUiSettings().setAllGesturesEnabled(false);
                        googleMap.getUiSettings().setCompassEnabled(false);
                        googleMap.getUiSettings().setIndoorLevelPickerEnabled(false);
                        googleMap.getUiSettings().setMapToolbarEnabled(false);
                        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                        googleMap.getUiSettings().setRotateGesturesEnabled(false);
                        googleMap.getUiSettings().setZoomControlsEnabled(false);
                        googleMap.getUiSettings().setZoomGesturesEnabled(false);
                        googleMap.getUiSettings().setScrollGesturesEnabled(false);
                        googleMap.getUiSettings().setTiltGesturesEnabled(false);
                        googleMap.setMyLocationEnabled(false);
                        if (tripHistory != null && tripHistory.trip != null && tripHistory.trip.getTrip_id() != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        LatLng pickUp = new LatLng(Double.parseDouble(tripHistory.trip.getTrip_scheduled_pick_lat()), Double.parseDouble(tripHistory.trip.getTrip_scheduled_pick_lng()));
                                        if (tripHistory.trip.getTrip_actual_pick_lat() != null && !tripHistory.trip.getTrip_actual_pick_lat().trim().isEmpty() &&
                                                !tripHistory.trip.getTrip_actual_pick_lat().trim().equalsIgnoreCase("null") &&
                                                !tripHistory.trip.getTrip_actual_pick_lat().trim().equalsIgnoreCase("0") &&
                                                tripHistory.trip.getTrip_actual_pick_lng() != null && !tripHistory.trip.getTrip_actual_pick_lng().trim().isEmpty() &&
                                                !tripHistory.trip.getTrip_actual_pick_lng().trim().equalsIgnoreCase("null") &&
                                                !tripHistory.trip.getTrip_actual_pick_lng().trim().equalsIgnoreCase("0")) {

                                            pickUp = new LatLng(Double.parseDouble(tripHistory.trip.getTrip_actual_pick_lat()),
                                                    Double.parseDouble(tripHistory.trip.getTrip_actual_pick_lng()));
                                        }

                                        LatLng drop = new LatLng(Double.parseDouble(tripHistory.trip.getTrip_scheduled_drop_lat()), Double.parseDouble(tripHistory.trip.getTrip_scheduled_drop_lng()));
                                        if (tripHistory.trip.getTrip_actual_drop_lat() != null && !tripHistory.trip.getTrip_actual_drop_lat().trim().isEmpty() &&
                                                !tripHistory.trip.getTrip_actual_drop_lat().trim().equalsIgnoreCase("null") &&
                                                !tripHistory.trip.getTrip_actual_drop_lat().trim().equalsIgnoreCase("0") &&
                                                tripHistory.trip.getTrip_actual_drop_lng() != null && !tripHistory.trip.getTrip_actual_drop_lng().trim().isEmpty() &&
                                                !tripHistory.trip.getTrip_actual_drop_lng().trim().equalsIgnoreCase("null") &&
                                                !tripHistory.trip.getTrip_actual_drop_lng().trim().equalsIgnoreCase("0")) {

                                            drop = new LatLng(Double.parseDouble(tripHistory.trip.getTrip_actual_drop_lat()),
                                                    Double.parseDouble(tripHistory.trip.getTrip_actual_drop_lng()));
                                        }

                                        Bitmap bitmapSrc = BitmapFactory.decodeResource(getResources(), R.drawable.source11);
                                        Bitmap bitmapDes = BitmapFactory.decodeResource(getResources(), R.drawable.destination11);

                                        googleMap.addMarker(new MarkerOptions().position(pickUp).icon(BitmapDescriptorFactory.fromBitmap(bitmapSrc)));
                                        googleMap.addMarker(new MarkerOptions().position(drop).icon(BitmapDescriptorFactory.fromBitmap(bitmapDes)));
                                        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
                                        bounds.include(pickUp);
                                        bounds.include(drop);
                                        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 150));
                                        getTripRoute(tripHistory.trip.getTrip_id(), googleMap);
                                    } catch (Exception e) {
                                        Log.e(TAG, "onUpdateDeviceTokenOnServer: " + e.getMessage(), e);
                                    }
                                }
                            }, 500);
                        }
                    }
                });

            CircleImageView circleImageView = findViewById(R.id.driver_profile);
            TextView promo_amount = findViewById(R.id.promo_amount);
            TextView rating = findViewById(R.id.user_average_rating);
            TextView user_name = findViewById(R.id.user_name);
            TextView car_name = findViewById(R.id.car_name);
            RatingBar ratingBar1 = findViewById(R.id.ratingBar1);
            TextView detaildate = findViewById(R.id.detaildate);
            TextView tripStatusView = findViewById(R.id.tripStatus);
            Log.e("tripStatus", "" + tripHistory.tripStatus);
            if (tripHistory.trip.getTrip_status() != null && !tripHistory.trip.getTrip_status().equalsIgnoreCase("null") && (tripHistory.trip.getTrip_status().equalsIgnoreCase("cancel") || tripHistory.trip.getTrip_status().equals("paid_cancel") || tripHistory.trip.getTrip_status().equalsIgnoreCase("hide_alert"))) {
                tripStatusView.setVisibility(View.VISIBLE);
            } else {
                tripStatusView.setVisibility(View.GONE);
            }
            TextView pick = findViewById(R.id.detailpickup1);
            TextView drop = findViewById(R.id.detaildrop1);
            TextView amount = findViewById(R.id.total_amount);
            TextView distance = findViewById(R.id.distance);


            TextView tax_amount1 = findViewById(R.id.tax_amount1);
            View layoutTax = findViewById(R.id.layoutTax);

            try {
                if (Double.parseDouble(tripHistory.trip.getTrip_tax_amt()) > 0) {
                    tax_amount1.setText(controller.formatAmountWithCurrencyUnit(tripHistory.trip.getTrip_tax_amt()));
                } else {
                    tax_amount1.setText(controller.formatAmountWithCurrencyUnit("0"));
                }
            } catch (Exception e) {
                tax_amount1.setText(controller.formatAmountWithCurrencyUnit("0"));
            }

            TextView waitTime = findViewById(R.id.tvWaitingTime);
            String waitDuration = tripHistory.trip.getWait_duration();
            if (waitDuration != null && !waitDuration.equals("") && !waitDuration.equalsIgnoreCase("null")) {
                waitTime.setText(String.format("%s %s", waitDuration, getString(R.string.minute)));
            } else {
                waitTime.setText(String.format("0 %s", getString(R.string.minute)));
            }
            String categoryId = tripHistory.driver.getCategory_id();
            ArrayList<CategoryActors> categoryResponseList = CategoryActors.parseCarCategoriesResponse(controller.pref.getCategoryResponse());
            String catgoryName = "";
            for (CategoryActors catagories : categoryResponseList) {
                if (catagories.getCategory_id().equals(categoryId)) {
                    catgoryName = catagories.getCat_name();
                }
            }

            float ratings = Float.parseFloat(tripHistory.user.getRating());
            rating.setText(String.valueOf(new DecimalFormat("##.#").format(ratings)));
            ratingBar1.setRating(Float.parseFloat(tripHistory.user.getRating()));
            user_name.setText(tripHistory.user.getU_name());
            car_name.setText(catgoryName);
            detaildate.setText(Utils.convertServerDateToAppLocalDate(tripHistory.trip.getTrip_date()));
            Double amountDouble1 = Double.valueOf(tripHistory.trip.getTrip_pay_amount());
            //amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));
            distance.setText(controller.formatDistanceWithUnit(tripHistory.trip.getTrip_distance()));
            if (tripHistory.trip.getTrip_promo_code() != null && tripHistory.trip.getTrip_promo_code().trim().length() != 0 && !tripHistory.trip.getTrip_promo_code().equals("null")) {
                if (tripHistory.trip.getTrip_status() != null && !tripHistory.trip.getTrip_status().equals("cancel") && !tripHistory.trip.getTrip_status().equals("paid_cancel"))
                    amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1 )));
                else
                    amount.setText(controller.formatAmountWithCurrencyUnit("0"));

                promo_amount.setText(String.format("%s %s", tripHistory.trip.getTrip_promo_code(), controller.formatAmountWithCurrencyUnit(tripHistory.trip.getTrip_promo_amt())));
            } else {
                if (tripHistory.trip.getTrip_status() != null && !tripHistory.trip.getTrip_status().equals("cancel") && !tripHistory.trip.getTrip_status().equals("paid_cancel"))
                    amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble1)));
                else
                    amount.setText(controller.formatAmountWithCurrencyUnit("0"));
                promo_amount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(0)));
            }
            if (tripHistory.trip != null && tripHistory.trip.getActual_from_loc() != null && tripHistory.trip.getActual_from_loc().trim().length() != 0) {
                if (tripHistory.trip.getPickup_notes() != null)
                    pick.setText(String.format("%s\n\n%s %s",  tripHistory.trip.getActual_from_loc(), Localizer.getLocalizerString("k_1_s8_special_notes"), tripHistory.trip.getPickup_notes()));
                else
                    pick.setText(tripHistory.trip.getActual_from_loc());

//                pick.setText(tripHistory.trip.getActual_from_loc());
            } else {

                if (Objects.requireNonNull(tripHistory.trip).getPickup_notes() != null)
                    pick.setText(String.format("%s\n\n%s %s",  tripHistory.trip.getTrip_from_loc(), Localizer.getLocalizerString("k_1_s8_special_notes"), tripHistory.trip.getPickup_notes()));
                else
                    pick.setText(Objects.requireNonNull(tripHistory.trip).getTrip_from_loc());

//                pick.setText(Objects.requireNonNull(tripHistory.trip).getTrip_from_loc());
            }
            if (tripHistory.trip != null && tripHistory.trip.getActual_to_loc() != null && tripHistory.trip.getActual_to_loc().trim().length() != 0) {
                drop.setText(tripHistory.trip.getActual_to_loc());
            } else {
                drop.setText(Objects.requireNonNull(tripHistory.trip).getTrip_to_loc());
            }
            if (tripHistory.user.isProfileImagePathEmpty()) {
                AppUtil.getImageLoaderInstanceForProfileImage(controller).displayImage(Constants.IMAGE_BASE_URL + tripHistory.user.getUProfileImagePath(), circleImageView);
            }
        }
    }

    public void getTripRoute(final String tripId, final GoogleMap googleMap) {
        if (controller == null || controller.getLoggedDriver() == null)
            return;

        Map<String, String> params = new HashMap<>();
        params.put("trip_id", tripId);
        params.put("api_key", controller.getLoggedDriver().getApiKey());
        params.put("user_id", controller.getLoggedDriver().getDriverId());

        WebServiceUtil.excuteRequestWithNoAlert(this, params, Constants.Urls.UPDATE_TRIP_GET_ROUTE_URL, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    try {
                        JSONObject obj = new JSONObject(String.valueOf(data));
                        if (obj.has("response") && obj.get("response") instanceof JSONArray && obj.getJSONArray("response").length() > 0) {
                            JSONObject response = obj.getJSONArray("response").getJSONObject(0);
                            if (response.has("trip_id") && tripId.equals(response.getString("trip_id")) && response.has("route_json")) {
                                JSONArray routesJson = new JSONArray(response.getString("route_json"));
                                final ArrayList<LatLng> latLngs = new ArrayList<>();
                                for (int i = 0; i < routesJson.length(); i++) {
                                    JSONObject objLatLng = routesJson.getJSONObject(i);
                                    latLngs.add(new LatLng(Double.parseDouble(objLatLng.getString("lat")),
                                            Double.parseDouble(objLatLng.getString("lng"))));
                                }
                                if (latLngs.size() > 1) {
                                    if (googleMap != null) {
                                        googleMap.clear();
                                        PolylineOptions lineOptions = new PolylineOptions();
                                        lineOptions.addAll(latLngs);
                                        lineOptions.width(8);
                                        lineOptions.color(Color.BLACK);
                                        lineOptions.geodesic(true);
                                        lineOptions.zIndex(4);
                                        googleMap.addPolyline(lineOptions);
                                        Bitmap bitmapSrc = BitmapFactory.decodeResource(getResources(), R.drawable.source11);
                                        Bitmap bitmapDes = BitmapFactory.decodeResource(getResources(), R.drawable.destination11);

                                        googleMap.addMarker(new MarkerOptions().position(latLngs.get(0)).icon(BitmapDescriptorFactory.fromBitmap(bitmapSrc)));
                                        googleMap.addMarker(new MarkerOptions().position(latLngs.get(latLngs.size() - 1)).icon(BitmapDescriptorFactory.fromBitmap(bitmapDes)));
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    LatLngBounds.Builder bounds = new LatLngBounds.Builder();
                                                    bounds.include(latLngs.get(0));
                                                    for (LatLng ltln : latLngs) {
                                                        bounds.include(ltln);
                                                    }
                                                    bounds.include(latLngs.get(latLngs.size() - 1));
                                                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 150));
                                                } catch (Exception e) {
                                                    Log.e(TAG, "onUpdateDeviceTokenOnServer: " + e.getMessage(), e);
                                                }
                                            }
                                        }, 500);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onUpdateDeviceTokenOnServer: " + e.getMessage(), e);
                    }

                }
            }
        });
    }

}