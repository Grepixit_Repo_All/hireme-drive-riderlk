package com.ridertechrider.driver.GetDriverAsset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDriverAssetResponse {

    @SerializedName("driver_asset_id")
    @Expose
    private String driverAssetId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("image_id")
    @Expose
    private String imageId;
    @SerializedName("asset_type")
    @Expose
    private String assetType;
    @SerializedName("expire_date")
    @Expose
    private String expireDate;
    @SerializedName("is_front")
    @Expose
    private String isFront;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("is_delete")
    @Expose
    private String isDelete;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("img_type")
    @Expose
    private String imgType;
    @SerializedName("img_name")
    @Expose
    private String imgName;
    @SerializedName("img_description")
    @Expose
    private String imgDescription;
    @SerializedName("img_path")
    @Expose
    private String imgPath;
    @SerializedName("video_path")
    @Expose
    private Object videoPath;
    @SerializedName("img_created")
    @Expose
    private String imgCreated;
    @SerializedName("img_modified")
    @Expose
    private String imgModified;

    public String getDriverAssetId() {
        return driverAssetId;
    }

    public void setDriverAssetId(String driverAssetId) {
        this.driverAssetId = driverAssetId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getIsFront() {
        return isFront;
    }

    public void setIsFront(String isFront) {
        this.isFront = isFront;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getImgType() {
        return imgType;
    }

    public void setImgType(String imgType) {
        this.imgType = imgType;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public String getImgDescription() {
        return imgDescription;
    }

    public void setImgDescription(String imgDescription) {
        this.imgDescription = imgDescription;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Object getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(Object videoPath) {
        this.videoPath = videoPath;
    }

    public String getImgCreated() {
        return imgCreated;
    }

    public void setImgCreated(String imgCreated) {
        this.imgCreated = imgCreated;
    }

    public String getImgModified() {
        return imgModified;
    }

    public void setImgModified(String imgModified) {
        this.imgModified = imgModified;
    }

}