package com.ridertechrider.driver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class TransactionList implements Serializable {

    public Transactions transactions;
    public DriverTransactions driverTransactions;

    public static TransactionList parseJson(String s) {
        TransactionList tripModel = new TransactionList();
        tripModel.transactions = Transactions.parseJson(s);
        JSONObject tripJsonObject;
        try {
            tripJsonObject = new JSONObject(s);
            if (tripJsonObject.has("Transaction") && tripJsonObject.get("Transaction") instanceof JSONObject)
                tripModel.driverTransactions = DriverTransactions.parseJson(tripJsonObject.getJSONObject("Transaction").toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tripModel;
    }

}
