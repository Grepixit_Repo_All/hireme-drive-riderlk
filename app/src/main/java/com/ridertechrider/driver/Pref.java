package com.ridertechrider.driver;

import android.content.Context;
import android.content.SharedPreferences;

import com.ridertechrider.driver.webservice.Constants;

import java.util.Locale;

/**
 * Created by Grepix on 12/5/2016.
 */
public class Pref {

    private final SharedPreferences sharedPreferences;
    private final String IS_LOGIN = "is_login";
    private final String PASSWORD = "d_password";
    private final String D_IS_AVAILABLE = "d_is_available";
    private final String BITMAP_STRING = "bitmap_string";
    private final String Driver_Lat = "d_lat";
    private final String Driver_Lng = "d_lng";
    private final String Driver_Bearing = "d_bearing";
    private final String TRIP_DISTANCE = "trip_distance";
    private final String TRIP_PAY_AMOUNT = "trip_pay_amount";
    private final String LICENSE_BITMAP_TO_STRING_ = "license";
    private final String TIME_DISTANCE_VIEW = "time_distance_view";
    private final String TRIP_DURATION_CHANGEABLE = "duration";
    private final String TRIP_ID = "trip_id";
    private final String TRIP_DISTANCE_CHANGEABLE_FLOAT = "distance_float";
    private final String TRIP_DURATION_CHANGEABLE_INT = "duration_int";
    private final String TRIP_DISTANCE_CALCULATED = "distance_calculated";
    private final String TRIP_START_TIME = "trip_start_time";
    private final String LOCAL_TRIP_STATUS = "local_trip_status";
    private final String INSURANCE_BITMAP_TO_STRING_ = "insurance";
    private final String CATEGORY_RESPONSE = "category_response";
    private final String DRIVER_TRIP_START_LATITUDE = "trip_latitude";
    private final String DRIVER_TRIP_START_LONGITUDE = "trip_longitude";
    private final String REJECTED_TRIP = "rejected_trip";
    private final String CONSTANT_RESPONCE = "constant_distance";
    private final String CITY_RESPONCE = "city_response";
    private final String AGENCY_RESPONCE = "agency_response";
    private final String IS_SINGLE_MODE = "is_single_mode";
    private final String WAITING_TIME = "waiting_min";
    private final String WAITING_START_TIME = "waiting_start_time";
    private final String WAITING_STOP_TIME = "waiting_stop_time";
    private final String CAR_IMAGE_TO_STRING = "car_image";

    private final String SETTINGS_RESPONCE = "Settings_Reponse";

    public String getSettingsResponse() {
        return sharedPreferences.getString(SETTINGS_RESPONCE, "{\"response\": []}");
    }


    public Pref(Context context) {
        sharedPreferences = context.getSharedPreferences("hireme-driver", Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor edit() {
        return sharedPreferences.edit();
    }

    public void setTripIds(String tripId) {
        edit().putString("t_ids", tripId).commit();
    }

    public String getRejectedTrip() {
        return sharedPreferences.getString(REJECTED_TRIP, "");
    }

    public void setRejectedTrip(String categoryResponse) {

        edit().putString(REJECTED_TRIP, categoryResponse).commit();
    }
    public String getCAR_IMAGE_TO_STRING() {
        return sharedPreferences.getString(CAR_IMAGE_TO_STRING, null);
    }

    public void setCAR_IMAGE_TO_STRING(String car_string) {
        edit().putString(CAR_IMAGE_TO_STRING, car_string).commit();
    }

    public String geTripIds() {
        return sharedPreferences.getString("t_ids", "");
    }

    public void setTripId(String tripId) {
        edit().putString(TRIP_ID, tripId).commit();
    }

    public String geTripId() {
        return sharedPreferences.getString(TRIP_ID, "");
    }

    public String getCategoryResponse() {
        return sharedPreferences.getString(CATEGORY_RESPONSE, "");
    }

    public void setCategoryResponse(String categoryResponse) {
        edit().putString(CATEGORY_RESPONSE, categoryResponse).commit();
    }

    public String getDriverStartLatitude() {
        return sharedPreferences.getString(DRIVER_TRIP_START_LATITUDE, "0");
    }

    public void setDriverStartLatitude(String startLocation) {
        edit().putString(DRIVER_TRIP_START_LATITUDE, startLocation).commit();
    }

    public String getSelectedLanguage() {
        Locale aDefault = Locale.getDefault();
        String appLanguage = sharedPreferences.getString("localeString", null);
        if (appLanguage == null) {
            if (aDefault.getLanguage().equals(Constants.Language.PORTUGEUESE)) {
                return aDefault.getLanguage();
            } else {
                return Constants.Language.ENGLISH;
            }
        }
        return appLanguage;
    }

    public void setSelectedLanguage(String localeString) {

        edit().putString("localeString", localeString).commit();
    }

    public String getDriverStartLongitude() {
        return sharedPreferences.getString(DRIVER_TRIP_START_LONGITUDE, "0");
    }

    public void setDriverStartLongitude(String startLocation) {
        edit().putString(DRIVER_TRIP_START_LONGITUDE, startLocation).commit();
    }

    public String getLocalTripState() {
        return sharedPreferences.getString(LOCAL_TRIP_STATUS, "");
    }

    public void setLocalTripState(String car_model) {
        edit().putString(LOCAL_TRIP_STATUS, car_model).commit();
    }

    public void setCalculateDistance(boolean calculateDistance) {
        String IS_CALCULATE_TRIP = "trip_is_calculate";
        edit().putBoolean(IS_CALCULATE_TRIP, calculateDistance).commit();
    }

    public double getCalculatedDistance() {
        return sharedPreferences.getFloat(TRIP_DISTANCE_CALCULATED, 0);
    }

    public void setCalculatedDistance(double calculatedDistance) {
        edit().putFloat(TRIP_DISTANCE_CALCULATED, (float) calculatedDistance).commit();
    }

    public String getTripStartTime() {
        return sharedPreferences.getString(TRIP_START_TIME, "");
    }

    public void setTripStartTime(String trip_start_time) {
        edit().putString(TRIP_START_TIME, trip_start_time).commit();
    }

    public boolean getIsLogin() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void setIsLogin(boolean isPush) {
        edit().putBoolean(IS_LOGIN, isPush).commit();
    }

    public void setTRIP_DISTANCE_CHANGEABLE(String distance_changeable) {
        String TRIP_DISTANCE_CHANGEABLE = "distance";
        edit().putString(TRIP_DISTANCE_CHANGEABLE, distance_changeable).commit();
    }

    public String getTRIP_DISTANCE_CHANGEABLE_FLOAT() {
        return sharedPreferences.getString(TRIP_DISTANCE_CHANGEABLE_FLOAT, "0");
    }

    public void setTRIP_DISTANCE_CHANGEABLE_FLOAT(String distance_changeable) {
        edit().putString(TRIP_DISTANCE_CHANGEABLE_FLOAT, distance_changeable).commit();
    }

    public String getTRIP_DURATION_CHANGEABLE() {
        return sharedPreferences.getString(TRIP_DURATION_CHANGEABLE, null);
    }

    public void setTRIP_DURATION_CHANGEABLE(String duration_changeable) {
        edit().putString(TRIP_DURATION_CHANGEABLE, duration_changeable).commit();
    }

    public int getTRIP_DURATION_CHANGEABLE_INT() {
        return sharedPreferences.getInt(TRIP_DURATION_CHANGEABLE_INT, 0);
    }

    public void setTRIP_DURATION_CHANGEABLE_INT(int duration_changeable) {
        edit().putInt(TRIP_DURATION_CHANGEABLE_INT, duration_changeable).commit();
    }

    public boolean getTIME_DISTANCE_VIEW() {
        return sharedPreferences.getBoolean(TIME_DISTANCE_VIEW, false);
    }

    public void setTIME_DISTANCE_VIEW(boolean time_distance_view) {
        edit().putBoolean(TIME_DISTANCE_VIEW, time_distance_view).commit();
    }

    public String getINSURANCE_BITMAP_TO_STRING_() {
        return sharedPreferences.getString(INSURANCE_BITMAP_TO_STRING_, null);
    }

    public void setINSURANCE_BITMAP_TO_STRING_(String insurance_string) {
        edit().putString(INSURANCE_BITMAP_TO_STRING_, insurance_string).commit();
    }

    public String getLICENSE_BITMAP_TO_STRING_() {
        return sharedPreferences.getString(LICENSE_BITMAP_TO_STRING_, null);
    }

    public void setLICENSE_BITMAP_TO_STRING_(String license_string) {
        edit().putString(LICENSE_BITMAP_TO_STRING_, license_string).commit();
    }

    public void setString(String key, String value) {
        edit().putString(key, value).commit();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, null);
    }

    public String getBITMAP_STRING() {
        return sharedPreferences.getString(BITMAP_STRING, null);
    }

    public void setBITMAP_STRING(String bitmap_string) {
        edit().putString(BITMAP_STRING, bitmap_string).commit();
    }

    public void setIS_FARE_SUMMARY_OPEN(String fareSummaryOpen) {
        String IS_FARE_SUMMARY_OPEN = "fare_summary_open";
        edit().putString(IS_FARE_SUMMARY_OPEN, fareSummaryOpen).commit();
    }

    public String getTRIP_PAY_AMOUNT() {
        return sharedPreferences.getString(TRIP_PAY_AMOUNT, null);
    }

    public void setTRIP_PAY_AMOUNT(String trip_pay_amount) {
        edit().putString(TRIP_PAY_AMOUNT, trip_pay_amount).commit();
    }

    public String getTRIP_DISTANCE() {
        return sharedPreferences.getString(TRIP_DISTANCE, null);
    }

    public void setTRIP_DISTANCE(String trip_distance) {
        edit().putString(TRIP_DISTANCE, trip_distance).commit();
    }

    public void setTRIP_PICKUP_TIME(String trip_pickup_time) {
        String TRIP_PICKUP_TIME = "trip_pickup_time";
        edit().putString(TRIP_PICKUP_TIME, trip_pickup_time).commit();
    }

    public void setTRIP_FARE(String trip_fare) {
        String TRIP_FARE = "trip_fare";
        edit().putString(TRIP_FARE, trip_fare).commit();
    }

    public String getDriver_Lat() {
        return sharedPreferences.getString(Driver_Lat, "0");
    }

    public void setDriver_Lat(String d_lat) {
        edit().putString(Driver_Lat, d_lat).commit();
    }

    public String getDriver_Lng() {
        return sharedPreferences.getString(Driver_Lng, "0");
    }

    public void setDriver_Lng(String d_lng) {
        edit().putString(Driver_Lng, d_lng).commit();
    }

    public String getDriver_Bearing() {
        return sharedPreferences.getString(Driver_Bearing, "0");
    }

    public void setDriver_Bearing(String d_lng) {
        edit().putString(Driver_Bearing, d_lng).commit();
    }

    public String getD_IS_AVAILABLE() {
        return sharedPreferences.getString(D_IS_AVAILABLE, null);
    }

    public void setD_IS_AVAILABLE(String d_is_available) {
        edit().putString(D_IS_AVAILABLE, d_is_available).commit();
    }

    public String getPASSWORD() {
        return sharedPreferences.getString(PASSWORD, null);
    }

    public void setPASSWORD(String password) {
        edit().putString(PASSWORD, password).commit();
    }

    public void setTRIP_DURATION_VALUE_CHANGEABLE(String duration_changeable) {
        String TRIP_DURATION_VALUE_CHANGEABLE = "duration_value";
        edit().putString(TRIP_DURATION_VALUE_CHANGEABLE, duration_changeable).commit();
    }

    public String getConstantResponse() {
        return sharedPreferences.getString(CONSTANT_RESPONCE, "{\"response\": []}");
    }

    public void setConstantResponse(String response) {
        edit().putString(CONSTANT_RESPONCE, response).commit();
    }

    public String getCitiesResponse() {
        return sharedPreferences.getString(CITY_RESPONCE, "{\"response\": []}");
    }

    public void setCitiesResponse(String response) {
        edit().putString(CITY_RESPONCE, response).commit();
    }

    public String getAgenciesResponse() {
        return sharedPreferences.getString(AGENCY_RESPONCE, "{\"response\": []}");
    }

    public void setAgenciesResponse(String response) {
        edit().putString(AGENCY_RESPONCE, response).commit();
    }

    public void setSingleMode(boolean isSingleMode) {
        edit().putBoolean(IS_SINGLE_MODE, isSingleMode).commit();
    }

    public boolean getIsSingleMode() {
        return sharedPreferences.getBoolean(IS_SINGLE_MODE, false);
    }

    public int getWaitingTime() {
        return sharedPreferences.getInt(WAITING_TIME, 0);
    }

    public void setWaitingTime(int duration_changeable) {
        edit().putInt(WAITING_TIME, duration_changeable).commit();
    }

    public long getWaitingStartTime() {
        return sharedPreferences.getLong(WAITING_START_TIME, 0);
    }

    public void setWaitingStartTime(long startTime) {
        edit().putLong(WAITING_START_TIME, startTime).commit();
    }

    public long getWaitingStopTime() {
        return sharedPreferences.getLong(WAITING_START_TIME, 0);
    }

    public void setWaitingStopTime(long stopTime) {
        edit().putLong(WAITING_START_TIME, stopTime).commit();
    }

    public boolean getIsWaiting() {
        return sharedPreferences.getBoolean("is_waiting", false);
    }

    public void setIsWaiting(boolean isPush) {
        edit().putBoolean("is_waiting", isPush).commit();
    }

    public String getLocalizeData() {
        return sharedPreferences.getString("LocalizeKey", null);
    }

    public void setLocalizeData(String data) {
        edit().putString("LocalizeKey", data).commit();
    }

    public String getLocalizeLang() {
        return sharedPreferences.getString("LocalizeLang", null);
    }

    public void setLocalizeLang(String data) {
        edit().putString("LocalizeLang", data).commit();
    }
}
