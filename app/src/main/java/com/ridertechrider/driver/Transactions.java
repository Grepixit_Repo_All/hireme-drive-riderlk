package com.ridertechrider.driver;

import com.google.gson.Gson;

import java.io.Serializable;

public class Transactions implements Serializable {
    private String trans_drv_id;
    private String transaction_id;
    private String trans_type;
    private String driver_id;
    private String amount;
    private String remarks;
    private String created;
    private String modified;
    private String current_bal;

    public static Transactions parseJson(String transaction) {
        return new Gson().fromJson(transaction, Transactions.class);
    }

    public String getCurrent_bal() {
        return current_bal;
    }

    public void setCurrent_bal(String current_bal) {
        this.current_bal = current_bal;
    }

    public String getTrans_drv_id() {
        return trans_drv_id;
    }

    public void setTrans_drv_id(String trans_drv_id) {
        this.trans_drv_id = trans_drv_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
