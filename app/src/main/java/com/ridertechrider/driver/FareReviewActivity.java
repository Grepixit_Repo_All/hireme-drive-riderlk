package com.ridertechrider.driver;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.webservice.CategoryActors;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class FareReviewActivity extends BaseActivity {

    private Button cancelfarereview;
    private TextView wait;
    private TextView tax;
    private TextView total;
    private TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fare_summary_activity);
        Controller controller = (Controller) getApplicationContext();

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        RatingBar ratingBar = findViewById(R.id.rb_review);
        TextView averageRating = findViewById(R.id.driver_average_rating);
        TextView pickup = findViewById(R.id.pickuploc);
        TextView drop = findViewById(R.id.droploc);
        TextView distance = findViewById(R.id.distance);
        TextView totalAmount = findViewById(R.id.total_amount);
        TextView tax_amount = findViewById(R.id.tax_amount);
        TextView tripid = findViewById(R.id.tripid);
        TextView driverid = findViewById(R.id.driverid);
        TextView tvCarName = findViewById(R.id.car_name);
        ImageView recancel = findViewById(R.id.back_fare);
        TextView name = findViewById(R.id.name);
        LinearLayout tax_layout = findViewById(R.id.tax_layout);

        recancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final TripModel currentTripModel = AppData.getInstance(getApplicationContext()).getTripModel();
        ArrayList<CategoryActors> categoryResponseList = CategoryActors.parseCarCategoriesResponse(controller.pref.getCategoryResponse());
        String catgoryName = "";
        if (currentTripModel.driver != null) {
            for (CategoryActors catagories : categoryResponseList)
                if (catagories.getCategory_id().equals(currentTripModel.driver.getCategory_id())) {
                    catgoryName = catagories.getCat_name();
                }
            tvCarName.setText(catgoryName);
            driverid.setText(currentTripModel.driver.getDriverId());
        }


        CircleImageView thumbNail = findViewById(R.id.icon);
        Picasso.get().load(Constants.IMAGE_BASE_URL + currentTripModel.user.getUProfileImagePath()).error(R.drawable.circleuser).into(thumbNail);


        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                String TAG = "MainScreenActivity";
                try {
                    // Customise map styling via JSON file
                    boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(FareReviewActivity.this, R.raw.uberstyle));

                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }

                MarkerOptions marker = new MarkerOptions();
                LatLng pick;
                LatLng drop;

                if (currentTripModel.trip.getTrip_actual_pick_lat() == null || currentTripModel.trip.getTrip_actual_pick_lat().equalsIgnoreCase("null")) {
                    pick = new LatLng(Double.parseDouble(currentTripModel.trip.getTrip_scheduled_pick_lat()), Double.parseDouble(currentTripModel.trip.getTrip_scheduled_pick_lng()));
                    drop = new LatLng(Double.parseDouble(currentTripModel.trip.getTrip_scheduled_drop_lat()), Double.parseDouble(currentTripModel.trip.getTrip_scheduled_drop_lng()));
                } else {
                    pick = new LatLng(Double.parseDouble(currentTripModel.trip.getTrip_actual_pick_lat()), Double.parseDouble(currentTripModel.trip.getTrip_actual_pick_lng()));
                    drop = new LatLng(Double.parseDouble(currentTripModel.trip.getTrip_actual_drop_lat()), Double.parseDouble(currentTripModel.trip.getTrip_actual_drop_lng()));
                }
                marker.position(pick).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_20));
                googleMap.addMarker(marker);
                marker.position(drop).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_20));
                googleMap.addMarker(marker);

                googleMap.getUiSettings().setScrollGesturesEnabled(false);
                googleMap.getUiSettings().setZoomGesturesEnabled(false);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                //the include method will calculate the min and max bound.
                builder.include(pick);
                builder.include(drop);
                LatLngBounds bounds = builder.build();

                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels / 2;
                int padding = (int) (width * 0.30); // offset from edges of the map 10% of screen
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                googleMap.animateCamera(cu);
            }
        });
        if (currentTripModel.driver.getD_rating() != null) {
            float rating = Float.parseFloat(currentTripModel.driver.getD_rating());
            averageRating.setText(new DecimalFormat("##.#").format(rating));
            ratingBar.setRating(rating);
        }

        pickup.setText(currentTripModel.trip.getTrip_to_loc());
        if (currentTripModel.trip.getPickup_notes() != null)
            drop.setText(String.format("%s, %s", currentTripModel.trip.getPickup_notes(), currentTripModel.trip.getTrip_from_loc()));
        else
            drop.setText(currentTripModel.trip.getTrip_from_loc());
        if (currentTripModel.trip != null && currentTripModel.trip.getActual_from_loc() != null && currentTripModel.trip.getActual_from_loc().trim().length() != 0) {
            if (currentTripModel.trip.getPickup_notes() != null)
                pickup.setText(String.format("%s, %s", currentTripModel.trip.getPickup_notes(), currentTripModel.trip.getActual_from_loc()));
            else
                pickup.setText(currentTripModel.trip.getActual_from_loc());
            //pickup.setText(currentTripModel.trip.getActual_from_loc());
        } else {

            if (Objects.requireNonNull(currentTripModel.trip).getPickup_notes() != null)
                pickup.setText(String.format("%s, %s", Objects.requireNonNull(currentTripModel.trip).getPickup_notes(), Objects.requireNonNull(currentTripModel.trip).getTrip_from_loc()));
            else
                pickup.setText(Objects.requireNonNull(currentTripModel.trip).getTrip_from_loc());
//            pickup.setText(Objects.requireNonNull(currentTripModel.trip).getTrip_from_loc());
        }
        if (currentTripModel.trip != null && currentTripModel.trip.getActual_to_loc() != null && currentTripModel.trip.getActual_to_loc().trim().length() != 0) {
            drop.setText(currentTripModel.trip.getActual_to_loc());
        } else {
            drop.setText(Objects.requireNonNull(currentTripModel.trip).getTrip_to_loc());
        }
        name.setText(currentTripModel.user.getU_fname() + " " + currentTripModel.user.getU_lname());
        tripid.setText(currentTripModel.trip.getTrip_id());
        Double amountDouble = Double.valueOf(currentTripModel.trip.getTrip_pay_amount());
        Double promoAmountDouble = Double.valueOf(currentTripModel.trip.getTrip_promo_amt());
        try {


            distance.setText(controller.formatDistanceWithUnit(currentTripModel.trip.getTrip_distance()));
            if (currentTripModel.trip.getTrip_tax_amt().equalsIgnoreCase("0")) {
                tax_layout.setVisibility(View.GONE);
            } else {
                tax_amount.setText(controller.formatAmountWithCurrencyUnit(currentTripModel.trip.getTrip_tax_amt()));
            }
            totalAmount.setText(controller.formatAmountWithCurrencyUnit(String.valueOf(amountDouble - promoAmountDouble)));
        } catch (Exception e) {
            e.printStackTrace();
            total.setText("");
        }
    }
}
