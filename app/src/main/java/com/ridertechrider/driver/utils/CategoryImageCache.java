package com.ridertechrider.driver.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

@SuppressWarnings("ALL")
public class CategoryImageCache {

    private static HashMap<String, Bitmap> stringBitmapHashMap = new HashMap<>();
    private static String json;

    private static void readData(Context context) {
        json = ReadFromfile(context);
    }

    @SuppressLint("WorldReadableFiles")
    private static String ReadFromfile(Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open("image.json", Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception ignore) {
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception ignore) {
            }
        }
        return returnString.toString();
    }

    public static Bitmap getImageCateId(Context context, String id) {
        if (stringBitmapHashMap.containsKey("cate" + id)) {
            return stringBitmapHashMap.get("cate" + id);
        }
        if (json == null) {
            readData(context);
        }
        try {
            JSONObject jsonObject = new JSONObject(json);
            byte[] decodedString = Base64.decode(jsonObject.getString("cate" + id), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Bitmap size = scaleBitmap(decodedByte);
            return size;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Bitmap scaleBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        Log.v("Pictures", "Width and height are " + width + "--" + height);

        int maxWidth = 100;
        int maxHeight = 100;

        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int) (height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int) (width / ratio);
        } else {
            // square
            height = maxHeight;
            width = maxWidth;
        }

        Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);

        bm = Bitmap.createScaledBitmap(bm, width, height, true);
        return bm;
    }
}
