package com.ridertechrider.driver;

import android.content.Context;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ridertechrider.driver.fonts.Fonts;

/**
 * Created by grepix on 8/2/2016.
 */
class ClientPickedUpView {
    private final Context context;
    private final View view;
    private final boolean isDelivery;
    private ClientPickedUpCallback callback;

    public ClientPickedUpView(Context context, View view, boolean isPickedUp) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        this.context = context;
        this.view = view;
        this.isDelivery = isPickedUp;
        init();
    }

    public void setClientPickedUpCallback(ClientPickedUpCallback callback) {
        this.callback = callback;

    }

    public void hide() {
        this.view.setVisibility(View.GONE);
    }

    public void show() {
        this.view.setVisibility(View.VISIBLE);
    }

    /*public ClientPickedUpView(Context context, View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        this.context = context;
        this.view = view;
        init();
    }*/

    private void init() {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), Fonts.HELVETICA_NEUE);

        TextView tvClientPickedup = view.findViewById(R.id.tv_client_picked_up);
        tvClientPickedup.setText(Localizer.getLocalizerString("k_23_s4_client_picked_up"));
        tvClientPickedup.setTypeface(typeface);
        if (isDelivery) {
            tvClientPickedup.setText(R.string.picket_up);
        }
        Button btnYes = view.findViewById(R.id.btn_yes);
        btnYes.setText(Localizer.getLocalizerString("k_21_s4_yes"));
        btnYes.setTypeface(typeface);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
                callback.onYesButtonClicked();
            }
        });
        Button btnNo = view.findViewById(R.id.btn_no);
        btnNo.setText(Localizer.getLocalizerString("k_22_s4_no"));
        btnNo.setTypeface(typeface);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
                callback.onNoButtonClicked();
            }
        });
    }

    public interface ClientPickedUpCallback {
        void onYesButtonClicked();

        void onNoButtonClicked();
    }


}
