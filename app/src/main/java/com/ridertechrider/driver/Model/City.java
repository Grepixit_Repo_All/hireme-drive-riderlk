package com.ridertechrider.driver.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class City {

    private String city_id;
    private String country_id;
    private String city_name;
    private String city_code;
    private String country_code = "0";
    private String city_cur;
    private String city_comm;
    private String city_tax;
    private String city_active;
    private String city_created;
    private String city_modified;
    private String city_pay_options;
    private String city_dist_unit;
    private String is_d_set_cost;

    public City() {
    }

    public City(String city_name) {
        this.city_name = city_name;
    }

    public static City parse(JSONObject obj) throws JSONException {
        City city = new City();
        if (obj.has("city_id"))
            city.setCity_id(obj.getString("city_id"));
        if (obj.has("country_id"))
            city.setCountry_id(obj.getString("country_id"));
        if (obj.has("city_name"))
            city.setCity_name(obj.getString("city_name"));
        if (obj.has("city_code"))
            city.setCity_code(obj.getString("city_code"));
        if (obj.has("country_code"))
            city.setCountry_code(obj.getString("country_code"));
        if (obj.has("city_cur"))
            city.setCity_cur(obj.getString("city_cur"));
        if (obj.has("city_comm"))
            city.setCity_comm(obj.getString("city_comm"));
        if (obj.has("city_tax"))
            city.setCity_tax(obj.getString("city_tax"));
        if (obj.has("city_active"))
            city.setCity_active(obj.getString("city_active"));
        if (obj.has("city_created"))
            city.setCity_created(obj.getString("city_created"));
        if (obj.has("city_modified"))
            city.setCity_modified(obj.getString("city_modified"));
        if (obj.has("city_pay_options"))
            city.setCity_pay_options(obj.getString("city_pay_options"));
        if (obj.has("is_d_set_cost"))
            city.setIs_d_set_cost(obj.getString("is_d_set_cost"));
        if (obj.has("city_dist_unit"))
            city.setCity_dist_unit(obj.getString("city_dist_unit"));
//        Log.d("City", "parse: " + city.getCity_name());
        return city;
    }

    public static ArrayList<City> parseCities(String response) {
        ArrayList<City> cities = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(response);
            if (obj.has("status") && !obj.isNull("status") &&
                    obj.getString("status").equalsIgnoreCase("OK") && obj.has("response")) {
                JSONArray objArray = obj.getJSONArray("response");

                if (objArray != null && objArray.length() > 0) {
                    for (int i = 0; i < objArray.length(); i++) {
                        cities.add(parse(objArray.getJSONObject(i)));
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(City.class.getSimpleName(), "parseAgencies: " + e.getMessage(), e);
        }
        return cities;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_code() {
        return city_code;
    }

    public void setCity_code(String city_code) {
        this.city_code = city_code;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCity_cur() {
        return city_cur;
    }

    public void setCity_cur(String city_cur) {
        this.city_cur = city_cur;
    }

    public String getCity_comm() {
        return city_comm;
    }

    public void setCity_comm(String city_comm) {
        this.city_comm = city_comm;
    }

    public String getCity_active() {
        return city_active;
    }

    public void setCity_active(String city_active) {
        this.city_active = city_active;
    }

    public String getCity_created() {
        return city_created;
    }

    public void setCity_created(String city_created) {
        this.city_created = city_created;
    }

    public String getCity_modified() {
        return city_modified;
    }

    public void setCity_modified(String city_modified) {
        this.city_modified = city_modified;
    }

    public String getCity_tax() {
        return city_tax;
    }

    public void setCity_tax(String city_tax) {
        this.city_tax = city_tax;
    }

    public String getCity_pay_options() {
        return city_pay_options;
    }

    public void setCity_pay_options(String city_pay_options) {
        this.city_pay_options = city_pay_options;
    }

    public String getIs_d_set_cost() {
        return is_d_set_cost;
    }

    public void setIs_d_set_cost(String is_d_set_cost) {
        this.is_d_set_cost = is_d_set_cost;
    }

    public String getCity_dist_unit() {
        return city_dist_unit;
    }

    public void setCity_dist_unit(String city_dist_unit) {
        this.city_dist_unit = city_dist_unit;
    }
}
