package com.ridertechrider.driver;

import com.ridertechrider.driver.webservice.model.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryResponse extends BaseResponse {
    @SerializedName("response")
    private ArrayList<Catagories> catagories;

    public ArrayList<Catagories> getCatagories() {
        return catagories;
    }
}
