package com.ridertechrider.driver;

import com.google.gson.Gson;

import java.io.Serializable;

public class DriverTransactions implements Serializable {

    private String transaction_id, trip_id, trans_date, trans_type, trans_description, total_amt, commission_amt, tax_amt, other_amt, net_amt, trans_status, created, modified, trans_pay_mode;


    public static DriverTransactions parseJson(String userJson) {
        return new Gson().fromJson(userJson, DriverTransactions.class);
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getTrans_date() {
        return trans_date;
    }

    public void setTrans_date(String trans_date) {
        this.trans_date = trans_date;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getTrans_description() {
        return trans_description;
    }

    public void setTrans_description(String trans_description) {
        this.trans_description = trans_description;
    }

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getCommission_amt() {
        return commission_amt;
    }

    public void setCommission_amt(String commission_amt) {
        this.commission_amt = commission_amt;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public String getOther_amt() {
        return other_amt;
    }

    public void setOther_amt(String other_amt) {
        this.other_amt = other_amt;
    }

    public String getNet_amt() {
        return net_amt;
    }

    public void setNet_amt(String net_amt) {
        this.net_amt = net_amt;
    }

    public String getTrans_status() {
        return trans_status;
    }

    public void setTrans_status(String trans_status) {
        this.trans_status = trans_status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getTrans_pay_mode() {
        return trans_pay_mode;
    }
}
