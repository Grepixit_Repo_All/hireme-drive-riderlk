package com.ridertechrider.driver;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.ridertechrider.driver.custom.BTextView;

/**
 * Created by grepix on 7/25/2016.
 */
class BackToOrderView {
    public static EditText editText;
    public static RelativeLayout relativeLayout;
    private final View view;
    private BackToOrderViewCallback callback;

    public BackToOrderView(View view) {

        this.view = view;
        init(view);
    }

    public void setBackToOrderCallback(BackToOrderViewCallback callback) {
        this.callback = callback;

    }

    public void hide() {
        this.view.setVisibility(View.GONE);
    }

    public void show() {
        this.view.setVisibility(View.VISIBLE);
    }

    private void init(View view) {
        relativeLayout = view.findViewById(R.id.back_to_relative1);
        Button okButton = view.findViewById(R.id.ok_button_id11);
        editText = view.findViewById(R.id.et_why_not_value11);
        BTextView tv_why_not11 = view.findViewById(R.id.tv_why_not11);
        tv_why_not11.setText(Localizer.getLocalizerString("k_25_s4_why_not"));
        okButton.setText(Localizer.getLocalizerString("k_r8_s8_ok"));

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  hide();
                callback.onOkButtonClicked();
            }
        });
    }

    public interface BackToOrderViewCallback {
        void onOkButtonClicked();

    }
}
