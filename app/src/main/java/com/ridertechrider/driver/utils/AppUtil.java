package com.ridertechrider.driver.utils;

import android.content.Context;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.ridertechrider.driver.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class AppUtil {

    private static final String APP_TRIP_DATE_FROMAT = "dd/MM/yy HH:mm:ss";
    private static ImageLoader instance;

    public static boolean hasM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static ImageLoader getImageLoaderInstanceForProfileImage(Context context) {
        if (instance == null) {
            DisplayImageOptions displayimageOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.user) // resource or drawable
                    .showImageForEmptyUri(R.drawable.user) // resource or drawable
                    .showImageOnFail(R.drawable.user) // resource or drawable
                    .resetViewBeforeLoading(false) // default
                    .cacheInMemory(true) // default
                    .cacheOnDisk(false) // default
                    .displayer(new SimpleBitmapDisplayer()) // default
                    .handler(new Handler()) // default
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).
                    defaultDisplayImageOptions(displayimageOptions).build();
            instance = ImageLoader.getInstance();
            instance.init(config);
        }
        return instance;
    }


    @NonNull
    public static DateFormat getDateFormat() {
        return new SimpleDateFormat(APP_TRIP_DATE_FROMAT, Locale.ENGLISH);
    }

    public static Date convertStringDateToAppTripDate(String date) {
        if (date != null && date.trim().length() != 0) {
            DateFormat df = getDateFormat();
            try {
                return df.parse(date);
            } catch (ParseException e) {
               // e.printStackTrace();
            }
        }

        return null;
    }

    public static double getTimeBetweenTwoDateDifference(Date date1, Date date2) {
        long diffInMilli = date2.getTime() - date1.getTime();
        return diffInMilli / (60.0 * 1000.0);

    }

    public static long getTimeBetweenTwoDateDifferenceInMi(Date date1, Date date2) {
        return date2.getTime() - date1.getTime();

    }

    public static String getCurrentDateInGMTZeroInServerFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Utils.SERVER_DATE_FORMAT, Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(new Date());
    }
}
