package com.ridertechrider.driver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ridertechrider.driver.custom.BTextView;

public class Dest {

    public TextView delivery_address;
    public LinearLayout dest;
    public TextView distance;
    public ImageView deliveryImage;
    BTextView viewImage;
    private View view;
    private Context context;

    public Dest(Context context, int layoutId, View.OnClickListener onClickListener) {
        this.context = context;
        this.view = LayoutInflater.from(context).inflate(layoutId, null, false);
        intView(view);
        view.setOnClickListener(onClickListener);
    }

    public Dest(Context context, View view) {
        this.context = context;
        this.view = view;
        intView(view);
    }

    private void intView(View view) {
        delivery_address = view.findViewById(R.id.delivery_address);
        dest = view.findViewById(R.id.dest);
        distance = view.findViewById(R.id.distance);
        deliveryImage = view.findViewById(R.id.deliveryImage);
        viewImage = view.findViewById(R.id.viewImage);
    }

    public View getView() {
        return view;
    }
}
