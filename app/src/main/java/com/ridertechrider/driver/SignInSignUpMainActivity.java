package com.ridertechrider.driver;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class SignInSignUpMainActivity extends BaseCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_sign_up_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        new GetDeviceID().execute();
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.addTab(tabs.newTab().setText(R.string.login));
        tabs.addTab(tabs.newTab().setText(R.string.register));
        ViewPager vpPager = findViewById(R.id.vpPager);
        tabs.setTabGravity(0);
        vpPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        MyPagerAdapter adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);

        ImageView mimageView = findViewById(R.id.img);

        Bitmap mbitmap = ((BitmapDrawable) getResources().getDrawable(R.mipmap.ic_launcher)).getBitmap();
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 30, 30, mpaint);// Round Image Corner 100 100 100 100
        mimageView.setImageBitmap(imageRounded);

        LinearLayout linearLayout = (LinearLayout) tabs.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);

    }

    class MyPagerAdapter extends FragmentPagerAdapter {

        MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return 2;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return SignInFragment.newInstance("", "Page # 1");
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return RegisterFragment.newInstance("", "Page # 2");
                default:
                    return SignInFragment.newInstance("", "Page # 1");
            }

        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 1) {
                return getString(R.string.register);
            } else {

                return getString(R.string.login);
            }

        }

    }

    private class GetDeviceID extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            int currentapiVersion = Build.VERSION.SDK_INT;
            //System.out.println("Current API Version is " + currentapiVersion);
            //System.out.println("Check API version is" + Build.VERSION_CODES.M);
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                int hasLocationPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
                int hasSMSPermission = checkSelfPermission(android.Manifest.permission.SEND_SMS);
                int hasAccessLocation = checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION);
                int hasGetAccounts = checkSelfPermission(android.Manifest.permission.GET_ACCOUNTS);
                int hasInternet = checkSelfPermission(android.Manifest.permission.INTERNET);
                int hasAccessNetwork = checkSelfPermission(android.Manifest.permission.ACCESS_NETWORK_STATE);
                int hasAccounts = checkSelfPermission(android.Manifest.permission.ACCOUNT_MANAGER);
                int hasCamera = checkSelfPermission(android.Manifest.permission.CAMERA);
                int hasVibrate = checkSelfPermission(android.Manifest.permission.VIBRATE);
                int hasReadContacts = checkSelfPermission(android.Manifest.permission.READ_CONTACTS);
                int hasWriteContacts = checkSelfPermission(android.Manifest.permission.WRITE_CONTACTS);
                int hasReadStorage = checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                int hasWriteStorage = checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                int hasWakeLock = checkSelfPermission(android.Manifest.permission.WAKE_LOCK);
                int hasphonestate = checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE);
                //System.out.println("HAS PHONE STATE" + hasphonestate);

                int hasChangeNetwork = checkSelfPermission(android.Manifest.permission.CHANGE_NETWORK_STATE);


                List<String> permissions = new ArrayList<>();

                if (hasCamera != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.CAMERA);
                }
                if (hasphonestate != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.READ_PHONE_STATE);
                }

                if (hasVibrate != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.VIBRATE);
                }
                if (hasReadContacts != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.READ_CONTACTS);
                }
                if (hasReadStorage != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                }
                if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
                }

                if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
                }

                if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.SEND_SMS);
                }

                if (hasAccessLocation != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
                }
                if (hasGetAccounts != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.GET_ACCOUNTS);
                }

                if (hasInternet != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.INTERNET);
                }

                if (hasAccessNetwork != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
                }

                if (hasAccounts != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.ACCOUNT_MANAGER);
                }

                if (hasChangeNetwork != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.CHANGE_NETWORK_STATE);
                }

                if (hasWakeLock != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(android.Manifest.permission.WAKE_LOCK);
                }
                if (hasWriteStorage != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }


                if (!permissions.isEmpty()) {
                    int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 1;
                    requestPermissions(permissions.toArray(new String[0]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);

                }
                //System.out.println("HAS PHONE STATE after Give Permission" + hasphonestate);

                //    new GetDeviceID().execute();
            }

        }

    }

}
