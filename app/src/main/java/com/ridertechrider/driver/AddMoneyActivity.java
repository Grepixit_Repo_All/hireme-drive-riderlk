package com.ridertechrider.driver;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.WebServiceUtil;

import java.util.HashMap;
import java.util.Map;

public class AddMoneyActivity extends BaseCompatActivity {

    private EditText amount;
    private EditText desc;
    private CustomProgressDialog progressDialog;
    private Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);

        progressDialog = new CustomProgressDialog(AddMoneyActivity.this);
        controller = (Controller) getApplicationContext();

        ImageButton recancel = findViewById(R.id.recancel);
        amount = findViewById(R.id.amount);
        desc = findViewById(R.id.desc);
        Button add_money = findViewById(R.id.add_money);

        recancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddMoneyActivity.this, WalletActivity.class));
                finish();
            }
        });

        add_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMoneyWitoutTrip();
            }
        });

    }

    private void addMoneyWitoutTrip() {
        progressDialog.showDialog();
        if (amount.getText().length() != 0 && desc.getText().length() != 0) {
            Map<String, String> params = new HashMap<>();
            params.put("total_amt", amount.getText().toString());
            params.put("trans_description", desc.getText().toString());
            params.put("api_key", controller.getLoggedDriver().getApiKey());
            params.put("driver_id", controller.getLoggedDriver().getDriverId());
            params.put("user_id", "0");

            ServerApiCall.callWithApiKeyAndDriverId(this, params, Constants.Urls.ADD_TRANS_WITHOUT_TRIP, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    progressDialog.dismiss();
                    if (isUpdate) {
                        Toast.makeText(AddMoneyActivity.this, getString(R.string.successfully_add_money_), Toast.LENGTH_SHORT).show();
                        amount.setText("");
                        desc.setText("");
                        startActivity(new Intent(AddMoneyActivity.this, WalletActivity.class));
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(AddMoneyActivity.this, getString(R.string.errors), Toast.LENGTH_SHORT).show();

                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AddMoneyActivity.this, WalletActivity.class));
        finish();
    }
}
