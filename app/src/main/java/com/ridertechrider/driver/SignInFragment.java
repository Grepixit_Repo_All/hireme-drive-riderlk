package com.ridertechrider.driver;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.app.Request;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.BEditText;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.hbb20.CountryCodePicker;
import com.ridertechrider.driver.utils.Utils;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import butterknife.ButterKnife;

import static com.ridertechrider.driver.utils.Utils.removedFirstZeroMobileNumber;
import static com.ridertechrider.driver.webservice.Constants.Values.USE_EMAIL_AUTH;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = SignInFragment.class.getSimpleName();
    private EditText mobile_no;
    private TextView labelField;
    private EditText Psw;
    private CountryCodePicker ccp;
    //    private Spinner spCountryCode;
//    private ArrayList<City> citiesCountryCode = new ArrayList<>();
//    private CountryCodeAdapter adapterCountryCode;
    private CustomProgressDialog progressDialog;
    private Controller controller;
    private Context context;
    private TextView login;
    private TextView forgotpsw;
    private TextView tvLoginPassword;


    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        login = view.findViewById(R.id.dones);
        mobile_no = view.findViewById(R.id.mobile_no);
        labelField = view.findViewById(R.id.labelField);
        ccp = view.findViewById(R.id.ccp);
        Psw = view.findViewById(R.id.passwords);
        forgotpsw = view.findViewById(R.id.forgotpsw);
        forgotpsw.setText(Localizer.getLocalizerString("k_5_s1_forgot_password"));
        login.setText(Localizer.getLocalizerString("k_6_s1_login"));
//        spCountryCode = view.findViewById(R.id.spCountryCode);

//        citiesCountryCode = Controller.getInstance().getCities();
//        if (!USE_EMAIL_AUTH)
//            citiesCountryCode.add(0, new City(getString(R.string.select)));
//        adapterCountryCode = new CountryCodeAdapter(getContext(), R.layout.spinner_text_cc, citiesCountryCode);
//        spCountryCode.setAdapter(adapterCountryCode);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new CustomProgressDialog(getActivity());
        controller = (Controller) Objects.requireNonNull(getActivity()).getApplicationContext();
        ButterKnife.bind(getActivity());

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (USE_EMAIL_AUTH) {
                    signInNow();
                } else {
                    signInPhoneAuth();
                }
            }
        });

        forgotpsw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reset = new Intent(getActivity(), ResetPasswordActivity.class);
                startActivity(reset);
            }
        });
        if (USE_EMAIL_AUTH) {
//            spCountryCode.setVisibility(View.GONE);
            ccp.setVisibility(View.GONE);
            mobile_no.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            mobile_no.setHint(Localizer.getLocalizerString("k_6_s3_email_address"));
            labelField.setText(Localizer.getLocalizerString("k_13_s1_email"));
        } else {
            mobile_no.setInputType(InputType.TYPE_CLASS_PHONE);
            mobile_no.setHint(Localizer.getLocalizerString("k_2_s1_mobile_number_hint"));
            labelField.setText(Localizer.getLocalizerString("k_r1_s4_mob_no"));
        }

        Psw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                if (str.length() > 0 && str.contains(" ")) {
                    Psw.setError(Localizer.getLocalizerString("k_7_s1_psd_space_nt_allowed"));
                    str = str.replaceAll(" ", "");
                    Psw.setText(str);
                }
            }
        });

//        getCities();
        setLocalizeData(view);
    }

    private void setLocalizeData(View view) {
        BEditText passwords = view.findViewById(R.id.passwords);
        tvLoginPassword = view.findViewById(R.id.tvLoginPassword);
        passwords.setHint(Localizer.getLocalizerString("k_4_s1_enter_password_hint"));
        tvLoginPassword.setText(Localizer.getLocalizerString("k_r3_s1_password"));
    }

    private void signInPhoneAuth() {
        final String phoneNo_ = mobile_no.getText().toString().toLowerCase();
        final String password_ = Psw.getText().toString();
        Pattern pattern = Pattern.compile("\\d+");
        if (phoneNo_.length() <= 5 || phoneNo_.length() >= 15 || !pattern.matcher(phoneNo_).matches()) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_8_s1_plz_enter_mobile"), Toast.LENGTH_LONG).show();
            mobile_no.requestFocus();
//        } else if (spCountryCode.getSelectedItemPosition() == 0) {
        } else if (ccp.getSelectedCountryCodeAsInt() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_10_s1_plz_sel_country_code"), Toast.LENGTH_LONG).show();
            mobile_no.requestFocus();
        } else if (password_.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_22_s2_plz_enter_valid_password"), Toast.LENGTH_LONG).show();
            Psw.requestFocus();
        } else {
            Request.Login login = new Request.Login();
            HashMap<String, String> params = login.addUsername(removedFirstZeroMobileNumber(phoneNo_))
//                    .addCityId((adapterCountryCode.getCityId(spCountryCode.getSelectedItemPosition())))
//                    .addCountryCode((adapterCountryCode.getCountryCode(spCountryCode.getSelectedItemPosition())))
                    .addCountryCode(ccp.getSelectedCountryCodeAsInt() + "")
                    .addPassword(password_)
                    .addIsAvailable(String.valueOf(1))
                    .build();

            Log.e("register params", "" + params);

            progressDialog.showDialog();
            WebServiceUtil.excuteRequest(getActivity(), params, Constants.Urls.URL_DRIVER_PHONE_SIGN_IN, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    if (isUpdate) {
                        String response = data.toString();
                        Log.e("response", "" + response);
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        if (res.isStatus()) {
                            final Driver driver = Driver.parseJsonAfterLogin(response);
//                            if (Objects.requireNonNull(driver).getD_is_verified()) {
                            controller.setDocUpdate(false);
                            controller.setLoggedDriver(driver);
                            controller.pref.setPASSWORD(Psw.getText().toString());

                            driverTokenUpateProfile();
//                            } else {
//                                if (!driver.isDocUploaded()) {
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()), R.style.Dialog);
//                                    builder.setMessage(R.string.loginMessage);
//                                    builder.setCancelable(false);
//                                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            progressDialog.dismiss();
//                                            dialog.cancel();
//                                        }
//                                    });
//                                    builder.show();
//                                } else {
//                                    controller.setLoggedDriver(driver);
//                                    controller.pref.setIsLogin(true);
//                                    Intent main = new Intent(getActivity(), DocUploadActivity.class);
//                                    main.putExtra("isFromRegister", true);
//                                    main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(main);
//                                    Objects.requireNonNull(getActivity()).finishAffinity();
//                                }
//                            }
                        } else {
                            try {
                                progressDialog.dismiss();
                            } catch (Exception ignored) {
                            }
                            Toast.makeText(getActivity(), res.getError(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception ignored) {
                        }
                    }
                }
            });
        }

    }

    private void signInNow() {
        final String email_ = mobile_no.getText().toString().toLowerCase();
        final String password_ = Psw.getText().toString();
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email_).matches()) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_11_s1_plz_enter_valid_email"), Toast.LENGTH_LONG).show();
            mobile_no.requestFocus();
        } else if (password_.length() == 0) {
            Toast.makeText(getActivity(), Localizer.getLocalizerString("k_22_s2_plz_enter_valid_password"), Toast.LENGTH_LONG).show();
            Psw.requestFocus();
        } else {
            Request.Login login = new Request.Login();
            HashMap<String, String> params = login.addEmail(email_)
                    .addPassword(password_)
                    .addIsAvailable(String.valueOf(1))
                    .build();
            progressDialog.showDialog();
            WebServiceUtil.excuteRequest(getActivity(), params, Constants.Urls.URL_DRIVER_LOGIN, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    if (isUpdate) {
                        String response = data.toString();
                        Log.e("response", "" + response);
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        controller.setDocUpdate(false);
                        if (res.isStatus()) {
                            final Driver driver = Driver.parseJsonAfterLogin(response);
//                            if (Objects.requireNonNull(driver).getD_is_verified()) {

                            controller.setDocUpdate(false);
                            controller.setLoggedDriver(driver);
                            controller.pref.setPASSWORD(Psw.getText().toString());

                            driverTokenUpateProfile();
//                            } else {
//                                AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()), R.style.Dialog);
//                                builder.setMessage(R.string.loginMessage);
//                                builder.setCancelable(false);
//                                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        progressDialog.dismiss();
//                                        dialog.cancel();
//                                    }
//                                });
//                                builder.show();
//                            }


                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), res.getError(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        progressDialog.dismiss();
                    }
                }
            });
        }

    }

    private void driverTokenUpateProfile() {
        Request.UpdateProfile profile = new Request.UpdateProfile();
        String token = Controller.getSaveDiceToken();
        profile.addIsAvailable(String.valueOf(1)).addDviceType();
        if (token != null) {
            profile.addDviceToken(token);
        }
        Map<String, String> params = profile.build();

        params.put(Constants.Keys.DEV_TYPE, Build.BRAND + " " + Build.PRODUCT + " " + Build.MODEL);
        params.put(Constants.Keys.DEVICE_VERSION, Build.VERSION.SDK_INT + "");
        params.put(Constants.Keys.APP_VERSION, BuildConfig.VERSION_NAME);

        ServerApiCall.callWithApiKeyAndDriverId(getActivity(), params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    progressDialog.dismiss();
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        controller.pref.setIsLogin(true);
                        Driver driver = Driver.parseJsonAfterLogin(response);
                        controller.setLoggedDriver(driver);
                        if (Objects.requireNonNull(driver).getD_lang() != null && !driver.getD_lang().trim().equalsIgnoreCase("null") && !driver.getD_lang().trim().isEmpty()) {
                            onSusscessSavedLanguageToServer(driver.getD_lang());
//                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(controller);
//                            onSusscessSavedLanguageToServer(prefs.getString(Constants.Keys.APP_LANGUAGE, Constants.Language.ENGLISH));
                        } else {
                            Intent intent = new Intent(getActivity(), SlideMainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Objects.requireNonNull(getActivity()).finish();
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void onSusscessSavedLanguageToServer(String language) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        prefs.edit().putString(Constants.Keys.APP_LANGUAGE, language).commit();
        Controller controller = (Controller) getActivity().getApplicationContext();
        controller.pref.setSelectedLanguage(language);
        Utils.applyAppLanguage(getActivity());
        Intent intent = new Intent(getActivity(), SlideMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        if (getActivity() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                getActivity().finishAffinity();
            } else {
                getActivity().finish();
            }
        }
    }

//    public void getCities() {
//        WebServiceUtil.excuteRequest(getActivity(), null, Constants.Urls.URL_GET_CITIES, new WebServiceUtil.DeviceTokenServiceListener() {
//            @Override
//            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
//                Log.d(TAG, "onUpdateDeviceTokenOnServer: dat: " + data);
//                if (isUpdate) {
//                    String response = data.toString();
//                    ErrorJsonParsing parser = new ErrorJsonParsing();
//                    CloudResponse res = parser.getCloudResponse("" + response);
//                    citiesCountryCode = City.parseCities(response);
//                    if (!USE_EMAIL_AUTH)
//                        citiesCountryCode.add(0, new City(getString(R.string.select)));
//                    controller.pref.setCitiesResponse(response);
//                    adapterCountryCode.setCountryCodes(citiesCountryCode);
//                } else {
//                    if (error instanceof ServerError) {
//                        String s = new String(error.networkResponse.data);
//                        try {
//                            JSONObject jso = new JSONObject(s);
////Toast.makeText(getApplicationContext(), jso.getString("message"), Toast.LENGTH_LONG).show();
////
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        if (controller != null)
//                            Toast.makeText(controller, Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//    }

}