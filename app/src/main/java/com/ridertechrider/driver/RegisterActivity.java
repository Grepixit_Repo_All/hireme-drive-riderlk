package com.ridertechrider.driver;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("NewApi")
public class RegisterActivity extends BaseActivity {
    protected static final String TAG = "RegisterActivity";
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.mobile)
    EditText etmobile;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.repassword)
    EditText repassword;
    @BindView(R.id.first_name)
    EditText etfirst_name;
    @BindView(R.id.last_name)
    EditText etlast_name;
    private CustomProgressDialog dialog;
    private Controller controller;

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        controller = (Controller) getApplicationContext();
        dialog = new CustomProgressDialog(RegisterActivity.this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        etfirst_name.requestFocus();
    }

    @OnClick(R.id.next)
    public void nextButton() {
        signUpDriver();

    }

    @OnClick(R.id.cancel)
    public void backButton() {
        finish();
    }

    private void signUpDriver() {


        final String f_name_ = etfirst_name.getText().toString();
        final String l_name_ = etlast_name.getText().toString();
        final String email_ = email.getText().toString();
        final String mobileNo_ = etmobile.getText().toString();
        final String password_ = password.getText().toString();


        if (f_name_.length() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.f_name), Toast.LENGTH_LONG).show();
            etfirst_name.requestFocus();
        } else if (l_name_.length() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.l_name), Toast.LENGTH_LONG).show();
            etlast_name.requestFocus();
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email_).matches()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.email1), Toast.LENGTH_LONG).show();
            email.requestFocus();
        } else if (mobileNo_.length() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.mobile), Toast.LENGTH_LONG).show();
            etmobile.requestFocus();
        } else if (mobileNo_.length() <= 5 || mobileNo_.length() >= 15) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.mobile), Toast.LENGTH_LONG).show();
            etmobile.requestFocus();
        } else if (password_.length() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.passwords), Toast.LENGTH_LONG).show();
            password.requestFocus();
        } else if (password_.length() <= 5 || password_.length() >= 15) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.passwords), Toast.LENGTH_LONG).show();
            password.requestFocus();
        } else {

            Map<String, String> params = new HashMap<>();
            params.put(Constants.Keys.EMAIL, email_);
            params.put(Constants.Keys.PHONE, mobileNo_);
            params.put(Constants.Keys.USERNAME, mobileNo_);
            params.put(Constants.Keys.PASSWORD, password_);

            String refreshedToken = Controller.getSaveDiceToken();
            if (refreshedToken != null) {
                params.put(Constants.Keys.DEVICE_TYPE, Constants.Values.DEVICE_TYPE_ANDROID);
                params.put(Constants.Keys.DEVICE_TOKEN, refreshedToken);
            }
            params.put(Constants.Keys.FNAME, f_name_);
            params.put(Constants.Keys.LNAME, l_name_);
            //params.put("d_is_verified", String.valueOf(1));
            dialog.showDialog();
            WebServiceUtil.excuteRequest(RegisterActivity.this, params, Constants.Urls.URL_DRIVER_PHONE_REGISTRATION, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    dialog.dismiss();
                    if (isUpdate) {
                        String response = data.toString();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        dialog.dismiss();
                        if (res.isStatus()) {
                            controller.pref.setPASSWORD(password.getText().toString());
                            Driver driver = Driver.parseJsonAfterLogin(response);
                            controller.setLoggedDriver(driver);
                            controller.pref.setIsLogin(true);
                            Intent main = new Intent(getApplicationContext(), DocUploadActivity.class);
                            main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(main);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                    }
                }
            });


        }
    }
}
