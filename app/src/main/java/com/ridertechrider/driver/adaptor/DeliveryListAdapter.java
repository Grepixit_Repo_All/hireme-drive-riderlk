package com.ridertechrider.driver.adaptor;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ridertechrider.driver.R;

import java.util.ArrayList;

public class DeliveryListAdapter extends BaseAdapter {
    // private final Activity activity;
    private final ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList;


    public DeliveryListAdapter(ArrayList<com.ridertechrider.driver.DeliverResponse.Response> deliveryList) {
        //  this.activity = activity;

        if (deliveryList != null)
            this.deliveryList = deliveryList;
        else
            this.deliveryList = new ArrayList<>();


    }

    @Override
    public int getCount() {
        return deliveryList.size();
    }

    @Override
    public Object getItem(int i) {
        return deliveryList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_list_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }


        mViewHolder.tvName.setText(deliveryList.get(position).getDeliveryAddress());
        mViewHolder.deliveryIdView.setText(deliveryList.get(position).getDeliveryId());
        if (deliveryList.get(position).getDeliveryStatus().equals("Created")) {
            mViewHolder.statusButton.setText(R.string.finish_delivery);
        } else {
            mViewHolder.statusButton.setText(deliveryList.get(position).getDeliveryStatus());
        }

        return convertView;
    }

   /* public void clear() {
        this.deliveryList.clear();
        notifyDataSetChanged();
    }*/

    private class MyViewHolder {
        final TextView tvName;
        final TextView deliveryIdView;
        final TextView statusButton;

        MyViewHolder(View view) {
            this.tvName = view.findViewById(R.id.delivery_name);
            this.deliveryIdView = view.findViewById(R.id.idDelivery);
            this.statusButton = view.findViewById(R.id.delivery_button);
        }
    }
}
