package com.grepix.grepixutils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by grepixinfotech on 07/07/16.
 */
public class WebServiceUtil {

    private static final String TAG = WebServiceUtil.class.getSimpleName();

    public static RequestQueue excuteRequest(final Context contaxt, final Map<String, String> params, final String url, final DeviceTokenServiceListener listener) {
        return excuteRequest(contaxt, params, url, listener, "tag");
    }

    public static RequestQueue excuteRequest(final Context contaxt, final Map<String, String> params, final String url, final DeviceTokenServiceListener listener, String tag) {

        Log.d(TAG, "excuteRequest: url: " + url);
        if (params != null) {
            Log.d(TAG, "excuteRequest: params: " + params.toString());
            String paramsString = "excuteRequest: params: ";
            for (String key : params.keySet()) {
                paramsString += "\n" + key + ":" + params.get(key);
            }
            Log.d(TAG, paramsString);
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onUpdateDeviceTokenOnServer(response, response != null, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("onErrorResponse", "onErrorResponse: url: " + url);
                        if (params != null)
                            Log.d("onErrorResponse", "onErrorResponse: params: " + params.toString());
                        System.out.println("VolleyError : " + error);
                        if (error instanceof NoConnectionError)
                            Log.d("WebServiceUtil", "onErrorResponse: NoConnectionError");
//                            Toast.makeText(contaxt, "No internet available", Toast.LENGTH_SHORT).show();
                        else if (error instanceof ServerError) {

                            String d = new String(error.networkResponse.data);
                            try {
                                JSONObject jso = new JSONObject(d);
                                // String message = jso.getString("message");
//                                 if(message.equalsIgnoreCase("Driver is not found"))
//                                 {
//                                     Toast.makeText(contaxt, jso.getString("Wrong password. Try again."), Toast.LENGTH_LONG).show();
//                                 }
//                                else
//                                 {
                                Toast.makeText(contaxt, jso.getString("message"), Toast.LENGTH_LONG).show();
//                                 }
                            } catch (JSONException e) {
                                listener.onUpdateDeviceTokenOnServer(null, false, error);
                                e.printStackTrace();
                            }
                        }

                        listener.onUpdateDeviceTokenOnServer(null, false, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        try {
            stringRequest.setTag(tag);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(contaxt);
            requestQueue.add(stringRequest);
            return requestQueue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static RequestQueue excuteRequestWithNoAlert(final Context contaxt, final Map<String, String> params, final String url, final DeviceTokenServiceListener listener) {

        Log.d(TAG, "excuteRequest: url: " + url);
        if (params != null) {
            Log.d(TAG, "excuteRequest: params: " + params.toString());
            String paramsString = "excuteRequest: params: ";
            for (String key : params.keySet()) {
                paramsString += "\n" + key + ":" + params.get(key);
            }
            Log.d(TAG, paramsString);
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onUpdateDeviceTokenOnServer(response, response != null, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.onUpdateDeviceTokenOnServer(null, false, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        try {
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(contaxt);
            requestQueue.add(stringRequest);
            return requestQueue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface DeviceTokenServiceListener {
        void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error);
    }
}
