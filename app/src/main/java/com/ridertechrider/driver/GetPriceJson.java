package com.ridertechrider.driver;

import android.content.Context;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GetPriceJson {

    //    private ;
    private final HashMap<String, List<GetPriceBeanClass>> stringListHashMap = new HashMap<>();
    private final String jsonSpecial;
    private final String[] weeksDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    String json = null;

/*
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open("category_id1");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
*/


    public GetPriceJson(Context context, String categoryId, String jsonSpecial) {
        this.jsonSpecial = jsonSpecial;

    }

    public void parseJsonFile() {


       /* try {
            InputStream is = null;
            try {
                if(categoryId.equalsIgnoreCase("1")){
                   // is = context.getAssets().open("category_id1");
                    is = context.getAssets().open("category_id1");
                }else
                    if (categoryId.equalsIgnoreCase("2")){
                      //  is = context.getAssets().open("category_id2");
                        is = context.getAssets().open("category_id2");
                    }else
                    if (categoryId.equalsIgnoreCase("3")){
                       // is = context.getAssets().open("category_id3");
                        is = context.getAssets().open("category_id3");
                    }else
                    if (categoryId.equalsIgnoreCase("4")){
                        //is = context.getAssets().open("category_id4");
                        is = context.getAssets().open("category_id4");
                    }else
                    if (categoryId.equalsIgnoreCase("5")){
                       // is = context.getAssets().open("category_id_5");
                        is = context.getAssets().open("category_id_5");
                    }else
                    if (categoryId.equalsIgnoreCase("6")){
                       // is = context.getAssets().open("category_id6");
                        is = context.getAssets().open("category_id6");
                    }else
                    if (categoryId.equalsIgnoreCase("7")){
                       // is = context.getAssets().open("category_id7");
                        is = context.getAssets().open("category_id7");
                    }else
                    if (categoryId.equalsIgnoreCase("8")){
                        //is = context.getAssets().open("category_id8");
                        is = context.getAssets().open("category_id8");
                    }

            } catch (IOException e) {
                e.printStackTrace();
            }
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            //json = new String(jsonSpecial, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();

        }*/

        try {
            //JSONObject jsonObject = new JSONObject(jsonSpecial);
            if (jsonSpecial != null && !jsonSpecial.equals("[]")) {
                JSONObject jsonObject = new JSONObject(jsonSpecial);


                for (String weeksDay : weeksDays) {
                    if (jsonObject.has(weeksDay)) {
                        stringListHashMap.put(weeksDay, getJsonArray(jsonObject, weeksDay));
                    }
                }
            }
//
//            JSONArray jsonArray = getJsonArray(jsonObject);
//            JSONArray jsonArray1 = jsonObject.getJSONArray("Tue");
//            for (int i = 0; i < jsonArray1.length(); i++) {
//                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
//                getPriceBeanClass.setTime(jsonObject1.optString("time"));
//                getPriceBeanClass.setPrice_per_km(jsonObject1.optString("price_per_km"));
//                getPriceBeanClass.setDay("Tue");
//                getPriceBeanClassList.add(getPriceBeanClass);
//
//            }
//            JSONArray jsonArray2 = jsonObject.getJSONArray("Wed");
//            for (int i = 0; i < jsonArray2.length(); i++) {
//                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
//                getPriceBeanClass.setTime(jsonObject1.optString("time"));
//                getPriceBeanClass.setPrice_per_km(jsonObject1.optString("price_per_km"));
//                getPriceBeanClass.setDay("Wed");
//                getPriceBeanClassList.add(getPriceBeanClass);
//
//            }
//            JSONArray jsonArray3 = jsonObject.getJSONArray("Thu");
//            for (int i = 0; i < jsonArray3.length(); i++) {
//                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
//                getPriceBeanClass.setTime(jsonObject1.optString("time"));
//                getPriceBeanClass.setPrice_per_km(jsonObject1.optString("price_per_km"));
//                getPriceBeanClass.setDay("Thu");
//                getPriceBeanClassList.add(getPriceBeanClass);
//
//            }
//            JSONArray jsonArray4 = jsonObject.getJSONArray("Fri");
//            for (int i = 0; i < jsonArray4.length(); i++) {
//                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
//                getPriceBeanClass.setTime(jsonObject1.optString("time"));
//                getPriceBeanClass.setPrice_per_km(jsonObject1.optString("price_per_km"));
//                getPriceBeanClass.setDay("Fri");
//                getPriceBeanClassList.add(getPriceBeanClass);
//
//            }
//            JSONArray jsonArray5 = jsonObject.getJSONArray("Sat");
//            for (int i = 0; i < jsonArray5.length(); i++) {
//                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
//                getPriceBeanClass.setTime(jsonObject1.optString("time"));
//                getPriceBeanClass.setPrice_per_km(jsonObject1.optString("price_per_km"));
//                getPriceBeanClass.setDay("Sat");
//                getPriceBeanClassList.add(getPriceBeanClass);
//
//            }
//            JSONArray jsonArray6 = jsonObject.getJSONArray("Sun");
//            for (int i = 0; i < jsonArray6.length(); i++) {
//                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
//                getPriceBeanClass.setTime(jsonObject1.optString("time"));
//                getPriceBeanClass.setPrice_per_km(jsonObject1.optString("price_per_km"));
//                getPriceBeanClass.setDay("Sun");
//                getPriceBeanClassList.add(getPriceBeanClass);
//            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private List<GetPriceBeanClass> getJsonArray(JSONObject jsonObject, String daykEYnme) throws JSONException {
        List<GetPriceBeanClass> getPriceBeanClassList = new ArrayList<>();
        JSONArray jsonArray = jsonObject.getJSONArray(daykEYnme);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
            GetPriceBeanClass getPriceBeanClass = new GetPriceBeanClass();
            getPriceBeanClass.setTime(jsonObject1.optString("time"));
            getPriceBeanClass.setPrice_per_km(jsonObject1.optString("perc"));
            getPriceBeanClass.setDay(daykEYnme);
            getPriceBeanClassList.add(getPriceBeanClass);

        }
        return getPriceBeanClassList;
    }

    public GetPriceBeanClass getPrice(Date currentDate) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(currentDate);
        int dayOfWeek = instance.get(Calendar.DAY_OF_WEEK);
        String weekDay = weeksDays[dayOfWeek - 1];
        List<GetPriceBeanClass> getPriceBeanClassList = stringListHashMap.get(weekDay);
        return findMatchPrice(getPriceBeanClassList, instance);
    }

    private GetPriceBeanClass findMatchPrice(List<GetPriceBeanClass> getPriceBeanClassList, Calendar instance) {
        if (getPriceBeanClassList != null) {
            for (GetPriceBeanClass getPriceBeanClass :
                    getPriceBeanClassList) {
                if (getPriceBeanClass.isbetween(instance.getTime())) {
                    return getPriceBeanClass;
                }
            }
        }
        return null;
    }


}

