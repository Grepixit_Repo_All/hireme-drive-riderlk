package com.ridertechrider.driver;


import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.adaptor.Driver;
import com.ridertechrider.driver.app.Request;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity {


    @BindView(R.id.emails)
    EditText Email;
    @BindView(R.id.passwords)
    EditText Psw;
    private CustomProgressDialog progressDialog;
    private Controller controller;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signin);


        progressDialog = new CustomProgressDialog(SignInActivity.this);

        controller = (Controller) getApplicationContext();

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        Email.requestFocus();

    }


    @OnClick(R.id.dones)
    public void signInNow() {
        final String email_ = Email.getText().toString().toLowerCase();
        final String password_ = Psw.getText().toString();
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email_).matches()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.email1), Toast.LENGTH_LONG).show();
            Email.requestFocus();
        } else if (password_.length() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.passwords), Toast.LENGTH_LONG).show();
            Psw.requestFocus();
        } else if (password_.length() <= 5 || password_.length() >= 15) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.passwords), Toast.LENGTH_LONG).show();
            Psw.requestFocus();
        } else {
            Request.Login login = new Request.Login();
            HashMap<String, String> params = login.addUsername(email_)
                    .addPassword(password_)
                    .addIsAvailable(String.valueOf(1))
                    .build();
            progressDialog.showDialog();
            WebServiceUtil.excuteRequest(SignInActivity.this, params, Constants.Urls.URL_DRIVER_PHONE_SIGN_IN, new WebServiceUtil.DeviceTokenServiceListener() {
                @Override
                public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                    if (isUpdate) {
                        String response = data.toString();
                        ErrorJsonParsing parser = new ErrorJsonParsing();
                        CloudResponse res = parser.getCloudResponse("" + response);
                        if (res.isStatus()) {
                            final Driver driver = Driver.parseJsonAfterLogin(response);
                            controller.setLoggedDriver(driver);
                            controller.pref.setPASSWORD(Psw.getText().toString());

                            if (Objects.requireNonNull(driver).getD_is_verified()) {
                                driverTokenUpateProfile();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this, R.style.Dialog);
                                builder.setMessage(Localizer.getLocalizerString("k_55_s4_loginMessage"));
                                builder.setCancelable(false);
                                builder.setPositiveButton(Localizer.getLocalizerString("k_r8_s8_ok"), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        progressDialog.dismiss();
                                        dialog.cancel();
                                        driverTokenUpateProfile();
                                    }
                                });
                                builder.show();
                            }


                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        progressDialog.dismiss();
                    }
                }
            });
        }

    }

    @OnClick(R.id.signuplink)
    public void goToSignUp() {
        Intent reset = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(reset);

    }

    @OnClick(R.id.forgotpsw)
    public void goToForgot() {
        Intent reset = new Intent(getApplicationContext(), ResetPasswordActivity.class);
        startActivity(reset);

    }

    @OnClick(R.id.cancels)
    public void backToHome() {
        finish();

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void onBackPressed() {
        Intent can = new Intent(getApplicationContext(), HomeActivity.class);
        Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.trans_right_in, R.anim.trans_right_out).toBundle();
        startActivity(can, bndlanimation);
        finish();

    }

    private void driverTokenUpateProfile() {
        Request.UpdateProfile profile = new Request.UpdateProfile();
        String token = Controller.getSaveDiceToken();
        profile.addIsAvailable(String.valueOf(1)).addDviceType();
        if (token != null) {
            profile.addDviceToken(token);
        }
        ServerApiCall.callWithApiKeyAndDriverId(SignInActivity.this, profile.build(), Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {
                    progressDialog.dismiss();
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        controller.pref.setIsLogin(true);
                        controller.setLoggedDriver(Driver.parseJsonAfterLogin(response));
                        Intent intent = new Intent(getApplicationContext(), SlideMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

}
