package com.ridertechrider.driver;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;


class OperationHours {
    private final HashMap<String, ArrayList<OperationHoursTime>> stringArrayListHashMap = new HashMap<>();
    private Activity activity;

    public OperationHours() {

    }

    public OperationHours(Activity activity, String operationHours) {
        this.activity = activity;
        try {
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset());
            String[] weeksDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
            for (String weeksDay : weeksDays) {
                ArrayList<OperationHoursTime> operationHoursTimes = new ArrayList<>();
                JSONArray jssonArray = jsonObject.getJSONArray(weeksDay);
                for (int j = 0; j < jssonArray.length(); j++) {
                    String time = jssonArray.get(j).toString();
                    operationHoursTimes.add(new OperationHoursTime(time));
                }
                stringArrayListHashMap.put(weeksDay, operationHoursTimes);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<OperationHoursTime> getTimes(String weekKey) {
        return stringArrayListHashMap.get(weekKey);
    }

    private String loadJSONFromAsset() {
        String json;
        try {

            InputStream is = activity.getAssets().open("category_id1");
            int size = is.available();
            byte[] buffer = new byte[size];
            int isRead = is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    class OperationHoursTime {
        private final String start;
        private final String end;

        OperationHoursTime(String time) {
            String[] time1 = time.split("-");
            start = time1[0].trim();
            end = time1[1].trim();
        }

        public String getEnd() {
            return end;
        }

        public String getStart() {
            return start;
        }
    }
}

