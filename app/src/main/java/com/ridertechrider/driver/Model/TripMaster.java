package com.ridertechrider.driver.Model;

import android.util.Log;

import com.ridertechrider.driver.adaptor.Trip;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TripMaster implements Serializable {

    @SerializedName("m_trip_id")
    @Expose
    private String mTripId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("act_id")
    @Expose
    private String actId;
    @SerializedName("max_seat")
    @Expose
    private String maxSeat;
    @SerializedName("n_seat")
    @Expose
    private String nSeat;
    @SerializedName("n_trips")
    @Expose
    private String nTrips;
    @SerializedName("is_share")
    @Expose
    private String isShare;
    @SerializedName("is_delete")
    @Expose
    private String isDelete;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("Trip")
    @Expose
    private List<Trip> trips = null;

    public static TripMaster parseTripMaster(String json) {
        try {
            return new Gson().fromJson(json, TripMaster.class);
        } catch (Exception e) {
            Log.e(TripMaster.class.getSimpleName(), "parseTripMaster: " + e.getMessage(), e);
            return null;
        }
    }

    public String getmTripId() {
        return mTripId;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getActId() {
        return actId;
    }

    public String getMaxSeat() {
        return maxSeat;
    }

    public String getnSeat() {
        return nSeat;
    }

    public String getnTrips() {
        return nTrips;
    }

    public String getIsShare() {
        return isShare;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public String getCreated() {
        return created;
    }

    public String getModified() {
        return modified;
    }

    public List<Trip> getTrips() {
        return trips;
    }
}
