package com.grepix.grepixutils;

import android.content.Context;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by grepixinfotech on 08/07/16.
 */
public class Utils {
    private final static String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static boolean isLatLngZero(Location myCurrentLocation) {
        return myCurrentLocation.getLatitude() == 0.0 && myCurrentLocation.getLongitude() == 0.0;
    }

   /* interface AlertCallBack {
        void onClickAlertButton(boolean isYes);
    }*/
    public static Date stringToDate(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat
                (SERVER_DATE_FORMAT, Locale.ENGLISH);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return inputFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean net_connection_check(Context context) {
        ConnectionManager cm = new ConnectionManager(context);

        boolean connection = cm.isConnectingToInternet();

        if (!connection) {

            Toast toast = Toast.makeText(context.getApplicationContext(), "There is no network please connect.", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 70);
            toast.show();
        }
        return connection;
    }

    public int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

}
