package com.ridertechrider.driver.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.Pref;
import com.ridertechrider.driver.R;
import com.ridertechrider.driver.adaptor.TripModel;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.webservice.Constants;
import com.ridertechrider.driver.webservice.model.AppData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.grepix.grepixutils.WebServiceUtil;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Nayanesh Gupte
 */
public class PickUpLocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ILocationConstants {


    private static final String TAG = PickUpLocationService.class.getSimpleName();

    /**
     * Provides the entry point to Google Play services.
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;
    private Location preLocation;
    private int timeForSkipSameLocation = 10;

    /**
     * Time when the location was updated represented as a String.
     */
    private String mLastUpdateTime;


    /**
     * Total distance covered
     */
    private Controller controller;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback locationCallback;
    private float distance;
    private Location oldLocation;

    @Override
    public void onCreate() {
        super.onCreate();
        if (controller == null) {

            controller = (Controller) getApplicationContext();
        }
        controller.isUpdaingLocation = false;
        Log.d(TAG, "onCreate Distance: " + distance);
        oldLocation = new Location("Point A");
        mLastUpdateTime = "";
        Log.d(TAG, "onCreate Distance: " + distance);
        if (PreferencesUtils.getBoolean(this, R.string.is_recording_start, false)) {
            distance = (int) PreferencesUtils.getFloat(this, R.string.recorded_distance);
            Log.e("savedDistance", "" + distance);
        } else {
            distance = 0;
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildGoogleApiClient();

        mGoogleApiClient.connect();

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "location_id";
            Notification.Builder builder = new Notification.Builder(this, channelId)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.trip_is_started))
                    .setAutoCancel(true);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(channelId, "Rider Driver", importance);
            mNotificationManager.createNotificationChannel(mChannel);
            Notification notification = builder.build();
            startForeground(764, notification);
        } else {
            String channelId = "location_id";
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.trip_is_started))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);
            Notification notification = builder.build();
            startForeground(764, notification);
        }
        return START_NOT_STICKY;
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    private synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    private void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }
                    for (Location location : locationResult.getLocations()) {
                        if (location != null) {
                            if (!Controller.isActivityVisible()) {
                                Pref pref = new Pref(PickUpLocationService.this);
                                TripModel tripModel = AppData.getInstance(PickUpLocationService.this).getTripModel();
                                if (tripModel != null) {
                                    if (tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
//                                        TrackRouteSaveData.getInstance(getApplicationContext()).addLatLongD(new LatLng(location.getLatitude(), location.getLongitude()));
//                                        TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
                                    }
                                }
                            }
                            mCurrentLocation = location;
                            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                            updateUI();

//                            PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);
                            Log.d(TAG, "onLocationChanged :" + location + "  Cover Distance : " + distance);

                        }
                    }
                }
            };
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, null);


        } catch (Exception ignore) {


        }
    }

    /**
     * Updates the latitude, the longitude, and the last location time in the UI.
     */
    private void updateUI() {

        if (null != mCurrentLocation) {
            if (controller == null) {
                controller = (Controller) getApplicationContext();
            }
            if (!Controller.isActivityVisible()) {
                if (isCanMakeApiCall(mCurrentLocation)) {
                    updateDriverProfileApi(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    preLocation = mCurrentLocation;
                }
            }
            StringBuilder sbLocationData = new StringBuilder();
            sbLocationData/*.append(mLatitudeLabel)
                    .append(" ")
                    .append(mCurrentLocation.getLatitude())
                    .append("\n")
                    .append(mLongitudeLabel)
                    .append(" ")
                    .append(mCurrentLocation.getLongitude())
                    .append("\n")
                    .append(mLastUpdateTimeLabel)
                    .append(" ")
                    .append(mLastUpdateTime)
                    .append("\n")
                    .append(mDistance)
                    .append(" ")*/
                    .append(getUpdatedDistance());
            //.append(" meters");


            /*
             * update preference with latest value of distance
             */
//            appPreferences.putFloat(PREF_DISTANCE, distance);


            Log.d(TAG, "Location Data:\n" + sbLocationData.toString());

            sendLocationBroadcast(sbLocationData.toString());
        } else {

            Toast.makeText(this, R.string.unable_to_find_location, Toast.LENGTH_SHORT).show();
        }
    }

    private void sendLocationBroadcast(String sbLocationData) {

        Intent locationIntent = new Intent();
        locationIntent.setAction(PICKUP_LOCATION_ACTION);
        locationIntent.putExtra(LOCATION_MESSAGE, sbLocationData);

        if (mCurrentLocation != null) {
            locationIntent.putExtra("c_lat", "" + mCurrentLocation.getLatitude());
            locationIntent.putExtra("c_lng", "" + mCurrentLocation.getLongitude());
            locationIntent.putExtra("c_bearing", "" + mCurrentLocation.getBearing());
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(locationIntent);

    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        if (mGoogleApiClient != null && mFusedLocationClient != null && locationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
            //  LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    private void updateDriverProfileApi(final double lat, final double lng) {
        if (controller.isUpdaingLocation) {
            return;
        }
        controller.isUpdaingLocation = true;
        Map<String, String> params = new HashMap<>();
        params.put("d_lat", Double.toString(lat));
        params.put("d_lng", Double.toString(lng));
        ServerApiCall.callWithApiKeyAndDriverIdWithNoAlert(this, params, Constants.Urls.UPDATE_PROFILE, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdateTeamp, VolleyError error) {
                if (isUpdateTeamp) {
                    controller.getLoggedDriver().setD_lat(Double.toString(lat));
                    controller.getLoggedDriver().setD_lng(Double.toString(lng));
                }
                controller.isUpdaingLocation = false;
            }
        });
    }

    @Override
    public void onDestroy() {

        controller.isUpdaingLocation = false;
        stopLocationUpdates();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) throws SecurityException {
        Log.i(TAG, "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        mCurrentLocation = location;
                        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                        updateUI();
                    }
                }
            });

        }

        startLocationUpdates();

    }

    private boolean isCanMakeApiCall(Location myCurrentLocation) {
        if (preLocation == null) {
            return true;
        }
        if (preLocation.distanceTo(myCurrentLocation) <= 15) {
            timeForSkipSameLocation--;
            if (timeForSkipSameLocation < 0) {
                timeForSkipSameLocation = 0;
                timeForSkipSameLocation = 10;
                return true;
            }
            return false;
        } else {
            timeForSkipSameLocation = 10;
            return true;
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        if (!Controller.isActivityVisible()) {
            Pref pref = new Pref(this);
            TripModel tripModel = AppData.getInstance(this).getTripModel();
            if (tripModel != null) {
                if (tripModel.tripStatus != null && tripModel.tripStatus.equalsIgnoreCase("begin")) {
//                    TrackRouteSaveData.getInstance(getApplicationContext()).addLatLongD(new LatLng(location.getLatitude(), location.getLongitude()));
//                    TrackRouteSaveData.getInstance(getApplicationContext()).saveData();
                }
            }
        }
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateUI();
//        PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);

    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    private float getUpdatedDistance() {

        if (mCurrentLocation.getAccuracy() > ACCURACY_THRESHOLD) {
            return Float.parseFloat(String.format(Locale.ENGLISH, "0.2f", distance));
        }

        if (oldLocation.getLatitude() == 0 && oldLocation.getLongitude() == 0) {
            oldLocation.setLatitude(mCurrentLocation.getLatitude());
            oldLocation.setLongitude(mCurrentLocation.getLongitude());
            return Float.parseFloat(String.format(Locale.ENGLISH, "0.2f", distance));
        } else {


//            if(isMoving)
//            {

            if (mCurrentLocation.getAccuracy() < 60) {
                float tempDistance = mCurrentLocation.distanceTo(oldLocation);
                Log.e("tempDistance", "" + tempDistance);
//                distance += tempDistance;
//                PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);
//                oldLocation.setLatitude(mCurrentLocation.getLatitude());
//                oldLocation.setLongitude(mCurrentLocation.getLongitude());
                if (tempDistance > 20) {
                    distance += tempDistance;
//                    PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);
                    oldLocation.setLatitude(mCurrentLocation.getLatitude());
                    oldLocation.setLongitude(mCurrentLocation.getLongitude());
                }
            }
//            }
//            if (tempDistance > 50) {
//                distance += tempDistance;
//                PreferencesUtils.setFloat(getApplicationContext(), R.string.recorded_distance, distance);
//                oldLocation.setLatitude(mCurrentLocation.getLatitude());
//                oldLocation.setLongitude(mCurrentLocation.getLongitude());
//            }
        }
        return Float.parseFloat(String.format(Locale.ENGLISH, "%.2f", distance));
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
