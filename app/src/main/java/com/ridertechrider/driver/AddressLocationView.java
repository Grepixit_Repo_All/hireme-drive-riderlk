package com.ridertechrider.driver;

import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by grepix on 12/26/2016.
 */
class AddressLocationView {

    private final View view;
    private final Typeface typeface;
    private TextView tvLocTitle, tvLocAddress;

    public AddressLocationView(View view, Typeface typeface) {
        this.view = view;
        this.typeface = typeface;
        init();

    }

    public void setLocationTitle(CharSequence text) {
        if (text != null) {
            tvLocTitle.setText(text);
        }

    }


    public void hide() {

        this.view.setVisibility(View.GONE);

    }

    public void show() {

        this.view.setVisibility(View.VISIBLE);

    }

    private void init() {
        tvLocAddress = view.findViewById(R.id.tv_location_address);
        tvLocAddress.setTypeface(typeface);
        tvLocTitle = view.findViewById(R.id.tv_location_title);
        tvLocTitle.setTypeface(typeface);
        ImageView ivSearchBtn = view.findViewById(R.id.iv_search_btn);
        ivSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    public void setLocationAddress(String location) {
        if (location != null) {

            tvLocAddress.setText(location);
        }
    }

}
