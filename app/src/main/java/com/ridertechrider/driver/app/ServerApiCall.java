package com.ridertechrider.driver.app;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.ridertechrider.driver.Localizer;
import com.ridertechrider.driver.webservice.model.AppData;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.grepix.grepixutils.WebServiceUtil.excuteRequestWithNoAlert;

public class ServerApiCall {

    public static void callWithApiKey(Context context, Map<String, String> params, String url, WebServiceUtil.DeviceTokenServiceListener deviceTokenServiceListener) {
        AppData.Bulder bulder = new AppData.Bulder(context);
        HashMap<String, String> build = bulder.addApiKey().build();
        if (params != null) {
            build.putAll(params);
        }
        WebServiceUtil.excuteRequest(context, build, url, deviceTokenServiceListener);
    }

    public static RequestQueue callWithApiKeyWithNoAlert(Context context, Map<String, String> params, String url, WebServiceUtil.DeviceTokenServiceListener deviceTokenServiceListener) {
        AppData.Bulder bulder = new AppData.Bulder(context);
        HashMap<String, String> build = bulder.addApiKey().build();
        if (params != null) {
            build.putAll(params);
        }
        return excuteRequestWithNoAlert(context, build, url, deviceTokenServiceListener);
    }

    public static void callWithApiKeyAndDriverId(Context context, Map<String, String> params, String url, WebServiceUtil.DeviceTokenServiceListener deviceTokenServiceListener) {
        callWithApiKeyAndDriverId(context, params, url, deviceTokenServiceListener, "tag");
    }

    public static RequestQueue callWithApiKeyAndDriverId(Context context, Map<String, String> params, String url, WebServiceUtil.DeviceTokenServiceListener deviceTokenServiceListener, String tag) {
        AppData.Bulder bulder = new AppData.Bulder(context);
        HashMap<String, String> build = bulder.addApiKey().addDriverId().build();
        if (params != null) {
            build.putAll(params);
        }
        return WebServiceUtil.excuteRequest(context, build, url, deviceTokenServiceListener, tag);
    }

    public static void callWithApiKeyAndDriverIdWithNoAlert(Context context, Map<String, String> params, String url, WebServiceUtil.DeviceTokenServiceListener deviceTokenServiceListener) {
        AppData.Bulder bulder = new AppData.Bulder(context);
        HashMap<String, String> build = bulder.addApiKey().addDriverId().build();
        if (params != null) {
            build.putAll(params);
        }
        excuteRequestWithNoAlert(context, build, url, deviceTokenServiceListener);
    }


    public static void handleError(Context context, VolleyError error) {
        if (error instanceof NoConnectionError)
            Toast.makeText(context, Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_SHORT).show();
        else if (error instanceof ServerError) {
            String d = new String(error.networkResponse.data);
            try {
                JSONObject jso = new JSONObject(d);
                String message = jso.getString("message");
                Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "No internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }
}