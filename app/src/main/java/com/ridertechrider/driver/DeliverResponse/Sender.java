package com.ridertechrider.driver.DeliverResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sender implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("fire_id")
    @Expose
    private String fireId;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("md5password")
    @Expose
    private String md5password;
    @SerializedName("text_password")
    @Expose
    private String textPassword;
    @SerializedName("u_name")
    @Expose
    private String uName;
    @SerializedName("u_fname")
    @Expose
    private String uFname;
    @SerializedName("u_lname")
    @Expose
    private String uLname;
    @SerializedName("u_email")
    @Expose
    private String uEmail;
    @SerializedName("u_password")
    @Expose
    private String uPassword;
    @SerializedName("u_phone")
    @Expose
    private String uPhone;
    @SerializedName("u_street")
    @Expose
    private String uStreet;
    @SerializedName("u_address")
    @Expose
    private String uAddress;
    @SerializedName("u_city")
    @Expose
    private String uCity;
    @SerializedName("u_state")
    @Expose
    private String uState;
    @SerializedName("u_country")
    @Expose
    private String uCountry;
    @SerializedName("u_zip")
    @Expose
    private String uZip;
    @SerializedName("u_lat")
    @Expose
    private String uLat;
    @SerializedName("u_lng")
    @Expose
    private String uLng;
    @SerializedName("u_degree")
    @Expose
    private String uDegree;
    @SerializedName("image_id")
    @Expose
    private Object imageId;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("rating_count")
    @Expose
    private String ratingCount;
    @SerializedName("u_device_type")
    @Expose
    private String uDeviceType;
    @SerializedName("u_device_token")
    @Expose
    private String uDeviceToken;
    @SerializedName("u_is_available")
    @Expose
    private String uIsAvailable;
    @SerializedName("u_last_loggedin")
    @Expose
    private String uLastLoggedin;
    @SerializedName("emergency_contact_1")
    @Expose
    private Object emergencyContact1;
    @SerializedName("emergency_contact_2")
    @Expose
    private Object emergencyContact2;
    @SerializedName("emergency_contact_3")
    @Expose
    private Object emergencyContact3;
    @SerializedName("emergency_email_1")
    @Expose
    private String emergencyEmail1;
    @SerializedName("emergency_email_2")
    @Expose
    private String emergencyEmail2;
    @SerializedName("emergency_email_3")
    @Expose
    private String emergencyEmail3;
    @SerializedName("u_wallet")
    @Expose
    private String uWallet;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("u_fbid")
    @Expose
    private String uFbid;
    @SerializedName("u_google_id")
    @Expose
    private String uGoogleId;
    @SerializedName("is_delete")
    @Expose
    private String isDelete;
    @SerializedName("u_created")
    @Expose
    private String uCreated;
    @SerializedName("u_modified")
    @Expose
    private String uModified;
    @SerializedName("u_profile_image_path")
    @Expose
    private Object uProfileImagePath;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFireId() {
        return fireId;
    }

    public void setFireId(String fireId) {
        this.fireId = fireId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMd5password() {
        return md5password;
    }

    public void setMd5password(String md5password) {
        this.md5password = md5password;
    }

    public String getTextPassword() {
        return textPassword;
    }

    public void setTextPassword(String textPassword) {
        this.textPassword = textPassword;
    }

    public String getUName() {
        return uName;
    }

    public void setUName(String uName) {
        this.uName = uName;
    }

    public String getUFname() {
        return uFname;
    }

    public void setUFname(String uFname) {
        this.uFname = uFname;
    }

    public String getULname() {
        return uLname;
    }

    public void setULname(String uLname) {
        this.uLname = uLname;
    }

    public String getUEmail() {
        return uEmail;
    }

    public void setUEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getUPassword() {
        return uPassword;
    }

    public void setUPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getUPhone() {
        return uPhone;
    }

    public void setUPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getUStreet() {
        return uStreet;
    }

    public void setUStreet(String uStreet) {
        this.uStreet = uStreet;
    }

    public String getUAddress() {
        return uAddress;
    }

    public void setUAddress(String uAddress) {
        this.uAddress = uAddress;
    }

    public String getUCity() {
        return uCity;
    }

    public void setUCity(String uCity) {
        this.uCity = uCity;
    }

    public String getUState() {
        return uState;
    }

    public void setUState(String uState) {
        this.uState = uState;
    }

    public String getUCountry() {
        return uCountry;
    }

    public void setUCountry(String uCountry) {
        this.uCountry = uCountry;
    }

    public String getUZip() {
        return uZip;
    }

    public void setUZip(String uZip) {
        this.uZip = uZip;
    }

    public String getULat() {
        return uLat;
    }

    public void setULat(String uLat) {
        this.uLat = uLat;
    }

    public String getULng() {
        return uLng;
    }

    public void setULng(String uLng) {
        this.uLng = uLng;
    }

    public String getUDegree() {
        return uDegree;
    }

    public void setUDegree(String uDegree) {
        this.uDegree = uDegree;
    }

    public Object getImageId() {
        return imageId;
    }

    public void setImageId(Object imageId) {
        this.imageId = imageId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(String ratingCount) {
        this.ratingCount = ratingCount;
    }

    public String getUDeviceType() {
        return uDeviceType;
    }

    public void setUDeviceType(String uDeviceType) {
        this.uDeviceType = uDeviceType;
    }

    public String getUDeviceToken() {
        return uDeviceToken;
    }

    public void setUDeviceToken(String uDeviceToken) {
        this.uDeviceToken = uDeviceToken;
    }

    public String getUIsAvailable() {
        return uIsAvailable;
    }

    public void setUIsAvailable(String uIsAvailable) {
        this.uIsAvailable = uIsAvailable;
    }

    public String getULastLoggedin() {
        return uLastLoggedin;
    }

    public void setULastLoggedin(String uLastLoggedin) {
        this.uLastLoggedin = uLastLoggedin;
    }

    public Object getEmergencyContact1() {
        return emergencyContact1;
    }

    public void setEmergencyContact1(Object emergencyContact1) {
        this.emergencyContact1 = emergencyContact1;
    }

    public Object getEmergencyContact2() {
        return emergencyContact2;
    }

    public void setEmergencyContact2(Object emergencyContact2) {
        this.emergencyContact2 = emergencyContact2;
    }

    public Object getEmergencyContact3() {
        return emergencyContact3;
    }

    public void setEmergencyContact3(Object emergencyContact3) {
        this.emergencyContact3 = emergencyContact3;
    }

    public String getEmergencyEmail1() {
        return emergencyEmail1;
    }

    public void setEmergencyEmail1(String emergencyEmail1) {
        this.emergencyEmail1 = emergencyEmail1;
    }

    public String getEmergencyEmail2() {
        return emergencyEmail2;
    }

    public void setEmergencyEmail2(String emergencyEmail2) {
        this.emergencyEmail2 = emergencyEmail2;
    }

    public String getEmergencyEmail3() {
        return emergencyEmail3;
    }

    public void setEmergencyEmail3(String emergencyEmail3) {
        this.emergencyEmail3 = emergencyEmail3;
    }

    public String getUWallet() {
        return uWallet;
    }

    public void setUWallet(String uWallet) {
        this.uWallet = uWallet;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUFbid() {
        return uFbid;
    }

    public void setUFbid(String uFbid) {
        this.uFbid = uFbid;
    }

    public String getUGoogleId() {
        return uGoogleId;
    }

    public void setUGoogleId(String uGoogleId) {
        this.uGoogleId = uGoogleId;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getUCreated() {
        return uCreated;
    }

    public void setUCreated(String uCreated) {
        this.uCreated = uCreated;
    }

    public String getUModified() {
        return uModified;
    }

    public void setUModified(String uModified) {
        this.uModified = uModified;
    }

    public Object getUProfileImagePath() {
        return uProfileImagePath;
    }

    public void setUProfileImagePath(Object uProfileImagePath) {
        this.uProfileImagePath = uProfileImagePath;
    }

}
