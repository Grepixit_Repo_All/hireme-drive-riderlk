package com.ridertechrider.driver.adaptor;

import com.ridertechrider.driver.Model.TripMaster;
import com.ridertechrider.driver.webservice.Constants;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by grepix on 2/1/2017.
 */
public class Trip implements Serializable {


    public String trip_base_fare;
    public String trip_currency;
    String wait_duration;
    private String trip_id;
    private String trip_date;
    private String trip_driver_id;
    private String trip_user_id;
    private String trip_from_loc;
    private String trip_to_loc;
    private String trip_distance;
    private String trip_fare;
    private String trip_wait_time;
    private String actual_to_loc;
    private String pickup_notes;
    private String actual_from_loc;
    private String is_prepaid;
    private String city_id;
    private String trip_pickup_time, trip_drop_time, trip_reason, trip_validity, trip_feedback, trip_status, trip_rating, trip_scheduled_pick_lat;
    private String trip_scheduled_pick_lng, trip_actual_pick_lat, trip_actual_pick_lng, trip_scheduled_drop_lat, trip_scheduled_drop_lng;
    private String trip_actual_drop_lat, trip_actual_drop_lng, trip_searched_addr, trip_search_result_addr, trip_pay_mode, trip_pay_amount;
    private String trip_pay_date, trip_pay_status, trip_driver_commision, trip_created, trip_modified;
    private String trip_promo_amt;
    private String tax_amt;
    private String promo_id;
    private String trip_promo_code;
    private String is_delivery;
    private String is_ride_later;
    private String otp;
    private String m_trip_id;
    private String is_share;
    private String trip_total_time;
    private String seats;
    @SerializedName("MTrip")
    private TripMaster tripMaster = null;
    @SerializedName("User")
    private User user = null;
    @SerializedName("Driver")
    private Driver driver = null;
    private String trip_customer_details;

    public static Trip parseJson(String tripJson) {
        return new Gson().fromJson(tripJson, Trip.class);
    }

    public String getTrip_total_time() {
        if (trip_total_time == null || trip_total_time.equalsIgnoreCase("") || trip_total_time.equalsIgnoreCase("nul"))
            return "0";
        return trip_total_time;
    }

    public TripMaster getTripMaster() {
        return tripMaster;
    }

    public User getUser() {
        return user;
    }

    public Driver getDriver() {
        return driver;
    }

    public String getM_trip_id() {
        return m_trip_id;
    }

    public String getIs_share() {
        return is_share;
    }

    public String getSeats() {
        return seats;
    }

    public String getWait_duration() {
        return wait_duration;
    }

    public void setWait_duration(String wait_duration) {
        this.wait_duration = wait_duration;
    }

    public String getActual_to_loc() {
        return actual_to_loc;
    }

    public void setActual_to_loc(String actual_to_loc) {
        this.actual_to_loc = actual_to_loc;
    }

    public String getActual_from_loc() {
        return actual_from_loc;
    }

    public void setActual_from_loc(String actual_from_loc) {
        this.actual_from_loc = actual_from_loc;
    }

    public String getPickup_notes() {
        if (pickup_notes != null && !pickup_notes.trim().isEmpty() && !pickup_notes.trim().equals("null"))
            return pickup_notes;
        return null;
    }

    public void setPickup_notes(String pickup_notes) {
        this.pickup_notes = pickup_notes;
    }

    public String getIs_prepaid() {
        return is_prepaid;
    }

    public void setIs_prepaid(String is_prepaid) {
        this.is_prepaid = is_prepaid;
    }

    public String getIs_delivery() {
        if (is_delivery != null && !is_delivery.trim().isEmpty() && !is_delivery.trim().equals("null"))
            return is_delivery;
        return "0";
    }

    public void setIs_delivery(String is_delivery) {
        this.is_delivery = is_delivery;
    }

    public String getTrip_promo_amt() {
        return trip_promo_amt;
    }

    public String getTrip_promo_id() {
        return promo_id;
    }

    public String getTrip_promo_code() {
        return trip_promo_code;
    }

    public String getTrip_tax_amt() {
        return tax_amt;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public String getTrip_date() {
        return trip_date;
    }

    public String getTrip_driver_id() {
        return trip_driver_id;
    }

    public String getTrip_user_id() {
        return trip_user_id;
    }

    public String getTrip_from_loc() {
        return trip_from_loc;
    }

    public String getTrip_to_loc() {
        return trip_to_loc;
    }

    public void setTrip_to_loc(String trip_to_loc) {
        this.trip_to_loc = trip_to_loc;
    }

    public String getTrip_distance() {
        return trip_distance;
    }

    public void setTrip_distance(String trip_distance) {
        this.trip_distance = trip_distance;
    }


    public String getTrip_fare() {
        return trip_fare;
    }


    public String getTrip_wait_time() {
        return trip_wait_time;
    }


    public String getTrip_pickup_time() {
        return trip_pickup_time;
    }


    public String getTrip_drop_time() {
        return trip_drop_time;
    }


    public String getTrip_reason() {
        return trip_reason;
    }


    public String getTrip_validity() {
        return trip_validity;
    }


    public String getTrip_feedback() {
        return trip_feedback;
    }


    public String getTrip_status() {
        return trip_status;
    }

    public void setTrip_status(String trip_status) {
        this.trip_status = trip_status;
    }

    public String getTrip_rating() {
        return trip_rating;
    }

    public String getTrip_scheduled_pick_lat() {
        return trip_scheduled_pick_lat;
    }

    public String getTrip_scheduled_pick_lng() {
        return trip_scheduled_pick_lng;
    }

    public String getTrip_actual_pick_lat() {
        return trip_actual_pick_lat;
    }

    public String getTrip_actual_pick_lng() {
        return trip_actual_pick_lng;
    }

    public String getTrip_scheduled_drop_lat() {
        return trip_scheduled_drop_lat;
    }

    public String getTrip_scheduled_drop_lng() {
        return trip_scheduled_drop_lng;
    }

    public String getTrip_actual_drop_lat() {
        return trip_actual_drop_lat;
    }

    public String getTrip_actual_drop_lng() {
        return trip_actual_drop_lng;
    }

    public String getTrip_searched_addr() {
        return trip_searched_addr;
    }

    public String getTrip_search_result_addr() {
        return trip_search_result_addr;
    }

    public String getTrip_pay_mode() {
        return trip_pay_mode;
    }

    public String getTrip_pay_amount() {
        return trip_pay_amount;
    }

    public void setTrip_pay_amount(String trip_pay_amount) {
        this.trip_pay_amount = trip_pay_amount;
    }

    public String getTrip_pay_date() {
        return trip_pay_date;
    }

    public String getTrip_pay_status() {
        return trip_pay_status;
    }

    public String getTrip_driver_commision() {
        return trip_driver_commision;
    }

    public String getTrip_created() {
        return trip_created;
    }

    public String getTrip_modified() {
        return trip_modified;
    }

    public boolean isTripCancel() {
        return getTrip_status().equals(Constants.TripStatus.DRIVER_CANCEL_AT_PICKUP) || getTrip_status().equals(Constants.TripStatus.DRIVER_CANCEL_AT_DROP) || getTrip_status().equals(Constants.TripStatus.CANCEL);
    }

    public String getIs_ride_later() {
        return is_ride_later;
    }

    public void setIs_ride_later(String is_ride_later) {
        this.is_ride_later = is_ride_later;
    }

    public String getCity_id() {
        return city_id;
    }

    public String getTrip_base_fare() {
        return trip_base_fare;
    }

    public String getTrip_currency() {
        return trip_currency;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public String getPromo_id() {
        return promo_id;
    }

    public String getOtp() {
        return otp;
    }

    public String getTripTotalTime() {
        if (trip_total_time == null || trip_total_time.equals("") || trip_total_time.equals("null"))
            return null;
        return trip_total_time;
    }

    public String getTrip_customer_details() {
        return trip_customer_details;
    }

    public void setTrip_customer_details(String trip_customer_details) {
        this.trip_customer_details = trip_customer_details;
    }
}
