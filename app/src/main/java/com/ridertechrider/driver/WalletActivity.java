package com.ridertechrider.driver;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ridertechrider.driver.app.ServerApiCall;
import com.ridertechrider.driver.custom.CustomProgressDialog;
import com.ridertechrider.driver.custom.XListView.XListView;
import com.ridertechrider.driver.webservice.Constants;
import com.grepix.grepixutils.CloudResponse;
import com.grepix.grepixutils.ErrorJsonParsing;
import com.grepix.grepixutils.WebServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WalletActivity extends BaseActivity implements XListView.IXListViewListener {

    private static final String TAG = WalletActivity.class.getSimpleName();
    private final int limit = 10;
    private WalletAdapter adapter;
    private ArrayList<TransactionList> transactionLists;
    private XListView mListView;
    private int offSet = 0;
    private Controller controller;
    private CustomProgressDialog progressDialog;
    private boolean isHasMoreData;
    private TextView wallet_balance;
    private String wallet_amt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        controller = (Controller) getApplicationContext();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        progressDialog = new CustomProgressDialog(WalletActivity.this);
        ImageButton recancel = findViewById(R.id.recancel);
        wallet_balance = findViewById(R.id.wallet_balance);

        TextView add_money = findViewById(R.id.add_money);
        transactionLists = new ArrayList<>();
        mListView = findViewById(R.id.listtransaction);
        mListView.setPullLoadEnable(true);
        recancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        add_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WalletActivity.this, AddMoneyActivity.class));
                finish();
            }
        });


        getDriverProfile();
        setLocalizerString();

    }

    private void setLocalizerString() {
        TextView textView14, text, text1, add_money;
        textView14 = findViewById(R.id.textView14);
        text = findViewById(R.id.text);
        text1 = findViewById(R.id.text1);
        add_money = findViewById(R.id.add_money);

        textView14.setText(Localizer.getLocalizerString("k_1_s10_wallet"));
        text.setText(Localizer.getLocalizerString("k_2_s10_current_bal"));
        text1.setText(Localizer.getLocalizerString("k_3_s10_recents"));
        add_money.setText(Localizer.getLocalizerString("k_4_s10_add_money"));
    }

    private void getDriverProfile() {
        Map<String, String> params = new HashMap<>();
        params.put("driver_id", controller.getLoggedDriver().getDriverId());
        params.put("api_key", controller.getLoggedDriver().getApiKey());
        progressDialog.showDialog();
        WebServiceUtil.excuteRequest(WalletActivity.this, params, Constants.Urls.URL_USER_GET_DRIVER, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError error) {
                if (isUpdate) {

                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(data.toString());
                        JSONArray response = jsonObj.getJSONArray("response");
                        JSONObject c = response.getJSONObject(0);
                        wallet_amt = c.has("d_wallet") ? c.getString("d_wallet") : "0";

                        if (net_connection_check()) {
                            adapter = new WalletAdapter(WalletActivity.this, transactionLists);
                            mListView.setXListViewListener(WalletActivity.this);
                            mListView.setAdapter(adapter);
                            callwebservice(0, false);
                        } else {
                            Toast.makeText(WalletActivity.this, Localizer.getLocalizerString("k_38_s4_internet_connection_failed"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    wallet_balance.setText(controller.formatAmountWithCurrencyUnit(wallet_amt));
                    try {
                        if (Float.parseFloat(wallet_amt) > 0) {
                            wallet_balance.setTextColor(Color.rgb(34, 139, 34));
                        } else {
                            wallet_balance.setTextColor(Color.RED);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onUpdateDeviceTokenOnServer: " + e.getMessage(), e);
                    }
                } else {
                    progressDialog.dismiss();
                }
            }
        });


    }

    private boolean net_connection_check() {
        ConnectionManager cm = new ConnectionManager(this);

        boolean connection = cm.isConnectingToInternet();

        if (!connection) {

            Toast toast = Toast.makeText(getApplicationContext(), R.string.there_is_no_network_please_connect, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 70);
            toast.show();
        }
        return connection;
    }

    private void callwebservice(final int nextoffset, final boolean isLoadMore) {

        if (!isLoadMore) {
            progressDialog.showDialog();
        }
        Map<String, String> params = new HashMap<>();
        params.put("is_request", "0");
        params.put("offset", String.valueOf(nextoffset));
        params.put("limit", String.valueOf(limit));
        Log.e("params", "" + params);
        ServerApiCall.callWithApiKeyAndDriverId(WalletActivity.this, params, Constants.Urls.GET_DRIVER_TRANSACTIONS_DETAILS, new WebServiceUtil.DeviceTokenServiceListener() {
            @Override
            public void onUpdateDeviceTokenOnServer(Object data, boolean isUpdate, VolleyError
                    error) {
                progressDialog.dismiss();
                mListView.stopLoadMore();
                mListView.stopRefresh();
                if (isUpdate) {
                    String response = data.toString();
                    ErrorJsonParsing parser = new ErrorJsonParsing();
                    CloudResponse res = parser.getCloudResponse("" + response);
                    if (res.isStatus()) {
                        handleHistoryResponse(response, isLoadMore);
                    } else {
                        Toast.makeText(getApplicationContext(), res.getError(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Localizer.getLocalizerString("k_28_s2_internet_error"), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void handleHistoryResponse(String s, boolean isLoadMore) {
        try {
            JSONObject jsonRootObject = new JSONObject(s);
            JSONArray response = jsonRootObject.getJSONArray(Constants.Keys.RESPONSE);
            isHasMoreData = response.length() == limit;
            ArrayList<TransactionList> transactiLists = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                JSONObject childObject = response.getJSONObject(i);
                TransactionList transactionList = TransactionList.parseJson(childObject.toString());
                if (transactionList != null && transactionList.driverTransactions != null)
                    transactiLists.add(transactionList);
            }
            if (!isLoadMore) {
                transactionLists.clear();
            }
            transactionLists.addAll(transactiLists);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
        adapter.notifyDataSetInvalidated();
    }

    @Override
    public void onRefresh() {
        adapter.clear();
        getDriverProfile();
    }

    @Override
    public void onLoadMore() {
        if (isHasMoreData) {
            offSet += limit;
            callwebservice(offSet, true);
        } else {
            mListView.stopLoadMore();
            Toast.makeText(this, getResources().getString(R.string.no_more_data_available), Toast.LENGTH_SHORT).show();
        }
    }
}
