package com.ridertechrider.driver;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

import com.ridertechrider.driver.custom.BTextView;
import com.ridertechrider.driver.fonts.Fonts;

/**
 * Created by grepix on 8/12/2016.
 */
class DistanceAndTime {
    private final Context context;
    private final View view;
    /*public Button accept;
    public Button decline;*/
    private final String distance;
    private final String approxTime;
    private final String cuTime;

    private BTextView tvDistanceValue;
    private BTextView tvTimeValue;
    private BTextView tvCuTimeValue;

    public DistanceAndTime(Context context, View view, String distance, String approxTime, String cuTime) {
        this.context = context;
        this.view = view;

        this.distance = distance;
        this.approxTime = approxTime;
        this.cuTime = cuTime;

        init();

    }

    public void setDistanceAndTime(DistanceViewCallBack callback) {
    }

    public void hide() {
        this.view.setVisibility(View.GONE);
    }

    public void show() {
        this.view.setVisibility(View.VISIBLE);
    }

    private void init() {

        BTextView tvAta = view.findViewById(R.id.tv_eta);
        BTextView tvDistanceKey = view.findViewById(R.id.tv_distance_key);
        tvDistanceValue = view.findViewById(R.id.tv_distance_value);
        tvDistanceValue.setText(distance);
        BTextView tvTimeKey = view.findViewById(R.id.tv_time_key);
        tvTimeValue = view.findViewById(R.id.tv_time_value);
        tvTimeValue.setText(approxTime);
        BTextView tvCuTimeKey = view.findViewById(R.id.tv_am_pm);
        tvCuTimeValue = view.findViewById(R.id.tv_cu_time_value);
        tvCuTimeValue.setText(cuTime);

        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), Fonts.KARLA);
            tvDistanceValue.setTypeface(typeface);
            tvAta.setTypeface(typeface);
            tvDistanceKey.setTypeface(typeface);
            tvTimeKey.setTypeface(typeface);
            tvTimeValue.setTypeface(typeface);
            tvCuTimeKey.setTypeface(typeface);
            tvCuTimeValue.setTypeface(typeface);

        } catch (Exception e) {
            Log.e(DistanceAndTime.class.getSimpleName(), "init: " + e.getMessage(), e);
        }

    }

    public void setDistanceTimeData(String distance, String approxTime, String cuTime) {
        tvDistanceValue.setText(distance);
        tvTimeValue.setText(approxTime);
        tvCuTimeValue.setText(cuTime);

    }

    private interface DistanceViewCallBack {
//        void setDistanceValue();
//        void setTimeValue();
//        void setCurrentTime();

    }

}
