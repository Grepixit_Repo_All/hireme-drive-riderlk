package com.ridertechrider.driver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.ridertechrider.driver.Controller;
import com.ridertechrider.driver.webservice.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.ridertechrider.driver.webservice.Constants.Language.ENGLISH;

public class Utils {
    public final static String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final static String APP_DATE_FORMAT = "MMM, dd yyyy hh:mm a";
    private final static String APP_DATE_ONLY_FORMAT = "MMM, dd yyyy";
    private final static String APP_TIME_FORMAT = "hh:mm a";
    private final static String SERVER_DATE_FORMAT1 = "yyyy-MM-dd HH:mm:ss";

    private static String dateToString(Date date) {
        SimpleDateFormat outputFormat = new SimpleDateFormat(APP_DATE_FORMAT, Locale.ENGLISH);
        return outputFormat.format(date);
    }

    public static Date stringToDate(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat
                (SERVER_DATE_FORMAT, Locale.ENGLISH);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return inputFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String removedFirstZeroMobileNumber(String mobileNo_) {
        return mobileNo_.startsWith("0") ? mobileNo_.replaceFirst("0", "") : mobileNo_;
    }

    static public void applyAppLanguage(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String language = prefs.getString(Constants.Keys.APP_LANGUAGE, ENGLISH);
        // To resolve this issue, https://issuetracker.google.com/issues/128908783 (marked as fixed now)
        // if ((language.equals(LANG_GLK) || language.equals(LANG_AZB)) && Build.VERSION.SDK_INT == Build.VERSION_CODES.P) {
        //    localeCode = LANG_FA;
        // }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration config = resources.getConfiguration();
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(locale);
        }
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    public static String convertServerDateToAppLocalDate(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        Date date = stringToDate(stringDate);
        Log.e("dateformated", "" + date);
        if (date != null) {
            return dateToString(date);
        }
        return null;
    }

    public static String convertServerDateToAppLocalDateOnly(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        Date date = stringToDate(stringDate);
        Log.e("dateformated", "" + date);
        if (date != null) {
            return dateOnlyToString(date);
        }
        return null;
    }

    public static String convertServerDateToAppLocalTime(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        Date date = stringToTime(stringDate);
        Log.e("dateformated", "" + date);
        if (date != null) {
            return timeToString(date);
        }
        return null;
    }

    public static Date stringToTime(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat
                (SERVER_DATE_FORMAT, Locale.ENGLISH);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return inputFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Date stringToDate1(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat
                (SERVER_DATE_FORMAT1, Locale.ENGLISH);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return inputFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String dateOnlyToString(Date date) {
        SimpleDateFormat outputFormat = new SimpleDateFormat(APP_DATE_ONLY_FORMAT, Locale.ENGLISH);
        return outputFormat.format(date);
    }

    private static String timeToString(Date date) {
        SimpleDateFormat outputFormat = new SimpleDateFormat(APP_TIME_FORMAT, Locale.ENGLISH);
        return outputFormat.format(date);
    }

/*
    public static Date convertServerDateToAppLocalDateDate(String stringDate) {
        if (stringDate == null) {
            return null;
        }
        Date date = stringToDate1(stringDate);
        return date;
    }
*/

    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public static String getTextForStatus(String tripStatus, String tripPayStatus) {
        if (tripStatus.equalsIgnoreCase("request"))
            return "Request";
        else if (tripStatus.equalsIgnoreCase("assign") || tripStatus.equalsIgnoreCase("accept") || tripStatus.equalsIgnoreCase("arrive") || tripStatus.equalsIgnoreCase("begin"))
            return "Ongoing";
        else if (tripStatus.equalsIgnoreCase("driver_cancel") || tripStatus.equalsIgnoreCase("p_cancel_pickup") || tripStatus.equalsIgnoreCase("p_cancel_drop") || tripStatus.equalsIgnoreCase("cancel"))
            return "Cancelled";
        else if (tripPayStatus != null && tripPayStatus.equalsIgnoreCase("paid") && tripStatus.equalsIgnoreCase("completed"))
            return "Completed";
        else if (tripPayStatus != null && !tripPayStatus.equalsIgnoreCase("paid") && tripStatus.equalsIgnoreCase("completed"))
            return "Payment Awaited";
        else
            return tripStatus;
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = Controller.getInstance().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String getTextOfStatus(String tripStatus) {
        if (tripStatus == null)
            return "";
        if (tripStatus.equalsIgnoreCase("request"))
            return "Accept";
        else if (tripStatus.equalsIgnoreCase("assign")) {
            return "Arrive";
        } else if (tripStatus.equalsIgnoreCase("accept")) {
            return "Arrived";
        } else if (tripStatus.equalsIgnoreCase("arrive")) {
            return "Pick Up";
        } else if (tripStatus.equalsIgnoreCase("begin"))
            return "End";
        else if (tripStatus.equalsIgnoreCase("completed"))
            return "Complete";
        else
            return tripStatus;
    }

    public static boolean isOngoingTrip(String trip_status) {
        if (trip_status == null)
            return false;
        if (trip_status.equalsIgnoreCase("accept") ||
                trip_status.equalsIgnoreCase("arrive") ||
                trip_status.equalsIgnoreCase("begin"))
            return true;
        return false;
    }

    public int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = Controller.getInstance().getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
